class ChatroomsController < ApplicationController

  def index
    @chatroom = Chatroom.new
    @chatrooms = Chatroom.all
  end

  def create
    @chatroom = Chatroom.new(chatroom_params)
    puts params
    @property = Property.find(params[:chatroom][:property_id])
    puts "******** New Chatroom created *******"
    puts @chatroom.inspect
    puts "*************************************"
    if @chatroom.save
      redirect_to chatroom_path(@chatroom, property_id: @property.id.to_s), notice: "Vous pouvez démarrer une conversation ici !"
    else
      puts @chatroom.errors.messages
    end
  end

  def show
    @chatroom = Chatroom.find(params[:id])
    @property = Property.find(params[:property_id])
    @photos = handle_property_photos(@property)
    @demand = @chatroom.v2_demand
    @agency = @property.agency

    @unseen_customer_messages = []
    @unseen_agency_messages = []

    @customer_choice = @property.customer_choices.where(v2_demand_id: @demand.id).last

    if @customer_choice
      puts "***********************"
      puts "Customer choice present"
      puts "***********************"
    else
      puts "***********************"
      puts "Customer choice not present"
      puts "***********************"
    end

    if @chatroom.chat_messages != []
      if customer_signed_in?
        handle_unseen_customer_messages(@chatroom.chat_messages)
      end
      if agency_signed_in?
        handle_unseen_agency_messages(@chatroom.chat_messages)
      end
    end

    # agency_warden_auth = main_app.scope.request.env['warden'].user("agency")
    # customer_warden_auth = main_app.scope.request.env['warden'].user("customer")

    @messages = @chatroom.chat_messages
    @chatroom.save
    @chat_message = ChatMessage.new
  end

  private

  def handle_unseen_customer_messages(messages)
    results = []
    messages.each do |mess|
      mess.seen_by_customer = true
      mess.save
    end
  end

  def handle_unseen_agency_messages(messages)
    results = []
    messages.each do |mess|
      mess.seen_by_agency = true
      mess.save
    end
  end

  def handle_property_photos(property)
    results = []
    fields = [:img_1_url, :img_2_url, :img_3_url, :img_4_url, :img_5_url, :img_6_url, :img_7_url, :img_8_url]
    fields.each do |field|
      remote_photo = property.send("#{field.to_s}")
      if remote_photo
        results << remote_photo
      end
    end
    return results
  end

  def chatroom_params
    params.require(:chatroom).permit(:topic, :property_id, :customer_id, :demand_id, :agency_id, :v2_demand_id)
  end

end
