class Api::Customer::CustomersController < Api::Customer::BaseController

  def show
    if current_customer
      puts '---- Inside show action (Customer) ----'
    end
    render json: {resource: "customer", object: current_customer}
  end

  def status
    if current_customer
      puts '---- Inside status action (Customer) ----'
    end
    render json: {status: "connected", last_authenticate_at: "#{current_customer.last_api_authenticate_at}", email: current_customer.email}.as_json, status: :ok
  end

end
