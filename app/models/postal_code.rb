class PostalCode
  include Mongoid::Document
  include Mongoid::Timestamps

  field :values, type: Array
  field :hashed_values, type: Hash

end
