class PdfPropertiesController < ApplicationController

  def create
    @params = safe_params
    @pdf_property = PdfProperty.new(safe_params)
    if @pdf_property.save
      redirect_to agency_zone_path, notice: "Pdf transmis avec succès"
    else
      render :pdf_upload
    end
  end

  def pdf_upload
    @pdf_property = PdfProperty.new()
    render 'pdf_properties/pdf-upload'
  end

  private

  def safe_params
    params.require(:pdf_property).permit(:status, :agency_id, :name, :photo_cache, :photo)
  end
end
