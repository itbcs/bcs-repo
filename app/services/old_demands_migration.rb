require "csv"

class OldDemandsMigration

  def initialize(demand)
    @demand = demand
  end

  def call
    test_function
    service_success
  end

  private

  def test_function
    @customer = @demand.customer
    @properties = @demand.properties.any? ? @demand.properties : nil
    @chatrooms = @demand.chatrooms.any? ? @demand.chatrooms : nil
    @choices = @demand.customer_choices.any? ? @demand.customer_choices : nil
    @agency_views = @demand.demand_agency_views.any? ? @demand.demand_agency_views : nil
    @demand_comments = @demand.demand_comments.any? ? @demand.demand_comments : nil


    if @demand.project == "D'investissement locatif, la pierre c'est du solide"
      @alt_destination = handle_destination(@demand.project)
    else
      @real_project = handle_project(@demand.project)
    end


    @values = {created_at: @demand.created_at,
               updated_at: @demand.updated_at,
               customer: @demand.customer,
               search_formatted_area: @demand.localisation,
               search_zipcodes_area: @demand.postal_codes,
               property_type: handle_property_type(@demand.property_type),
               email: @demand.customer.email,
               firstname: @demand.customer.first_name,
               budget: handle_budget_array(@demand.budget),
               surface: handle_surface_array(@demand.surface),
               criterias: @demand.important_fields,
               project: @real_project ? @real_project : handle_project(@demand.project),
               destination: @alt_destination ? @alt_destination : handle_destination(@demand.destination),
               actual_situation: handle_actual_situation(@demand.actual_situation),
               bathrooms: handle_bathrooms(@demand.bathrooms),
               principal_space: handle_principal_space(@demand.principal_space),
               last_precision: @demand.last_precision,
               what_is_liked: handle_what_is_liked(@demand.actual_pros),
               style: handle_style(@demand.style),
               gender: handle_genre(@demand.customer.first_name),
               banker_contact: handle_banker_contact(@demand.banker_contact),
               broker_contact: handle_broker_contact(@demand.broker_contact),
               actual_housing: handle_actual_housing(@demand.actual_housing),
               exposition: handle_exposition(@demand.exposition),
               equipments: handle_equipments(@demand.equipements),
               state: handle_state(@demand),
               dpe: handle_dpe(@demand.dpe),
               kitchen: handle_kitchen(@demand.kitchen),
               age: handle_age(@demand.age),
               more_rooms: handle_more_rooms(@demand.more_rooms),
               rooms: handle_rooms(@demand.rooms),
               heating: handle_heating(@demand.heating),
               heating_type: handle_heating_type(@demand.heating_type),
               completed: handle_completed(@demand.completed),
               activated: handle_activated(@demand.activated),
               deactivated: handle_deactivated(@demand.deactivated),
               refused: handle_refused(@demand.refused),
               flat_stairs: handle_accessibility(@demand.accessibility),
               house_stairs: handle_house_stairs(@demand.house_stairs),
               house_works: handle_house_works(@demand.travaux),
               security: handle_security(@demand.security),
               parking: handle_parking(@demand.parking),
               status: handle_status(@demand.status),
               sending_customer_recap_email: @demand.sending_customer_recap_email,
               deactivation_comment: handle_deactivation_comment(@demand.deactivation_comment),
               deactivation_reason: handle_deactivation_reason(@demand.deactivation_reason),
               from_oldplatform: @demand.from_oldplatform
              }
    puts @values
    @new_demand = V2Demand.new(@values)

    if @properties
      @filtered_agencies = []
      puts "**** PROPERTIES ****"
      puts @properties.inspect
      puts "********************"
      @new_demand.properties = @properties
      @properties.each do |prop|
        puts "**** prop ****"
        if prop.agency
          @filtered_agencies << prop.agency
        end
        @proposal_date = DemandProperty.where(demand_id: @demand.id.to_s, property_id: prop.id.to_s).last

        if @proposal_date
          @created_at = @proposal_date.created_at
          @demand_property = V2DemandProperty.new(created_at: @created_at, v2_demand: @new_demand, property: prop)
          if @demand_property.save
            puts "V2 Demand property successfully created !!!!"
          end
        end
      end
    end

    # Linked here all associations

    @new_demand.remote_browser_screenshot_url = @demand.browser_screenshot_url

    if @new_demand.save
      puts "********* V2 Demand save ********"
      puts @new_demand.inspect
      puts "*********************************"
    end

    if @agency_views
      puts "**** VIEWS ****"
      puts @agency_views.inspect
      puts "********************"
      @agency_views.each do |av|
        av.v2_demand = @new_demand
        av.save
      end
      @new_demand.demand_agency_views = @agency_views
    end


    if @demand_comments
      puts "**** DEMAND COMMENTS ****"
      puts @demand_comments.inspect
      @demand_comments.each do |comment|
        comment_values = {body: comment.body, author: comment.author, important: comment.important, v2_demand: @new_demand}
        @new_comment = V2DemandComment.create(comment_values)
        puts "================="
        puts @new_comment
        puts "================="
      end
      @new_demand.save
    end


    if @chatrooms
      puts "**** CHATROOMS ****"
      puts @chatrooms.inspect
      puts "********************"
      @chatrooms.each do |cr|
        cr.v2_demand = @new_demand
        cr.save
      end
      @new_demand.chatrooms = @chatrooms
    end


    if @properties
      @filtered_agencies.each do |ag|
       ag.v2_demands.push(@new_demand)
      end
    end

    if @choices
      puts "**** CHOICES ****"
      puts @choices.inspect
      puts "********************"

      @choices.each do |choice|
        choice.v2_demand = @new_demand
        choice.save
      end

      @new_demand.customer_choices = @choices
    end

    @customer.v2_demands.push(@new_demand)
  end

  def handle_budget_array(budget)
    if budget
      budget.reject!{|num| num.to_s.empty?}
      if budget != []
        sanitize_array = budget.reject{|num| num.to_s.empty?}
        return sanitize_array.map!{|number| number.strip.gsub(/[[:space:]]/,'')}
      else
        return nil
      end
    else
      return nil
    end
  end

  def handle_surface_array(surface)
    if surface
      surface.reject!{|num| num.to_s.empty?}
      if surface != []
        sanitize_array = surface.reject{|num| num.to_s.empty?}
        return sanitize_array.map!{|number| number.strip.gsub(/[[:space:]]/,'')}
      else
        return nil
      end
    else
      return nil
    end
  end


  def handle_deactivation_comment(comment)
    if comment
      @comment = comment
    else
      @comment = nil
    end
    return @comment
  end

  def handle_deactivation_reason(reason)
    if reason
      @reason = reason
    else
      @reason = nil
    end
    return @reason
  end

  # Mapping

  def handle_what_is_liked(what_is_liked)
    what_is_liked = what_is_liked ? what_is_liked.first : nil
    if what_is_liked
      if what_is_liked != ""
        @what_is_liked = what_is_liked
      else
        @what_is_liked = nil
      end
    else
      @what_is_liked = nil
    end
    return @what_is_liked
  end

  def handle_status(status)
    status ? status : nil
  end

  def handle_parking(parking)
    if parking
      if parking != []
        @parking = []
        parking.each do |park|
          if park == "D'un local vélo"
            @parking << "102"
          elsif park == "D'une place de parking"
            @parking << "103"
          elsif park == "D'un parking fermé"
            @parking << "104"
          elsif park == "D'un garage double impérativement"
            @parking << "104"
          end
        end
      else
        @parking = nil
      end
    else
      @parking = nil
    end
    return @parking
  end

  def handle_security(security)
    if security
      if security != []
        @security = []
        security.each do |secu|
          if secu == "Un gardien"
            @security << "107"
          elsif secu == "Un interphone/visiophone"
            @security << "108"
          elsif secu == "Un digicode"
            @security << "109"
          end
        end
      else
        @security = nil
      end
    else
      @security = nil
    end
    return @security
  end

  def handle_house_works(travaux)
    travaux = travaux ? travaux.first : nil
    if travaux == "Houlala, aucun travaux !!"
      @house_works = "38"
    elsif travaux == "Oui j'aime mettre à mon goût la décoration"
      @house_works = "39"
    elsif travaux == "Oui, je suis enthousiaste à l'idée de rénover, agrandir ..."
      @house_works = "40"
    elsif travaux == nil
      @house_works = nil
    end
    return @house_works
  end

  def handle_accessibility(accessibility)
    accessibility = accessibility ? accessibility.first : nil
    if accessibility == "Au rez-de-chaussée"
      @accessibility = "77"
    elsif accessibility == "A partir du 1er étage"
      @accessibility = "78"
    elsif accessibility == "Au dernier étage uniquement"
      @accessibility = "79"
    elsif accessibility == nil
      @accessibility = nil
    end
    return @accessibility
  end

  def handle_house_stairs(house_stairs)
    house_stairs = house_stairs ? house_stairs.first : nil
    if house_stairs == "Non, de plain pied"
      @house_stairs = "75"
    elsif house_stairs == "Oui, mais un seul étage"
      @house_stairs = "74"
    elsif house_stairs == "Deux étages ne me font pas peur"
      @house_stairs = "73"
    elsif house_stairs == nil
      @house_stairs = nil
    end
    return @house_stairs
  end

  def handle_completed(completed)
    completed ? true : false
  end

  def handle_activated(activated)
    activated ? true : false
  end

  def handle_deactivated(deactivated)
    deactivated ? true : false
  end

  def handle_refused(refused)
    refused ? true : false
  end

  def handle_heating_type(heating_type)
    if heating_type
      if heating_type != []
        @heating_type = []
        heating_type.each do |heating_item|
          if heating_item == "Au fioul"
            @heating_type << "112"
          elsif heating_item == "Au gaz ou électrique"
            @heating_type << "114"
          elsif heating_item == "Pompe à chaleur, poêle à granulés, poêle à bois, cheminée"
            @heating_type << "113"
          end
        end
      else
        @heating_type = nil
      end
    else
      @heating_type = nil
    end
    return @heating_type
  end

  def handle_heating(heating)
    if heating == "Individuel"
      @heating = "110"
    elsif heating == "Collectif"
      @heating = "111"
    elsif heating == nil
      @heating = nil
    end
    return @heating
  end

  def handle_rooms(rooms)
    return rooms.to_i
  end

  def handle_more_rooms(more_rooms)
    more_rooms.delete("Un bureau") if more_rooms
    if more_rooms
      if more_rooms != []
        @rooms = []
        more_rooms.each do |room|
          if room == "Une buanderie"
            @rooms << "89"
          elsif room == "Une cave"
            @rooms << "90"
          elsif room == "Un cellier"
            @rooms << "91"
          elsif room == "Une véranda"
            @rooms << "92"
          end
        end
      else
        @rooms = nil
      end
    else
      @rooms = nil
    end
    return @rooms
  end

  def handle_age(age)
    if age == "Moins de 35 ans"
      @age = "44"
    elsif age == "Entre 35 et 50 ans"
      @age = "45"
    elsif age == "Entre 50 et 65 ans"
      @age = "46"
    elsif age == "Plus de 65 ans"
      @age = "47"
    end
    return @age
  end

  def handle_kitchen(kitchen)
    if kitchen == "Cuisine ouverte"
      @kitchen = "84"
    elsif kitchen == "Cuisine fermée"
      @kitchen = "85"
    elsif kitchen == "L'une ou l'autre"
      @kitchen = "86"
    end
    return @kitchen
  end

  def handle_dpe(dpes)
    if dpes
      if dpes != []
        @dpe = []
        dpes.each do |dpe|
          if dpe == "A ou B"
            @dpe << "115"
          elsif dpe == "C"
            @dpe << "116"
          elsif dpe == "D"
            @dpe << "116"
          elsif dpe == "Même E et au-delà"
            @dpe << "117"
          end
        end
      else
        @dpe = nil
      end
    else
      @dpe = nil
    end
    return @dpe
  end

  def handle_state(demand)
    if demand.house_state
      if demand.house_state == "Ancienne"
        @state = "32"
      elsif demand.house_state == "Récente*"
        @state = "33"
      elsif demand.house_state == "Neuve**"
        @state = "34"
      end
    elsif demand.flat_state
      if demand.flat_state == "Ancien"
        @state = "32"
      elsif demand.flat_state == "Récent*"
        @state = "33"
      elsif demand.flat_state == "Neuf**"
        @state = "34"
      end
    end
    return @state
  end

  def handle_property_type(property_type)
    if property_type == "Une maison"
      @property_type = "21"
    elsif property_type == "Un appartement"
      @property_type = "22"
    elsif property_type == "Un terrain"
      @property_type = "23"
    end
    return @property_type
  end

  def handle_destination(destination)
    if destination == "Votre résidence principale"
      @destination = "4"
    elsif destination == "Votre résidence secondaire"
      @destination = "5"
    elsif destination == "Un investissement locatif"
      @destination = "6"
    elsif destination == "D'investissement locatif, la pierre c'est du solide"
      @destination = "6"
    end
    return @destination
  end

  def handle_equipments(equipments)
    if equipments
      @equipments = []
      equipments.each do |equi|
        if equi == "Un vrai dressing"
          @equipments << "95"
        elsif equi == "Des placards"
          @equipments << "96"
        elsif equi == "Une alarme"
          @equipments << "93"
        elsif equi == "La climatisation"
          @equipments << "94"
        end
      end
    else
      @equipments = nil
    end
    return @equipments
  end

  def handle_project(project)
    if project == "Solo, il devra me plaire à moi et rien qu’à moi"
      @project = "7"
    elsif project == "De couple, nous vivrons à 2 dans ce logement"
      @project = "8"
    elsif project == "Familial, un grand toit est nécessaire"
      @project = "9"
    elsif project == "D'une Famille recomposée, il faut satisfaire toutes les tribus"
      @project = "10"
    elsif project == "D'investissement locatif, la pierre c'est du solide"
      @project = nil
    end
    return @project
  end

  def handle_actual_situation(actual_situation)
    if actual_situation == "Non, locataire, et une grande première"
      @actual_situation = "11"
    elsif actual_situation == "Oui, je suis propriétaire actuellement(ou je l'ai été)"
      @actual_situation = "12"
    end
    return @actual_situation
  end

  def handle_bathrooms(bathrooms)
    if bathrooms == "Non"
      @bathrooms = "87"
    elsif bathrooms == "2"
      @bathrooms = "88"
    elsif bathrooms == "3"
      @bathrooms = "88"
    elsif bathrooms == "3 et +"
      @bathrooms = "88"
    end
    return @bathrooms
  end

  def handle_principal_space(principal_space)
    if principal_space
      sanitize_array = principal_space.reject{|num| num.to_s.empty?}
      return sanitize_array.map!{|number| number.strip.gsub(/[[:space:]]/,'')}.first
    else
      return nil
    end
  end

  def handle_style(style)
    if style
      if style != []
        first_style = style.first.downcase.parameterize
        if first_style == "classique"
          @style = "35"
        elsif first_style == "moderne"
          @style = "36"
        elsif first_style == "atypique"
          @style = "37"
        end
      else
        @style = nil
      end
    else
      @style = nil
    end
    return @style
  end

  def handle_banker_contact(banker_contact)
    if banker_contact == "Je l'ai rencontré récemment et il en sait un peu"
      @banker_contact = "59"
    elsif banker_contact == "Je pensais lui en parler à l'occasion"
      @banker_contact = "58"
    elsif banker_contact == "Ouh là ! Cela fait très longtemps que je ne l'ai pas vu"
      @banker_contact = "58"
    else
      @banker_contact = nil
    end
    return @banker_contact
  end

  def handle_broker_contact(broker_contact)
    if broker_contact == "Oui !"
      @broker_contact = "62"
    elsif broker_contact == "Bonne idée, à faire"
      @broker_contact = "62"
    elsif broker_contact == "Un court-quoi ?"
      @broker_contact = "61"
    else
      @broker_contact = nil
    end
  end

  def handle_actual_housing(actual_housing)
    if actual_housing == "Une maison"
      @actual_housing = "15"
    elsif actual_housing == "Un appartement"
      @actual_housing = "14"
    else
      @actual_housing = nil
    end
  end

  def handle_exposition(exposition)
    if exposition
      if exposition != []
        @expos = []
        exposition.each do |expo|
          if expo == "Une belle vue"
            @expos << "97"
          elsif expo == "Pas de vis-à-vis"
            @expos << "98"
          elsif expo == "Une exposition Sud"
            @expos << "99"
          elsif expo == "Une exposition sans Nord"
            @expos << "100"
          elsif expo == "Pas d'idée préconçue"
            @expos << "101"
          end
        end
      else
        @expos = nil
      end
    else
      @expos = nil
    end
    return @expos
  end

  def handle_genre(firstname)
    @names = []
    CSV.foreach("prenoms.csv", headers: false, encoding: 'iso-8859-1:utf-8') do |row|
      insee_firstname = row.first.split(";").first.parameterize
      @names << {name: insee_firstname, genre: row.first.split(";")[1]}
    end
    @array_result = @names.map!{|name| name.values}
    @array_result.each do |result|
      if result.first == firstname.parameterize
        if result.last.to_sym == :f
          @real_genre = "1"
        elsif result.last.to_sym == :m
          @real_genre = "2"
        end
      end
    end
    return @real_genre ? @real_genre : "1"
  end

  def service_success
    ServiceSuccess.new(@new_demand)
  end


end
