class DemandProperty
  include Mongoid::Document
  include Mongoid::Timestamps

  field :demand_id, type: String
  field :property_id, type: String
  field :v2_demand_id, type: String

end
