class Candidacy
  include Mongoid::Document
  include Mongoid::Timestamps

  field :firstname, type: String
  field :lastname, type: String
  field :email, type: String
  field :phone, type: String
  field :jobname, type: String
  field :message, type: String

end
