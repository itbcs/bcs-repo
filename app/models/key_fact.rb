class KeyFact
  include Mongoid::Document
  include Mongoid::Timestamps
  mount_uploader :photo, KeyfactUploader

  field :number, type: String
  field :text, type: String
  field :number_type, type: String
  field :logo, type: String
end
