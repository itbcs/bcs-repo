class MapCircle
  include Mongoid::Document
  embedded_in :demand

  field :x, type: Float
  field :y, type: Float
  field :radius, type: Float
  field :circle_id, type: Integer
  field :layer_id, type: Integer
  field :auto_id, type: Integer

end
