function formatNumericInput() {
  $(".formatted-numeric-input").keyup(function() {
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
      return;
    }
    var input = $(this).val();
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt( input, 10 ) : 0;
    $(this).val( function() {
      return ( input === 0 ) ? "" : input.toLocaleString( "fr-FR" );
    });
  });
}




formatNumericInput();

