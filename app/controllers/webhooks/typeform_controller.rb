class Webhooks::TypeformController < ActionController::API
  include ApplicationHelper
  def submit
    event = JSON.parse(request.body.read)
    @results = handle_fields(event.to_h)
    @webhook = Webhook.new(service: "#{controller_name}", description: "Marketing forms service", logo: "typeform", action: "#{action_name}", content: @results)
    if @webhook.save
      puts "Webhook saved !!"
      ActionCable.server.broadcast 'webhooks',
        content: @webhook.content.as_json,
        service: "#{@webhook.service.downcase.capitalize}",
        description: "#{@webhook.description}",
        event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
        logo: svg(@webhook.logo),
        filter_target: "#{controller_name.downcase}",
        custom_action: "#{@webhook.action.capitalize}"
    end
    puts "----------------------------"
    render :json => {status: 200}
  end

  private

  def handle_fields(content)
    values = {}

    event_type = content["event_type"]
    form_id = content["form_response"]["form_id"]
    token = content["form_response"]["token"]
    agency_name = content["form_response"]["hidden"]["agency"]
    agency_id = content["form_response"]["hidden"]["agency_id"]
    agency_mail = content["form_response"]["hidden"]["agency_mail"]
    agency_phone = content["form_response"]["hidden"]["agency_phone"]

    values[:form_id] = form_id
    values[:token] = token
    values[:agency_name] = agency_name
    values[:agency_id] = agency_id
    values[:agency_mail] = agency_mail
    values[:agency_phone] = agency_phone

    content.to_h["form_response"]["answers"].each do |item|
      field = item["field"]["ref"].to_sym
      if item["text"]
        value = item["text"]
      elsif item["choice"]
        value = item["choice"]["label"]
      elsif item["number"]
        value = item["number"]
      elsif item["date"]
        value = item["date"]
      elsif item["boolean"]
        value = item["boolean"]
      elsif item["email"]
        value = item["email"]
      elsif item["phone_number"]
        value = item["phone_number"]
      end
      values[field] = value
    end
    values.to_h
  end

end
