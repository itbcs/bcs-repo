class Api::Customer::AuthenticationController < Api::Customer::BaseController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_customer_request

  def authenticate
    set_api_authenticate_at
    command = AuthenticateCustomer.call(params[:email], params[:password])
    if command.success?
      puts command.result
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

  private

  def set_api_authenticate_at
    if params[:email] && params[:password]
      email = params[:email]
      password = params[:password]
      @customer = Customer.find_by(email: params[:email])
      if @customer && Customer.authenticate(email,password)
        @customer.last_api_authenticate_at = Time.now
        @customer.save(validate: false)
      end
    end
  end

end
