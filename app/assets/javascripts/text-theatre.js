function handleTheaterAnimation() {
  var mainContainer = document.querySelector("main.scene");
  if (mainContainer) {
    console.log("Theater JS is on fire");
    var theater = theaterJS();
    theater
      .addActor('house',{ accuracy: 1, speed: 1 })
    theater
      .addScene(1500, 'house:Mon appartement', 1500, 'house:Ma cabane', 1500, 'house:Mon château', 1500, 'house:Ma maison', 1500)
      .addScene(theater.replay)
  }
}

handleTheaterAnimation();
