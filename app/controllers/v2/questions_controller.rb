class V2::QuestionsController < ApplicationController

  def edit
    @question = Question.find(params[:id])
    render :edit
  end

  def update
    @question = Question.find(params[:id])
    if @question.update_attributes(safe_params)
      puts "Question successfully update"
      redirect_to v2_demand_view_path, notice: "Question modifiée avec succès !!"
    else
      render :update, alert: "Une erreur est survenue !!"
    end
  end

  private

  def safe_params
    params.require(:question).permit(:label, :required, :second_part_label, :label_resume, :investor_exit, :demand_attribute, :fem_label)
  end

end
