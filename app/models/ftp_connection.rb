class FtpConnection
  include Mongoid::Document

  field :start_at, type: Time
  field :end_at, type: Time
  field :count, type: String
  field :duration, type: String

  def open_connection
    start_time = Time.zone.now
    Net::FTP.open(ENV['FTP_HEKTOR_URL'],ENV['FTP_HEKTOR_LOGIN'],ENV['FTP_HEKTOR_PASSWORD']) do |ftp|
      message(ftp)
      self.start_at = start_time
      # handle_zip_files(ftp)
      # handle_xml_files(ftp)
      # ZipParser.new().get_file(ftp)
      prop_count = XmlParser.new().get_file(ftp)
      self.end_at = Time.zone.now
      self.count = prop_count.to_s
      time_in_seconds = self.end_at - self.start_at
      self.duration = test_time(time_in_seconds)
      self.save
    end
  end

  def message(ftp)
    puts "-------------------------------------"
    puts "-------------------------------------"
    puts "---------- FTP OPEN SUCCESS ---------"
    puts "#{ftp.inspect}"
  end

  def test_time(time_diff)
    time_diff = time_diff.round.abs
    hours = time_diff / 3600
    dt = DateTime.strptime(time_diff.to_s, '%s').utc
    "#{hours}h:#{dt.strftime "%Mm:%Ss"}"
  end

end
