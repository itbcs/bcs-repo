# frozen_string_literal: true

# class Agency
class Agency
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  include ActiveModel::SecurePassword

  has_many :chat_messages
  has_many :chatrooms
  has_many :properties
  has_many :pdf_properties
  has_many :demand_agency_views
  has_and_belongs_to_many :demands, inverse_of: nil
  has_and_belongs_to_many :v2_demands, inverse_of: nil


  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  field :remember_created_at, type: Time

  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Api authenticatable (from browser, ios app, android app)
  field :secure_token, type: String, default: nil


  field :min_price, type: Integer, default: 0
  field :insurer_name, type: String
  field :city, type: String
  field :address, type: String
  field :source, type: String
  field :subscription_source, type: String
  field :name, type: String
  field :firstname, type: String
  field :lastname, type: String
  field :location, type: String
  field :siret, type: String
  field :url, type: String
  field :logo, type: String
  field :location_cp, type: String
  field :location_y, type:  Float
  field :location_x, type:  Float
  field :agency_key, type:  Integer
  field :phone, type:  String
  field :professional_card, type: String
  field :postal_codes, type: Array
  field :partner, type: Boolean, default: false
  field :catchment_area, type: Array
  field :status, type: String
  field :activated, type: Boolean, default: false
  field :personal_phone, type: String
  field :postal_code, type: String
  field :presence, type: Boolean
  field :rent_typeform_contact_email, type: String
  field :typeform_id, type: String
  field :is_renter, type: Boolean, default: false
  field :active_renter, type: Boolean, default: false
  field :typeform_id, type: String
  field :flux_status, type: Boolean, default: false
  field :flux_synchro, type: Boolean, default: false
  field :flux_origin, type: String

  scope :renter, -> {where(is_renter: true)}
  scope :not_renter, -> {where(is_renter: nil)}
  scope :active_renter, -> {where(is_renter: true).where(active_renter: true)}
  scope :inactive_renter, -> {where(is_renter: true).where(active_renter: false)}
  scope :platform_agency, -> {where(activated: true)}

  def self.authenticate(email,password)
    agency = Agency.find_for_authentication(:email => email)
    agency&.valid_password?(password) ? agency : nil
  end


  def self.bcs
    Agency.where(email: "laure@bientotchezsoi.com").last
  end

  def sold_properties
    self.properties.where(status: "5")
  end

  def active_demands
    self.v2_demands.where(activated: true)
  end

  def agency_stats
    @results = {}
    ag_demands = self.v2_demand_ids
    ag_properties = self.property_ids
    ag_with_choices = CustomerChoice.where(:v2_demand_id.in => ag_demands).where(:property_id.in => ag_properties).map{|choice| choice.choice}
    ag_views = DemandAgencyView.where(:v2_demand_id.in => ag_demands).where(agency_id: self.id.to_s).size
    proposed_props = V2Demand.where(:property_ids.in => ag_properties).size
    demands_count = self.v2_demands.size
    @results[:demands] = demands_count
    if ag_with_choices != []
      @results[:choices] = ag_with_choices
    else
      @results[:choices] = nil
    end
    if (demands_count - ag_views) != 0
      @results[:unviewed_demands] = demands_count - ag_views
    else
      @results[:unviewed_demands] = nil
    end
    @results[:proposed_props] = proposed_props
    @results[:name] = self.name
    @results[:views] = ag_views
    return @results
  end

end
















