class Admin::SuggestionsController < ApplicationController

  def generate_xls_file
    @suggestions = Suggestion.all
    @filename = "customer-messages"
    respond_to do |format|
      format.xlsx {
        response.headers['Content-Disposition'] = "attachment; filename='#{@filename}.xlsx'"
      }
    end
  end

  def index
    @suggestions = Suggestion.order_by(created_at: :desc).all
    render "admin/suggestions/index", layout: "admin"
  end

  def show
    @suggestion = Suggestion.find(params[:id].to_s)
    render "admin/suggestions/show", layout: "admin"
  end

  def destroy
    @suggestion = Suggestion.find(params[:id].to_s)
    @suggestion.destroy
    redirect_to admin_suggestions_path, layout: "admin"
  end

end
