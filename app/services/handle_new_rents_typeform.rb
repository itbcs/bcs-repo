class HandleNewRentsTypeform

  def initialize form_id
    @errors = []
    @full_rent_results = []
    @form_id = form_id
  end

  def call
    typeform_responses(@form_id)
  end

  private

  def typeform_responses(typeform_id)
    result = RestClient.get("https://api.typeform.com/forms/#{typeform_id}/responses?completed=true", {Authorization: "Bearer GwcruNPuS6UEzxi3iSxZPDXsYx3J2Ux12m5MRvrRbGdb"})
    @api_result = JSON.parse(result.body)["items"]
    @new_persisted_typeforms = handle_typeform_responses(@api_result)
    if @new_persisted_typeforms
      handle_customer_mail_sending(@new_persisted_typeforms)
      handle_agency_mail_sending(@new_persisted_typeforms)
    end
    @new_persisted_typeforms ? @new_persisted_typeforms : []
  end

  def handle_customer_mail_sending(new_persisted_typeforms)
    new_persisted_typeforms.each do |typeform|
      agency = Agency.find(typeform.agency_id.to_s)
      if agency
        puts "Agency found"
      end
      CustomerMailer.customer_rent_request(typeform.email,typeform.first_name,agency.phone,agency.email,agency.address,agency.name).deliver
    end
  end

  def handle_agency_mail_sending(new_persisted_typeforms)
    puts "Enter into agency typeform rent mail sending"
    new_persisted_typeforms.each do |typeform|
      puts "New persisted typeform - #{typeform.id.to_s}"
      s3_url = typeform.pdf_url
      filename = typeform.pdf_filename
      vars = mail_vars_alt(typeform)
      agency = Agency.find(typeform.agency_id.to_s)
      if agency
        puts "Agency found"
      end
      AgencyMailer.typeform_rent_test(typeform,vars,filename,s3_url,agency.rent_typeform_contact_email).deliver
    end
  end

  def mail_vars_alt(typeform)
    values = {}
    typeform.as_json.except("_id","created_at","updated_at").each do |key,value|
      if key.to_sym == :guarantor
        puts "Guarantor field"
        if value.to_s.downcase == "non"
          puts "Guarantor field with negative value"
        else
          values["#{key}"] = value
        end
      else
        values["#{key}"] = value
      end
    end
    if Phonelib.valid?(typeform.phone)
      raw_phone = Phonelib.parse(typeform.phone)
      @formatted_phone = raw_phone.national
    else
      @formatted_phone = typeform.phone
    end
    values["formatted_phone"] = @formatted_phone
    return values
  end

  def generate_nested_vars(typeform)
    renter_keys = [:first_name, :budget, :birthdate, :email, :employee_status, :housing, :last_name, :income]
    values = {}
    values["name"] = "locataire"
    values["content"] = []
    typeform.as_json.except("_id","created_at","updated_at").each do |key,value|
      if renter_keys.include?(key.to_sym)
        values["content"].push(value)
      end
    end
  end

  def handle_typeform_responses(results)
    @new_typeforms = []
    results.each do |result|
      @values = {}
      result.to_h.each do |key1,value1|
        if key1 == "token"
          @typeform_token_id = value1
          @values[key1.to_sym] = value1
        end
        if key1 == "submitted_at"
          @sending_at = value1.to_date
          @values[key1.to_sym] = value1.to_date
        end
        if key1 == "hidden"
          value1.to_h.each do |k,v|
            @values[k.to_sym] = v.to_s
          end
        end
        if key1 == "answers"
          value1.each do |hashed_values|
            hashed_values.each do |key,value|
              if value.is_a?(Hash)
                @ref = value["ref"]
              else
                @answer_field = value
                if hashed_values.has_key?(@answer_field.to_s)
                  @real_value = hashed_values["#{@answer_field}"]
                  if @real_value.is_a?(Hash)
                    @values[@ref.to_sym] = @real_value["label"]
                  else
                    @values[@ref.to_sym] = @real_value
                  end
                end
              end
            end
          end
        end
      end
      @exsisted_typeform = Typeform.where(token: @typeform_token_id).last
      if @exsisted_typeform
        puts "This typeform exsisted !!"
      else
        @typeform = Typeform.new(@values)
        if @typeform.save
          puts "Typeform successfully saved !"
          @new_typeforms << @typeform
          @full_rent_results << @typeform
          puts @typeform.inspect
        end

        if @typeform.roomate_name
          @typeform.has_roomate = true
        else
          @typeform.has_roomate = false
        end

        @number = rand(0..100000)
        filename = "location-#{@number}"
        typeform_categories = generate_mail_category_block(@typeform)

        if Phonelib.valid?(@typeform.phone)
          raw_phone = Phonelib.parse(@typeform.phone)
          @formatted_phone = raw_phone.national
        else
          @formatted_phone = @typeform.phone
        end

        pdf = ApplicationController.new.render_to_string(pdf: "location-#{@number}", template: "pdfs/rent_agency.html.haml", encoding: "UTF-8", locals: {typeform: @typeform, typeform_categories: typeform_categories, formatted_phone: @formatted_phone})
        file = File.open("#{filename}.pdf", "w")
        file_path = File.join("#{filename}.pdf")
        file.puts(pdf.force_encoding("UTF-8"))
        @typeform.pdf = file
        @typeform.pdf_filename = filename
        @typeform.save
        file.close
        File.delete(file_path) if File.exist?(file_path)
      end
    end
    (@new_typeforms.size > 0) ? @new_typeforms : nil
  end

  def generate_mail_category_block(typeform)
    @hashed_full_result = {}
    @renter_infos_results = []
    @renter_guarant_infos_results = []
    @renter_roomate_infos_results = []
    @renter_roomate_situation_results = []
    @renter_search_infos_results = []
    @renter_situation_results = []
    @renter_more_infos_results = []
    @renter_second_guarant_results = []
    @renter_roomate_guarantor_results = []

    @renter_roomate_guarantor = [:roomate_guarant_occupation, :roomate_guarant_salary_income, :roomate_guarant_other_occupation]
    @renter_more_infos = [:tenant, :rental]
    @renter_infos = [:first_name, :last_name, :email, :phone, :birthdate]
    @renter_situation = [:occupation, :other_occupation, :entrepreneur, :employee_status, :income]
    @renter_roomate_situation = [:roomate_tenant, :roomate_rental, :roomate_housing_grant_eligibility, :roomate_tax]
    @renter_roomate_infos = [:roomate_occupation, :roomate_entrepreneur, :roomate_occupation_status, :roomate_income]
    @renter_guarant_infos = [:guarantor_occupation, :guarant_other_occupation, :guarant_employee_status, :guarantor_income, :guarantor]
    @renter_search_infos = [:tax, :housing, :rooms, :furnished, :tenant, :budget]
    @renter_second_guarant = [:second_guarant_occupation, :second_guarant_other_occupation, :second_guarant_employee_status, :second_guarant_salary_income]
    typeform.as_json.each do |field, value|

      if @renter_roomate_guarantor.include?(field.to_sym)
        if field.to_sym == :roomate_guarant_occupation
          @renter_roomate_guarantor_results << {"Activité": value}
        elsif field.to_sym == :roomate_guarant_salary_income
          @renter_roomate_guarantor_results << {"Revenus": "#{ActionController::Base.helpers.number_with_delimiter(value, locale: :fr)} € mensuels"}
        elsif field.to_sym == :roomate_guarant_other_occupation
          @renter_roomate_guarantor_results << {"Situation": value}
        else
          @renter_roomate_guarantor_results << {"#{field.to_s.capitalize}": value}
        end
      end

      if @renter_roomate_situation.include?(field.to_sym)
        if field.to_sym == :roomate_tenant
          @renter_roomate_situation_results << {"Situation actuelle": value}
        else
          @renter_roomate_situation_results << {"#{field.to_s.capitalize}": value}
        end
      end

      if @renter_second_guarant.include?(field.to_sym)
        if field.to_sym == :second_guarant_occupation
          @renter_second_guarant_results << {"Activité": value}
        elsif field.to_sym == :second_guarant_other_occupation
          @renter_second_guarant_results << {"Autre activité": value}
        elsif field.to_sym == :second_guarant_employee_status
          @renter_second_guarant_results << {"Activité": value}
        elsif field.to_sym == :second_guarant_salary_income
          @renter_second_guarant_results << {"Revenus": "#{ActionController::Base.helpers.number_with_delimiter(value, locale: :fr)} € mensuels"}
        else
          @renter_second_guarant_results << {"#{field.to_s.capitalize}": value}
        end
      end

      if @renter_more_infos.include?(field.to_sym)
        if field.to_sym == :tenant
          @renter_more_infos_results << {"Situation actuelle": value.split("(").first}
        elsif field.to_sym == :rental
          if value.to_s.downcase.to_sym == :locataire
            @renter_more_infos_results << {"A jour du paiement des loyers ?": value}
          end
        elsif field.to_sym == :guarantor
          @renter_more_infos_results << {"Garant": value}
        else
          @renter_more_infos_results << {"#{field.to_s.capitalize}": value}
        end
      end

      if @renter_situation.include?(field.to_sym)
        if field.to_sym == :occupation
          @renter_situation_results << {"Activité": value}
        elsif field.to_sym == :other_occupation
          @renter_situation_results << {"Autre activité": value}
        elsif field.to_sym == :entrepreneur
          if value == true
            @renter_situation_results << {"Entrepeneur": "Oui"}
          else
            @renter_situation_results << {"Entrepeneur": "Non"}
          end
        elsif field.to_sym == :employee_status
          @renter_situation_results << {"Situation": value}
        elsif field.to_sym == :income
          @renter_situation_results << {"Revenus": "#{value} € mensuels"}
        else
          @renter_situation_results << {"#{field.to_s.capitalize}": value}
        end
      end
      if @renter_infos.include?(field.to_sym)
        if field.to_sym == :first_name
          @renter_infos_results << {"Prénom": value}
        elsif field.to_sym == :last_name
          @renter_infos_results << {"Nom": value}
        elsif field.to_sym == :phone
          @renter_infos_results << {"Téléphone": value}
        elsif field.to_sym == :birthdate
          @renter_infos_results << {"Date de naissance": value}
        else
          @renter_infos_results << {"#{field.to_s.capitalize}": value}
        end
      end

      if @renter_guarant_infos.include?(field.to_sym)
        if field.to_sym == :guarant_employee_status
          @renter_guarant_infos_results << {"Activité": value}
        elsif field.to_sym == :guarantor
          @renter_guarant_infos_results << {"Lien": value.split(",").last}
        elsif field.to_sym == :guarantor_income
          @renter_guarant_infos_results << {"Revenus": "#{value} €"}
        elsif field.to_sym == :guarantor_occupation
          @renter_guarant_infos_results << {"Situation": value}
        else
          @renter_guarant_infos_results << {"#{field}": value}
        end
      end

      if @renter_roomate_infos.include?(field.to_sym)
        if field.to_sym == :roomate_income
          @renter_roomate_infos_results << {"Revenus": "#{value} €"}
        elsif field.to_sym == :roomate_occupation
          @renter_roomate_infos_results << {"Activité": value}
        elsif field.to_sym == :roomate_occupation_status
          @renter_roomate_infos_results << {"Situation": value}
        elsif field.to_sym == :roomate_entrepreneur
          @renter_roomate_infos_results << {"Entrepeneur": value}
        else
          @renter_roomate_infos_results << {"#{field}": value}
        end
      end
      if @renter_search_infos.include?(field.to_sym)
        if field.to_sym == :furnished
          if value.downcase == "meublé"
            @renter_search_infos_results << {"Meublé": value}
          elsif value.downcase == "non meublé"
            @renter_search_infos_results << {"Meublé": value}
          elsif value.downcase == "l'un ou l'autre"
            @renter_search_infos_results << {"Meublé": "meublé ou non"}
          end
        elsif field.to_sym == :housing
          @renter_search_infos_results << {"Type recherché": value}
        elsif field.to_sym == :rooms
          @renter_search_infos_results << {"Chambres": value}
        elsif field.to_sym == :budget
          @renter_search_infos_results << {"Budget": "#{value} €"}
        else
          @renter_search_infos_results << {"#{field}": value}
        end
      end
    end
    @hashed_full_result[:renter] = @renter_infos_results
    @hashed_full_result[:renter_guarant] = @renter_guarant_infos_results
    @hashed_full_result[:renter_roomate] = @renter_roomate_infos_results
    @hashed_full_result[:renter_roomate_situation] = @renter_roomate_situation_results
    @hashed_full_result[:renter_roomate_guarantor] = @renter_roomate_guarantor_results
    @hashed_full_result[:renter_search] = @renter_search_infos_results
    @hashed_full_result[:renter_situation] = @renter_situation_results
    @hashed_full_result[:renter_more_infos] = @renter_more_infos_results
    @hashed_full_result[:renter_second_guarant] = @renter_second_guarant_results

    puts @hashed_full_result.inspect
    return @hashed_full_result
  end

  def service_success
    ServiceSuccess.new(@full_rent_results)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
