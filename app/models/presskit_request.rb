class PresskitRequest
  include Mongoid::Document
  include Mongoid::Timestamps

  field :firstname, type: String
  field :lastname, type: String
  field :email, type: String
  field :phone, type: String
  field :object, type: String
  field :enterprise, type: String
  field :message, type: String

  validates_presence_of :firstname, :lastname, :email

end
