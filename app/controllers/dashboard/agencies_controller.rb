class Dashboard::AgenciesController < ApplicationController
  before_action :authenticate_agency!, only: [:home, :demand, :demands, :properties, :messages, :property_show]


  def messages
    @agency = current_agency
    @agency_chatrooms = Chatroom.where(agency_id: @agency.id.to_s)

    @unseen_messages = {}
    @agency_chatrooms.each do |chatroom|
      unseen_messages = chatroom.chat_messages.where(seen_by_agency: false)
      if unseen_messages.to_a != []
        @unseen_messages[chatroom] = []
        @unseen_messages[chatroom] << unseen_messages.to_a
      else
        puts "Empty results"
      end
    end

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    render :messages
  end


  def home
    @agency = current_agency

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    @properties_zipcodes = @agency.properties.map do |prop|
      if prop.cp
        prop.cp.to_s.strip
      elsif prop.zipcode
        prop.zipcode.to_s.strip
      end
    end
    render :home
  end

  def agency_activity
    @test = "Activity page"
    render :activity
  end

  def edit
    @agency = Agency.find(params[:id])
    @agency.update(agency_params)
    if @agency.save
      redirect_to v2_agency_dashboard_path, notice: "Vos infos ont été mise à jour"
    else
      render agency_infos_edit_path, alert: "Des problèmes sont survenus"
    end
  end

  def update_infos
    @agency = current_agency
    render "dashboard/agencies/agency_infos_edit"
  end

  def update
    @agency = Agency.find(params[:id])
    @agency.update_attributes(agency_params)
    @agency.min_price = agency_params[:min_price].strip.gsub(/[[:space:]]/,'').to_i
    @agency.email = agency_params[:email]


    if @agency.save(validate: false)
      puts "Agency name: #{@agency.name}"
      puts "Agency name: #{@agency.email}"
      puts "Agency saved with: #{@agency.min_price}"
      puts "Agency saved with: #{@agency.catchment_area}"
      bypass_sign_in(@agency)
    else
      puts @agency.errors.inspect
    end


    # handle postal_codes matching
    if @agency.activated
      result = AgencyActivation.new(@agency).call
      demands = result.data if result.success?
      puts demands.count
      status = handle_results(@agency, demands.uniq)
      if !status.nil?
        redirect_to v2_agency_dashboard_path, notice: "Vos infos ont été mise à jour - #{demands.size} demandes trouvées"
      else
        redirect_to v2_agency_dashboard_path, alert: "Vos infos ont été mise à jour - aucune demande trouvée"
      end
    else
      redirect_to v2_agency_dashboard_path, alert: "Vos infos ont été mise à jour - Votre agence doit être activée pour avoir accès aux biens !!"
    end
  end

  def demands
    @demands = current_agency.demands
    @agency = current_agency
    if unseen_messages_count != nil
      @count = unseen_messages_count
    end
    render :demands
  end



  def demand
    @demand = V2Demand.find(params[:demand_id])
    @agency = current_agency
    @ordered_answers = v2_demand_recap(@demand)
    @criterias = v2_criterias(@demand)


    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    @view_by_agency = DemandAgencyView.where(agency_id: @agency.id.to_s).where(v2_demand_id: @demand.id.to_s).where(view: true).last

    if !@view_by_agency
      @demand_agency_view = DemandAgencyView.new(agency: @agency, v2_demand: @demand, view: true)
      if @demand_agency_view.save
        puts "***************************"
        puts "#{@demand_agency_view}"
        puts "#{@demand.id}"
        puts "#{@agency.id}"
        puts "***************************"
      else
        puts "**** ERRORS ****"
        puts @demand_agency_view.errors.full_messages
      end
    end

    @demand_properties = @demand.properties
    @already_proposed_properties = []
    @demand_properties.each do |prop|
      if @agency.properties.include?(prop)
        @already_proposed_properties << prop
      else
        puts "No prop inside agency"
      end
    end
    @customer = @demand.customer
    @properties = @agency.properties
    @filtered_properties = @properties.reject{|prop| @already_proposed_properties.include?(prop)}
    render :demand
  end

  def property_show
    @property = Property.find(params[:property_id])
    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    @imgs_attrs.each do |field|
      puts @property.send("#{field}")
      if !@property.send("#{field}").nil?
        @present_imgs << (field.to_s.remove("img_").to_i)
      end
    end
    @present_imgs
    @linked_demands = V2Demand.where(:property_ids.in => [@property.id])
    render :property
  end

  def properties
    if params[:demands]
      puts params[:demands].inspect
      @matching_demands = params[:demands].map{|demand| V2Demand.find(demand)}
    elsif params[:empty]
      @empty_matching = Property.find(params[:empty])
    end
    @properties = current_agency.properties

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    render :properties
  end

  def property_create
    @agency = current_agency
    @property.pictures.build
    render :property_create
  end

  def property_edit
    @agency = current_agency
    @property = Property.find(params[:property_id])
    @results = []
    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    @imgs_attrs.each do |field|
      puts @property.send("#{field}")
      if @property.send("#{field}").nil?
        @results << (field.to_s.remove("img_").to_i)
      else
        @present_imgs << (field.to_s.remove("img_").to_i)
      end
    end

    if @property.pictures.empty?
      gon.push({
        empty: "yes"
      })
      @property.pictures.build
    elsif @property.pictures.size == 1
      gon.push({
        size: 1
      })
    else
      gon.push({
        empty: "no"
      })
    end
    @results
    @present_imgs
    render :property_edit
  end

  def property_update
    @property = Property.find(params[:property_id])
    @property.update(property_safe_params)
    @property.pictures.select{|pict| pict.photo_url == nil}.destroy_all
    if @property.save
      redirect_to dashboard_agency_properties_path, notice: "#{@property.title} à été mis à jour !"
    else
      render dashboard_agency_property_edit(@property), alert: "Un problème est survenu !"
    end
  end

  def property_destroy
    @property = Property.find(params[:property_id])
    @property.remove_img_1! if @property.img_1
    @property.remove_img_2! if @property.img_2
    @property.remove_img_3! if @property.img_3
    @property.remove_img_4! if @property.img_4
    @property.remove_img_5! if @property.img_5
    @property.remove_img_6! if @property.img_6
    @property.remove_img_7! if @property.img_7
    @property.remove_img_8! if @property.img_8
    if @property.destroy
      redirect_to v2_dashboard_agency_properties_path, notice: "Le bien à été supprimé !"
    else
      render dashboard_agency_properties_path, alert: "Un problème est survenu !"
    end
  end

  def pdf_property_create
    render :pdf_property_create
  end

  def pdf_property_edit
    @agency = current_agency
    @pdf_property = PdfProperty.find(params[:pdf_property_id])
  end

  def pdf_property_destroy
    render :pdf_property_destroy
  end

  private


  def unseen_messages_count
    @count = 0
    @agency = current_agency
    @agency_chatrooms = Chatroom.where(agency_id: @agency.id.to_s)

    @unseen_messages = {}
    @agency_chatrooms.each do |chatroom|
      unseen_messages = chatroom.chat_messages.where(seen_by_agency: false)
      if unseen_messages.to_a != []
        @unseen_messages[chatroom] = []
        @unseen_messages[chatroom] << unseen_messages.to_a
      end
    end

    @unseen_messages.each do |chatroom,chat_messages|
      chat_messages.each do |chatroom_message|
        chatroom_message.each do |msg|
          if msg.seen_by_agency == false
            @count += 1
          end
        end
      end
    end

    if @count == 0
      return nil
    else
      return @count
    end

  end


  def v2_demand_recap(demand)
    @responses = {}
    @sanitize_keys = demand.fields.except("_id","created_at","updated_at","version","modifier_id","customer_id","property_ids","map_photo","browser_screenshot","flat_state","house_state","travaux","flat_budget","terrain_budget","terrain_surface","house_construction_surface","actual_pros","heart_stroke","criterias","email","search_formatted_area","search_zipcodes_area","completed","activated","deactivated","refused","status","sending_customer_recap_email","from_oldplatform","activation_date","old_platform_created_at","deactivation_reason","deactivation_comment","estimation_request","man_persona","woman_persona","gender","age","firstname","min_budget_int","max_budget_int").keys
    @sanitize_keys.each do |key|
      @demand_value = demand.send("#{key}")
      @question_label_resume = Question.where(demand_attribute: key.to_sym).last.agency_resume
      @theme = Question.where(demand_attribute: key).last.theme

      if @responses.has_key?(@theme)
        if @demand_value
          @responses[@theme].push([@question_label_resume,@demand_value])
        end
      else
        @responses[@theme] = []
        if @demand_value
          @responses[@theme].push([@question_label_resume,@demand_value])
        end
      end

    end
    @responses["Le projet"].push(["Zone de recherche", @demand.search_zipcodes_area])
    @responses["Le projet"].push(["Critères importants", @demand.criterias])
    return @responses
  end


  def v2_criterias(demand)
    if demand.criterias
      @responses = []
      demand[:criterias].each do |crit|
        @question = Question.where(criteria_resume: crit).last
        if @question
          @resume = @question.criteria_resume
          @attribute = @question.demand_attribute
          @value = demand.send("#{@attribute}")
          @responses << [@resume,@value]
        end
      end
    else
      @responses = nil
    end
    puts @responses
    return @responses
  end


  def filtered_answers(demand)
    @attributes_by_theme = {}
    themes = []
    if demand.property_type == "Une maison"
      values = [6,12,13,14,15,17,23,24,27,28,31]
      @surveys = Survey.all.sort_by{|s| s.position}.reject{|s| values.include?(s.position.to_i)}
    elsif demand.property_type == "Un appartement"
      values = [5,12,13,14,15,16,17,25,32]
      @surveys = Survey.all.sort_by{|s| s.position}
    elsif demand.property_type == "Un terrain"
      values = [5,6,7,8,9,10,11,13,16,18,19,20,21,22,23,24,26,27,28,29,30,31,32]
      @surveys = Survey.all.sort_by{|s| s.position}
    end

    @surveys.each do |survey|
      (themes << survey.theme) unless themes.include?(survey.theme)
    end
    themes.each do |theme|
      label_plus_attribute = []
      if demand.property_type == "Une maison"
        values = [6,12,13,14,15,17,23,24,27,28,31]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      elsif demand.property_type == "Un appartement"
        values = [5,12,13,14,15,16,17,25,32]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      elsif demand.property_type == "Un terrain"
        values = [5,6,7,8,9,10,11,13,16,18,19,20,21,22,23,24,26,27,28,29,30,31,32]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      end
      @theme_surveys.each do |survey|


        if survey.demand_attribute == ("budget") || survey.demand_attribute == ("terrain_budget") || survey.demand_attribute == ("house_budget")
          (attribute = demand[survey.demand_attribute.to_sym].map {|attr| format_number(attr)}) if demand[survey.demand_attribute.to_sym] != nil
        elsif survey.demand_attribute == "surface" || survey.demand_attribute == "terrain_surface" || survey.demand_attribute == "principal_space" || survey.demand_attribute == "actual_housing_area"
          (attribute = demand[survey.demand_attribute.to_sym]) if demand[survey.demand_attribute.to_sym] != nil
        else
          if demand[survey.demand_attribute.to_sym].is_a?(Array)
            if demand[survey.demand_attribute.to_sym].size > 0
              if demand[survey.demand_attribute.to_sym].first != ""
                attribute = demand[survey.demand_attribute.to_sym]
                puts "******************"
                puts demand[survey.demand_attribute.to_sym]
                puts "******************"
              end
            end
          else
            attribute = demand[survey.demand_attribute.to_sym]
          end
        end
        if demand.criterias.include?(survey.resume)
          is_important_field = true
        else
          is_important_field = false
        end
        (label_plus_attribute << [survey.resume,attribute,survey.demand_attribute,is_important_field]) if attribute != nil
      end
      @attributes_by_theme[theme] = label_plus_attribute
    end


    @attributes_by_theme["Votre projet"].insert(1, ["Zone de recherche", demand.search_zipcodes_area])
    @attributes_by_theme
  end


  def format_number(number)
    if number != ""
      "#{number.gsub(',',' ')} €"
    end
  end

  def format_area(number)
    if number != ""
      "#{number} m²"
    end
  end

  def handle_results(agency, demands)
    agency.v2_demands.clear
    agency.save
    puts demands
    puts !demands.compact.empty?
    if !demands.compact.empty?
      demands.each do |demand|
        agency.v2_demands.push(demand)
      end
      agency.save
      return true
    else
      return nil
    end
  end


  def agency_params
    params.require(:agency).permit(:email, :partner, :professional_card, :phone, :siret, :city, :name, :min_price, :password, :password_confirmation, catchment_area: [])
  end

  def property_safe_params
    params.require(:property).permit(:agency_id, :localisation, :area, :price, :desc, :status, :title, :offer_type, :fees, :corps, :available_date, :property_type, :ground_space, :nb_room, :nb_sleeping_room, :exposition, :build_year, :country, :city, :dpe, :ges, :zipcode, :pdf_cache, :pdf, pictures_attributes: [:photo_cache, :photo])
  end

end

