function handleRentAgenciesActions() {
  var activateButtons = document.querySelectorAll(".activate-rent-agency");
  var deactivateButtons = document.querySelectorAll(".deactivate-rent-agency");

  activateButtons.forEach(function(item) {
    var rentAgencyId = item.dataset.agencyId;
    item.addEventListener("click", function() {
      var fullItem = this.parentNode.parentNode;
      $.ajax({ url: '/activate_rent_agency',
        type: 'POST',
        data: {agency: rentAgencyId},
        success: function(response) {
          handleActiveReplaceButton(item,rentAgencyId);
          appendActivatedClonedItem(fullItem);
          checkActiveParentCount();
          checkInactiveParentCount();
          handleRentAgenciesActions();
        }
      });
    })
  });

  deactivateButtons.forEach(function(item) {
    var rentAgencyId = item.dataset.agencyId;
    item.addEventListener("click", function() {
      var fullItem = this.parentNode.parentNode;
      $.ajax({ url: '/deactivate_rent_agency',
        type: 'POST',
        data: {agency: rentAgencyId},
        success: function(response) {
          handleDeactiveReplaceButton(item,rentAgencyId);
          appendDeactivatedClonedItem(fullItem);
          checkActiveParentCount();
          checkInactiveParentCount();
          handleRentAgenciesActions();
        }
      });
    })
  });

}


handleRentAgenciesActions();

function appendActivatedClonedItem(item) {
  console.log("Inside appendActivatedClonedItem function");
  var receiver = document.getElementById("active_rent_agency");
  var originReceiver = document.getElementById("inactive_rent_agency");
  var clonedItem = item.cloneNode(true);

  // if (Array.from(originReceiver.children).includes(item)) {
  //   console.log("Receiver contain child");
  //   originReceiver.removeChild(item);
  // }

  if (item) {
    originReceiver.removeChild(item);
  }

  var statusIndicator = clonedItem.querySelector(".status-circle--inactive");
  statusIndicator.classList.replace("status-circle--inactive","status-circle--active");
  receiver.appendChild(clonedItem);
}

function appendDeactivatedClonedItem(item) {
  console.log("Inside appendDeactivatedClonedItem function");
  var receiver = document.getElementById("inactive_rent_agency");
  var originReceiver = document.getElementById("active_rent_agency");
  var clonedItem = item.cloneNode(true);

  // if (Array.from(originReceiver.children).includes(item)) {
  //   console.log("Receiver contain child");
  //   originReceiver.removeChild(item);
  // }

  if (item) {
    originReceiver.removeChild(item);
  }

  var statusIndicator = clonedItem.querySelector(".status-circle--active");
  statusIndicator.classList.replace("status-circle--active","status-circle--inactive");
  receiver.appendChild(clonedItem);
}


function handleActiveReplaceButton(element,agency) {
  var svg = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-wifi-off"><line x1="1" y1="1" x2="23" y2="23"></line><path d="M16.72 11.06A10.94 10.94 0 0 1 19 12.55"></path><path d="M5 12.55a10.94 10.94 0 0 1 5.17-2.39"></path><path d="M10.71 5.05A16 16 0 0 1 22.58 9"></path><path d="M1.42 9a15.91 15.91 0 0 1 4.7-2.88"></path><path d="M8.53 16.11a6 6 0 0 1 6.95 0"></path><line x1="12" y1="20" x2="12" y2="20"></line></svg>';
  var newLogo = document.createElement("div");
  newLogo.classList.add("inline-svg","orange-stroke");
  newLogo.innerHTML = svg;
  var newButton = document.createElement("div");
  newButton.classList.add("inactive-item","deactivate-rent-agency");
  newButton.dataset.agencyId = agency;
  newButton.appendChild(newLogo);
  element.replaceWith(newButton);
}

function handleDeactiveReplaceButton(element,agency) {
  var svg = '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-wifi"><path d="M5 12.55a11 11 0 0 1 14.08 0"></path><path d="M1.42 9a16 16 0 0 1 21.16 0"></path><path d="M8.53 16.11a6 6 0 0 1 6.95 0"></path><line x1="12" y1="20" x2="12" y2="20"></line></svg>';
  var newLogo = document.createElement("div");
  newLogo.classList.add("inline-svg","green-stroke");
  newLogo.innerHTML = svg;
  var newButton = document.createElement("div");
  newButton.classList.add("active-item","activate-rent-agency");
  newButton.dataset.agencyId = agency;
  newButton.appendChild(newLogo);
  element.replaceWith(newButton);
}

function checkInactiveParentCount() {
  var logo = document.createElement("div");
  var svg = '<?xml version="1.0" encoding="iso-8859-1"?><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 486.463 486.463" style="enable-background:new 0 0 486.463 486.463;" xml:space="preserve"><g><g><path d="M243.225,333.382c-13.6,0-25,11.4-25,25s11.4,25,25,25c13.1,0,25-11.4,24.4-24.4 C268.225,344.682,256.925,333.382,243.225,333.382z"/><path d="M474.625,421.982c15.7-27.1,15.8-59.4,0.2-86.4l-156.6-271.2c-15.5-27.3-43.5-43.5-74.9-43.5s-59.4,16.3-74.9,43.4 l-156.8,271.5c-15.6,27.3-15.5,59.8,0.3,86.9c15.6,26.8,43.5,42.9,74.7,42.9h312.8 C430.725,465.582,458.825,449.282,474.625,421.982z M440.625,402.382c-8.7,15-24.1,23.9-41.3,23.9h-312.8 c-17,0-32.3-8.7-40.8-23.4c-8.6-14.9-8.7-32.7-0.1-47.7l156.8-271.4c8.5-14.9,23.7-23.7,40.9-23.7c17.1,0,32.4,8.9,40.9,23.8 l156.7,271.4C449.325,369.882,449.225,387.482,440.625,402.382z"/><path d="M237.025,157.882c-11.9,3.4-19.3,14.2-19.3,27.3c0.6,7.9,1.1,15.9,1.7,23.8c1.7,30.1,3.4,59.6,5.1,89.7 c0.6,10.2,8.5,17.6,18.7,17.6c10.2,0,18.2-7.9,18.7-18.2c0-6.2,0-11.9,0.6-18.2c1.1-19.3,2.3-38.6,3.4-57.9 c0.6-12.5,1.7-25,2.3-37.5c0-4.5-0.6-8.5-2.3-12.5C260.825,160.782,248.925,155.082,237.025,157.882z"/></g></g></svg>';
  logo.classList.add("inline-svg");
  logo.innerHTML = svg;
  var emptyCollectionDiv = document.createElement("div");
  emptyCollectionDiv.classList.add("empty-collection");
  emptyCollectionDiv.textContent = "Aucune agence inactive";
  emptyCollectionDiv.prepend(logo);
  var activeContainer = document.getElementById("inactive_rent_agency");
  var activeContainerChildrenCount = activeContainer.childElementCount;
  console.log(activeContainerChildrenCount);
  if (activeContainerChildrenCount === 0) {
    activeContainer.appendChild(emptyCollectionDiv);
    document.querySelector(".full-inactive-rent-agencies-wrapper").classList.add("empty");
    document.querySelector(".rent-agency-index-subheader.inactives").classList.add("hidden");
  } else {
    var emptyText = activeContainer.querySelector(".empty-collection");
    if (emptyText) {
      emptyText.remove();
      document.querySelector(".full-inactive-rent-agencies-wrapper").classList.remove("empty");
      document.querySelector(".rent-agency-index-subheader.inactives").classList.remove("hidden");
    }
  }
}

function checkActiveParentCount() {
  var logo = document.createElement("div");
  var svg = '<?xml version="1.0" encoding="iso-8859-1"?><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 486.463 486.463" style="enable-background:new 0 0 486.463 486.463;" xml:space="preserve"><g><g><path d="M243.225,333.382c-13.6,0-25,11.4-25,25s11.4,25,25,25c13.1,0,25-11.4,24.4-24.4 C268.225,344.682,256.925,333.382,243.225,333.382z"/><path d="M474.625,421.982c15.7-27.1,15.8-59.4,0.2-86.4l-156.6-271.2c-15.5-27.3-43.5-43.5-74.9-43.5s-59.4,16.3-74.9,43.4 l-156.8,271.5c-15.6,27.3-15.5,59.8,0.3,86.9c15.6,26.8,43.5,42.9,74.7,42.9h312.8 C430.725,465.582,458.825,449.282,474.625,421.982z M440.625,402.382c-8.7,15-24.1,23.9-41.3,23.9h-312.8 c-17,0-32.3-8.7-40.8-23.4c-8.6-14.9-8.7-32.7-0.1-47.7l156.8-271.4c8.5-14.9,23.7-23.7,40.9-23.7c17.1,0,32.4,8.9,40.9,23.8 l156.7,271.4C449.325,369.882,449.225,387.482,440.625,402.382z"/><path d="M237.025,157.882c-11.9,3.4-19.3,14.2-19.3,27.3c0.6,7.9,1.1,15.9,1.7,23.8c1.7,30.1,3.4,59.6,5.1,89.7 c0.6,10.2,8.5,17.6,18.7,17.6c10.2,0,18.2-7.9,18.7-18.2c0-6.2,0-11.9,0.6-18.2c1.1-19.3,2.3-38.6,3.4-57.9 c0.6-12.5,1.7-25,2.3-37.5c0-4.5-0.6-8.5-2.3-12.5C260.825,160.782,248.925,155.082,237.025,157.882z"/></g></g></svg>';
  logo.classList.add("inline-svg");
  logo.innerHTML = svg;
  var emptyCollectionDiv = document.createElement("div");
  emptyCollectionDiv.classList.add("empty-collection");
  emptyCollectionDiv.textContent = "Aucune agence active";
  emptyCollectionDiv.prepend(logo);
  var activeContainer = document.getElementById("active_rent_agency");
  var activeContainerChildrenCount = activeContainer.childElementCount;
  console.log(activeContainerChildrenCount);
  if (activeContainerChildrenCount === 0) {
    activeContainer.appendChild(emptyCollectionDiv);
    document.querySelector(".full-active-rent-agencies-wrapper").classList.add("empty");
    document.querySelector(".rent-agency-index-subheader.actives").classList.add("hidden");
  } else {
    var emptyText = activeContainer.querySelector(".empty-collection");
    if (emptyText) {
      emptyText.remove();
      document.querySelector(".full-active-rent-agencies-wrapper").classList.remove("empty");
      document.querySelector(".rent-agency-index-subheader.actives").classList.remove("hidden");
    }
  }
}

