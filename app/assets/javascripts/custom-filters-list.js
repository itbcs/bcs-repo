$("ul.list-inline.custom-list li").click(function(){
  var sentence = $(this).find(".order-sentence");
  var currentItem = $(this);
  var currentIcon = $(this).find(".fa");
  $("ul.list-inline.custom-list li .fa.fa-chevron-down").removeClass("rotate");
  $("ul.list-inline.custom-list li").removeClass("green");

  $("ul.list-inline.custom-list li .order-sentence").each(function() {
    if ($(this).hasClass("hidden")) {
      console.log("Hidden class present");
    } else {
      $(this).addClass("hidden");
    }
  });

  currentItem.toggleClass("green");
  currentIcon.toggleClass("rotate");
  sentence.removeClass("hidden");
});
