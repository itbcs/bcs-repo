class V4::PagesController < ApplicationController
  before_action :first_time_visit, unless: -> { cookies[:first_visit] }

  def matching_algo_test
    @demand = V2Demand.find("5d5d35a244c98200071b7207")
    result = MatchingAlgoTest.new(@demand).call
    if result.success?
      result.data.each do |property|
        puts property.inspect
      end
    end
  end

  def home_v4
    if session[:bcs_customer_id]
      session.delete(:bcs_customer_id)
    end
    @bcs_properties = Agency.where(email: "laure@bientotchezsoi.com").last.properties.reject{|prop| prop.id.to_s == "5ccc0fb3733e2f00075d8603"}
    @testimonials = Testimonial.all.sort_by{|testimonial| testimonial.rating}.reverse
    @posts = Post.includes(:blog_category).sort_by{|post| post.created_at}.last(3)
    @fourth_posts = Post.includes(:blog_category).sort_by{|post| post.created_at}.last(4)
    @fourth_posts = BlogCategory.all.to_a.select{|cat| cat.name.parameterize != "ventes-immobilieres-bientot-chez-soi"}.last(4)
    @page = Page.find_by(name: "home")

    # @random_home = [0,1,0,1,0,1,0,1,0,1].sample
    # if @random_home == 1
    #   random_photo
    #   render "v4/pages/home_v4_alt"
    # elsif @random_home == 0
    #   render "v4/pages/home_v4"
    # end

    # if cookies[:first_visit]
    #   cookies.delete :first_visit
    #   puts "There is a cookie key as first_visit"
    # end
    random_photo
    render "v4/pages/home_v4_alt"
  end

  private

  def first_time_visit
    cookies[:first_visit] = true
    @first_visit = true
  end

  def random_photo
    @result = {}
    photo = ["https://cdn.bientotchezsoi-dev.com/svgs/icons/alexandra-gorn-485551-unsplash.jpg", "https://cdn.bientotchezsoi-dev.com/svgs/icons/nathan-fertig-249917-unsplash.jpg", "https://cdn.bientotchezsoi-dev.com/svgs/icons/sylwia-pietruszka-218324-unsplash.jpg"].sample
    if photo == "https://cdn.bientotchezsoi-dev.com/svgs/icons/alexandra-gorn-485551-unsplash.jpg"
      @result[:photo] = 'https://cdn.bientotchezsoi-dev.com/svgs/icons/alexandra-gorn-485551-unsplash.jpg'
      @result[:color] = "#ffffff"
    elsif photo == "https://cdn.bientotchezsoi-dev.com/svgs/icons/nathan-fertig-249917-unsplash.jpg"
      @result[:photo] = 'https://cdn.bientotchezsoi-dev.com/svgs/icons/nathan-fertig-249917-unsplash.jpg'
      @result[:color] = "#ffffff"
    elsif photo == "https://cdn.bientotchezsoi-dev.com/svgs/icons/sylwia-pietruszka-218324-unsplash.jpg"
      @result[:photo] = 'https://cdn.bientotchezsoi-dev.com/svgs/icons/sylwia-pietruszka-218324-unsplash.jpg'
      @result[:color] = "#ffffff"
    end
    return @result
  end

  def testimonials
    testimonial_1 = Testimonial.create(first_name:"Anaïs", last_name:"R.", rating: 5, content: "Accueil chaleureux et personnes sympathiques. C'est toujours un petit plus de sortir avec le sourire ! :)", image: 'https://cdn.bientotchezsoi-dev.com/svgs/icons/anaisr.jpg')
    testimonial_2 = Testimonial.create(first_name:"Antal", last_name:"G.", rating: 5, content: "Agence très pro et aux petits soins... même après l’achat (conseils quand on s’installe etc.)")
    testimonial_3 = Testimonial.create(first_name:"Pierre", last_name:"P.", rating: 5, content: "Accompagnement parfait, vente rapide et efficace. Une expérience nouvelle qui simplifie la recherche immobilière :)", image: 'https://cdn.bientotchezsoi-dev.com/svgs/icons/photo.jpg')
    testimonial_4 = Testimonial.create(first_name:"Marie-Neige", last_name:"C.", rating: 5, content: "Professionalisme mais aussi de réelles qualités d'écoute et une équipe très sympathique!", image: 'https://cdn.bientotchezsoi-dev.com/svgs/icons/marieneigec.jpg')
    testimonial_5 = Testimonial.create(first_name:"Xavier", last_name:"F.", rating: 4, content: "Un grand Merci. Bientôt chez Soi est vraiment différent de ce que j'ai connu jusqu'ici... et c'est plutôt cool")
    testimonial_6 = Testimonial.create(first_name:"Eric", last_name:"G.", rating: 4, content: "J'ai eu le sentiment d'être (enfin) compris ! Une recherche facilitée et surtout beaucoup d'avis à concilier.")
  end

end
