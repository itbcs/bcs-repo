class UserProgression

  def initialize demand
    @demand = demand
    @errors = []
  end

  def call
    @degree = scoring_about_user
    service_success
  end

  private

  def scoring_about_user
    @user_progression = 0
    @counter_user = 0
    date_now = Date.today
    date_sent = Date.parse(@demand.created_at.strftime('%F'))
    date_sent_on_connexion = Date.parse(@demand.customer.last_sign_in_at.strftime('%F'))
    day_passed_after_create = (date_now - date_sent).to_i
    day_passed_since_last_connexion = (date_now - date_sent_on_connexion).to_i


    if @demand.actual_situation
      if V2Answer.find_by(auto_identifier: @demand["actual_situation"]).auto_identifier == 11
        @counter_user += V2Demand.scoring_hash["actual_situation"]["auto_identifier_11"]
      elsif V2Answer.find_by(auto_identifier: @demand["actual_situation"]).auto_identifier == 12
        @counter_user += V2Demand.scoring_hash["actual_situation"]["auto_identifier_12"]
      elsif V2Answer.find_by(auto_identifier: @demand["actual_situation"]).auto_identifier == 13
        @counter_user += V2Demand.scoring_hash["actual_situation"]["auto_identifier_13"]
      end
    end

    if @demand.age
      if V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 44
        @counter_user += V2Demand.scoring_hash["age"]["auto_identifier_44"]
      elsif V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 45
        @counter_user += V2Demand.scoring_hash["age"]["auto_identifier_45"]
      elsif V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 46
        @counter_user += V2Demand.scoring_hash["age"]["auto_identifier_46"]
      elsif V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 47
        @counter_user += V2Demand.scoring_hash["age"]["auto_identifier_47"]
      end
    end

    if @demand.search_time
      if V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 24
        @counter_user += V2Demand.scoring_hash["search_time"]["auto_identifier_24"]
      elsif V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 25
        @counter_user += V2Demand.scoring_hash["search_time"]["auto_identifier_25"]
      elsif V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 26
        @counter_user += V2Demand.scoring_hash["search_time"]["auto_identifier_26"]
      elsif V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 27
        @counter_user += V2Demand.scoring_hash["search_time"]["auto_identifier_27"]
      end
    end

    if @demand.project
      if V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 7
        @counter_user += V2Demand.scoring_hash["project"]["auto_identifier_7"]
      elsif V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 8
        @counter_user += V2Demand.scoring_hash["project"]["auto_identifier_8"]
      elsif V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 9
        @counter_user += V2Demand.scoring_hash["project"]["auto_identifier_9"]
      elsif V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 10
        @counter_user += V2Demand.scoring_hash["project"]["auto_identifier_10"]
      end
    end

    if @demand.created_at
      if day_passed_after_create <= 7
        @counter_user += V2Demand.scoring_hash["created_at"]["moins de 7 jours"]
      elsif (day_passed_after_create >= 8) && (day_passed_after_create <= 21)
        @counter_user += V2Demand.scoring_hash["created_at"]["de 8 à 21 jours"]
      elsif (day_passed_after_create >= 22) && (day_passed_after_create <= 35)
        @counter_user += V2Demand.scoring_hash["created_at"]["de 22 à 35 jours"]
      elsif day_passed_after_create >= 36
        @counter_user += V2Demand.scoring_hash["created_at"]["plus de 36 jours"]
      end
    end

    if @demand.customer.last_sign_in_at
      if day_passed_since_last_connexion <= 7
        @counter_user += V2Demand.scoring_hash["created_at"]["moins de 7 jours"]
      elsif (day_passed_since_last_connexion >= 8) && (day_passed_since_last_connexion <= 21)
        @counter_user += V2Demand.scoring_hash["created_at"]["de 8 à 21 jours"]
      elsif (day_passed_since_last_connexion >= 22) && (day_passed_since_last_connexion <= 35)
        @counter_user += V2Demand.scoring_hash["created_at"]["de 22 à 35 jours"]
      elsif day_passed_since_last_connexion >= 36
        @counter_user += V2Demand.scoring_hash["created_at"]["plus de 36 jours"]
      end
    end

    if @demand.personality
      @demand["personality"].each do |id|
        if V2Answer.find_by(auto_identifier: id).auto_identifier == 136
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_136"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 137
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_137"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 139
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_139"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 138
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_138"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 140
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_140"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 141
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_141"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 142
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_142"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 143
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_143"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 144
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_144"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 145
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_145"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 146
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_146"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 147
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_147"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 148
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_148"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 149
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_149"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 150
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_150"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 151
          @counter_user += V2Demand.scoring_hash["personality"]["auto_identifier_151"]
        end
      end
    end

    generate_degrees(@counter_user)

  end

  def generate_degrees counter_user
    if (counter_user < 13) || (counter_user == 13)
      @user_progression = -90
    elsif (counter_user > 13) && (counter_user <= 17)
      @user_progression = -70
    elsif (counter_user > 17) && (counter_user <= 21)
      @user_progression = -47
    elsif (counter_user > 21) && (counter_user <= 25)
      @user_progression = -20
    elsif (counter_user > 25) && (counter_user <= 30)
      @user_progression = 0
    elsif (counter_user > 30) && (counter_user <= 34)
      @user_progression = 25
    elsif (counter_user > 34) && (counter_user <= 38)
      @user_progression = 45
    elsif counter_user > 38
      @user_progression = 75
    end
  end

  def service_success
    ServiceSuccess.new(@degree)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
