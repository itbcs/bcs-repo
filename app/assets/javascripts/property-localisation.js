function propertyLocalisation(){
  var mapContainer = document.getElementById("property-map");
  if (mapContainer) {
    if (gon.lat && gon.lng) {
      console.log(gon.lat);
      console.log(gon.lng);
      var map = L.map('property-map').setView([gon.lat, gon.lng], 15);
      var marker = L.marker([gon.lat, gon.lng]).addTo(map);
      marker.bindPopup("<b>" + gon.title + "</b>").openPopup();
    } else {
      var map = L.map('property-map').setView([44.836151, -0.580816], 15);
      var marker = L.marker([44.836151, -0.580816]).addTo(map);
      marker.bindPopup("<b>Hello ici Kenotte!</b>").openPopup();
    }
    L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'your.mapbox.access.token'
    }).addTo(map)
  }
};
propertyLocalisation();
