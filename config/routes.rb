require 'resque/server'

Rails.application.routes.draw do
  mount Resque::Server.new, at: "/resque"
  root to: 'v4/pages#home_v4'

  mount ActionCable.server => '/cable'
  # handle websocket for messagerie
  resources :chatrooms
  resources :chat_messages
  resources :customer_requests, only: [:create]

  # Api version of bcs
  namespace :api do
    namespace :agency do
      resources :agencies, only: [:show]
      post 'authenticate', to: 'authentication#authenticate'
      get 'status', to: "agencies#status"
    end
    namespace :customer do
      resources :customers, only: [:show]
      post 'authenticate', to: 'authentication#authenticate'
      get 'status', to: "customers#status"
    end
  end

  namespace :rent do
    resources :agencies
  end

  namespace :apifluxes do
    resources :properties
    resources :agencies
    post "activate_agency_flux_recovery/:id", to: "agencies#activate_agency_flux_recovery", as: "activate_flux"
  end

  post 'activate_rent_agency', to: "rent/agencies#activate"
  post 'deactivate_rent_agency', to: "rent/agencies#deactivate"

  get 'widgets/:id/:template/', to: 'widgets#show'
  get 'widgets/iframe_version', to: 'widgets#iframe_version'

  get "estimations/new_estimation", to: "estimations#new_estimation", as: "new_estimation"

  get "sending_sms", to: "phone_messages#sending_sms", as: "sending_sms"
  get "change_property_status/:id", to: "properties#change_status", as: "change_status"


  namespace :webhooks do
    post "releases", to: "heroku#releases", as: "releases"
    post "builds", to: "heroku#builds", as: "builds"
    post "app", to: "heroku#app", as: "app"
    post "submit", to: "typeform#submit", as: "submit"
    post "sent", to: "mandrill#sent", as: "sent"
    post "open", to: "mandrill#open", as: "open"
    post "click", to: "mandrill#click", as: "click"
    post "build", to: "codeship#build", as: "build"
    post "delivery", to: "nexmo#delivery", as: "delivery"
    post "malab/submit", to: "malab#submit", as: "malab_submit"
    get "malab_view", to: "pages#malab_view", as: "malab_view"
    get "sent", to: "mandrill#sent"
    get "open", to: "mandrill#open"
    get "click", to: "mandrill#click"
    get "list", to: "pages#list", as: "list"
    get "typeform_list", to: "pages#typeform_list", as: "typeform_list"
    get "visits", to: "pages#visits", as: "visits"
    post "survey", to: "aristid#survey", as: "survey"
    get "aristid/test", to: "aristid#test", as: "aristid_test"
    get "aristid/initial_request", to: "aristid#initial_request"
    post "aristid/initial_request", to: "aristid#initial_request", as: "initial_request"
  end


  namespace :analytics do
    get "demands", to: "demands#index", as: "demands"
    get "demands/:id", to: "demands#show", as: "demand"
  end

  namespace :v4 do
    get "new-home", to: "pages#home_v4", as: "home_v4"
    get "matching_algo_test", to: "pages#matching_algo_test", as: "matching_algo_test"
  end

  namespace :typeform do
    get "advanced_input_return", to: "rents#advanced_input_return", as: "advanced_input_return"
    get "advanced_input", to: "rents#advanced_input", as: "advanced_input"
    get "responses", to: "rents#responses", as: "responses"
    get "mail_response", to: "rents#mail_response", as: "mail_response"
    get "persisted_typeforms", to: "rents#persisted_typeforms", as: "persisted_typeforms"
    get "mailer_test", to: "rents#mailer_test", as: "mailer_test"
  end

  namespace :access do
    resources :properties
    get "success", to: "properties#success", as: "success"
    get "service_test", to: "properties#service_test", as: "service_test"
    get "budget_script", to: "properties#handle_demands_budget_to_integer", as: "budget_script"
    get "new_mailer", to: "properties#all_offers_without_choice", as: "new_mailer"
  end

  namespace :v2 do
    get "test_api_connect", to: "pages#test_api_connect", as: 'test_api_connect'
    resources :v2_demands
    get "results", to: "v2_demands#results", as: "results"
    get "ajax_query", to: "v2_demands#influence_ajax_query", as: "influence_ajax_query"

    get "pdf", to: "pdfs#show", as: "pdf"
    get "demand-stats", to: "pages#demand_stats", as: "demand_stats"
    get "migrations", to: "pages#migrations", as: "migrations"
    get "new-survey", to: "pages#new_survey", as: "new_survey"
    post "next-question", to: "pages#next_question", as: "next_survey_v2"
    post "last-question", to: "pages#last_question", as: "last_survey_v2"
    post "next-question-on-skip", to: "pages#next_question_on_skip", as: "next_survey_on_skip"
    get "demand-view", to: "pages#demand_view", as: "demand_view"
    post "orderable", to: "pages#orderable", as: "survey_orderable"
    post "new-question", to: "pages#new_question", as: "new_question"
    get "test-new-survey", to: "pages#test_new_survey", as: "test_new_survey"
    post "test-next-question", to: "pages#test_next_question", as: "test_next_question"
    post "criterias-v2", to: "pages#criterias", as: "criterias_v2"
    get "summary", to: "pages#summary", as: "summary"
    get "map", to: "pages#map", as: "map"
    get "start", to: "pages#start", as: "start"
    post "multi_circle_batch_constructor", to: "pages#multi_circle_batch_constructor", as: "mcbc"
    get "dashboard", to: "customers#home", as: "old_customer_dashboard"
    resources :questions, only: [:edit, :update]
    resources :sentences, only: [:edit, :update]
    resources :v2_conditions, only: [:new, :create, :edit, :update, :destroy]
    resources :v2_answers, only: [:new, :create, :edit, :update]
    get "week_stats", to: "statistics#week_stats", as: "week_stats"

    # Arthur routing
    get "dashboard/agency/home", to: "agencies#home", as: "agency_dashboard"
    get "dashboard/agency/props_waiting_for_choice", to: "agencies#props_waiting_for_choice", as: "props_waiting_for_choice"
    get "dashboard/agency/demands", to: "agencies#demands", as: "dashboard_agency_demands"
    get "dashboard/agency/properties", to: "agencies#properties", as: "dashboard_agency_properties"
    get "bcs-property-show", to: "agencies#property_show", as: "bcs_property_show"
    get "dashboard/agency/pending_offers", to: "agencies#pending_offers", as: "dashboard_agency_pending_offers"
    get 'dashboard/agency/demand/:demand_id', to: "agencies#demand", as: "dashboard_agency_demand"
    post "dashboard/agency/demands/search_with_ajax", to: "agencies#search_with_ajax", as: "dashboard_agency_demands_search_with_ajax"

    get "ajaxified_customer_choice", to: "customer_choices#customer_choice_ajax", as: "ajaxified_choices"

    # New customer dashboard routing
    get "dashboard/customer/home", to: "customers#home", as: "customer_dashboard"
    get "dashboard/customer/mes_petits_plus", to: "customers#my_little_mores", as: "mores"
    get "dashboard/customer/advices", to: "customers#advices", as: "advices"
    get "dashboard/customer/journey", to: "customers#journey", as: "journey"
    get "dashboard/customer/selection", to: "customers#selection", as: "selection"
    get "dashboard/customer/properties", to: "customers#properties", as: "customer_dashboard_properties"
    get "dashboard/customer/properties/:property_id", to: "customers#property", as: "customer_dashboard_property"
    get "dashboard/customer/messages", to: "customers#messages", as: "customer_dashboard_messages"
    post "dashboard/customer/properties/:property_id/hate", to: "customer_choices#hate", as: "customer_choice_hate"
    post "dashboard/customer/properties/:property_id/like", to: "customer_choices#like", as: "customer_choice_like"
    post "dashboard/customer/properties/customer_unlike_choice_ajax", to: "customers#customer_unlike_choice_ajax", as: "customer_dashboard_properties_customer_unlike_choice_ajax"
    post "dashboard/customer/properties/customer_like_choice_ajax", to: "customers#customer_like_choice_ajax", as: "customer_dashboard_properties_customer_like_choice_ajax"

  end

  get "s3_migration", to: "pages#s3_to_cloudinary_migration", as: "s3_migration"
  get "s3_migration_for_hektor", to: "pages#s3_migration_for_hektor_flux", as: "s3_hektor_migration"

  get "test_unviewed_demands_mailing", to: "pages#test_unviewed_demands_mailing", as: "mailing_test"
  get "dico-de-kenotte", to: "pages#glossary", as: "glossary"
  get "bcs-properties", to: "pages#bcs_properties", as: "exclu_bcs_properties"
  get "bcs-property-show", to: "pages#bcs_property_show", as: "home_bcs_property_show"
  get "estimation", to: "pages#estimation_v3", as: "estimation_v3"
  get "devenir-partenaire", to: "pages#partner_landing", as: "partner_landing"
  get "estimation-v2", to: "pages#estimation_v2", as: "estimation_v2"
  get "employee-model-test", to: 'pages#employee_test', as: "employee"
  get "email-job-testing", to: 'pages#email_job_testing', as: "email_job_testing"
  get "partenaires", to: "pages#partners", as: "static_partners"
  get "ajax-upload", to: "properties#ajax_photo_upload", as: "ajax_upload"
  get "conversation", to: "conversations#conversation", as: "conversation"
  post "conversation", to: "conversations#new_message", as: "conversation_message"
  get "faq", to: "pages#faq", as: "faq"

  resources :demands, only: [:create, :destroy]
  resources :sponsorships, only: [:create]
  resources :customer_advices, only: [:create]
  resources :contacts, only: [:create]
  resources :customer_questions, only: [:create]
  resources :newsletters, only: [:create]
  resources :key_facts, only: [:create]
  resources :candidacies, only: [:create]
  resources :estimations, only: [:create]
  resources :presskit_requests, only: [:create]

  resources :partners

  get "keyfact-creation", to: "key_facts#new"
  get "espace-presse", to: "pages#presse", as: "presse"
  get "chasse-immobiliere", to: "pages#hunt", as: "hunt"


  namespace :admin do
    resources :estimations, only: [:show, :index, :destroy]
    resources :press_releases, only: [:new, :show, :index, :create, :destroy, :edit, :update]
    resources :presskit_requests, only: [:show, :index, :destroy]
    resources :pages
    resources :posts
    resources :demand_comments
    resources :v2_demand_comments
    resources :suggestions
    patch "demands/:id", to: "demands#update", as: "v2_demand"
    get "refused-demand", to: "demands#refused_demand", as: "refused_demand"
    get "deactivated-demand", to: "demands#deactivate_demand", as: "deactivate_demand"
    get "deactivated-demand-with-message", to: "demands#deactivated_with_message", as: "deactivated_with_message"
    patch "deactivated-edit", to: "demands#deactivate_demand_with_message", as: "deactivated_demand_with_message"
  end

  resources :press_releases, only: [:show]


  get "errors/404", to: "errors#not_found", as: "not_found"

  get "estimation/resultat/:estimation_id", to: "estimations#result", as: "estimation_result"
  patch "estimation/finish/:estimation_id", to: "estimations#update", as: "estimation_update"

  post 'dashboard/customer/property/hate', to: "customer_choices#hate", as: "customer_choice_hate"
  post 'dashboard/customer/property/like', to: "customer_choices#like", as: "customer_choice_like"
  post 'dashboard/customer/property/love', to: "customer_choices#love", as: "customer_choice_love"

  get 'dashboard/customer/property-like', to: "customer_advices#create", as: "dashboard_customer_likes"
  get 'dashboard/customer/new-advice', to: "customer_advices#new", as: "dashboard_customer_new_advice"


  get 'dashboard/customer/messages', to: "dashboard/customers#messages", as: "dashboard_customer_messages"


  get 'dashboard/customer/home', to: "dashboard/customers#home", as: 'customer_zone'
  get 'dashboard/customer/partnerships', to: "dashboard/customers#partnerships", as: 'dashboard_customer_partnerships'
  get 'dashboard/customer/profil', to: "dashboard/customers#profil", as: 'dashboard_customer_profil'
  get 'dashboard/customer/advices', to: "dashboard/customers#advices", as: 'dashboard_customer_advices'
  get 'dashboard/customer/vip', to: "dashboard/customers#vip", as: 'dashboard_customer_vip'
  get 'dashboard/customer/selections', to: "dashboard/customers#selections", as: 'dashboard_customer_selections'
  get 'dashboard/customer/properties', to: "dashboard/customers#properties", as: 'dashboard_customer_properties'
  get 'dashboard/customer/properties/:property_id', to: 'dashboard/customers#property', as: "dashboard_customer_property"
  get 'dashboard/customer/properties/partial/:property_id', to: 'dashboard/customers#property_partial', as: "dashboard_customer_property_partial"

  get 'dashboard/agency/home', to: "dashboard/agencies#home", as: "agency_zone"
  get 'dashboard/agency/demand/:demand_id', to: "dashboard/agencies#demand", as: "dashboard_agency_demand"
  get 'dashboard/agency/demands', to: "dashboard/agencies#demands", as: "dashboard_agency_demands"
  post 'dashboard/agency/properties/create', to: "dashboard/agencies#property_create", as: "dashboard_agency_property_create"
  get 'dashboard/agency/properties/:property_id', to: "dashboard/agencies#property_edit", as: "dashboard_agency_property_edit"
  patch 'dashboard/agency/properties/:property_id', to: "dashboard/agencies#property_update", as: "dashboard_agency_property_update"
  delete 'dashboard/agency/properties/:property_id', to: "dashboard/agencies#property_destroy", as: "dashboard_agency_property_destroy"
  post 'dashboard/agency/pdf_properties/create', to: "dashboard/agencies#pdf_property_create", as: "dashboard_agency_pdf_property_create"
  delete 'dashboard/agency/pdf_properties/:pdf_property_id', to: "dashboard/agencies#pdf_property_destroy", as: "dashboard_agency_pdf_property_destroy"
  get 'dashboard/agency/activity', to: "dashboard/agencies#agency_activity", as: "agency_activity"
  get 'dashboard/agency/biens', to: "dashboard/agencies#properties", as: "dashboard_agency_properties"
  get 'dashboard/agency/infos', to: "dashboard/agencies#update_infos", as: "dashboard_agency_update_infos"

  get 'dashboard/agency/property/:property_id', to: "dashboard/agencies#property_show", as: "dashboard_agency_property_show"
  get 'dashboard/agency/messages', to: "dashboard/agencies#messages", as: "dashboard_agency_messsages"

  namespace :dashboard do
    resources :agencies, only: [:update]
  end



  resources :pdf_properties, only: [:create]
  get 'mon-agence/upload-pdf', to: 'pdf_properties#pdf_upload', as: 'agency_zone_upload_pdf'
  get 'url-to-test', to: "map_screenshots#url_to_test", as: "url_to_test"
  get 'agency-infos-edit/:agency_id', to: "pages#agency_infos_edit", as: "agency_infos_edit"

  get 'proposer-un-bien', to: "properties#property_proposition", as: "property_proposition"
  post 'envoyer-un-bien', to: "properties#add_property_to_demand", as: "add_property_to_demand"

  get 'test-agency', to: 'pages#test_agency'
  get 'job-test', to: 'pages#job_test'

  resources :suggestions, only: [:create]


  post 'multiple_circles_create', to: "map_circles#multiple_circles_create", as: "multiple_circles_create"
  post 'remove_specific_circle', to: "map_circles#remove_specific_circle", as: "remove_specific_circle"
  post 'multi_circle_batch_constructor', to: "map_circles#multi_circle_batch_constructor", as: "multi_circle_batch_constructor"
  get 'fetch_multiple_circles', to: "map_circles#fetch_multiple_circles", as: "fetch_multiple_circles"
  post 'create_circle', to: "map_circles#create", as: "create_circle"
  get 'fetch_circles', to: "map_circles#fetch_circles", as: "fetch_circles"
  post 'remove_circle', to: "map_circles#remove", as: "remove_circle"
  post 'update_circle', to: "map_circles#update_circle_position_and_radius", as: "update_circle"

  get 'handle_carrierwave_remote_map_upload', to: "map_circles#handle_carrierwave_remote_map_upload", as: "handle_carrierwave_remote_map_upload"

  resources :sms_validations, only: [:new, :create, :update]
  post 'sms-return', to: 'sms_validations#sms_return_validation', as: 'sms_return'


  # // Update demand //
  post 'admin/demand/validation', to: 'admin/demands#validate_demand', as: "validate_demand"
  delete 'admin/demands/destroy', to: 'admin/demands#multiple_destroy', as: "multiple_destroy"
  post 'admin/agency/activation', to: 'admin/agencies#activate_agency', as: "activate_agency"

  # // Admin namespace //
  get 'admin/export/demands/xls', to: "admin/demands#generate_xls_file", as: "generate_demands_xls_file"
  get 'admin/export/agencies/xls', to: "admin/agencies#generate_xls_file", as: "generate_agencies_xls_file"
  get 'admin/export/customers/xls', to: "admin/customers#generate_xls_file", as: "generate_customers_xls_file"
  get 'admin/export/suggestions/xls', to: "admin/suggestions#generate_xls_file", as: "generate_suggestions_xls_file"

  get 'admin/script', to: "admin/properties#add_agency_key_into_property", as: "script"

  get 'admin', to: "admin/pages#home", as: "admin_dashboard"
  get 'admin/demands', to: "admin/demands#index", as: "admin_demands"
  get 'admin/demands/orphan', to: "admin/demands#orphan", as: "admin_orphan_demands"
  get 'admin/demands/active', to: "admin/demands#active", as: "admin_active_demands"
  get 'admin/demands/inactive', to: "admin/demands#inactive", as: "admin_inactive_demands"
  get 'admin/demands/pending', to: "admin/demands#pending", as: "admin_pending_demands"
  get 'admin/demands/refuse', to: "admin/demands#refuse", as: "admin_refuse_demands"
  get 'admin/demands/old', to: "admin/demands#old", as: "admin_real_old_demands"


  get 'admin/agencies/active', to: "admin/agencies#active", as: "admin_active_agencies"
  get 'admin/agencies/partner', to: "admin/agencies#partner", as: "admin_partner_agencies"
  get 'admin/agencies/flux', to: "admin/agencies#flux", as: "admin_flux_agencies"
  get 'admin/agencies/waiting', to: "admin/agencies#waiting", as: "admin_waiting_agencies"


  get 'admin/customers/valid', to: "admin/customers#valid", as: "admin_valid_customers"
  get 'admin/customers/unvalid', to: "admin/customers#unvalid", as: "admin_unvalid_customers"


  get 'admin/demands/:id', to: 'admin/demands#show', as: "admin_demand"
  delete 'admin/demands/:id', to: 'admin/demands#destroy', as: "admin_demand_destroy"

  get 'admin/agencies', to: "admin/agencies#index", as: "admin_agencies"
  get 'admin/agencies/:id', to: 'admin/agencies#show', as: "admin_agency"
  post 'admin/agencies/activation/:id', to: "admin/agencies#agency_activation", as: "admin_agency_activation"
  delete 'admin/agencies/:id', to: 'admin/agencies#destroy', as: "admin_agency_destroy"

  get 'admin/properties', to: "admin/properties#index", as: "admin_properties"
  get 'admin/properties/:id', to: 'admin/properties#show', as: "admin_property"

  get 'admin/customers', to: "admin/customers#index", as: "admin_customers"
  get 'admin/customers/:id', to: "admin/customers#show", as: "admin_customer"
  delete 'admin/customers/:id', to: "admin/customers#destroy", as: "admin_customer_destroy"
  get 'admin/customers/:id/edit', to: "admin/customers#edit", as: "edit_admin_customer"
  patch 'admin/customers/:id', to: "admin/customers#update", as: "update_admin_customer"



  get 'admin/sponsorships', to: "admin/sponsorships#index", as: "admin_sponsorships"
  get 'admin/sponsorships/:id', to: "admin/sponsorships#show", as: "admin_sponsorship"

  get 'admin/old-platform-demands', to: "admin/old_platform_demands#index", as: "admin_old_demands"
  get 'admin/old-platform-demand', to: "admin/old_platform_demands#show", as: "admin_old_demand"


  get "aristid_survey", to: "pdfs#aristid_survey", as: "aristid_pdf"
  get "aristid_survey_agency", to: "pdfs#aristid_survey_agency", as: "aristid_agency_pdf"

  get 'resultat-pdf', to: "pdfs#show", as: 'generate_pdf'
  get 'resultat-pdf-admin', to: "pdfs#admin_show", as: 'generate_admin_pdf'
  # // SMS SENDING //
  get 'retrieve_phone_number', to: "sendsms#retrieve_phone_number", as: 'retrieve_phone'
  # // MAP AJAX CALLS //
  post 'nearby_zip_codes', to: "maps#nearby_zip_codes", as: "nearby_zip_codes"


  # // DEVISE ROUTES //
  devise_for :customers, controllers: { sessions: 'customers/sessions', registrations: 'customers/registrations' }, path_names: { sign_in: 'connexion', sign_up: "inscription" }
  devise_for :agencies, controllers: { sessions: 'agencies/sessions', registrations: 'agencies/registrations' }, path_names: { sign_in: 'connexion', sign_up: "inscription" }
  devise_for :employees, controllers: { sessions: 'employees/sessions', registrations: 'employees/registrations' }, path_names: { sign_in: 'connexion', sign_up: "inscription" }
  # // DEVISE ROUTES //


  # // ROLES ZONES ROUTING //
  get 'quota-demande', to: "pages#restricted_demand_count", as: 'restricted_demand_count'
  get 'mon-compte', to: 'pages#customer_zone', as: 'old_customer_zone'
  get 'mon-agence', to: 'pages#agency_zone', as: 'old_agency_zone'
  get 'mon-agence/creation-de-bien', to: 'pages#agency_zone_create_property', as: 'agency_zone_create_property'
  resources :properties, only: [:create, :edit, :update]
  post 'mon-compte/offre-active/:demand_id', to: 'pages#active_demand', as: 'active_demand'
  # // ROLES ZONES ROUTING //


  # // JOBS ROUTES //
  get 'jobs', to: 'jobs#index'
  get '/jobs/:lname', to: 'jobs#show', as: 'job'
  # // JOBS ROUTES //


  # // AJAX REQUESTS //
  post 'next_question', to: 'surveys#next_question', as: 'next_survey'
  post 'next_question_edit', to: 'surveys#next_question_edit', as: 'next_survey_edit'
  get 'append_user_answer', to: 'surveys#append_user_answer', as: 'append_user_answer'
  get 'user_results_recap', to: 'surveys#user_results_recap', as: 'user_results_recap'
  post 'criterias_submit', to: 'surveys#criterias_submit', as: 'criterias_submit'
  # // AJAX REQUESTS //

  # // DEMAND EDIT //
  get 'demand-edit', to: 'surveys#demand_edit', as: 'demand_edit'
  # // DEMAND EDIT //

  get 'je-cherche/map', to: 'maps#map', as: 'map'
  post 'je-cherche/map', to: 'maps#map', as: 'initial_location'
  get 'je-cherche/map/restricted', to: "maps#restricted_demand_map", as: "restricted_demand_map"

  get 'je-cherche/map-edit/:demand_id', to: "maps#map_edit", as: 'map_edit'

  get 'je-cherche/questionnaire', to: 'surveys#intital_survey', as: 'questionnaire'
  get 'je-cherche/questionnaire/:demand_id', to: 'surveys#intital_survey', as: 'questionnaire_get'

  get 'je-cherche', to: 'pages#search'
  get 'je-vends', to: 'pages#sell', as: 'je_vends'
  get 'je-m-informe', to: 'pages#inform', as: "i_inform_myself"
  get 'je-construis', to: 'pages#construction', as: "i_construct"
  get 'contact', to: 'pages#contact', as: 'contact'
  get 'test', to: 'pages#test'
  get 'nos-honoraires', to: 'pages#fees', as: 'fees'
  get 'avant-premiere', to: 'pages#preview', as: 'preview'


  get 'qui-sommes-nous', to: 'pages#who_we_are', as: 'who_we_are'
  get 'lequipe', to: "pages#team", as: 'team'


  get 'charte', to: 'pages#convention', as: 'convention'
  get 'cgu-particuliers', to: 'pages#cgu_customer', as: "cgu_customer"
  get 'cgu-professionnelles', to: "pages#cgu_pro", as: "cgu_pro"
  get 'mentions_legales', to: "pages#mentions_legales", as: "mentions_legales"

  get 'vente-appartement-neuf-t3', to: 'pages#sell_new_flat', as: "sell_new_flat"
  get 'parrainage', to: "pages#parrainage", as: "parrainage"
  get "ambassadeur", to: "pages#ambassador", as: "ambassador"

  resources :ambassadors, only: [:create]

  get 'bien-a-vendre', to: "prospect_properties#bien_a_vendre", as: "property_to_sell"
  post 'bien-a-vendre', to: "prospect_properties#create", as: "property_to_sell_create"

  # // BLOG ACTIONS //
  get 'blog', to: 'pages#blog', as: 'blog'
  get 'blog/:lname', to: 'blog_categories#show', as: 'blog_category'
  get 'blog/:cat_lname/:lname', to: 'posts#show', as: 'post'

  # // BLOG - TAGS ACTIONS //
  get 'blog-tags/:tag_lname', to: 'posts#tags', as: 'blog_tag'
  get 'blog-author/:author_name', to: 'posts#author', as: 'author'

  get '*unmatched_route', to: 'application#not_found_redirect'

end
