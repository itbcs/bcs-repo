require 'rest-client'

class MapsController < ApplicationController

  def map
    if customer_signed_in? && current_customer.demands.any?
      @customer_adapted_blog_post = Post.all.where(name: "Avant / après la visite d’un bien : le mémo !").last
      gon.push({
        lat: 44.836151,
        lng: -0.580816,
        origin: "restricted",
        city: "Bordeaux"
      })
      render "map/restricted_demand_map", layout: 'map'
    else
      create_empty_demand
      gon.push({
        lat: params[:lat],
        lng: params[:lng],
        city: params[:locality].to_s,
        demand: @demand.id.to_s
      })
      if params[:lat] && params[:lng]
        add_lat_and_lng_to_demand(@demand.id.to_s, params[:lat], params[:lng])
      end
      if params[:locality]
        add_locality_to_demand(@demand.id.to_s,params[:locality])
      end
      render "map/map", layout: 'map'
    end

  end

  def map_edit
    @demand_id = params[:demand_id]
    @demand = Demand.find(@demand_id.to_s)
    gon.push({
      lat: @demand.localisation_lat,
      lng: @demand.localisation_lng,
      city: @demand.localisation,
      origin: "edit",
      demand: @demand.id.to_s
    })
    render "map/map-edit", layout: 'map'
  end

  def restricted_demand_map
    render "map/restricted_demand_map", layout: 'map'
  end


  def nearby_zip_codes
    demand_id = params[:demand_id].to_s
    postal_codes = []
    integer_radius = (params[:radius].to_f) / 1000
    radius = integer_radius.round(0)
    get_zipcode_list = RestClient.get("https://www.villes-voisines.fr/getcp.php?cp=#{params[:postal_code]}&rayon=#{radius}")
    result_array = get_zipcode_list.body
    results = parse_zipcodes_result_json(result_array)

    if results.is_a?(Array)

      add_locality_to_demand(demand_id,results.last)

      if results.first != []
        add_postal_codes_to_demand(demand_id,results.first)
      else
        add_postal_codes_to_demand(demand_id,[params[:postal_code]])
      end
      params[:only_zipcode] = results.first
      render :json => params.to_json
    elsif results.is_a?(FalseClass)
      params[:only_zipcode] = results
      render json: {status: "failed"}
    end
  end

  private

  def parse_zipcodes_result(results)
    only_zipcode = []
    results.each do |result|
      parsing = result.split(',').map {|item| item if item != ""}.compact
      (zipcode = parsing[0].gsub(/[^0-9]/, '').to_i) if parsing[0]
      (city = parsing[1].split(":").last.gsub('"', '')) if parsing[1]
      if zipcode != 0 && city != nil
        only_zipcode << zipcode
      end
    end
    only_zipcode.uniq
  end


  def parse_zipcodes_result_json(results)
    if results == "null"
      return false
    end
    @json = JSON.parse(results.to_s)
    @results = {}
    only_zipcode = []
    @json.each do |result|

      puts "€€€€€€€€€€€€€"
      puts "#{result}"
      puts "€€€€€€€€€€€€€"

      if result.is_a?(Array)
        zipcode = result.last["code_postal"]
        city_name = result.last["nom_commune"]
      elsif result.is_a?(Hash)
        zipcode = result["code_postal"]
        city_name = result["nom_commune"]
      end


      if zipcode.to_s.chars.to_a.size == 5 && zipcode != "0"
        only_zipcode << zipcode
        if @results.has_key?(city_name.to_sym)
          @results[city_name.to_sym].push("#{zipcode}")
        else
          @results[city_name.to_sym] = ["#{zipcode}"]
        end
      end
    end
    formatted_localisation = format_localisation_string(@results)
    uniq_array = only_zipcode.uniq
    uniq_array
    return [uniq_array, formatted_localisation]
  end


  def format_localisation_string(results)
    all_results = []

    if results != {}
      results.each do |zipcode, city_collection|
        if city_collection.size == 1
          value = "#{zipcode} (#{city_collection.first})"
          all_results << value
        elsif city_collection.size == 2
          value = "#{zipcode} (#{city_collection.first} et #{city_collection.last})"
          all_results << value
        elsif city_collection.size > 2
          value = "#{zipcode} (#{city_collection[0..-2].join(', ')} et #{city_collection.last})"
          all_results << value
        end
      end
      return "#{all_results.join(', ')}"
    else
      return "Vide"
    end
  end


  def create_empty_demand
    if session[:bcs_customer_id]
      @customer = Customer.find(session[:bcs_customer_id])
      if @customer.demands.any?
        @demand = @customer.demands.last
      else
        @demand = Demand.new()
        @demand.customer = @customer
        @demand.save
      end
    else
      @demand = Demand.new()
      @customer = Customer.new()
      @demand.customer = @customer
      @demand.save
      @customer.save(validate: false)
      session[:bcs_customer_id] = @customer.id.to_s
    end
  end

  def add_postal_codes_to_demand(demand_id,results)
    @demand = Demand.find(demand_id)
    @demand.postal_codes = results.to_a.map{|zipcode| zipcode.to_s}
    @demand.save
  end

  def add_locality_to_demand(demand_id,locality)
    @demand = Demand.find(demand_id.to_s)
    @demand.localisation = locality.to_s
    @demand.save
  end

  def add_lat_and_lng_to_demand(demand_id, lat, lng)
    @demand = Demand.find(demand_id)
    @demand.localisation_lng = lng
    @demand.localisation_lat = lat
    @demand.save
  end

end
