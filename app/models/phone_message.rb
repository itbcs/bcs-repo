class PhoneMessage
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  field :from, type: String
  field :to, type: String
  field :body, type: String
  field :template, type: String

end
