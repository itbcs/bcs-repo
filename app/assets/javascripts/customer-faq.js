$('.faq-visible img').each(function(index, element){
  $(element).click(function(){

    var content = $(element).next();
    if ($(content).css('height') == "0px") {
      $(content).css('height', '100px');
      $(content).css('padding-top', '15px');
    } else {
      $(content).css('height', '0px');
      $(content).css('padding-top', '0px');
    }
  });
});
