class OldPlatformAgency
  include Mongoid::Document
  include Mongoid::Timestamps

  field :old_platform_id, type: Integer
  field :old_platform_user_id, type: Integer
  field :company, type: String
  field :siret, type: Integer
  field :pro_card, type: String
  field :insurer_name, type: String
  field :postal_codes, type: Array
  field :min_price, type: Integer

end
