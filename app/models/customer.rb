class Customer
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  has_many :chat_messages
  has_many :chatrooms
  has_many :demands, dependent: :destroy
  has_many :v2_demands, dependent: :destroy
  has_many :rewards, dependent: :destroy
  has_one :score, dependent: :destroy
  has_many :customer_questions, dependent: :destroy
  has_many :old_platform_demands
  belongs_to :tracking_code, optional: true

  validates_presence_of :last_name, :first_name, :country
  validates_inclusion_of :promo_code, in: PromoCode.codelist, allow_blank: true
  validates :cgu, acceptance: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time
  field :remember_created_at, type: Time
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  field :last_name, type: String
  field :first_name, type: String
  field :password, type: String
  field :address, type: String
  field :city, type: String
  field :country, type: String
  field :postal_code, type: String
  field :phone, type: String
  field :code, type: String
  field :role, type: String
  field :status, type: String
  field :tags, type: Array
  field :maturity, type: String
  field :customer_type, type:  String
  field :admin_comment, type:  String
  field :last_platform_id, type: Integer
  field :cgu, type: Boolean
  field :promo_code, type: String
  field :vip, type: Boolean, default: false
  field :customer_reference, type: String
  field :references_collection, type: Array
  field :presence, type: Boolean
  field :enum_status, type: Integer
  field :badges, type: Array
  field :points, type: Integer
  field :pdf_customer_guide_request, type: Boolean, default: false
  field :last_api_authenticate_at, type: Time


  def self.authenticate(email,password)
    customer = Customer.find_for_authentication(:email => email)
    customer&.valid_password?(password) ? customer : nil
  end

  def enum_status
    values = {1 => "primo", 2 => "investisseur", 3 => "expert"}
    if self[:enum_status]
      return values[self[:enum_status]]
    else
      nil
    end
  end

  def has_status?
    if self[:enum_status]
      true
    else
      false
    end
  end

  def handle_index_strips(index)
    if index.to_s.chars.size == 1
      puts "Position size equal 1"
      collection_position = "000#{true_position}"
    elsif index.to_s.chars.size == 2
      puts "Position size equal 2"
      collection_position = "00#{true_position}"
    elsif index.to_s.chars.size == 3
      puts "Position size equal 3"
      collection_position = "0#{true_position}"
    elsif index.to_s.chars.size == 4
      puts "Position size equal 4"
      collection_position = "#{true_position}"
    end
    return collection_position
  end

  def generate_vip_ref
    current_id = self.id.to_s
    @customer = Customer.find(current_id)
    @demand = @customer.v2_demands.last

    if @demand.property_type == "Une maison"
      property_type = "01"
    elsif @demand.property_type == "Un appartement"
      property_type = "02"
    elsif @demand.property_type == "Un terrain"
      property_type = "03"
    end

    if @demand.search_zipcodes_area != []
      first_zipcode = @demand.search_zipcodes_area.first.to_s.chars.first(2).join("")
    end

    if @demand.destination == "Votre résidence principale"
      destination_type = "01"
    elsif @demand.destination == "Votre résidence secondaire"
      destination_type = "02"
    elsif @demand.destination == "Un investissement"
      destination_type = "03"
    end

    Demand.all.sort_by{|demand| demand.created_at}.each_with_index do |demand,index|
      position = (index+1).to_s.chars
      true_position = (index+1).to_s
      if demand.id.to_s == @demand.id.to_s
        puts "*************************"
        puts "#{demand.id.to_s}"
        puts "#{@demand.id.to_s}"
        puts "#{position}"
        puts "#{true_position}"
        puts position.size
        puts "*************************"
        if position.size == 1
          puts "Position size equal 1"
          @collection_position = "000#{true_position}"
        elsif position.size == 2
          puts "Position size equal 2"
          @collection_position = "00#{true_position}"
        elsif position.size == 3
          puts "Position size equal 3"
          @collection_position = "0#{true_position}"
        elsif position.size == 4
          puts "Position size equal 4"
          @collection_position = "#{true_position}"
        else
          puts "Position size equal nil"
          @collection_position = "Test"
        end
      end
    end
    if @customer.vip
      customer_type = 1
    else
      customer_type = 2
    end
    result = "#{customer_type} #{Date.today.year.to_s.chars.last(2).join("")} #{property_type} #{first_zipcode} #{destination_type} #{@collection_position}"
  end

end

