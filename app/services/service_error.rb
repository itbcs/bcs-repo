class ServiceError
  def initialize errors
    @errors = errors
  end

  def success?
    false
  end

  def errors
    @errors
  end
end
