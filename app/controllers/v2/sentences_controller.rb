class V2::SentencesController < ApplicationController

  def edit
    @sentence = Sentence.find(params[:id])
    render :edit
  end

  def update
    @sentence = Sentence.find(params[:id])
    if @sentence.update_attributes(safe_params)
      puts "Sentence successfully update"
      redirect_to v2_demand_view_path, notice: "Phrase modifiée avec succès !!"
    else
      render :update, alert: "Une erreur est survenue !!"
    end
  end

  private

  def safe_params
    params.require(:sentence).permit(:content, :content_after_variable, :demand_attribute)
  end

end
