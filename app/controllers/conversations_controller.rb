class ConversationsController < ApplicationController

  def conversation
    @sender = Customer.find(params[:customer_id])
    @receiver = Agency.find(params[:agency_id])
    @property = Property.find(params[:property_id])


    @conversation = Conversation.where(sender: @sender).where(property: @property).last
    if @conversation
    else
      @conversation = Conversation.new()
      @conversation.sender = @sender
      @conversation.receiver = @receiver
      @conversation.property = @property
      @conversation.save
    end
    render :conversation
  end

  def new_message
    @body = params[:customer_message][:body]
    @origin = params[:origin]
    @conversation = Conversation.find(params[:conversation_id])
    @message = Message.create(body: @body)
    @conversation.messages.push(@message)
    @customer = Customer.find(params[:customer_id])
    @message.origin = @origin
    @message.customer = @customer
    @message.agency = @conversation.receiver
    @conversation.save
    @creation_date = "Envoyé à #{l(@message.created_at, format: :count)}"

    puts @body
    puts @message.attributes

    if @message.save
      render json: [@message.attributes, @creation_date, @origin].to_json
    else
      render json: {errors: @message.errors.full_message}.to_json
    end
  end

  private

  def safe_params
    params.require(:conversation).permit(:customer_message)
  end

end
