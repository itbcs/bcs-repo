class DemandAgencyView
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :agency
  belongs_to :demand, optional: true
  belongs_to :v2_demand, optional: true

  field :view, type: Boolean, default: false

end
