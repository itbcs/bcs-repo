class AutomaticPropertyCreationMatching

  def initialize(property)
    @property = property
    @agency = @property.agency
    @errors = []
    @matching = []
  end

  def call
    automatic_matching
    service_success
  end

  def automatic_matching
    if @property.property_type == "Maison"
      @type = "21"
    elsif @property.property_type == "Appartement"
      @type = "22"
    elsif @property.property_type == "Terrain"
      @type = "23"
    end

    @delta = @property.price.gsub(/[[:space:]]/,'').to_i * 0.15
    @min_price = @property.price.gsub(/[[:space:]]/,'').to_i - @delta
    @max_price = @property.price.gsub(/[[:space:]]/,'').to_i + @delta
    @price_range = (@min_price.to_i.to_s..@max_price.to_i.to_s).to_a
    @zipcode = [@property.zipcode]
    @sanitize_area = @property.area.to_s.gsub(/[[:space:]]/,'').to_i
    @area_delta = @sanitize_area * 0.15
    @min_area = @sanitize_area - @area_delta
    @max_area = @sanitize_area + @area_delta
    @area_range = (@min_area.to_i.to_s..@max_area.to_i.to_s).to_a
    @rooms_count = (@property.nb_sleeping_room.to_s..(@property.nb_sleeping_room.to_i + 6).to_s).to_a

    # results = V2Demand.where(activated: true).where(property_type: @type).where(:search_zipcodes_area.in => @zipcode).where(:budget.in => @price_range).count
    results = V2Demand.where(activated: true).where(property_type: @type).where(:search_zipcodes_area.in => @zipcode).where(:budget.in => @price_range).where(:rooms.in => @rooms_count).where(:surface.in => @area_range)

    puts "**** AUTOMATIC MATCHING SERVICE ****"
    puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    puts "Prix du bien: #{@property.price}"
    puts "Code postal: #{@property.zipcode}"
    puts "Surface: #{@property.area}"
    puts "Delta prix: #{@min_price} - #{@max_price}"
    puts "Delta surface: #{@min_area} - #{@max_area}"
    puts "Chambres: #{@property.nb_sleeping_room}"
    puts "Nombre de demandes trouvées: #{results.count}"
    results.each do |result|
      puts "_______ Demande trouvée ______"
      puts "Budget de la demande: #{result.budget}"
      puts "Zone de recherche: #{result.search_zipcodes_area}"
      puts "Surface demandée: #{result.surface}"
      puts "Nombre de chambres souhaitée: #{result.rooms}"
      puts "______________________________"
      result.properties.push(@property)

      V2DemandProperty.create(v2_demand: result, property_id: @property.id.to_s)

      if result.save
        puts "Demand successfully update with new property"
        @agency.v2_demands.push(result)
      else
        puts "An error occured !!"
      end
      @matching << result.id.to_s
    end
    puts "************************************"
  end

  private

  def service_success
    ServiceSuccess.new(@matching)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
