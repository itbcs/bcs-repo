class Message
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :conversation
  belongs_to :customer, optional: true
  belongs_to :agency, optional: true

  field :body, type: String
  field :read, type: Boolean, default: false
  field :origin, type: String

end
