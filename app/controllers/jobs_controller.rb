class JobsController < ApplicationController

  def index
    @jobs = Job.all
    @candidacy = Candidacy.new()
  end

  def show
    @job = Job.find_by(lname: params[:lname])
  end

  private

  def job_params
    params.require(:job).permit(:picto, :name, :lname, :mission, :profil, :more, :starting_date)
  end

end
