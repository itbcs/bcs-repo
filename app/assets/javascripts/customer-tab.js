$("#user-nav-tabs li").on('click', function(e) {
    var targetLink = $(e.currentTarget.children[0]).attr("href").slice(1);

    var content_map = {
        c1  : "#content1",
        c2  : "#content2",
        c3  : "#content3",
        c4  : "#content4"
    }

    $(e.currentTarget).siblings().removeClass("tab-activated");

    $.each(content_map, function(hash, elid) {
        if (hash == targetLink) {
            $(elid).show();
            $(e.currentTarget).addClass("tab-activated");
        } else {
            $(elid).hide();
        }
    });
});
