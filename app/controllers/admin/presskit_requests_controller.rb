class Admin::PresskitRequestsController < ApplicationController

  def show
    @sponsorship = Sponsorship.find(params[:id].to_s)
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @presskit_request = PresskitRequest.find(params[:id])
    @pages = Page.all
    render "admin/presskit_requests/show", layout: "admin"
  end

  def index
    @sponsorship = Sponsorship.find(params[:id].to_s)
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/presskit_requests/index", layout: "admin"
  end

  def destroy
    @presskit_request = PresskitRequest.find(params[:id])
    if @presskit_request.destroy
      redirect_to admin_presskit_requests_path, notice: "Demande effacée !!"
    else
      render admin_presskit_requests_path, alert: "Un problème est survenu !!"
    end
  end

end
