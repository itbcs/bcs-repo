function handleServiceModal() {
  $('.widget-service-link').click(function(e) {
    e.preventDefault();
    url = $(this).data("url-target");
    var icon = $("<div class='loader-services-container'><i class='fa fa-circle-o-notch fa-spin'></i></div>");
    var receiverContainer = $(".modal-body.dynamic-receiver");
    $(receiverContainer).empty();
    $(receiverContainer).append(icon);
    $.ajax({
      method: "GET",
      data: {origin: "dashboard"},
      url: "/partners/" + url,
      beforeSend: function() {
        console.log("BeforeSend trigger");
      },
      success: function(data) {
        $(receiverContainer).empty();
        $(receiverContainer).html(data);
        console.log("Success trigger");
      },
    })

  });
}


handleServiceModal();
