var triggers = document.querySelectorAll("[data-filtertrigger]");
var activeItems = []
var activeNestedFilters = []
var filterContainer = document.getElementById("webhook-filter-container");
var resultCount = 0;
var resultText = document.querySelector(".webhook-filter-result-count");
var nestedFiltersContainer = document.querySelector(".webhook-nested-filter-container");

triggers.forEach(function(element) {
  element.addEventListener('click', function() {
    var allItems = document.querySelectorAll(".webhook-card");
    var allItemsSize = allItems.length;
    var dataValue = element.dataset.filtertrigger;
    var dataNested = element.dataset.nestedfilter;

    if (dataNested) {
      var actions = JSON.parse(element.dataset.actions);
    }

    element.classList.toggle("active");

    if (element.classList.contains("active")) {
      activeItems.push(dataValue);
    } else {
      activeItems = activeItems.filter(item => item !== dataValue);
    }


    if (actions && element.classList.contains("active")) {
      actions.forEach(function(actionItem) {
        activeNestedFilters.push(actionItem);
      });
    } else if (actions) {
      actions.forEach(function(actionItem) {
        activeNestedFilters = activeNestedFilters.filter(item => item !== actionItem);
      });
    }

    console.log(activeNestedFilters);

    if (activeNestedFilters.length === 0) {
      Array.from(nestedFiltersContainer.children).forEach(function(child) {
        child.remove();
      });
    } else {
      Array.from(nestedFiltersContainer.children).forEach(function(child) {
        child.remove();
      });
      activeNestedFilters.forEach(function(activeNestedFilter) {
        var domNestedFilter = document.createElement("div");
        domNestedFilter.classList.add("nested-filter-btn");
        domNestedFilter.textContent = activeNestedFilter;
        nestedFiltersContainer.appendChild(domNestedFilter);
      });
    }

    if (activeItems.length === 0) {
      resultCount = allItemsSize;
      allItems.forEach(function(item) {
        item.classList.remove("hidden");
      });
    } else {
      resultCount = 0;
      allItems.forEach(function(item) {
        var itemDataValue = item.dataset.filtertarget;
        if (activeItems.includes(itemDataValue)) {
          item.classList.remove("hidden");
          resultCount += 1;
        } else {
          item.classList.add("hidden");
        }
      });
    }

    resultText.textContent = resultCount;

  });
});









