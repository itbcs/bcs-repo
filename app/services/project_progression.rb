class ProjectProgression

  def initialize demand
    @demand = demand
    @errors = []
  end

  def call
    @degree = scoring_about_project
    service_success
  end

  private

  def scoring_about_project
    @project_progression = 0
    @counter_project = 0

    if @demand.installation_date
      if V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 16
        @counter_project += V2Demand.scoring_hash["installation_date"]["auto_identifier_16"]
      elsif V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 17
        @counter_project += V2Demand.scoring_hash["installation_date"]["auto_identifier_17"]
      elsif V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 18
        @counter_project += V2Demand.scoring_hash["installation_date"]["auto_identifier_18"]
      elsif V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 19
        @counter_project += V2Demand.scoring_hash["installation_date"]["auto_identifier_19"]
      end
    end

    if @demand.visited_property_count
      if V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 28
        @counter_project += V2Demand.scoring_hash["visited_property_count"]["auto_identifier_28"]
      elsif V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 29
        @counter_project += V2Demand.scoring_hash["visited_property_count"]["auto_identifier_29"]
      elsif V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 30
        @counter_project += V2Demand.scoring_hash["visited_property_count"]["auto_identifier_30"]
      elsif V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 31
        @counter_project += V2Demand.scoring_hash["visited_property_count"]["auto_identifier_31"]
      end
    end

    if @demand.banker_contact
      if V2Answer.find_by(auto_identifier: @demand["banker_contact"]).auto_identifier == 58
        @counter_project += V2Demand.scoring_hash["banker_contact"]["auto_identifier_58"]
      elsif V2Answer.find_by(auto_identifier: @demand["banker_contact"]).auto_identifier == 59
        @counter_project += V2Demand.scoring_hash["banker_contact"]["auto_identifier_59"]
      elsif V2Answer.find_by(auto_identifier: @demand["banker_contact"]).auto_identifier == 60
        @counter_project += V2Demand.scoring_hash["banker_contact"]["auto_identifier_60"]
      end
    end

    if @demand.broker_contact
      if V2Answer.find_by(auto_identifier: @demand["broker_contact"]).auto_identifier == 61
        @counter_project += V2Demand.scoring_hash["broker_contact"]["auto_identifier_61"]
      elsif V2Answer.find_by(auto_identifier: @demand["broker_contact"]).auto_identifier == 62
        @counter_project += V2Demand.scoring_hash["broker_contact"]["auto_identifier_62"]
      elsif V2Answer.find_by(auto_identifier: @demand["broker_contact"]).auto_identifier == 63
        @counter_project += V2Demand.scoring_hash["broker_contact"]["auto_identifier_63"]
      end
    end

    if @demand.destination
      if V2Answer.find_by(auto_identifier: @demand["destination"]).auto_identifier == 61
        @counter_project += V2Demand.scoring_hash["destination"]["auto_identifier_61"]
      elsif V2Answer.find_by(auto_identifier: @demand["destination"]).auto_identifier == 62
        @counter_project += V2Demand.scoring_hash["destination"]["auto_identifier_62"]
      elsif V2Answer.find_by(auto_identifier: @demand["destination"]).auto_identifier == 63
        @counter_project += V2Demand.scoring_hash["destination"]["auto_identifier_63"]
      end
    end

    if @demand.properties
      if @demand.properties.count <= 2
        @counter_project += V2Demand.scoring_hash["properties"]["moins de 2 offres"]
      elsif (@demand.properties.count >= 3) && (@demand.properties.count <= 5)
        @counter_project += V2Demand.scoring_hash["properties"]["de 3 à 5 offres"]
      elsif (@demand.properties.count >= 6) && (@demand.properties.count <= 8)
        @counter_project += V2Demand.scoring_hash["properties"]["de 6 à 8 offres"]
      elsif @demand.properties.count > 8
        @counter_project += V2Demand.scoring_hash["properties"]["plus de 8 offres"]
      end
    end

    if @demand.customer_choices
      if @demand.customer_choices == []
        @counter_project += V2Demand.scoring_hash["customer_choices"]["0"]
      elsif (@demand.customer_choices.size >= 1) && (@demand.customer_choices.size <= 3)
        @counter_project += V2Demand.scoring_hash["customer_choices"]["de 1 à 3 réactions"]
      elsif (@demand.customer_choices.size >= 4) && (@demand.customer_choices.size <= 6)
        @counter_project += V2Demand.scoring_hash["customer_choices"]["de 4 à 6 réactions"]
      elsif @demand.customer_choices.size > 6
        @counter_project += V2Demand.scoring_hash["customer_choices"]["plus de 6 réactions"]
      end
    end

    generate_degrees(@counter_project)
  end

  def generate_degrees counter_project
    if (counter_project < 8) || (counter_project == 8)
      @project_progression = -90
    elsif (counter_project > 8) && (counter_project <= 11)
      @project_progression = -70
    elsif (counter_project > 11) && (counter_project <= 14)
      @project_progression = -47
    elsif (counter_project > 14) && (counter_project <= 17)
      @project_progression = -20
    elsif (counter_project > 17) && (counter_project <= 20)
      @project_progression = 0
    elsif (counter_project > 20) && (counter_project <= 23)
      @project_progression = 25
    elsif (counter_project > 23) && (counter_project <= 26)
      @project_progression = 45
    elsif counter_project > 26
      @project_progression = 75
    end
  end

  def service_success
    ServiceSuccess.new(@degree)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
