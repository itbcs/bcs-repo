function handleDeviseAjaxForm() {
  $(".insurvey-customer-form form").submit(function(event) {
    event.preventDefault();
    var form = $(this);
    var url = $(form).attr('action');
    var method = $(form).attr('method');
    $.ajax({
      url: url,
      type: method,
      data: $(form).serialize(),
      success: function(data) {
        if (data.errors) {
          var submitButton = $(form).find("input[type='submit']");
          $(submitButton).attr("disabled", false);
          for (var [ key, value ] of Object.entries(data.errors)) {
            if (key === "email") {
              var sentence = $("<div class='custom-input-error " + key + "'>Cet email " + value[0] + "</div>");
            } else if (key === "first_name") {
              var sentence = $("<div class='custom-input-error " + key + "'>Votre prénom est obligatoire</div>");
            } else if (key === "last_name") {
              var sentence = $("<div class='custom-input-error " + key + "'>Votre nom est obligatoire</div>");
            } else if (key === "password") {
              var sentence = $("<div class='custom-input-error " + key + "'>Mot de passe obligatoire</div>");
            } else {
              var sentence = $("<div class='custom-input-error " + key + "'>Ce champ " + value[0] + "</div>");
            }
            var idToCheck = "#customer_" + key;
            $(idToCheck).addClass("input-error-border");
            var currentInput = $(idToCheck);
            $(sentence).insertAfter($(currentInput));
            $(currentInput).focus(function() {
              $(this).removeClass("input-error-border");
              $(this).next().remove();
            });
          }
        }
        if (data.url) {
          document.location.href = data.url
        }
        if (data.connected) {
          alert(data.connected);
        }
      },
      error: function(data) {
        console.log(data);
      }
    });
  });
}


handleDeviseAjaxForm();
