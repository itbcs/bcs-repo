function init() {
      if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
      var $ = go.GraphObject.make;  // for conciseness in defining templates

      myDiagram =
        $(go.Diagram, "myDiagramDiv",  // must be the ID or reference to div
          {
            "toolManager.hoverDelay": 100,  // 100 milliseconds instead of the default 850
            allowCopy: false,
            layout:  // create a TreeLayout for the family tree
              $(go.TreeLayout,
                { angle: 90, nodeSpacing: 10, layerSpacing: 40, layerStyle: go.TreeLayout.LayerUniform })
          });

      var bluegrad = '#d0dfe8';
      var pinkgrad = '#dee8f7';
      var greengrad = "#def7ef";
      var redgrad = "#f9e0e7";

      // Set up a Part as a legend, and place it directly on the diagram
      // myDiagram.add(
      //   $(go.Part, "Table",
      //     { position: new go.Point(300, 10), selectable: false },
      //     $(go.TextBlock, "Key",
      //       { row: 0, font: "700 14px Cerebri Sans, sans-serif" }),  // end row 0
      //     $(go.Panel, "Horizontal",
      //       { row: 1, alignment: go.Spot.Left },
      //       $(go.Shape, "RoundedRectangle",
      //         { desiredSize: new go.Size(30, 30), fill: bluegrad, margin: 5 }),
      //       $(go.TextBlock, "Males",
      //         { font: "700 13px Cerebri Sans, sans-serif" })
      //     ),  // end row 1
      //     $(go.Panel, "Horizontal",
      //       { row: 2, alignment: go.Spot.Left },
      //       $(go.Shape, "RoundedRectangle",
      //         { desiredSize: new go.Size(30, 30), fill: pinkgrad, margin: 5 }),
      //       $(go.TextBlock, "Females",
      //         { font: "700 13px Cerebri Sans, sans-serif" })
      //     )  // end row 2
      //   ));

      // get tooltip text from the object's data
      function tooltipTextConverter(person) {
        var str = "";
        str += "Born: " + person.birthYear;
        if (person.deathYear !== undefined) str += "\nDied: " + person.deathYear;
        if (person.reign !== undefined) str += "\nReign: " + person.reign;
        if (person.answers !== undefined) str += "\nNombre de réponse: " + person.answers;
        if (person.condition !== undefined) str += "\nCondition: " + person.condition;
        return str;
      }

      // define tooltips for nodes
      var tooltiptemplate =
        $("ToolTip",
          { "Border.fill": "whitesmoke", "Border.stroke": "white" },
          $(go.TextBlock,
            {
              font: "bold 8pt Cerebri Sans, sans-serif",
              wrap: go.TextBlock.WrapFit,
              margin: 5
            },
            new go.Binding("text", "", tooltipTextConverter))
        );

      // define Converters to be used for Bindings
      function genderBrushConverter(gender) {
        if (gender === "TITLE") return bluegrad;
        if (gender === "QUESTION") return pinkgrad;
        if (gender === "PATH1") return greengrad;
        if (gender === "PATH2") return redgrad;
        return "orange";
      }

      // replace the default Node template in the nodeTemplateMap
      myDiagram.nodeTemplate =
        $(go.Node, "Auto",
          { deletable: false, toolTip: tooltiptemplate },
          new go.Binding("text", "name"),
          $(go.Shape, "RoundedRectangle",
            {
              fill: "lightgray",
              stroke: null, strokeWidth: 0,
              stretch: go.GraphObject.Fill,
              alignment: go.Spot.Center
            },
            new go.Binding("fill", "gender", genderBrushConverter)),
          $(go.TextBlock,
            {
              font: "500 12px Cerebri Sans, sans-serif",
              textAlign: "center",
              margin: 10, maxSize: new go.Size(210, NaN)
            },
            new go.Binding("text", "name"))
        );

      // define the Link template
      myDiagram.linkTemplate =
        $(go.Link,  // the whole link panel
          { routing: go.Link.Orthogonal, corner: 5, selectable: false },
          $(go.Shape, { strokeWidth: 3, stroke: '#dee8f7' }));  // the gray link shape

      // here's the family data
      var nodeDataArray = [
        { key: 0, name: "Questionaire", gender: "TITLE", birthYear: "1865", deathYear: "1936", reign: "1910-1936" },
        { key: 1, parent: 0, name: "Q1", gender: "QUESTION", birthYear: "1894", deathYear: "1972", reign: "1936" },
        { key: 2, parent: 0, name: "Q2", gender: "QUESTION", birthYear: "1895", deathYear: "1952", reign: "1936-1952" },
        { key: 3, parent: 1, name: "Q3", gender: "QUESTION", birthYear: "1926", reign: "1952-" },
        { key: 4, parent: 1, name: "Q4", gender: "PATH1", birthYear: "1948" },
        { key: 5, parent: 2, name: "Q5", gender: "PATH2", birthYear: "1982" },
        { key: 6, parent: 2, name: "Q6", gender: "QUESTION", birthYear: "1984" },
        { key: 7, parent: 2, name: "Q7", gender: "QUESTION", birthYear: "1950" }
      ];

      var nodeDataArray = gon.datas

      // create the model for the family tree
      myDiagram.model = new go.TreeModel(nodeDataArray);

      document.getElementById('zoomToFit').addEventListener('click', function() {
        myDiagram.zoomToFit();
      });

      document.getElementById('centerRoot').addEventListener('click', function() {
        myDiagram.scale = 1;
        myDiagram.scrollToRect(myDiagram.findNodeForKey(0).actualBounds);
      });

    }

init();
