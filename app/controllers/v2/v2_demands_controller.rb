class V2::V2DemandsController < ApplicationController

  before_action :find_demand, only: [:show,:edit,:update,:destroy]

  def influence_ajax_query
    @zipcode = params["zipcode"]
    @locality = params["locality"]
    @lat = params["lat"]
    @lng = params["lng"]
    @radius = params["radius"]
    @demand = V2Demand.find(params["demand"])
    @results = handle_influence_area(@lat,@lng,@radius,@demand)
    @api_return = handle_coordinates(@results,@demand)

    render :json => @api_return.to_json
  end

  def show
    if params[:results]
      @api_return = params[:results]
    end
  end

  def results
    if params[:results]
      @api_return = params[:results]
    end
  end

  def index
  end

  def create
  end

  def new
  end

  def edit
  end

  def update
    @zipcodes = []
    params[:v2_demand][":geoloc_items"].to_a.each_with_index do |item,index|
      puts "---- ITEM ----"
      puts item
      puts "--------------"
      if index + 1 == 1
        @zipcode = item.to_enum.to_h[":zipcode"]
        @locality = item.to_enum.to_h[":locality"]
        @lat = item.to_enum.to_h[":coordinates"][":lat"]
        @lng = item.to_enum.to_h[":coordinates"][":lng"]
        @radius = item.to_enum.to_h[":coordinates"][":radius"].to_i
        puts "#{@zipcode} -- #{@locality} -- #{@lat} -- #{@lng} -- #{@radius}"
        if @zipcode
          @zipcodes << @zipcode
        end
        @results = handle_influence_area(@lat,@lng,@radius,@demand)
        @api_return = handle_coordinates(@results,@demand)
      else
        @zipcode = item.to_enum.to_h[":zipcode"]
        @locality = item.to_enum.to_h[":locality"]
        @lat = item.to_enum.to_h[":coordinates"][":lat"]
        @lng = item.to_enum.to_h[":coordinates"][":lng"]
        @radius = item.to_enum.to_h[":coordinates"][":radius"].to_enum.to_h.map{|key,value| value}.last.to_i
        puts "#{@zipcode} -- #{@locality} -- #{@lat} -- #{@lng} -- #{@radius}"
        if @zipcode
          @zipcodes << @zipcode
        end
        @results = handle_influence_area(@lat,@lng,@radius,@demand)
        @api_return = handle_coordinates(@results,@demand)
      end
    end
    # redirect_to v2_results_path(results: @api_return)
    redirect_to v2_v2_demand_path(@demand,results: @api_return)
  end

  def destroy
  end

  private

  def handle_influence_area(lat,lng,radius,demand)
    radius_in_km = (radius.to_f / 1000).round(1)
    puts "--- RADIUS IN KM ---"
    puts radius_in_km
    puts radius_in_km.class
    puts "--------------------"
    circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{lat}&lng=#{lng}&radius=#{radius_in_km}&username=agaloppe84").body
    @json = JSON.parse(circle_influence_zone.to_s)
    puts "--- JSON API FULL RETURN ---"
    puts "http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{lat}&lng=#{lng}&radius=#{radius_in_km}&username=agaloppe84"
    puts @json.inspect
    puts "----------------------------"
    coordinates = handle_json(@json,demand)
    return coordinates
  end


  def handle_json(json_return,demand)
    @string_results = []
    @only_cities = []
    @coordinates = []

    json_return.each do |api_return|
      api_return.last.each do |element|
        puts "--- ELEMENT ----"
        puts element.inspect
        puts "----------------"
        distance = element["distance"]
        lng = element["lng"]
        lat = element["lat"]
        gps_coordinate = "#{lat},#{lng}"
        cityname = element["name"].parameterize
        @string_results << " (#{distance}(Km) - #{cityname}) "
        @only_cities << cityname
        current_element_hash = {}
        current_element_hash[:coordinate] = gps_coordinate
        current_element_hash[:district] = cityname
        current_element_hash[:distance] = distance
        @coordinates << current_element_hash
      end
      puts "---- COORDINATES ----"
      puts @coordinates
      puts "---------------------"
    end
    return @coordinates
  end


  def handle_coordinates(coordinates,demand)
    @result_as_array = []
    @only_zipcodes = []
    coordinates.each do |coordinate|
      zipcode_finder = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + coordinate[:coordinate] + "&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA").body
      @zipcode_json = JSON.parse(zipcode_finder.to_s)
      components = @zipcode_json["results"][0]["address_components"]
      @result_as_hash = {}
      @result_as_hash[:distance] = coordinate[:distance].to_f.round(1).to_f
      components.each do |compo|
        if compo["types"][0] === "postal_code"
          @only_zipcodes << compo["short_name"]
          @result_as_hash["zipcode"] = compo["short_name"]
        end
        if compo["types"][0] === "locality"
          @result_as_hash["locality"] = compo["short_name"]
        end
      end

      if @result_as_hash != {}
        if (@result_as_hash["locality"].downcase.parameterize) != (coordinate[:district].downcase.parameterize)
          @result_as_hash["district"] = coordinate[:district].downcase.capitalize.gsub("-"," ")
        end
      end

      @result_as_array << @result_as_hash
    end

    puts "******************"
    puts "ALL HASHED RESULTS"
    puts @result_as_array
    puts "******************"

    # @uniq_hashed_results = @result_as_array.uniq{|h| h["zipcode"]}
    @uniq_hashed_results = @result_as_array

    puts "UNIQ HASHED RESULTS"
    puts @uniq_hashed_results
    puts @uniq_hashed_results.inspect
    return @uniq_hashed_results
  end


  def find_demand
    @demand = V2Demand.find(params[:id])
  end

  def demand_params
    params.require(:v2_demand).permit(:search_zipcodes_area, :search_formatted_area)
  end

end
