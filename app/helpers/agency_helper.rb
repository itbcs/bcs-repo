require "csv"

module AgencyHelper

  def handle_demand_properties_agency_links(demand_properties, agency_properties, demand)
    results = []
    agency_properties.each do |prop|
      if demand_properties.to_a.include?(prop)
        results << prop
      end
    end

    @demand_choices = {}
    @positive_choices = 0
    @negative_choices = 0
    @pending_choices = 0

    if results.size > 0
      results.each do |property|
        choice = CustomerChoice.where(property: property, v2_demand: demand).last
        if choice
          if choice.choice == "J'aime" || choice.choice == "J'adore"
            @positive_choices += 1
            @refined_result = "positive"
          elsif choice.choice == "Je n'aime pas"
            @negative_choices += 1
            @refined_result = "negative"
          end
        else
          @pending_choices += 1
          @refined_result = "pending"
        end
      end

      if @positive_choices > 0
        @demand_choices[:positive] = @positive_choices
      end

      if @negative_choices > 0
        @demand_choices[:negative] = @negative_choices
      end

      if @pending_choices > 0
        @demand_choices[:pending] = @pending_choices
      end

      puts @demand_choices

    else
      @refined_result = nil
    end

    return @demand_choices
  end

  def handle_demand_view(demand, current_agency)
    results = []
    demand.demand_agency_views.each{|demand_view| results << demand_view.agency}

    if results.include? current_agency
      "vu"
    else
      "non-vu"
    end
  end

  def handle_genre(firstname)
    @names = []
    CSV.foreach("prenoms.csv", headers: false, encoding: 'iso-8859-1:utf-8') do |row|
      insee_firstname = row.first.split(";").first.parameterize
      @names << {name: insee_firstname, genre: row.first.split(";")[1]}
    end

    @array_result = @names.map!{|name| name.values}

    @array_result.each do |result|
      if result.first == firstname.parameterize
        if result.last.to_sym == :f
          @real_genre = "1"
        elsif result.last.to_sym == :m
          @real_genre = "2"
        end
      end
    end
    return @real_genre ? @real_genre : "1"
  end

  def sanitize_customer_house_stairs(demand)
    if V2Answer.find_by(auto_identifier: demand).auto_identifier == 73
      result = "Oui, 2 étages"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 74
      result = "Oui, 1 étage"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 75
      result = "Non"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 76
      result = "Peu importe"
    end
    return result
  end

  def sanitize_customer_flat_stairs(demand)
    if V2Answer.find_by(auto_identifier: demand).auto_identifier == 77
      result = "Rez de chaussée"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 78
      result = "A partir du 1er étage"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 79
      result = "Dernier étage"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 80
      result = "Peu importe"
    end
    return result
  end

  def sanitize_customer_project(demand)
    if V2Answer.find_by(auto_identifier: demand).auto_identifier == 7
      result = "Célibataire"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 8
      result = "En couple"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 9
      result = "En famille"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 10
      result = "Une famille recomposée"
    end
    return result
  end

  def sanitize_customer_destination(demand)
    if V2Answer.find_by(auto_identifier: demand).auto_identifier == 4
      result = "Résidence principale"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 5
      result = "Résidence secondaire"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 6
      result = "Un investissement locatif"
    end
    return result
  end

  def sanitize_customer_actual_situation(demand)
    if V2Answer.find_by(auto_identifier: demand).auto_identifier == 11
      result = "Non"
    elsif V2Answer.find_by(auto_identifier: demand).auto_identifier == 12 || V2Answer.find_by(auto_identifier: demand).auto_identifier == 13
      result = "Oui"
    end
    return result
  end

  def user_choice_is_positive(property)
    @positive_counter = 0
    property.customer_choices.each do |user_choice|
      puts "---- USER CHOICE ----"
      puts user_choice.inspect
      puts "---------------------"
      if user_choice.choice == "J'aime" || user_choice.choice == "J'adore"
        @positive_counter += 1
      end
    end
    return @positive_counter
  end

  def user_choice_is_negative(property)
    @negative_counter = 0
    property.customer_choices.each do |user_choice|
      if user_choice.choice == "Je n'aime pas"
        @negative_counter += 1
      end
    end
    return @negative_counter
  end

  def demand_budget_display(demand)
    if demand.include?('et')
      result = demand.split('et').last
    else
      result = demand
    end
    return result
  end

  def formatted_url url
    @url = url.strip
    if @url.start_with?("http")
      result = @url.split("//").last
      @link = link_to "Site web", "http://" + result, target: "_blank"
    else
      @link = link_to "Site web", "http://" + @url, target: "_blank"
    end
    return @link
  end

  def format_zipcode zipcode
    if zipcode.to_s.length < 5
      code = "0" + zipcode.to_s
    elsif zipcode.to_s.length > 5
      code =  "Problem with zipcode - too long !!!!"
    else
      code = zipcode.to_s
    end
    return code
  end

  def formatted_address agency
    if agency.location && agency.city
      @location = agency.location.downcase.remove(agency.city.downcase)
    elsif agency.location
      @location = agency.location.strip.split[0...-1].join(" ")
    else
      @location = "Adresse non renseignée"
    end
    if agency.location_cp
      @location_cp = format_zipcode(agency.location_cp)
    else
      @location_cp = "Code postal non renseigné"
    end
    if agency.city
      @city = agency.city.upcase
    else
      @city = "Ville non renseignée"
    end
    address = "#{@location} - #{@location_cp} #{@city}"
  end


  def format_string_price(price)
    if price
      result = (price.to_i.to_s.chars.each_slice(3).map{|slice| slice.join("")}).join(" ")
      return "#{result} €"
    else
      return "Prix non renseigné"
    end
  end

  def agency_budget_format(budget_array)
    sanitized_array = budget_array.reject{|budget| budget == ""}
    if sanitized_array.size == 1
      result = "#{sanitized_array.first}"
    elsif sanitized_array.size > 1
      result = "Entre #{sanitized_array.first} et #{sanitized_array.last}"
    end
    return result
  end

  def agency_zipcode_format(zipcode_array)
    if zipcode_array.size == 1
      result = "#{zipcode_array.first}"
    elsif zipcode_array.size > 1
      result = "#{zipcode_array.first(4)[0..-2].join(', ')} et #{zipcode_array.last}"
    end
    return result
  end

  def handle_pending_offers(agency)
    @result = []

    agency.each do |demand|
      demand.customer_choices.each do |customer_choice|
        demand.properties.each do |prop|
          if customer_choice.choice == "J'aime"
            @result << prop
          end
        end
      end
    end
    return @result.uniq.sort_by{|d| d.created_at}.reverse
  end


  def handle_response_waiting(agency)
    @results = []
    @agency = agency
    @agency.v2_demands.each do |demand|
      demand.properties.each do |prop|
        choice = CustomerChoice.where(property: prop, v2_demand: demand).last
        if choice
          @results << demand
        end
      end
    end
    return @results
  end

  # def handle_viewed_demands(agency)
  #   @results = []
  #   if agency.demands != []
  #     agency.demands.each do |demand|
  #       @demand_view = DemandAgencyView.where(agency: agency).where(demand: demand).where(view: true).last
  #       if @demand_view
  #         @results << demand
  #       end
  #     end
  #   end
  #   return @results.sort_by{|demand| demand.created_at}.reverse
  # end

  # def handle_unviewed_demands(agency)
  #   @results = []
  #   if agency.demands != []
  #     agency.demands.each do |demand|
  #       @demand_view = DemandAgencyView.where(agency: agency).where(demand: demand).where(view: true).last
  #       if !@demand_view
  #         @results << demand
  #       end
  #     end
  #   end
  #   return @results.sort_by{|demand| demand.created_at}.reverse
  # end


  def handle_viewed_demands(agency)
    @results = []
    if agency.v2_demands != []
      agency.v2_demands.each do |demand|
        @demand_view = DemandAgencyView.where(agency: agency).where(v2_demand: demand).where(view: true).last
        if @demand_view
          @results << demand
        end
      end
    end
    return @results.sort_by{|demand| demand.created_at}.reverse
  end

  def handle_unviewed_demands(agency)
    @results = []
    if agency.v2_demands != []
      agency.v2_demands.each do |demand|
        @demand_view = DemandAgencyView.where(agency: agency).where(v2_demand: demand).where(view: true).last
        if !@demand_view
          @results << demand
        end
      end
    end
    return @results.sort_by{|demand| demand.created_at}.reverse
  end

  def handle_unviewed_v2_demands(agency)
    @results = []
    if agency.v2_demands != []
      agency.v2_demands.each do |demand|
        @demand_view = DemandAgencyView.where(agency: agency).where(v2_demand: demand).where(view: true).last
        if !@demand_view
          @results << demand
        end
      end
    end
    return @results.sort_by{|demand| demand.created_at}.reverse
  end



  def handle_demands_choices_view(current_agency, demand)
    @results = {}
    @love_count = 0
    @like_count = 0
    @hate_count = 0

    current_agency.properties.each do |prop|
      @choice = demand.customer_choices.where(property_id: prop.id).last
      if @choice
        puts @choice.choice
        if @choice.choice == "J'adore"
          @love_count += 1
        elsif @choice.choice == "J'aime"
          @like_count += 1
        elsif @choice.choice == "Je n'aime pas"
          @hate_count += 1
        end
      end
    end

    if @love_count != 0
      @results[:love] = @love_count
    end
    if @like_count != 0
      @results[:like] = @like_count
    end
    if @hate_count != 0
      @results[:hate] = @hate_count
    end

    if (@love_count != 0) || (@like_count != 0) || (@hate_count != 0)
      return @results
    else
      return nil
    end

  end


  def handle_demand_filter_tags(agency,demand,seen)
    tags = ["seebyagency","notviewed","future-visit","love","hate"]
    if seen
      @results = handle_demands_choices_view(agency, demand)
      @tags = ["seebyagency"]
      if @results
        @results.each do |key,value|
          if key.to_sym == :love
            if value.to_i > 0
              @tags << "love"
            end
          elsif key.to_sym == :like
            if value.to_i > 0
              @tags << "love"
            end
          elsif key.to_sym == :hate
            if value.to_i > 0
              @tags << "hate"
            end
          end
        end
      end
    else
      @results = handle_demands_choices_view(agency, demand)
      @tags = ["notviewed"]
    end
    puts @tags.uniq.join(",")
    return @tags.uniq.join(",")
  end



end

