class PropertyCustomerView
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :customer
  belongs_to :property

  field :view, type: Boolean, default: false

end
