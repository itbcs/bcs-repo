require 'rest-client'

class MapCirclesController < ApplicationController

  def create
    x_value = params[:x]
    y_value = params[:y]
    radius_value = params[:radius]
    circle_id = params[:circle_id]
    layer_id = params[:layer_id]
    auto_id = params[:auto_id]

    @circle = MapCircle.new(x: x_value, y: y_value, radius: radius_value, circle_id: circle_id, layer_id: layer_id, auto_id: auto_id)
    @demand = Customer.find(session[:bcs_customer_id].to_s).demands.last
    if @demand.map_circles.any?
      @demand.map_circles.destroy_all
    end
    @circle.demand = @demand
    @demand.map_circles.push(@circle)
    @demand.save

    if @circle.save
      puts "*******************************"
      puts "circle_mongo_id = #{@circle.id}, x = #{x_value}, y = #{y_value}, radius = #{radius_value}, circle_id = #{circle_id}, layer_id = #{layer_id}"
      puts "*******************************"
      render json: {status: "YES", mongo_id: @circle.id, x: x_value, y: y_value, radius: radius_value, circle_id: circle_id, layer_id: layer_id}.to_json
    else
      render json: {status: "NO"}.to_json
    end

  end


  def multiple_circles_create
    lat = params[:y]
    lng = params[:x]
    radius = params[:radius]
    position = params[:circle_position]
    @circle = MapCircle.new(x: lng, y: lat, radius: radius, circle_id: position)
    @demand = Customer.find(session[:bcs_customer_id].to_s).demands.last
    @circle.demand = @demand
    @demand.map_circles.push(@circle)

    if @circle.valid?
      render json: {status: "YES", mongo_id: position, x: lng, y: lat, radius: radius}.to_json
    else
      render json: {status: "NO"}.to_json
    end
  end


  def multi_circle_batch_constructor
    @demand = Customer.find(session[:bcs_customer_id].to_s).demands.last
    @only_cities = []
    @only_zipcodes = []
    @circle_objects = {}
    @string_results = []
    @coordinates = []
    @concatenate_radius = 0
    @message = "OK"
    @result_as_array = []

    if @demand
      @demand.map_circles.destroy_all
    end

    params["circles"].each do |key,value|
      if ((value["radius"].to_f) / 1000) < 1.10
        retrieve_radius = (value["radius"].to_f / 1000).round(3)
        retrieve_radius = (retrieve_radius * 2.0).round(3)
      else
        retrieve_radius = (value["radius"].to_f / 1000).round(3)
      end
      retrieve_lat = value["lat"]
      retrieve_lng = value["lng"]
      @circle = MapCircle.new(x: retrieve_lng, y: retrieve_lat, radius: retrieve_radius)
      @demand.map_circles.push(@circle)
      @concatenate_radius += retrieve_radius.to_f
    end

    @demand.save

    @demand.map_circles.each_with_index do |circle,index|
      @circle_objects[index + 1] = {circle_id: "#{index + 1}", x: "#{circle.x}", y: "#{circle.y}", radius: "#{circle.radius}"}.to_json
      meters_in_km = circle.radius

      begin
        circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{circle.y}&lng=#{circle.x}&radius=#{meters_in_km}&username=agaloppe84").body
        if JSON.parse(circle_influence_zone.to_s) == {"geonames"=>[]}
          puts "Old circle radius: #{meters_in_km}"
          meters_in_km = (meters_in_km * 1).round(2)
          puts "New circle radius: #{meters_in_km}"
          circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{circle.y}&lng=#{circle.x}&radius=#{meters_in_km}&username=agaloppe84").body
          if JSON.parse(circle_influence_zone.to_s) == {"geonames"=>[]}
            puts "Nested API call - 2nd times"
            puts "Nested - Old circle radius: #{meters_in_km}"
            meters_in_km = (meters_in_km * 2.5).round(2)
            puts "Nested - New circle radius: #{meters_in_km}"
            circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{circle.y}&lng=#{circle.x}&radius=#{meters_in_km}&username=agaloppe84").body
          end
        end
      rescue RestClient::ServiceUnavailable
        @message = "Service non disponible"

        # Recursive method
        multi_circle_batch_constructor
      end

      @json = JSON.parse(circle_influence_zone.to_s)
      @json.each do |api_return|
        api_return.last.each do |element|
          distance = element["distance"]
          lng = element["lng"]
          lat = element["lat"]
          gps_coordinate = "#{lat},#{lng}"
          cityname = element["name"].parameterize
          @string_results << " (#{distance}(Km) - #{cityname}) "
          @only_cities << cityname
          current_element_hash = {}
          current_element_hash[:coordinate] = gps_coordinate
          current_element_hash[:district] = cityname
          @coordinates << current_element_hash
        end
      end
    end
    # puts @string_results
    # puts @only_cities

    @coordinates.each do |coordinate|
      zipcode_finder = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + coordinate[:coordinate] + "&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA").body
      @zipcode_json = JSON.parse(zipcode_finder.to_s)
      components = @zipcode_json["results"][0]["address_components"]
      @result_as_hash = {}
      components.each do |compo|
        if compo["types"][0] === "postal_code"
          @only_zipcodes << compo["short_name"]
          @result_as_hash["zipcode"] = compo["short_name"]
        end
        if compo["types"][0] === "locality"
          @result_as_hash["locality"] = compo["short_name"]
        end
      end

      if (@result_as_hash["locality"].downcase.parameterize) != (coordinate[:district].downcase.parameterize)
        @result_as_hash["district"] = coordinate[:district].downcase.capitalize.gsub("-"," ")
      end

      @result_as_array << @result_as_hash
    end

    @uniq_hashed_results = @result_as_array.uniq{|h| h["zipcode"]}

    puts "UNIQ HASHED RESULTS"
    puts @uniq_hashed_results
    puts @uniq_hashed_results.inspect

    @zipcodes_to_append = []
    @uniq_hashed_results.each do |result_hash_values|
      @zipcodes_to_append << result_hash_values["zipcode"]
    end
    add_postal_codes_to_demand(@demand.id.to_s,@zipcodes_to_append)
    # add_locality_to_demand(@demand.id.to_s,@uniq_hashed_results.first["locality"])
    new_version_add_locality_to_demand(@demand.id.to_s, @uniq_hashed_results)


    if @uniq_hashed_results != []
      render json: {message: @message, radius: @concatenate_radius.to_i, cities: @only_cities, fullresponse: @string_results, arrayresponse: @uniq_hashed_results, demandid: @demand.id.to_s}
    elsif @uniq_hashed_results == []
      render json: {message: "empty"}
    end
  end


  def remove_specific_circle
    @demand = Customer.find(session[:bcs_customer_id].to_s).demands.last
    if @demand
      if @demand.map_circles?
        @circle_to_delete = @demand.map_circles.where(radius: params["radius"]).last

        puts @circle_to_delete.inspect
        puts params["radius"]

        if @circle_to_delete
          puts "Good circle to delete !!!!!!"
          @circle_to_delete.destroy
          render json: {status: "Success"}.to_json
        else
          puts "No circle found !!!!!!"
          render json: {status: "No circle found"}.to_json
        end
      end
    end
  end



  def remove
    @demand = Customer.find(session[:bcs_customer_id].to_s).demands.last
    if @demand
      @circle = @demand.map_circles.last
    end
    if @circle
      puts @circle.inspect
      if @circle.destroy
        render json: {status: "Success"}.to_json
      else
        render json: {status: "Failed"}.to_json
      end
    else
      render json: {status: "No circle found"}.to_json
    end

  end


  def update_circle_position_and_radius
    @demand = Customer.find(session[:bcs_customer_id].to_s).demands.last
    first_radius = params[:first_radius]
    new_radius = params[:radius]
    new_x = params[:x]
    new_y = params[:y]
    if @demand
      @circle = @demand.map_circles.last
    end

    if @circle
      @circle.x = new_x
      @circle.y = new_y
      @circle.radius = new_radius
      @circle.save
      render json: {x: @circle.x, y: @circle.y, radius: @circle.radius}.to_json
    else
      render json: {status: "Circle not found !!!"}.to_json
    end
  end


  def handle_carrierwave_remote_map_upload
    @demand = V2Demand.find(params["demandid"].to_s)
    @demand.remote_map_photo_url = params["url"]
    if @demand.save
      render json: {status: params}.to_json
    else
      render json: {status: "Error !!!"}.to_json
    end
  end

  # def fetch_circles
  #   demand_id = params[:demand_id].to_s
  #   if demand_id
  #     @demand = Demand.find(demand_id)
  #   end
  #   # @customer = Customer.find(session[:bcs_customer_id].to_s)
  #   @circle = @demand.map_circles.last
  #   if @circle
  #     @attributes = {x: @circle.x, y: @circle.y, radius: @circle.radius, status: "YES", map_lng: @demand.localisation_lng, map_lat: @demand.localisation_lat, city: @demand.localisation}.to_json
  #     render json: @attributes
  #   else
  #     @attributes = {status: "NO"}.to_json
  #     render json: @attributes
  #   end
  # end


  def fetch_circles
    demand_id = params[:demand_id].to_s
    if demand_id
      @demand = Demand.find(demand_id)
    end
    @circles = @demand.map_circles
    if @circles != []
      @values = {}
      @circles.each_with_index do |circle,index|
        @values[index + 1] = {x: circle.x, y: circle.y, radius: circle.radius, status: "YES", map_lng: @demand.localisation_lng, map_lat: @demand.localisation_lat, city: @demand.localisation}
      end
      render json: @values.to_json
    else
      @attributes = {status: "NO", map_lng: @demand.localisation_lng, map_lat: @demand.localisation_lat, city: @demand.localisation}.to_json
      render json: @attributes
    end
  end


  # def fetch_multiple_circles
  #   demand_id = params[:demand_id].to_s
  #   if demand_id
  #     @demand = Demand.find(demand_id)
  #   end
  #   @circles = @demand.map_circles
  #   if @circles != []
  #     values = {}
  #     @circles.each_with_index do |circle|
  #       @values[index + 1] = {x: circle.x, y: circle.y, radius: circle.radius, status: "YES", map_lng: @demand.localisation_lng, map_lat: @demand.localisation_lat, city: @demand.localisation}
  #     end
  #     render json: @values.to_json
  #   else
  #     @attributes = {status: "NO", map_lng: @demand.localisation_lng, map_lat: @demand.localisation_lat, city: @demand.localisation}.to_json
  #     render json: @attributes
  #   end
  # end


  def fetch_multiple_circles
    @demand = Customer.find(session[:bcs_customer_id].to_s).demands.last
    @circles = @demand.map_circles
    @string_results = []
    @only_cities = []
    @circle_objects = {}
    if @demand.map_circles != []
      puts "********************"
      puts "#{@demand.map_circles.size} Cercles"
      puts "********************"
      @demand.map_circles.each_with_index do |circle,index|
        @circle_objects[index + 1] = {circle_id: "#{index + 1}", x: "#{circle.x}", y: "#{circle.y}", radius: "#{circle.radius}"}.to_json
        meters_in_km = (circle.radius.to_f / 1000).round(2)
        circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{circle.y}&lng=#{circle.x}&radius=#{meters_in_km}&username=agaloppe84").body

        @json = JSON.parse(circle_influence_zone.to_s)
        puts @json
        @json.each do |api_return|
          api_return.last.each do |element|
            distance = element["distance"]
            population = element["population"]
            cityname = element["name"]
            @string_results << "#{distance}(Km) - #{population}(Habitants) - #{cityname}"
            @only_cities << cityname
          end
        end

      end
      render json: @circle_objects.to_json
      puts @string_results if @string_results.size != 0
      puts @only_cities.uniq!
    else
      puts @string_results if @string_results.size != 0
      puts @only_cities.uniq!
      render json: {message: "No circle found !!!!!"}
    end
  end

  private

  def add_postal_codes_to_demand(demand_id,results)
    @demand = Demand.find(demand_id)
    @demand.postal_codes = results.to_a
    @demand.save
  end

  def new_version_add_locality_to_demand(demand_id, hashed_results)
    hashed_return = {}
    hashed_results.each do |item|
      city = item["locality"]
      zipcode = item["zipcode"]
      if !hashed_return.has_key?(city)
        hashed_return[city] = []
        hashed_return[city].push(zipcode)
      else
        hashed_return[city].push(zipcode)
      end
    end
    @full_formatted_localisation = []
    hashed_return.each do |key,value|
      if value.size == 1
        fullstring_array = "#{key} (#{value[0]})"
      elsif value.size == 2
        fullstring_array = "#{key} (#{value[0]} et #{value[-1]})"
      elsif value.size > 2
        fullstring_array = "#{key} (#{value[0..-2].join(',')} et #{value[-1]})"
      end
      @full_formatted_localisation << fullstring_array
    end
    if @full_formatted_localisation.size == 1
      @string_test = "#{@full_formatted_localisation.first}"
    else
      @string_test = "#{@full_formatted_localisation[0..-2].join(', ')} et #{@full_formatted_localisation.last}"
    end
    @demand = Demand.find(demand_id.to_s)
    @demand.localisation = @string_test
    @demand.save
  end

  def add_locality_to_demand(demand_id,locality)
    @demand = Demand.find(demand_id.to_s)
    @demand.localisation = locality.to_s
    @demand.save
  end

  def safe_params
    params.require(:map_circle).permit(:json)
  end

end
