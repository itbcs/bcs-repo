class Picture
  include Mongoid::Document
  include Mongoid::Timestamps
  mount_uploader :photo, ImageUploader
  embedded_in :property
end
