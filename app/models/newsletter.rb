class Newsletter
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :email, type: String

  validates_format_of :email, with: /\A[^@\s]+@[^@\s]+\z/

end
