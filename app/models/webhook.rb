class Webhook
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  field :service, type: String
  field :action, type: String
  field :content, type: Hash
  field :logo, type: String
  field :description, type: String

  def validate_aristid_customer_phone
    if Phonelib.valid?(self.content["customer"]["phone_number"])
      raw_phone = Phonelib.parse(self.content["customer"]["phone_number"])
      @formatted_phone = raw_phone.national
    else
      @formatted_phone = self.content["customer"]["phone_number"]
    end
  end

  def handle_aristid_initial_request
    results = {}
    results[:sender] = self.content["mandrill_events"].first["msg"]["headers"]["From"]
    results[:sending_at] = self.content["mandrill_events"].first["msg"]["headers"]["Date"]
    results[:receiver] = self.content["mandrill_events"].first["msg"]["headers"]["To"].split(",").select{|item| item.include?("@aristid.immo")}.flatten.last
    results[:subject] = self.content["mandrill_events"].first["msg"]["headers"]["Subject"]
    # results[:content] = self.content["mandrill_events"].first["msg"]["text"]
    if self.content["mandrill_events"].first["msg"]["headers"]["Reply-To"]
      if self.content["mandrill_events"].first["msg"]["headers"]["Reply-To"].include?("<")
        results[:reply_to] = self.content["mandrill_events"].first["msg"]["headers"]["Reply-To"].scan(/\<(.*)\>/).flatten.first
      else
        results[:reply_to] = self.content["mandrill_events"].first["msg"]["headers"]["Reply-To"].strip
      end
    else
      puts "There is no reply-to key in this hash !!"
    end
    return results
  end

  def malab_sanitize_infos
    if self.service.downcase.to_sym == :malab
      id = self.content["form_response"]["definition"]["id"]
      title = self.content["form_response"]["definition"]["title"]
      fields = self.content["form_response"]["definition"]["fields"]
      answers = self.content["form_response"]["answers"]
      puts answers.inspect
      @typeform_answers = []

      @typeform_answers << {label: "Typeform id", value: id}
      @typeform_answers << {label: "Title", value: title}

      answers.each_with_index do |answer,index|
        type = answer["type"]
        if type.downcase.to_sym == :text
          value = answer["text"]
          id = answer["field"]["id"]
          fields.each do |field|
            if field["id"] == id
              title = field["title"]
              @typeform_answers << {label: title, value: value, position: index+1}
            end
          end
        elsif type.downcase.to_sym == :email
          value = answer["email"]
          id = answer["field"]["id"]
          fields.each do |field|
            if field["id"] == id
              title = field["title"]
              @typeform_answers << {label: title, value: value, position: index+1}
            end
          end
        elsif type.downcase.to_sym == :choice
          value = answer["choice"]["label"]
          id = answer["field"]["id"]
          fields.each do |field|
            if field["id"] == id
              title = field["title"]
              @typeform_answers << {label: title, value: value, position: index+1}
            end
          end
        end
      end
      return @typeform_answers
    else
      return nil
    end
  end

end
