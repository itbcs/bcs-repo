class V2DemandComment
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  embedded_in :v2_demand
  validates_presence_of :author

  field :body, type: String
  field :author, type: String
  field :important, type: Boolean, default: false

end
