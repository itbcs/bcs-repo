# frozen_string_literal: true

# class Survey
class Survey
  include Mongoid::Document
  include Mongoid::Timestamps
  embeds_one :condition, cascade_callbacks: true
  embeds_many :answers, cascade_callbacks: true

  field :position, type: Integer
  field :answer_type, type: String
  field :label_demand, type: String
  field :resume, type: String
  field :text, type: String
  field :required, type: Boolean
  field :theme, type: String
  field :comment, type: String
  field :demand_attribute, type: String
  field :active, type: Boolean
  field :checkable_on_criteria, type: Boolean
  field :client_side_condition, type: Boolean
  field :property_type, type: String

end
