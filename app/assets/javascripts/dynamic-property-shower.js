var receiverContainer = $('.dynamic-property-content');
var localisationReceiver = $(receiverContainer).find(".localisation");
var areaReceiver = $(receiverContainer).find(".area");
var priceReceiver = $(receiverContainer).find(".price");
var titleReceiver = $(receiverContainer).find(".title");
var offertypeReceiver = $(receiverContainer).find(".offer_type");
var propertytypeReceiver = $(receiverContainer).find(".property_type");
var cityReceiver = $(receiverContainer).find(".city");
var zipcodeReceiver = $(receiverContainer).find(".zipcode");
var expositionReceiver = $(receiverContainer).find(".exposition");
var linkReceiver = $(receiverContainer).find(".link");



var values;

function handlePropertyShow() {
  $('.proposition-item').click(function(){
    $(linkReceiver).empty();
    values = $(this).data("values");
    console.log(values);
    $(localisationReceiver).text(values.localisation);
    $(areaReceiver).text(values.area + "m2");
    $(priceReceiver).text(values.price + "€");
    $(titleReceiver).text(values.title);
    $(offertypeReceiver).text(values.offer_type);
    $(propertytypeReceiver).text(values.property_type);
    $(cityReceiver).text(values.city);
    $(expositionReceiver).text(values.exposition);
    $(zipcodeReceiver).text(values.zipcode);
    var htmlLink = "<a class='bcs-button-very-small' href='/dashboard/customer/properties/" + values._id.$oid + "'>Voir ce bien</a>";
    $(htmlLink).appendTo(linkReceiver);
    $(receiverContainer).show();
  });
}




function ajaxPropertyShow() {
  $('.proposition-item').click(function() {
    values = $(this).data("values");
    $.ajax({
      method: "GET",
      url: "/dashboard/customer/properties/partial/" + values._id.$oid,
      success: function(data) {
        $(receiverContainer).html(data).hide().show("slide",{direction: "right"},1000);
      },
    })

  });
}

// handlePropertyShow();
// ajaxPropertyShow();
