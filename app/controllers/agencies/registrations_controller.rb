require "rest-client"

class Agencies::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]

  def new
    super
    @range = ("01000".."99000").to_a.map{|number| "#{number}0"}
    @results = []

    # @range.each do |number|
    #   begin
    #     response = RestClient.get("https://geo.api.gouv.fr/communes?codePostal=#{number}")
    #   rescue RestClient::BadGateway
    #     puts "Bad gateway - 502 Error"
    #   end
    #   puts "---------------"
    #   puts "Rest Client Api calls - French Zipcodes"
    #   puts response if response
    #   puts "---------------"
    #   if response
    #     @results << (JSON.parse(response.body.to_s)) if response.code == 200
    #   end
    # end
    # @filter = @results.flatten.map!{|result| result.to_h["codesPostaux"].first if result.to_h["codesPostaux"].first}
    # @filter = @filter.uniq!
    # puts @filter
    # @postal_code = PostalCode.new(values: @filter.to_a)
    # @postal_code.save

  end

  def create
    super
    @zipcodes = []
    @full_hashed_result = []
    if resource.catchment_area != []
      resource.catchment_area = resource.catchment_area.map{|zipcode| zipcode if zipcode != ""}.compact
      resource.save
      resource.catchment_area.each do |placeid|
        response = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?place_id=#{placeid}&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA")
        @result = (JSON.parse(response.body.to_s)) if response.code == 200
        puts @result["results"][0]["address_components"]

        @result["results"][0]["address_components"].each do |hashed_value|
          if check_string(hashed_value["short_name"])
            zipcode = hashed_value["short_name"]
            @zipcodes << zipcode
          end
        end
        # @zipcode = @result["results"][0]["address_components"].last["short_name"]
        # puts @zipcode
        # @zipcodes << @zipcode
      end
      sanitize_zipcodes = @zipcodes.reject{|zipcode| zipcode == "FR"}
      resource.catchment_area = sanitize_zipcodes.uniq
      good_min_price = params[:agency][:min_price].strip.gsub(/[[:space:]]/,'').to_i
      resource.min_price = good_min_price
      resource.save
    end
    if resource.persisted?
      AgencyMailer.agency_welcome(resource).deliver
      Rails.configuration.admin_emails.each do |email|
        AdminMailer.pending_agency_validation(email,resource).deliver
      end
    end
  end

  def edit
    super
  end

  def update
    super
  end

  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  def check_string(string)
    string.scan(/\D/).empty?
  end

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute, :name, :phone, :address, :location_cp, :city, :professional_card, :min_price, :lastname, :firstname, :personal_phone, catchment_area: []])
  end

  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  end

  def after_sign_up_path_for(resource)
    v2_agency_dashboard_path
  end

end
