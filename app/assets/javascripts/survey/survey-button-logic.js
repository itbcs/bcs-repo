function handleClientSideconditions(position) {
  var firstInput,lastInput,firstInputValue,lastInputValue,firstInteger,lastInteger,threshold,inputs,hint;

  $(".formatted-numeric-input").on('keyup keypress', function(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
      e.preventDefault();
      console.log(keyCode);
      return false;
    }
  });

    if (position != "18") {
      inputs = $(".input-container");
      firstInput = $(".input-container input[type=text]").first();
      lastInput = $(".input-container input[type=text]").last();
    }


    keyCodesArray = [8,16,48,49,50,51,52,53,54,55,56,57,96,97,98,99,100,101,102,103,104,105];
    specialCharactersArray = ["&","é",'"',"'","(","§","è","!","ç","à",")","-","^","$","ù","`","=","+",":","/",".",";","?"];
    var firstAreaInput = $("input#10-0");
    var lastAreaInput = $("input#10-1");
    var currentForm = "#survey-form-" + position;

    if (position === "11") {
      var areaAnswer = $("[data-answerposition='10']");
      var areaAnswerValue = $(areaAnswer).find(".survey-recap-flexbox").first().text();
      var onlyNumbersValue = areaAnswerValue.replace(/\D/g,'');
      var onlyNumbersValueInteger = parseInt(onlyNumbersValue.replace(/\s/g, ""),10);

      if (onlyNumbersValueInteger < 100) {
        var allInputsContainer = $(currentForm).find(".all-inputs-container");
        var eachInput = $(allInputsContainer).find(".input-container").slice(4,7);
        $(eachInput).each(function() {
          console.log("surface infèrieur à 100");
          $(this).hide();
        });
      } else if (onlyNumbersValueInteger > 100) {
        var allInputsContainer = $(currentForm).find(".all-inputs-container");
        var eachInput = $(allInputsContainer).find(".input-container").slice(0,2);
        $(eachInput).each(function() {
          console.log("surface superieur à 100");
          $(this).hide();
        });
      }

    }


    if (position === "18") {
      var areaAnswer = $("[data-answerposition='10']");
      var areaAnswerValue = $(areaAnswer).find(".survey-recap-flexbox").first().text();
      var onlyNumbersValue = areaAnswerValue.replace(/\D/g,'');
      var onlyNumbersValueInteger = parseInt(onlyNumbersValue.replace(/\s/g, ""),10);
      var mainSpaceAreaInput = $("input#18-0");
      var icon = "<div class='icon-exclamation-circle'></div>";
      var budgetWarningContainer = $(currentForm).find('.budget-warning');
      var mainSpaceHint = "<div class='small-input-hint-container'>" + icon + "<div class='small-input-hint'>" + "Votre espace principal ne peut pas dépasser " + onlyNumbersValueInteger.toLocaleString("fr-FR") + " m²" + "</div>" + "</div>";
      $(mainSpaceHint).appendTo(budgetWarningContainer);

      $(mainSpaceAreaInput).keyup(function(e) {
        var mainSpaceValue = $(mainSpaceAreaInput).val().replace(/\s+/, "");
        var mainSpaceIntegerValue = parseInt(mainSpaceValue.replace(/\s/g, ""),10);
        if ($(mainSpaceAreaInput).val() === "") {
          console.log("Valeur vide");
          $(".hidden-btn").last().hide();
        }
        if (mainSpaceIntegerValue >= onlyNumbersValueInteger) {
          $(".hidden-btn").last().hide();
        } else if (mainSpaceIntegerValue < onlyNumbersValueInteger) {
          console.log("Valeur correcte");
          $(".hidden-btn").last().show();
        }
      });
    }



    $(firstAreaInput).keyup(function(e) {
      if (keyCodesArray.includes(e.keyCode)) {
        if(specialCharactersArray.includes(e.key)) {
          console.log(e.key);
        } else {
          $(".hidden-btn").last().show();
          $(currentForm + " .small-input-hint-container").show();
        }
      } else {
        $(".hidden-btn").last().hide();
      }

      firstAreaInputValue = $(firstAreaInput).val().replace(/\s+/, "");
      firstInteger = parseInt(firstAreaInputValue.replace(/\s/g, ""),10);
      threshold = ((firstInteger / 10) * 3) + firstInteger;
      var icon = "<div class='icon-exclamation-circle'></div>";
      if ($(currentForm + " .small-input-hint").size() === 0) {
        hint = "<div class='small-input-hint-container'>" + icon + "<div class='small-input-hint'>" + "Surface idéale entre " + firstInteger.toLocaleString("fr-FR") + threshold.toLocaleString( "fr-FR" ) + "m²" + "</div>" + "</div>";
        var budgetWarningContainer = $(currentForm).find('.budget-warning');
        $(hint).appendTo($(budgetWarningContainer));
      } else {
        hint = "Surface idéale entre " + firstInteger.toLocaleString("fr-FR") + "m²" + " et " + threshold.toLocaleString( "fr-FR" ) + "m²";
        $(currentForm + " .small-input-hint").text(hint);
      }
      if ($(firstAreaInput).val() === "") {
        $(currentForm + " .small-input-hint-container").hide();
        $(".hidden-btn").last().hide();
      } else {
        // $(".small-input-hint").show();
        // $(".hidden-btn").last().show();
      }
    });



    $(lastAreaInput).keyup(function(e) {
      lastAreaInputValue = $(lastAreaInput).val().replace(/\s+/, "");
      lastInteger = parseInt(lastAreaInputValue.replace(/\s/g, ""),10);
      threshold = ((firstInteger / 10) * 3) + firstInteger;
      if (lastInteger > threshold){
        $(".hidden-btn").last().hide();
      } else if (lastInteger < firstInteger) {
        $(".hidden-btn").last().hide();
      } else {
        $(".hidden-btn").last().show();
      }
    });


    if (position != "18") {
      $(firstInput).keyup(function(e) {
        console.log("First input keyup event !!!!!");
        if (keyCodesArray.includes(e.keyCode)) {
          if(specialCharactersArray.includes(e.key)) {
            console.log(e.key);
          } else {
            $(".hidden-btn").last().show();
            $(".small-input-hint-container").show();
          }
        } else {
          $(".hidden-btn").last().hide();
        }
        firstInputValue = $(firstInput).val().replace(/\s+/, "");
        firstInteger = parseInt(firstInputValue.replace(/\s/g, ""),10);
        threshold = ((firstInteger / 10) * 3) + firstInteger;
        var icon = "<div class='icon-exclamation-circle'></div>";
        if ($(".small-input-hint").size() === 0) {
          hint = "<div class='small-input-hint-container'>" + icon + "<div class='small-input-hint'>" + "Avec ce budget idéal, nous vous conseillons un budget maximum de " + threshold.toLocaleString( "fr-FR" ) + "€" + "</div>" + "</div>";
          $(hint).appendTo($(".budget-warning"));
        } else {
          hint = "Avec ce budget idéal, nous vous conseillons un budget maximum de " + Math.round(threshold).toLocaleString( "fr-FR" ) + "€";
          $(".small-input-hint").text(hint);
        }
        if ($(firstInput).val() === "") {
          $(".small-input-hint-container").hide();
          $(".hidden-btn").last().hide();
        } else {
          // $(".small-input-hint").show();
          // $(".hidden-btn").last().show();
        }
      });
      $(lastInput).keyup(function() {
        lastInputValue = $(lastInput).val().replace(/\s+/, "");
        lastInteger = parseInt(lastInputValue.replace(/\s/g, ""),10);
        threshold = ((firstInteger / 10) * 3) + firstInteger;
        if (lastInteger > threshold){
          $(".hidden-btn").last().hide();
        } else if (lastInteger < firstInteger) {
          $(".hidden-btn").last().hide();
        } else {
          $(".hidden-btn").last().show();
        }
      });
    }

};


function dynamicButtonsLogic(innerHTML) {
  var condition = $(innerHTML).find('input[name="condition"]').val();
  var position = $(innerHTML).find('input[name=position]').val();
  if (String(condition) === "true") {
    handleClientSideconditions(position);
  } else {
    $(".input-container input[type=text]").keyup(function(e){
      if ( $(this).val() ) {
        $(".hidden-btn").show();
        console.log("Generic method trig");
      } else {
        $(".hidden-btn").hide();
      }
    });
  }


  $(".input-container input[type=radio]").change(function(){
    $(".hidden-btn").show();
  });

  $(".input-container input[type=checkbox]").change(function(){
    $(".hidden-btn").show();
  });

  $(".textarea-input-container textarea").keyup(function(){
    if ( $(this).val() ) {
      $(".hidden-btn").show();
    } else {
      $(".hidden-btn").hide();
    }
  });

}

dynamicButtonsLogic();




function disableButtonOnClick() {
  $('.bcs-button-sm').click(function(){
    $(this).prop("disabled", "true");
  });
}
