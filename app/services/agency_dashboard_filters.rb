class AgencyDashboardFilters

  def initialize(params,agency)
    @params = params
    @errors = []
    @results = []
    @agency = agency
  end

  def call
    @most_expensive_budget = @agency.v2_demands.sort_by{|demand| demand.budget}.first[:budget].last.to_s
    @dynamic_query = generate_params_query
    query(@dynamic_query, @most_expensive_budget)
    service_success
  end

  private

  def query(dynamic_query, max_agency_demand_budget)
    dynamic_query.each do |item|
      field = item[:field]
      value = item[:value]
      operator = item[:operator]
      format = item[:format]

      if @demands
        if item[:operator]
          if item[:date]
            @demands = @demands.where("#{field}" => {"$gte" => item[:start_value], "$lte" => item[:end_value]})
          elsif item[:agency_view]
            @views = @agency.demand_agency_views.where(view: true)

            @demands.each do |demand|
              demand.demand_agency_views.each do |view|
                puts view.inspect
                if @views.include?(view)
                  @demands << demand
                else
                  @demands.remove(demand)
                end
              end
            end
          else
            @demands = @demands.where("#{field}" => {"#{operator}" => value})
          end
        else
          @demands = @demands.where("#{field}" => value)
        end
      else
        if item[:operator]
          if item[:date]
            @demands = @agency.v2_demands.where("#{field}" => {"$gte" => item[:start_value], "$lte" => item[:end_value]})
          elsif item[:agency_view]
            @views = @agency.demand_agency_views.where(view: true)
            @demands = []

            @agency.v2_demands.each do |demand|
              demand.demand_agency_views.each do |view|
                puts view.inspect
                if @views.include?(view)
                  @demands << demand
                end
              end
            end

          else
            @demands = @agency.v2_demands.where("#{field}" => {"#{operator}" => value})
          end
        else
          @demands = @agency.v2_demands.where("#{field}" => value)
        end
      end


      puts "**** SERVICE DEMANDS COUNT ****"
      puts @demands.count
      @demands.each do |demand|
        puts "$$$$ #{demand[:budget]} $$$$"
        puts "$$$$ #{demand.search_formatted_area} $$$$"
      end
      puts "*******************************"
    end
  end

  def generate_params_query
    dynamic_query = []
    @params.each do |key,value|
      if key.to_sym == :budget_min
        dynamic_query << {field: "min_budget_int", value: value.to_i, operator: "$gte", format: "string", min_range: true}
      elsif key.to_sym == :budget_max
        dynamic_query << {field: "max_budget_int", value: value.to_i, operator: "$lte", format: "string", max_range: true}
      elsif key.to_sym == :surface_min
        dynamic_query << {field: "surface", value: value, operator: "$gte", format: "string"}
      elsif key.to_sym == :surface_max
        dynamic_query << {field: "surface", value: value, operator: "$lte", format: "string"}
      elsif key.to_sym == :rooms_min
        dynamic_query << {field: "nb_room", value: value, operator: "$gte", format: "integer"}
      elsif key.to_sym == :rooms_max
        dynamic_query << {field: "nb_room", value: value, operator: "$lte", format: "integer"}
      elsif key.to_sym == :property_type
        if value == "Une maison"
          dynamic_query << {field: "property_type", value: "21", operator: nil, format: "string"}
        elsif value == "Un appartement"
          dynamic_query << {field: "property_type", value: "22", operator: nil, format: "string"}
        elsif value == "Un terrain"
          dynamic_query << {field: "property_type", value: "23", operator: nil, format: "string"}
        end
      elsif key.to_sym == :localisation
        dynamic_query << {field: "search_formatted_area", value: value, operator: "$regex", format: "string"}
      elsif key.to_sym == :agency_view
        dynamic_query << {field: "demand_agency_view_ids", value: value, operator: "$in", format: "integer", agency_view: true}
      elsif key.to_sym == :demand_created_at
        if value.to_s == "7 derniers jours"
          dynamic_query << {field: "created_at", value: value, operator: "$in", format: "integer", start_value: DateTime.now - 7.days, end_value: DateTime.now, date: true}
        elsif value.to_s == "14 derniers jours"
          dynamic_query << {field: "created_at", value: value, operator: "$in", format: "integer", start_value: DateTime.now - 14.days, end_value: DateTime.now, date: true}
        elsif value.to_s == "30 derniers jours"
          dynamic_query << {field: "created_at", value: value, operator: "$in", format: "integer", start_value: DateTime.now - 21.days, end_value: DateTime.now, date: true}
        elsif value.to_s == "Depuis l'an dernier"
          dynamic_query << {field: "created_at", value: value, operator: "$in", format: "integer", start_value: DateTime.now - 1.year, end_value: DateTime.now, date: true}
        end
      end
    end
    puts "**** DYNAMIC PARAMS ****"
    puts dynamic_query
    puts "************************"
    dynamic_query
  end

  def service_success
    ServiceSuccess.new(@results)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
