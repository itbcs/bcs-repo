class PromoCode
  include Mongoid::Document

  field :code, type: String
  field :start_date, type: Date
  field :end_date, type: Date
  field :title, type: String
  field :content, type: String

  def self.codelist
    results = PromoCode.all.map{|pc| pc.code.to_s.upcase}
    return results
  end

end
