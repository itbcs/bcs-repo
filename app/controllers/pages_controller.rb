class PagesController < ApplicationController
  include ApplicationHelper
  require 'net/ftp'
  require 'nokogiri'
  require 'yaml'
  require "json"
  require "csv"
  before_action :authenticate_customer!, only: [:customer_zone]
  before_action :authenticate_agency!, only: [:agency_zone]


  def s3_migration_for_hektor_flux
    @props = create_landing_props
    @props.each do |prop|
      s3_to_cloudinary_from_strings_array(prop)
    end
    redirect_to root_path, notice: "Congrat !!!"
  end

  def s3_to_cloudinary_from_strings_array(property)
    @property = property
    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    @imgs_attrs.each do |field|
      if !@property.send("#{field}").nil?
        @present_imgs << field
      end
    end

    @present_imgs.each_with_index do |image_url,index|
      @url = @property.send("#{image_url}")
      puts "******** s3 url *********"
      puts @url
      puts "*************************"
      @property.send("remote_image_#{index + 1}_url=", "#{@url}")
    end
    @property.save(validate: false)
  end


  def s3_to_cloudinary_migration
    @bcs_properties = Agency.where(email: "laure@bientotchezsoi.com").last.properties

    @bcs_properties.each do |property|

      @property = property
      @present_imgs = []
      @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

      @imgs_attrs.each do |field|
        if !@property.send("#{field}").nil?
          @present_imgs << field
        end
      end
      @present_imgs.each_with_index do |image_url,index|
        @url = @property.send("#{image_url}")
        puts "******** s3 url *********"
        puts @url
        puts "*************************"
        @property.send("remote_image_#{index + 1}_url=", "#{@url}")
      end
      if @property.save
        puts "S3 Migration successfull for #{@property.id.to_s}"
      else
        puts "S3 Migration failed"
      end

    end

    redirect_to root_path, notice: "S3 migration"
  end


  def website_scrapper(url)
    @doc = Nokogiri::HTML(open("#{url}", :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE))
  end

  def hunt
    render :hunt
  end

  def faq
  end

  def test_unviewed_demands_mailing
    @demands = Demand.where(activated: true)
    render "test_unviewed_demands_mailing"
  end


  def glossary
    puts "**************************"
    puts "**************************"
    @alphabet = ('A'..'Z').to_a
    glossary_hash
    @page = Page.find_by(name: "dico-de-kenotte")
    render :glossary
  end


  def glossary_hash
    @hash = {
      A: {
        "AERAS" => "Acronyme de s’Assurer et Emprunter avec un Risque Aggravé de Santé, c’est un dispositif conventionnel appliqué automatiquement par l'ensemble des banques et des assureurs proposant une assurance. Il permet à une personne présentant ou ayant présenté un risque aggravé de santé d'obtenir à des conditions spécifiques un prêt immobilier qu'elle ne pourrait pas obtenir dans les conditions standards.",
        "Amiante" => "Terme désignant certains minéraux à texture fibreuse utilisés dans l’industrie. Il a attiré l'attention de certains industriels à la fin du XIXe siècle pour sa résistance à la chaleur, au feu, à la tension, aux agressions électriques et chimiques, ainsi que pour son pouvoir absorbant. L'amiante constitue aujourd’hui un problème majeur de santé publique et de santé au travail : massivement utilisé, il s’est révélé hautement toxique et le nombre de maladies graves qu'il a induit ne cesse d'augmenter. Interdit en France depuis 1997, il reste présent dans de nombreux bâtiments et équipements. Pour être retiré il nécessite l’intervention d’une entreprise spécialisée détentrice de la certification nationale et titulaire d'une assurance professionnelle : c’est ce qu’on appelle le désamiantage.",
        "Amortissement" => "Dans le langage financier, l'amortissement est l'opération consistant au remboursement en totalité ou en partie d'un crédit bancaire ou d'un emprunt obligataire.",
        "Article 1641" => "Le vendeur est tenu de la garantie des vices cachés à raison des défauts cachés de la chose vendue qui la rendent impropre à l'usage auquel on la destine, ou qui diminuent tellement cet usage que l'acheteur ne l'aurait pas acquise, ou n'en aurait donné qu'un moindre prix, s'il les avait connus. L'article détermine les contours de la garantie à la charge du vendeur.",
      },
      B: {
        "Banque" => "Etablissement financier qui, recevant des fonds du public, les emploie pour effectuer des opérations de crédit et des opérations financières, et est chargé de l'offre et de la gestion des moyens de paiement.",
        "Bouquet" => "Il s'agit du capital versé le jour de l'achat d'un bien en viager. Le reste de la valeur du bien prend la forme d'une rente.",
      },
      C: {
        "Cadastre" => "Ensemble des documents établis à la suite de relevés topographiques et des opérations administratives. Il est destiné à permettre la détermination des propriétés foncières d'un territoire, la constatation de la nature de leurs produits et l'évaluation de leur revenu. C’est aussi le nom de l’administration qui a la charge de ces documents.",
        "Carnet d'entretien" => "Tout immeuble en copropriété doit posséder un carnet d'entretien. Ce document répertorie des informations techniques sur la maintenance et les travaux effectués dans l'immeuble. Il doit être réalisé, tenu et mis à jour par le syndic de copropriété.",
        "CCMI" => "Le Contrat de Construction de Maison Individuelle a été créé afin de protéger les particuliers de certains constructeurs douteux. C’est le contrat le plus utilisé dans la construction de maison individuelle. Il est fortement recommandé d’en signer un afin d’obtenir les garanties encadrées par la réglementation française. Il est obligatoire lorsque le terrain n’appartient pas au constructeur et que celui-ci est en charge de la réalisation d’un ou deux logements, destinés au même acquéreur. Véritable protection pour les maitres d’ouvrage, le CCMI offre également des garanties de livraison, de prix et de délais.",
        "Certification de conformité" => "La construction d’un bien immobilier nécessite le dépôt et l’obtention d’un permis de construire auprès de la mairie. Les travaux doivent alors être conformes au permis de construire en question et respecter les règles d'urbanisme en vigueur. La mairie peut effectuer un contrôle et délivrer un certificat de conformité si aucune anomalie n'est détectée. Le certificat de conformité n’est pas systématiquement obligatoire, seuls certains travaux le nécessitent. C’est par exemple le cas lorsque ces derniers concernent un immeuble classé monument historique ou encore une construction située dans un secteur sauvegardé.",
        "Charges générales" => "Charges payées par la totalité des copropriétaires, sans exception, et sans intervention de la notion d'utilité : honoraires de syndic, assurance de l'immeuble, entretien des espaces verts…",
        "Charges récupérables" => "Aussi appelées charges locatives, ce sont des dépenses prises en charges initialement par le propriétaire mais que celui-ci peut se faire rembourser par le locataire. Elles sont réglementairement définies. Le paiement s'effectue soit par le versement de provisions pour charges avec une régularisation annuelle, soit par la récupération ponctuelle des dépenses engagées.",
        "Charges spéciales" => "Charges entraînées par les services collectifs et les éléments d'équipements communs (article 10 de la loi du 10 juillet 1965). Elles sont payées par les seuls copropriétaires qui ont l'utilité de ces services et équipements.",
        "Clause résolutoire" => "Clause d'un contrat prévoyant à l'avance sa résiliation automatique dans le cas où l'une des parties ne respecte pas une de ses obligations contractuelles. La partie qui a commis ce manquement ne peut alors pas contester cette résiliation devant les tribunaux. Celle-ci se retrouve fréquemment dans les contrats de bail : une clause peut par exemple prévoir la résiliation du celui-ci lorsque le locataire ne paye pas son loyer.",
        "Clauses suspensives" => "Ce sont les conditions qu’on peut retrouver dans les contrats qui précèdent la vente d'un logement. Les conditions suspensives suspendent les effets de l'avant-contrat jusqu'à l'arrivée d'un terme futur et incertain. Si ce dernier ne se réalise pas dans le délai convenu, la promesse devient nulle et les parties sont libérées de leurs obligations.",
        "Compte séquestre" => "C’est un compte bancaire qui est sous le contrôle d'une tierce partie. Il est souvent utilisé par les acheteurs et les vendeurs pour les transactions immobilières. Le vendeur prend le dépôt de l'acheteur et ouvre un compte séquestre auprès d'une banque, d’un agent fiduciaire ou d'une compagnie immobilière. Le dépositaire légal supervise la fermeture et s'assure que tout le monde est payé convenablement. Il est également possible de créer un compte séquestre si un propriétaire refuse de faire des réparations dans votre logement ou si vous voulez un compte dédié au paiement des factures non mensuelles.",
        "Conseil syndical de copropriété" => "Il est composé des membres élus de la copropriété. Il coordonne les relations entre le syndic et les copropriétaires et assure une mission consultative, d'assistance et de contrôle du syndic. Les membres du conseil syndical ne sont pas rémunérés et sont élus pour un mandat qui ne peut excéder trois ans renouvelables.",
        "Contrat de réservation" => "Lorsque le vendeur (ou le promoteur) et l'acquéreur sont parvenus à un accord sur une vente, ils peuvent signer un contrat de réservation avant la signature de l'acte de vente définitif. Ce document n'est pas obligatoire mais il est recommandé pour exprimer l'accord mutuel du vendeur et de l'acquéreur et déterminer les conditions précises dans lesquelles la vente du logement s'effectuera.",
        "Copropriété" => "C’est l'organisation d'un immeuble bâti, ou d’un groupe d'immeubles bâtis, dont la propriété est répartie entre plusieurs personnes, par lots comprenant chacun une partie privative et une quote-part des parties communes.",
        "Courtier" => "Le courtier est le professionnel exerçant l'activité de courtage. Son action consiste à servir d'intermédiaire pour une transaction entre un vendeur et un acheteur, dont il est à tout moment indépendant. La transaction peut porter sur toute opération d'achat ou de vente de marchandises ou de prestations de services.",
      },
      D: {
        "Délai de rétractation" => "Après la signature du compromis de vente, l'acheteur bénéficie d'un droit de rétractation de dix jours qu'il peut exercer sans avoir besoin de justifier d'un motif particulier, et ce, que la transaction soit conclue entre particuliers ou devant notaire. Cela lui permet de revenir sur sa décision sans aucune pénalité. Le vendeur, quant à lui, ne bénéficie pas de ce droit. Une fois le compromis signé il est engagé à l'égard de l'acheteur et ne peut plus changer d’avis.",
        "Dépôt de garantie" => "C’est une somme de caution pouvant être exigée par le propriétaire pour couvrir d'éventuels manquements du locataire (loyers ou charges impayés, réalisation des réparations locatives...). Le montant du dépôt de garantie, son versement et sa restitution sont encadrés par la loi.",
        "Démembrement de propriété" => "Il s'agit d'un montage patrimonial permettant de partager un bien avant de le transmettre. Plus complexe que l'indivision, il apporte des solutions qui peuvent correspondre davantage aux objectifs du propriétaire, notamment en matière de donation et d'assurance vie. Il s’applique quand la propriété d'un bien est divisée entre un (ou plusieurs) usufruitier(s) et un (ou plusieurs) nu-propriétaire(s).",
        "Donation" => "La donation est un acte par lequel une personne, le donateur, transmet de son vivant et gratuitement la propriété d'un bien à une autre personne, le donataire. Le donataire peut être n'importe quelle personne. La donation peut se faire de manière libre ou obligatoirement par acte notarié dans certains cas. Pour que celle-ci se réalise, le donataire doit l'accepter.",
        "DPE" => "Acronyme de Diagnostic de Performance Énergétique, il renseigne sur la performance énergétique d’un logement ou d’un bâtiment en évaluant sa consommation d’énergie et son impact en termes d’émissions de gaz à effet de serre.",
        "DPU" => "Acronyme de Droit de Préemption Urbain, le propriétaire d'un bien situé dans une zone définie par une collectivité en vue de la réalisation d'opérations d'aménagement urbain doit, en priorité, proposer la vente du bien à cette collectivité. C'est ce que l'on appelle le droit de préemption. Le propriétaire du bien n'est donc pas libre de vendre son bien à l'acquéreur de son choix.",
        "Droits de mutation" => "Communément appelés frais de notaire car ils sont payés au notaire par l'acquéreur, ils sont en fait simplement collectés par le notaire pour le compte du Trésor Public. Ces frais fiscaux s'appliquent à la quasi-totalité des mutations à titre onéreux portant sur des biens immobiliers, notamment à tous les achats ou ventes de logements.",
      },
      E: {
        "Emprise au sol" => "L'article R420-1 du Code de l'urbanisme définit l'emprise au sol comme une projection verticale du volume de la construction, tous débords et surplombs inclus. Seules les constructions et volumes dont la projection au sol est possible sont pris en compte dans l'emprise au sol. Elle comprend donc l'épaisseur des murs extérieurs mais aussi les matériaux isolants et les revêtements extérieurs.",
        "Etat des risques et pollutions" => "L'acquéreur ou le locataire d'un bien immobilier doit être informé par le vendeur ou le bailleur des risques et pollutions (naturels, miniers, technologiques, sismiques, radon...) auxquels ce bien est exposé. Un diagnostic contenant ces informations transmises par le préfet du département doit obligatoirement être annexé à la promesse de vente ou au bail.",
        "Être frappé d'alignement" => "Il est d'intérêt public d'adapter les voies de circulation aux normes et besoins en vigueur. Les modifications urbaines peuvent soudain rendre une voie publique très utile pour desservir un nouveau quartier ou pour fluidifier un trafic devenu dense. Lorsqu'il a été envisagé d'élargir une voie existante, il peut être nécessaire d'intégrer au domaine public des portions de terrains limitrophes afin d'aligner la limite des domaines public et privé.",
        "Etude notariale" => "L'étude notariale, également appelée office notarial, est la structure au sein de laquelle exerce le notaire.",
      },
      F: {
        "FAI" => "Un Fournisseur d'Accès à Internet est un organisme, plus généralement une entreprise mais parfois aussi une association, offrant une connexion à Internet, le réseau informatique mondial.",
        "Fonds de travaux" => "Les copropriétés doivent mettre en place un fonds de travaux pour anticiper les dépenses de travaux à venir. Celui-ci est alimenté par une cotisation annuelle obligatoire versée par chaque copropriétaire sur un compte spécifique préalablement ouvert par le syndic.",
        "Frais de négociation" => "Les notaires pratiquent parfois la négociation immobilière et assistent les vendeurs comme les acquéreurs dans la vente ou la recherche et l’achat de biens immobiliers. Pour cette activité d’intermédiaire ils perçoivent un honoraire particulier appelé frais de négociation. Il est fixé librement et en accord avec le mandant. Les frais de négociation notariale sont souvent moins chers que les frais des autres professionnels de l’immobilier.",
      },
      G: {
        "Garantie biennale" => "Pendant les deux années qui suivent la réception des travaux, vous bénéficiez de la garantie biennale. Cette garantie impose à l'entreprise qui a réalisé les travaux de réparer ou remplacer les éléments d'équipement qui ne fonctionnent pas correctement pendant les deux années qui suivent la réception des travaux. Il s'agit de tous les éléments d'équipement qui peuvent être enlevés sans dégrader le bâtiment comme le ballon d'eau chaude, les volets...",
        "Garantie décennale" => "Pendant les dix années qui suivent la réception des travaux, vous bénéficiez de la garantie décennale. Cette garantie impose à l'entreprise de réparer les dommages survenus au cours des dix années suivant la réception des travaux. Il s'agit des dommages qui compromettent la solidité du bâtiment (risques d'effondrement) ou rendent la construction impropre à sa destination (défaut d'étanchéité, fissures importantes…).",
        "Garantie de parfait achèvement" => "Pendant l'année qui suit la réception des travaux, vous bénéficiez d'une garantie de parfait achèvement. Celle-ci impose à l'entreprise qui a réalisé les travaux de réparer tous les désordres signalés au cours de l'année qui suit la réception des travaux, quels que soient leur importance et leur nature. Les éléments couverts par cette garantie sont notamment : les canalisations, les tuyauteries, les revêtements, les portes et fenêtres.",
      },
      H: {
        "Honoraires" => "Rétribution monétaire versée par leurs clients aux pratiquants de professions libérales : notaires, huissiers, avocats…",
        "Hypothèque" => "Dans le cas d'un crédit immobilier, l'hypothèque est une garantie que prend la banque lorsqu'elle accorde un crédit pour financer l'achat d'un bien immobilier. Elle porte sur un bien immobilier qui peut être un bien vous appartenant déjà ou le bien pour lequel la banque vous accorde le crédit. Si vous ne remboursez pas votre crédit dans les délais prévus par le contrat, la banque pourra obtenir la saisie du bien immobilier afin de se faire rembourser les sommes non payées.",
      },
      I: {
        "Immeuble" => "Se dit d'un bien fixe, d'un fonds de terre et de ce qui y est incorporé (immeuble par nature) ou d'un bien meuble que la loi soumet au régime juridique des immeubles (immeuble par détermination de la loi).",
        "Indivision" => "C’est un mécanisme juridique permettant d'exercer à plusieurs le droit de propriété en attente d'un partage. Un bien est dit indivis lorsqu'il appartient à un ensemble de personnes, sans que l'on puisse le répartir en lots entre elles ou qu'elles puissent vendre leurs parts sans l'accord des autres.",
        "IPPD" => "Inscription de Privilège de Prêteurs de Deniers, aussi appelé PPD, fait partie des trois types de garanties qui assurent à un organisme prêteur d'être remboursé dans le cas où vous ne pourriez plus régler vos mensualités. Son fonctionnement se rapproche de l'hypothèque. Il permet en effet à l'organisme de crédit de toucher directement l'argent de la vente du bien immobilier pour lequel vous avez fait une demande d'emprunt dans le cas où il devrait être saisi et vendu. Comme son nom l'indique, le Privilège de Prêteurs de Deniers donne à l'organisme de crédit un avantage sur vos éventuels autres créanciers, il est prioritaire sur les autres hypothèques et devra donc être indemnisé en priorité.",
        "IRA" => "Acronyme de Indemnités de Remboursement Anticipés, ces frais sont fixés à la signature du contrat de prêt et prévoient le montant des pénalités à régler en cas de remboursement anticipé, qu’il soit total ou partiel.",
      },
      J: {
        "Jouissance" => "Il s'agit du droit temporaire d'utiliser un bien immobilier dont une autre personne est propriétaire mais sans en percevoir les revenus ou le mettre en location.",
      },
      L: {
        "Loi Carrez" => "Loi française n°96-1107 du 18 décembre 1996 qui améliore la protection des acquéreurs de lots de copropriété. Elle impose au vendeur d'un lot de copropriété (ou d'une fraction de lot) d'en mentionner la superficie privative dans tous les documents relatifs à la vente. Elle ne s'applique pas dans le cas de l'achat sur plan, ni à l'achat de terrains à bâtir.",
        "Loi Boutin" => "La loi de mobilisation pour le logement et lutte contre l'exclusion du 25 mars 2009 introduit plusieurs mesures dans les rapports locatifs, de copropriété, des logements HLM… Elle comprend notamment l’interdiction de demander un dépôt de garantie pour les sociétés, l’envoie gratuit de la quittance au locataire, la mention de la surface du logement dans le bail d’habitation et la restitution du dépôt de garantie en cas de changement de propriétaire.",
        "Lot de copropriété" => "C’est une fraction d'un immeuble suite à sa division. Il est constitué des parties privatives et d'une quote-part des parties communes. Un logement situé en copropriété et sa quote-part de parties communes constituent donc un lot de copropriété et chaque lot est déterminé par l'état descriptif de division.",
      },
      M: {
        "Mandat exclusif" => "Il confie à un seul et unique agent immobilier le soin de trouver un acquéreur. Vous ne pouvez donc pas trouver un acheteur par vos propres soins ou conclure la transaction par l'intermédiaire d'un autre professionnel, quel qu'il soit. Il est limité dans le temps et il est logique que l’agence immobilière qui en bénéficie soit davantage enclin à investir son temps pour trouver un acheteur.",
        "Mandat simple" => "Il est possible de confier la vente de votre bien à plusieurs agences immobilières et vous réserver tout de même le droit de chercher vous-même un acheteur. Il s'agit alors d'un mandat simple. L'agent immobilier percevra sa commission si la vente est conclue avec l'acquéreur qu'il vous a présenté.",
        "Micro-foncier" => "Le régime micro-foncier repose sur les mêmes principes que le régime des micro-entreprise. En deçà d'un certain chiffre d'affaires, le contribuable est imposé selon un système ultra-simplifié de déclaration. Ce dernier s'applique automatiquement, sauf option contraire, aux bailleurs qui perçoivent des revenus fonciers inférieurs à un certain plafond",
        "Millième" => "Fraction d'une grande utilité notamment dans l'immobilier, elle permet de déterminer ce que chaque copropriétaire doit payer en termes de charge de copropriété. Les millièmes généraux englobent les millièmes associés à un lot d'appartement et la quote-part sur les parties communes. La répartition des millièmes est indiquée sur l'état descriptif de division, en annexe du règlement de la copropriété.",
        "Moins-values" => "Aussi appelée perte en capital, c’est la différence négative entre le prix d'achat et le prix de revente d'une valeur mobilière ou d'un bien immobilier."
      },
      N: {
        "Notaire" => "Juriste de droit privé et officier public. Il est nommé par l’autorité publique et est chargé d’exécuter les actes juridiques civils, dits actes notariés, de juridiction non-contentieuse pour lesquels la forme authentique est prescrite par la loi ou requise par les parties.",
        "Nue-propriété" => "C’est la propriété d'un bien sur lequel une autre personne a un droit d'usufruit."
      },
      P: {
        "PC" => "Permis de Construire, il est exigé dans un certain nombre de cas. Il concerne les travaux de construction de grande ampleur (construction d'une maison individuelle et/ou ses annexes). Il s'applique également à plusieurs autres cas (agrandissements, construction d'un abri de jardin...). La demande de permis de construire doit être adressée par lettre recommandée avec avis de réception ou déposée à la mairie.",
        "Plomb" => "Métal malléable et gris bleuâtre de numéro atomique 82, c’est le plus lourd des éléments stables. Le plomb est un contaminant de l'environnement, toxique et écotoxique même à faibles doses. Les maladies et symptômes qu'il provoque chez l'homme ou l'animal sont regroupés sous le nom de saturnisme. Les risques d'exposition au plomb que l'on peut rencontrer dans les parties privatives d'une habitation ainsi que dans les parties communes d'un immeuble sont évalués par un diagnostic plomb. Ce dernier est obligatoire lors de la vente d'un bien à usage d'habitation construit avant 1949, il doit être réalisé sur les parties privatives et communes.",
        "PLU" => "Le Plan Local d’Urbanisme est un document stratégique, il comporte une sorte de schéma directeur des orientations sur l'évolution de la ville sur une quinzaine d’années. Il est également un document réglementaire régissant l’évolution des parcelles, notamment à travers l'instruction des permis de construire et de démolir. C'est en quelque sorte un projet de ville accompagné des règles sur lesquelles se fondent les décisions publiques et privées en matière d'urbanisme.",
        "Plus-values" => "Aussi appelé gain en capital, c’est la différence positive entre le prix d'achat et le prix de revente d'une valeur mobilière ou d'un bien immobilier.",
        "Prêt bancaire" => "C’est le fait pour un établissement de crédit (la plupart du temps une banque) de mettre à disposition des fonds à un bénéficiaire sans en exiger le remboursement immédiat. Du point de vue du bénéficiaire le prêt désigne l'action de solliciter des fonds en vue d'une transaction importante, avec l'engagement de rembourser les sommes empruntées à plus ou moins long terme. Plusieurs caractéristiques sont à prendre en compte dans un prêt bancaire : la somme empruntée, la durée du prêt, le taux d'emprunt et les éventuels frais. Le prêt bancaire est la plupart du temps utilisé pour financer des dépenses importantes ou pour réaliser un investissement immobilier.",
        "Prêt in fine" => "Il a été élaboré pour optimiser les déductions fiscales des personnes souhaitant principalement réaliser des investissements locatifs mais aussi, dans une moindre mesure, acquérir une résidence secondaire. Pendant la durée du crédit, vous ne remboursez que les intérêts. Ainsi le capital emprunté reste intact et, à l’échéance, il est remboursé en une seule fois. Augmenter le montant total des intérêts présente un avantage fiscal destiné aux personnes fortement imposées et qui perçoivent déjà des revenus fonciers.",
        "Promesse de vente" => "Appelée aussi promesse unilatérale de vente, le vendeur s'engage auprès de l’acheteur potentiel à lui vendre son bien à un prix déterminé en lui donnant ainsi en exclusivité une option pour un temps limité de généralement deux à trois mois.",
        "PTZ" => "Le Prêt à Taux Zéro permet d'acheter sa future résidence principale. Pour y avoir droit, il ne faut pas avoir été propriétaire de son domicile durant les deux années précédant le prêt (sauf cas particuliers). Le plafond de ressources à respecter et le montant du PTZ accordé dépendent de la zone où se situe le futur logement. Il ne doit financer qu'une partie de l'opération à réaliser et il est possible de le compléter avec un ou plusieurs autres prêts.",
        "PV et AG" => "Procès Verbal et Assemblée Générale",
      },
      R: {
        "Régime matrimonial" => "Ensemble de règles juridiques destinées à organiser les rapports patrimoniaux entre, d'une part, les époux entre eux, et, d'autre part, entre les époux et les tiers. Il est défini au moment du mariage. Il n'existe pas un régime matrimonial unique : le couple qui compte se marier a le choix entre différents régimes pour organiser sa vie future. Faute de contrat de mariage, les époux sont soumis d'office au régime légal de la communauté légale réduite aux acquêts. Ils peuvent aussi s'adresser à un notaire afin d'adopter un autre régime matrimonial ou insérer des clauses spécifiques dans le cadre du régime légal.",
        "Remboursement anticipe" => "Ce sont des versements pour régler une partie ou la totalité des sommes empruntées qui n'ont pas encore été remboursées par des échéances que l'on appelle le capital restant dû. Les banques ne peuvent s’opposer à un remboursement anticipé total, quelle qu’en soit la somme. Des frais sont parfois appliqués, ils correspondent en partie au manque à gagner de la banque en ce qui concerne les intérêts qui ne seront pas perçus.",
        "Rente viagère" => "Elle est prévue dans le cadre d'un achat immobilier en viager, c’est une somme d'argent versée à une fréquence périodique par l'acheteur jusqu'au décès de l'occupant du logement en viager. Son montant et les modalités de son versement sont prévus dans l'acte authentique de vente.",
        "Réseau d'assainissement" => "Appelé aussi réseau unitaire, c’est un système de collecte des eaux usées où toutes les eaux (usées et pluviales) transitent par une seule et même canalisation et se mélangent.",
      },
      S: {
        "SCI" => "Une Société Civile Immobilière est un contrat de société par lequel au moins deux personnes décident de mettre en commun un ou plusieurs biens immobiliers afin d'en partager les bénéfices ou de profiter de l'économie qui pourrait en résulter, tout en s'engageant à contribuer aux pertes.",
        "Servitude" => "Telle que prévue aux articles 637 et suivants du Code civil, c’est une contrainte qui s'impose au propriétaire d'un bien (le fonds servant), au profit du propriétaire d'un autre bien (le fonds dominant). Il peut s'agir par exemple d'un droit de passage ou d'une servitude de vue. Les servitudes sont des droits réels immobiliers : elles sont attachées aux biens et non aux personnes. Dans cette mesure, une servitude se transmet de propriétaire en propriétaire et est inscrite dans l'acte de vente ou de donation du bien. Dans le cadre d'un achat immobilier, il est important de vérifier si une servitude est attachée à la propriété.",
        "Signature" => "Elle est apposée à la fin d'un document par une personne pour signifier son approbation de l'ensemble des informations contenues dans celui-ci et dont il n'est pas forcément l'auteur : c’est ainsi le cas des contrats ou de tout document commercial. Dans le cas d’une transaction immobilière, c’est un acte obligatoire pour les deux parties et validant l’authentification de la vente. La signature doit alors se faire devant un officier public : notaire ou huissier.",
        "Sous seing privé" => "Un acte sous seing privé désigne un écrit rédigé par des personnes privées afin de constater un acte ou un fait juridique. Il doit être distingué de l'acte authentique, aucun officier public, notaire ou huissier, n'intervenant dans la rédaction de l'acte. L'acte sous seing privé peut être rédigé par les parties mais également par un avocat.",
        "Subrogation" => "Institution en vertu de laquelle une personne (subrogation personnelle) ou une chose (subrogation réelle) est substituée à une autre dans un rapport juridique.",
        "Synallagmatique" => "En droit, un contrat synallagmatique (du grec ancien synallagma ou sunállagma signifiant mise en relation) est une convention par laquelle les parties s'obligent réciproquement l'une envers l'autre. On parle aussi de contrat bilatéral ou multilatéral.",
        "Syndic de copropriété" => "Une copropriété doit être dotée d'un syndic professionnel (personne physique exerçant en son nom propre ou une société) ou non professionnel (choisi parmi les copropriétaires), pour l'administrer et gérer ses finances.",
      },
      T: {
        "Tantième" => "Fraction d'une grande utilité notamment dans l'immobilier, elle permet de déterminer ce que chaque copropriétaire doit payer en termes de charge de copropriété. Les tantièmes généraux englobent les tantièmes associés à un lot d'appartement et la quote-part sur les parties communes. La répartition des tantièmes est indiquée sur l'état descriptif de division, en annexe du règlement de la copropriété.",
        "Taux d'endettement" => "C’est le pourcentage du rapport entre les charges financières mensuelles et le revenu disponible d’un emprunteur potentiel. Il permet de vérifier si vous pourrez faire face aux échéances de votre crédit immobilier. Généralement, une banque ou un organisme ne prête pas quand le taux d’endettement excède 33 %.",
        "Taxe d'aménagement" => "Instituée le 1er mars 2012, elle doit être versée à l'occasion de la construction, la reconstruction, l'agrandissement de bâtiments et aménagements de toute nature nécessitant une autorisation d'urbanisme. Elle est due par le bénéficiaire de l'autorisation de construire ou d'aménager.",
        "Taxe d'habitation" => "C’est un impôt s’appliquant à chaque personne (propriétaire, locataire ou occupant à titre gratuit) disposant d’un bien immobilier. Elle est payée par la personne ayant la disposition ou la jouissance à titre privatif des locaux imposables au 1er janvier de l'année d'imposition.",
        "Taxe foncière" => "C’est un impôt local dû tous les ans par les propriétaires d'un bien immobilier.",
        "TEOM" => "Taxe d'Enlèvement des Ordures Ménagères destinée à financer la collecte des déchets ménagers et assimilés.",
        "TRACFIN" => "Le Traitement du Renseignement et Action Contre les Circuits FINanciers clandestins est un organisme du ministère de l'Économie et des Finances, chargé de la lutte contre la fraude, le blanchiment d'argent et le financement du terrorisme.",
      },
      U: {
        "Usufruit" => "C’est le droit temporaire d'utiliser un bien dont une autre personne est propriétaire et d'en percevoir les revenus en le mettant en location.",
      },
      V: {
        "VEFA" => "L’achat en VÉFA ou Vente en État Futur d’Achèvement correspond au contrat qui vous lie au promoteur lorsque vous achetez un appartement neuf sur plan. Ce contrat vous garantit l’achèvement du programme immobilier pour lequel vous vous êtes engagé à acheter un logement neuf, et ce même en cas de défaillance du promoteur.",
        "Vente longue" => "Depuis loi Boutin, l'article L. 290-1 du Code de la construction et de l'habitation mentionne que Toute promesse de vente ayant pour objet la cession d'un immeuble ou d'un droit réel immobilier, dont la validité est supérieure à dix-huit mois, ou toute prorogation d'une telle promesse portant sa durée totale à plus de dix-huit mois est nulle et de nul effet si elle n'est pas constatée par un acte authentique, lorsqu'elle est consentie par une personne physique. En d'autres termes, dès lors qu'elle s'étend sur une durée de plus de dix-huit mois, une promesse de vente doit être constatée par acte notarié et ce depuis le 1er juillet 2009.",
        "Viabilisation" => "Ensemble des raccordements d'un terrain aux différents réseaux d'eau, d'électricité, de gaz, de téléphone et d'assainissement. En fonction de la situation du terrain, les travaux peuvent être plus ou moins importants.",
        "Viager" => "Un achat en viager consiste à vendre un logement à une personne en échange du versement mensuel d'une rente pendant toute la durée de vie du vendeur.",
        "Vices cachés" => "Défaut d’une chose tel qu’il la rend impropre à l’usage auquel elle est destinée, ou qui diminue tellement cet usage que l’acquéreur ne l’aurait pas achetée ou l’aurait achetée à moindre prix s’il en avait eu connaissance. Le vendeur est tenu de délivrer des produits exempts de vices cachés et, à défaut, doit garantir l’acheteur de ces défauts non apparents (article 1641 du Code civil).",
        "VIR" => "La Vente d’Immeuble à Rénover existe depuis le 13 juillet 2006. Ce dispositif a été institué dans le cadre de la loi ENL (Engagement National pour le Logement) pour permettre à une personne, qui vend un immeuble bâti, ou une partie de cet immeuble, de réaliser directement, ou indirectement, des travaux sur cet immeuble. Cette personne doit s’engager dans un délai fixé dans l’acte notarié signé entre elle et l’acquéreur à réaliser effectivement les travaux prévus. A ce titre, elle perçoit des sommes d’argent de l’acquéreur avant la livraison des travaux. De son côté, l’acquéreur bénéfice d’une meilleure protection puisque le vendeur lui transfère immédiatement la propriété du sol et des constructions à mesure de la réalisation de la rénovation.",
      },
      Z: {
        "Zone tendue" => " Elle regroupe au total 1 149 communes composant 28 agglomérations dans lesquelles l’offre de logements est insuffisante (difficultés d’accès au logement, niveaux élevés de loyers). La liste des villes en zones tendues est fixée par le décret n°2013-392 du 10 mai 2013 : Ajaccio, Annecy, Arles, Bastia, Bayonne, Beauvais, Bordeaux, Draguignan, Fréjus, Genève-Annemasse, Grenoble, La Rochelle, La Teste-de-Buch - Arcachon, Lille, Lyon, Marseille - Aix-en-Provence, Meaux, Menton - Monaco, Montpellier, Nantes, Nice, Paris, Saint-Nazaire, Sète, Strasbourg, Thonon-les-Bains, Toulon, Toulouse. Les principales conséquences sur le contrat de bail sont les suivantes :
                                - En cas de fixation du loyer lors d'une relocation (moins de dix-huit mois écoulés entre les deux baux) : le nouveau loyer ne doit pas, sauf exceptions, excéder le dernier loyer appliqué au précédent locataire.
                                - Dans ces zones, la durée du préavis pour le locataire est réduite à un mois, et ce quelle que soit la date de signature du contrat de location.
                                - Le propriétaire peut être assujetti à la taxe sur les logements vacants en cas de vacance volontaire du logement d'au moins un an."
      }
    }

    @hash
  end

  def sanitize_agency_areas
    @agency = Agency.where(email: "annegaelle@bientotchezsoi.com").last
    @zipcodes = []
    @full_hashed_result = []
    if @agency.catchment_area != []
      @agency.catchment_area = @agency.catchment_area.map{|zipcode| zipcode if zipcode != ""}.compact
      @agency.save
      @agency.catchment_area.each do |placeid|
        response = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?place_id=#{placeid}&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA")
        @result = (JSON.parse(response.body.to_s)) if response.code == 200
        puts @result["results"][0]["address_components"]

        @result["results"][0]["address_components"].each do |hashed_value|
          if check_string(hashed_value["short_name"])
            zipcode = hashed_value["short_name"]
            @zipcodes << zipcode
          end
        end
        # @zipcode = @result["results"][0]["address_components"].last["short_name"]
        # puts @zipcode
        # @zipcodes << @zipcode
      end
      sanitize_zipcodes = @zipcodes.reject{|zipcode| zipcode == "FR"}
      @agency.catchment_area = sanitize_zipcodes.uniq
      # good_min_price = params[:agency][:min_price].strip.gsub(/[[:space:]]/,'').to_i
      # @agency.min_price = good_min_price
      @agency.save(validate: false)
    end
  end


  def check_string(string)
    string.scan(/\D/).empty?
  end



  def create_landing_props
    @props = []
    props = ["5c0c391e4a14046399ea00c2", "5c0c391c4a14046399ea00b8", "5c0c34b34a14046399e9f254"]
    props.each do |prop|
      @prop = Property.find(prop)
      @props << @prop
    end
    @props
  end

  def create_uniq_new_prop
    @prop = Property.find("5c0c391e4a14046399ea00c2")
    if @prop.images_array != [] || @prop.images_array
      @prop.images_array.first(4).each_with_index do |image,index|
        if (index+1) == 1
          @prop.remote_img_1_url = image
        elsif (index+1) == 2
          @prop.remote_img_1_url = image
        elsif (index+1) == 3
          @prop.remote_img_1_url = image
        elsif (index+1) == 4
          @prop.remote_img_1_url = image
        end
      end
    end
    @prop.save(validate: false)
  end

  def partners
  end

  def employee_test
  end

  def partner_landing
    @demands = Demand.where(activated: true).to_a.last(4)
    render :partner_landing
  end

  def email_job_testing
    @agency = Agency.all.where(email: "aurelien@bientotchezsoi.com").last
    JobTestMailer.agency_demand_matching_notif(@agency).deliver
    render :email_job_testing
  end

  def create_employee_role
    EmployeeRole.create(title: "superadmin")
    EmployeeRole.create(title: "admin")
    EmployeeRole.create(title: "agent")
    EmployeeRole.create(title: "editor")
    puts "***********************"
    puts EmployeeRole.all.size
    puts "***********************"
  end


  def presse
    @corp_post = Post.all.select{|p| p.blog_category.name == "Corporate"}
    @press_releases = PressRelease.all
    @page = Page.find_by(name: "presse")
    render :presse
  end

  def list_rails_routes
    @routes = Rails.application.routes.named_routes.helper_names
    @routes.each do |route|
      puts route
    end
  end

  def home
    @test = "Home page"
    # hektor_ftp_parser
    # old_users_migration
    # old_agencies_migration
    # @migration_result = old_demands_migration
    render '/home/home', layout: 'home'
  end


  def create_old_customer_csv
    CSV.open("spread-old-customer-datas.csv", "wb") do |csv|
      csv << ["email", "name", "firstname", "optin", "address", "city", "mobile", "customer_web"]
      OldPlatformDemand.all.each do |demand|
        @user = demand.customer
        puts demand.inspect
        csv << [@user.email, @user.last_name, @user.first_name, 1, @user.address.remove(","), @user.city, @user.phone, 1]
      end
    end
  end

  def create_old_demand_csv
    CSV.open("spread-old-demand-datas.csv", "wb") do |csv|
      csv << ["email", "amount", "idorder", "date", "state", "name", "firstname", "action", "localisation", "postal_codes", "project", "actual_situation", "property_type", "destination", "budget", "old_platform"]
      OldPlatformDemand.all.each do |demand|
        @user = demand.customer
        puts demand.inspect
        csv << [@user.email, format_budget_spread_data(demand.budget), generate_spread_order_id(demand), l(demand.old_platform_created_at, format: :spread), "10", @user.last_name, @user.first_name, "order", demand.localisation, format_postal_codes_spread_data(demand.postal_codes), demand.project, demand.actual_situation, demand.property_type, demand.destination, format_full_budget_spread_data(demand.budget), 1]
      end
    end
  end


  def parrainage
    @sponsorship = Sponsorship.new()
    render :parrainage
  end

  def ambassador
    @ambassador = Ambassador.new()
    render :ambassador
  end

  def job_test
    # Resque.enqueue(DeliverMail)
    @test_customer = Customer.all.where(email: 'aurelien@bientotchezsoi.com').last
    JobTestMailer.welcome(@test_customer).deliver
  end

  def agency_infos_edit
    @agency = Agency.find(params[:agency_id].to_s)
    render "pages/agency_zone/agency_infos_edit"
  end


  def estimation_v2
    render :estimation_v2
  end

  def estimation_v3
    if params["localisation"]
      @localisation = params["localisation"]
    end
    @bcs_properties = Agency.where(email: "laure@bientotchezsoi.com").last.properties
    redirect_to new_estimation_path(localisation: @localisation)
  end


  def home_v2
    @posts = Post.includes(:blog_category).sort_by{|post| post.created_at}.last(2)
    @corp_post = BlogCategory.where(name: "Corporate").last.posts.last
    kf1 = KeyFact.where(number: "79").last
    kf2 = KeyFact.where(number: "160 H").last
    @keyfacts = [kf1,kf2]
    if session[:bcs_customer_id]
      session.delete(:bcs_customer_id)
    end
    @bcs_properties = Agency.where(email: "laure@bientotchezsoi.com").last.properties.reject{|prop| prop.id.to_s == "5ccc0fb3733e2f00075d8603"}
    @promocodes = PromoCode.all
    @page = Page.find_by(name: "home")
    @tempo_props = []
    @props = create_landing_props

    # website_scrapper("https://www.seloger.com/list.htm?tri=initial&idtypebien=2,1&idtt=2,5&naturebien=1,2,4&ci=330063")
    render "home/home-v2"
  end


  def enhanced_french_zipcodes
    values = {}
    CSV.foreach("excel_french_zipcodes.csv") do |row|
      city = row.first
      zipcode = row.last
      if !values.has_key?(zipcode)
        if zipcode.chars.size == 5
          values[zipcode] = city
        elsif zipcode.chars.size == 4
          new_zipcode = "0#{zipcode}"
          values[new_zipcode] = city
          puts "************************"
          puts "Reformat zipcode with first integer as 0"
          puts "#{new_zipcode}"
          puts "************************"
        end
      end
    end
    PostalCode.create(hashed_values: values)
  end


  def generate_customer_vip_reference
    customers_with_demand = Customer.select{|customer| customer.demands?}
    customers_with_demand.each do |customer|
      demand = customer.demands.last
      @ref_prop_type = handle_ref_property_type(demand.property_type)
      @ref_prop_zipcode = handle_ref_zipcode(demand.postal_codes)
      @ref_prop_destination = handle_ref_destination(demand.destination)
      @ref_customer_type = handle_customer_type(customer)
      @ref_current_year = Date.today.year.to_s.chars.last(2).join("").to_i
      exsisted_collection = DemandAutoincrementIndex.where(zipcode: @ref_prop_zipcode).where(current_year: @ref_current_year).where(customer_type: @ref_customer_type).where(destination_type: @ref_prop_destination).where(property_type: @ref_prop_type).last
      if exsisted_collection
        last_index = exsisted_collection.last_index
        new_index = last_index + 1
        @ref_index = handle_ref_index(new_index)
      else
        @ref_index = handle_ref_index(1)
        DemandAutoincrementIndex.new(property_type: @ref_prop_type, destination_type: @ref_prop_destination, customer_type: @ref_customer_type, last_index: 1, zipcode: @ref_prop_zipcode, current_year: @ref_current_year)
      end
      customer_reference = "#{@ref_customer_type} #{@ref_current_year} #{@ref_prop_type} #{@ref_prop_zipcode} #{@ref_prop_destination} #{@ref_index}"
      customer.customer_reference = customer_reference
      customer.references_collection = [customer_reference]
      customer.save
    end
  end


  def handle_ref_index(index)
    if index.to_s.chars.size == 1
      puts "Position size equal 1"
      collection_position = "000#{true_position}"
    elsif index.to_s.chars.size == 2
      puts "Position size equal 2"
      collection_position = "00#{true_position}"
    elsif index.to_s.chars.size == 3
      puts "Position size equal 3"
      collection_position = "0#{true_position}"
    elsif index.to_s.chars.size == 4
      puts "Position size equal 4"
      collection_position = "#{true_position}"
    end
    return collection_position
  end


  def handle_customer_type(customer)
    if customer.vip
      customer_type = 1
    else
      customer_type = 2
    end
    customer_type
  end


  def handle_ref_destination(destination)
    if destination == "Votre résidence principale"
      destination_type = "01"
    elsif destination == "Votre résidence secondaire"
      destination_type = "02"
    elsif destination == "Un investissement"
      destination_type = "03"
    end
    destination_type
  end

  def handle_ref_zipcode(zipcodes)
    if zipcodes != []
      first_zipcode = zipcodes.first.to_s.chars.first(2).join("").to_i
    end
    first_zipcode
  end


  def handle_ref_property_type(property_type)
    if property_type == "Une maison"
      prop_type = "01"
    elsif property_type == "Un appartement"
      prop_type = "02"
    elsif property_type == "Un terrain"
      prop_type = "03"
    end
    prop_type
  end







  def add_catchment_area_to_old_agencies
    old_agencies = Agency.where(source: "Old platform")
    old_agencies.each do |agency|
      postal_codes = agency.postal_codes
      agency.catchment_area = postal_codes
      agency.save
    end
  end


  def bcs_properties
    @properties = Agency.find_by(name: "Julatis").properties
    render :bcs_properties
  end

  def bcs_property_show
    @property = Property.find(params[:property_id])

    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]
    @imgs_attrs.each do |field|
      puts @property.send("#{field}")
      if !@property.send("#{field}").nil?
        @present_imgs << (field.to_s.remove("img_").to_i)
      end
    end
    @present_imgs

    render :bcs_property_show
  end


  def create_promo_codes
    PromoCode.destroy_all
    promo_code_1 = PromoCode.new(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "Leroy Merlin", content: "Réduction de 10% sur le mobilier de salle de bain")
    promo_code_2 = PromoCode.new(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "Castorama", content: "Réduction de 10% sur le rayon matériaux")
    promo_code_3 = PromoCode.new(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "Super Mano", content: "Réduction de 5€ dès 70€ d'achats")


    all_promo_codes = [promo_code_1, promo_code_2, promo_code_3]
    all_promo_codes.each do |promo_code|
      promo_code.code = generate_promo_code(promo_code.title)
      promo_code.save
    end

    pc1 = PromoCode.create(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "LBC2018", content: "A définir", code: "LBC2018")
    pc2 = PromoCode.create(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "VIP2018", content: "A définir", code: "VIP2018")
    pc3 = PromoCode.create(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "SLG2018", content: "A définir", code: "SLG2018")
    pc4 = PromoCode.create(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "BCSWG18", content: "A définir", code: "BCSWG18")
    pc5 = PromoCode.create(start_date: Date.parse("01-11-2018"), end_date: Date.parse("30-11-2018"), title: "VOISIN18", content: "A définir", code: "VOISIN18")

  end

  def generate_promo_code(code_title)
    if code_title.downcase.remove(" ").to_sym == :leroymerlin
      result = "CASTOR" + code_title.upcase.remove(" ").first(5).to_s + (rand(900)).to_s
    elsif code_title.downcase.remove(" ").to_sym == :castorama
      result = "CASTOR" + code_title.upcase.remove(" ").first(5).to_s + (rand(900)).to_s
    elsif code_title.downcase.remove(" ").to_sym == :supermano
      result = "CASTOR" + code_title.upcase.remove(" ").first(5).to_s + (rand(900)).to_s
    end
    puts "€€€€€€€€€€€€€€"
    puts result
    puts "€€€€€€€€€€€€€€"
    return result
  end


  def old_blog_post_migration
    file = YAML.load_file("mod522_posts.yml")
    file.each do |post|
      post_name = post["post_name"]
      post_status = post["post_status"]
      post_title = post["post_title"]
      post_date = post["post_date"]
      ruby_date = Date.parse(post_date)
      post_content = post["post_content"]
      if post_status == "publish"
        OldBlogPost.create(name: post_name, title: post_title, status: post_status, old_date: ruby_date, content: post_content)
      end
    end
  end

  def dicton_creation
    Proverb.destroy_all
    CSV.foreach("dicton.csv", encoding: 'ISO-8859-1') do |row|
      date = row.to_s.split(";").first.remove("[",'"',)
      sentence = row.to_s.split(";").last.remove('"',"]")
      new_date = Date.parse(date)
      Proverb.create(active_date: new_date, sentence: sentence)
    end
  end



  def create_heartstroke_properties
    HeartstrokeProperty.destroy_all
    @properties = Property.all.select{|prop| prop.price }.select{|prop| prop.price.to_i > 600000}.to_a.last(70)


    @last_properties = @properties.to_a.sample(3)
    @blacklist = ["_id", "agency_id", "demand_ids", "id_auto_increment", "insert_at", "dateEnr", "mandat_key", "dateMaj", "reference_client", "s_mandat_start_date", "s_mandat_end_date", "s_available_date", "ssss", "nb_waterrooms", "garden_area", "plain_foot", "department", "image_1", "image_2", "image_3", "image_4", "image_5", "image_6", "image_7", "image_8", "image_9", "image_10", "image_11", "image_12", "image_13", "image_14", "image_15", "image_16", "image_17", "image_18", "image_19", "", "field93", "field94", "field95", "field96", "field97", "field98", "field99", "field100", "field101", "field102", "field103", "field104", "plain_feet"]
    @last_properties.each do |prop|
      attributes = prop.attributes.reject{|attr| @blacklist.include?(attr) }
      @heartstroke_property = HeartstrokeProperty.create(attributes)
    end
  end

  def create_facts
    KeyFact.destroy_all
    KeyFact.create(number: "57", text: "C'est le nombre de maison vendu en gironde en janvier 2018", number_type: "quantity")
    KeyFact.create(number: "79", text: "C'est le pourcentage de ménages qui ont fait l'acquisition d'un logement", number_type: "percent")
    KeyFact.create(number: "986 000", text: "transactions enregistrées en France en 2017", number_type: "quantity")
    KeyFact.create(number: "2h49", text: "C'est notre record pour une visite d'un appartement de 40m²", number_type: "duration")
  end

  def test_agency
    @agencies = TestAgency.all
    render "home/test-agency", layout: 'home'
  end

  def search
    @title = "Je cherche"
    render :je_cherche
  end

  def sell
    @title = "Je vends"
    @prospect_property = ProspectProperty.new
    @filtered_posts = Post.all.reject{|p| p.blog_category.name == "Corporate"}
    @advices_posts = @filtered_posts.select{|p| p.blog_category.name == "Ventes immobilières Bientôt Chez Soi"}
    @bcs_properties = Agency.where(email: "laure@bientotchezsoi.com").last.properties
    render :je_vends
  end

  def construction
    @title = "Je construis"
    render :je_construis
  end

  def inform
    @title = "Je m'informe"
    render :je_m_informe
  end

  def contact
    @page = Page.find_by(name: "contact")
    @contact = Contact.new()
  end

  def sell_new_flat
  end

  def cgu_customer
  end

  def cgu_pro
  end

  def mentions_legales
  end

  def blog
    last_sell_post = Post.where(category_name: "achat").last
    last_buy_post = Post.where(category_name: "vente").last
    @current_sell_and_buy_posts = [last_sell_post, last_buy_post]
    render 'blog', layout: 'blog'
  end


  def restricted_demand_count
    render "pages/customer_zone/restricted_demand_count"
  end

  def active_demand
    @demand = Demand.find(params[:demand_id])
    render "surveys/active_demand"
  end

  def agency_zone
    @agency = current_agency
    @agency.demands.each do |demand|
      demand.properties.each do |prop|
      end
    end
    render :agency_zone
  end

  def agency_zone_create_property
    @agency = current_agency
    @property = Property.new
    @property.pictures.new
  end

  def agency_zone_edit_property
    @property = Property.find(params[:id])
    @agency = current_agency
  end

  def fees
    render :fees
  end

  def preview
    render :avant_premiere
  end

  def team
    @keyfact = KeyFact.new()
    render :team
  end

  def who_we_are
    render :who_we_are
  end

  def convention
    render :convention
  end

  def pippotron
    @pippotron = Pippotron.new().dicos
  end

  private


  def find_formatted_survey_label(demand)
    labels = []
    demand.attributes.each do |attr|
      survey = Survey.where(demand_attribute: attr.first.to_s).last
      (labels << survey.resume) if survey
    end
    labels
  end

  def test_on_label(demand)
    labels = []
    @demand_filter = demand.attributes.map{|attr| attr if (attr != nil || attr != "")}
    @demand_filter.each do |attribute|
      labels << attribute
    end
    labels
  end




  def old_demands_migration
    OldPlatformDemand.destroy_all
    @final_results = {}
    file = YAML.load_file("bcs_user_answers.yml")
    file.each_with_index do |hashed_values,index|
      @final_results["Demande-00#{index+1}"] = {}
      hashed_values.each do |key,value|
        key = key.to_sym
        if key == :id
        elsif key == :user_id
          @old_platform_user_id = value.to_i
        elsif key == :created_at
          @old_platform_created_at = Date.parse(value)
          puts "^^^^^^^^^^^^^"
          puts @old_platform_created_at
          puts "^^^^^^^^^^^^^"
        elsif key == :postal_codes
          if value.is_a?(Integer)
            @postal_codes = [value.to_s]
          elsif value.is_a?(String)
            @postal_codes = value.split("-")
          end
        elsif key == :idq_important
          @formatted_important_fields = []
          if value != ""
            @question_ids = JSON.parse(value).reject{|v| v == ""}.map{|w| w.to_i}
            @question_ids.each do |field|
              resume = OldPlatformQuestion.all.where(old_id: field).last.resume
              @formatted_important_fields << resume
            end
          else
            @formatted_important_fields = []
          end
        elsif key == :answers
          @test_result = handle_old_json_demand(JSON.parse(value), @postal_codes,@formatted_important_fields,@old_platform_user_id, @old_platform_created_at)
        end
      end
    end
  end


  def old_questions_migration
    file = YAML.load_file("bcs_questions.yml")
    file.each_with_index do |hashed_values,index|
      hashed_values.each do |key,value|
        key = key.to_sym
        if key == :id
          @id = value.to_i
        elsif key == :question
          @question = value.to_s
        elsif key == :resume
          @resume = value.to_s
        end
      end
      @attributes = {old_id: @id, question: @question, resume: @resume}
      @old_platform_question = OldPlatformQuestion.create(@attributes)
    end
  end

  def handle_old_json_demand(json_file,postal_codes,important_fields,old_user_id,old_platform_created_at)
    @results = {}
    @house_exteriors = []
    @flat_interiors = []
    @flat_exteriors = []
    @surface = []
    @budget = []
    @heating = []
    @dpe = []
    @travaux = []
    @style = []
    @exposition = []
    @principal_space = []
    @more_rooms = []
    @equipements = []
    @flat_state = []
    @parking = []
    @security = []
    @heart_stroke = []
    @accessibility = []
    @actual_housing_area = []
    @house_stairs = []
    @flat_stairs = []
    @actual_pros = []
    @house_construction_surface = []
    @house_budget = []
    json_file.each do |key,value|
      @question = value["question"]
      @question_id = value["id_question"]
      @answers = value["answers"]
      @answers.each do |key,value|
        @answer_id = key.to_i
        @answer_value = value
        @results[@answer_id] = @answer_value
        if ["1","2","3"].include?(key)
          @property_type = value
        end
        if ["4","5","6"].include?(key)
          @house_state = value.to_s.remove("*")
        end
        if ["8","9"].include?(key)
          @budget << value.to_s
        end
        if ["10","11"].include?(key)
          @surface << value.to_s
        end
        if ["12"].include?(key)
          if value != ""
            @localisation = value.to_s.split(",").first.remove("(",")","1","2","3","4","5","6","7","8","9","0").strip
          else
            @localisation = nil
          end
        end
        if ["13","14","15"].include?(key)
          @project = value
        end
        if ["16","17","18","19"].include?(key)
          @age = value
        end
        if ["20","21"].include?(key)
          @actual_situation = value
        end
        if ["22","23","24"].include?(key)
          @style << value
        end
        if ["26","27","28"].include?(key)
          @travaux << value
        end
        if ["29","30","31","32","33","34","35"].include?(key)
          @rooms = value.to_s
        end
        if ["36"].include?(key)
          @principal_space << value.to_s
        end
        if ["37","38","39"].include?(key)
          @kitchen = value.to_s
        end
        if ["45","46","47"].include?(key)
          @bathrooms = value.to_s
        end
        if ["48","49","50","51","52","53"].include?(key)
          @more_rooms << value
        end
        if ["54","55","56","57"].include?(key)
          @equipements << value
        end
        if ["58","59","60"].include?(key)
          @flat_stairs << value
        end
        if ["61","62"].include?(key)
          @accessibility << value
        end
        if ["63","64","65"].include?(key)
          @house_stairs << value
        end
        if ["67","68","69","70","120"].include?(key)
          @exposition << value
        end
        if ["72","73","74","75"].include?(key)
          @parking << value
        end
        if ["76","77","78"].include?(key)
          @security << value
        end
        if ["79","80","81","82","83","84","85","86","87"].include?(key)
          @heating << value
        end
        if ["88","89","90","91"].include?(key)
          @dpe << value
        end
        if ["92","93","94"].include?(key)
          @flat_exteriors << value
        end
        if ["95","96","97"].include?(key)
          @house_exteriors << value
        end
        if ["99","100"].include?(key)
          @actual_property = value
        end
        if ["101","102","103","104"].include?(key)
          @installation_date = value
        end
        if ["107"].include?(key)
          @actual_pros << value.to_s
        end
        if ["108"].include?(key)
          @actual_housing_area << value.to_s
        end
        if ["109"].include?(key)
          @heart_stroke << value.to_s
        end
        if ["117","118","119"].include?(key)
          @destination = value
        end
        if ["121","122","123"].include?(key)
          @flat_state << value.to_s
        end
        if ["125"].include?(key)
          @house_budget << value.to_s
        end
        if ["127","128"].include?(key)
          @house_construction_surface << value.to_s
        end
        if ["131","132"].include?(key)
          @contact_constructor = value.to_s
        end
      end
    end
    if @flat_state != []
      @formatted_flat_state = @flat_state.first
    else
      @formatted_flat_state = nil
    end
    @attributes = {old_platform_created_at: old_platform_created_at,old_user_id: old_user_id,contact_constructor: @contact_constructor,house_budget: @house_budget,house_construction_surface: @house_construction_surface,actual_pros: @actual_pros,flat_stairs: @flat_stairs,house_stairs: @house_stairs,bathrooms: @bathrooms,kitchen: @kitchen,actual_housing_area: @actual_housing_area,important_fields: important_fields,equipements: @equipements,postal_codes: postal_codes,dpe: @dpe,property_type: @property_type,situation: @situation,age: @age,style: @style,travaux: @travaux,actual_property: @actual_property,actual_situation: @actual_situation,project: @project,flat_exteriors: @flat_exteriors,house_exteriors: @house_exteriors,heating: @heating,more_rooms: @more_rooms,budget: @budget,surface: @surface, destination: @destination, installation_date: @installation_date, exposition: @exposition, rooms: @rooms, principal_space: @principal_space, localisation: @localisation, flat_state: @formatted_flat_state, parking: @parking, security: @security, heart_stroke: @heart_stroke, accessibility: @accessibility,house_state: @house_state}
    generate_new_demand_attributes(@attributes)
  end


  def generate_new_demand_attributes(attributes)
    @attributes = attributes
    old_demand_attributes = { localisation: @attributes[:localisation],
                          postal_codes: @attributes[:postal_codes],
                          perimeter: "",
                          project: @attributes[:project],
                          actual_situation: @attributes[:actual_situation],
                          property_type: @attributes[:property_type],
                          destination: @attributes[:destination],
                          flat_state: @attributes[:flat_state],
                          house_state: @attributes[:house_state],
                          travaux: @attributes[:travaux],
                          budget: @attributes[:budget],
                          flat_budget: [],
                          terrain_budget: [],
                          house_budget: @attributes[:house_budget],
                          style: @attributes[:style],
                          surface: @attributes[:surface],
                          terrain_surface: [],
                          house_construction_surface: @attributes[:house_construction_surface],
                          rooms: @attributes[:rooms],
                          floor: "",
                          contact_constructor: @attributes[:contact_constructor],
                          principal_space: @attributes[:principal_space],
                          kitchen: @attributes[:kitchen],
                          bathrooms: @attributes[:bathrooms],
                          more_rooms: @attributes[:more_rooms],
                          equipements: @attributes[:equipements],
                          accessibility: @attributes[:accessibility],
                          flat_stairs: @attributes[:flat_stairs],
                          house_stairs: @attributes[:house_stairs],
                          exposition: @attributes[:exposition],
                          parking: @attributes[:parking],
                          security: @attributes[:security],
                          heating: @attributes[:heating],
                          dpe: @attributes[:dpe],
                          flat_exterior: @attributes[:flat_exteriors],
                          house_exterior: @attributes[:house_exteriors],
                          age: @attributes[:age],
                          actual_pros: @attributes[:actual_pros],
                          actual_housing: @attributes[:actual_property],
                          actual_housing_area: @attributes[:actual_housing_area],
                          installation_date: @attributes[:installation_date],
                          last_precision: [],
                          heart_stroke: @attributes[:heart_stroke],
                          comments: "",
                          important_fields: @attributes[:important_fields],
                          completed: true,
                          sending_customer_notification: false,
                          fetch_map_photo: false,
                          old_platform_created_at: @attributes[:old_platform_created_at]
                        }
    @new_demand = OldPlatformDemand.new(old_demand_attributes)
    @old_user_id = @attributes[:old_user_id].to_i
    @linked_customer = Customer.all.where(last_platform_id: @old_user_id).last
    @new_demand.customer = @linked_customer
    if @new_demand.save
      puts "Save ok !!!!"
    else
      puts @new_demand.errors.full_messages
      puts "Errors !!!!"
    end
  end



  def old_users_migration
    OldPlatformUser.destroy_all
    file = YAML.load_file("bcs_users.yml")
    file.each do |hashed_values|
      hashed_values.each do |key,value|
        key = key.to_sym
        if key == :lastname
          @lastname = value
        elsif key == :firstname
          @firstname = value
        elsif key == :id
          @old_id = value.to_i
        elsif key == :email
          @email = value
        elsif key == :password
          @old_encrypted_password = value
        elsif key == :address
          @address = value
        elsif key == :city
          @city = value
        elsif key == :country
          @country = value
        elsif key == :postal_code
          @postal_code = value.to_s
        elsif key == :phone
          @phone = value.to_s
        end
      end
      OldPlatformUser.create(old_platform_id: @old_id, lastname: @lastname, firstname: @firstname, email: @email, address: @address, city: @city, country: @country, phone: @phone)
      # new_password = Devise.friendly_token.first(12)
      new_password = "bcs-user-protect"
      @customer_to_delete = Customer.all.where(last_platform_id: @old_id)

      if @customer_to_delete
        puts "Customer found"
        puts @customer_to_delete
        @customer_to_delete.destroy_all
      else
        puts "Customer not found"
      end

      @fresh_customer = Customer.new(password: new_password,password_confirmation: new_password,email: @email, first_name: @firstname, last_name: @lastname, city: @city, country: @country, postal_code: @postal_code, phone: @phone, last_platform_id: @old_id, address: @address)
      if @fresh_customer.save
        puts "Customer save"
      else
        puts @fresh_customer.errors.full_messages
        puts "Customer not save"
      end

    end
    # c.reset_password(new_password,new_password)
    # c.send_reset_password_instructions
  end



  def old_platform_partners_agencies_migration
    OldPlatformAgency.destroy_all
    file = YAML.load_file("bcs_user_professionals.yml")
    file.each do |hashed_values|
      hashed_values.each do |key,value|
        key = key.to_sym
        if key == :company
          @company = value.downcase.capitalize
        elsif key == :id
          @old_platform_id = value.to_i
        elsif key == :user_id
          @old_platform_user_id = value.to_i
        elsif key == :siret
          @siret = value.to_i
        elsif key == :professional_card
          @professional_card = value.to_s
        elsif key == :min_price
          @min_price = value.to_i
        elsif key == :insurer_name
          @insurer_name = value
        elsif key == :postal_codes
          if value.is_a?(String)
            @postal_codes = value.split(",")
          else
            @postal_codes = [value.to_s]
          end
        elsif key == :created_at
          @date_time = DateTime.parse(value)
          @created_at = @date_time
        elsif key == :updated_at
          @updated_date_time = DateTime.parse(value)
          @updated_at = value
        end
      end
      if @min_price == nil
        @min_price = 0
      end
      @agency = OldPlatformAgency.create(old_platform_id: @old_platform_id, old_platform_user_id: @old_platform_user_id, postal_codes: @postal_codes, created_at: @created_at, updated_at: @updated_date_time, company: @company, siret: @siret, insurer_name: @insurer_name, min_price: @min_price, pro_card: @professional_card)
    end
  end


  def create_new_agency_from_old
    Agency.all.where(partner: true).destroy_all
    old_agencies = OldPlatformAgency.all
    old_agencies.each do |old_agency|
      linked_old_user = OldPlatformUser.find_by(old_platform_id: old_agency.old_platform_user_id.to_i)
      if linked_old_user
        @email = linked_old_user.email
        @city = linked_old_user.city
        @phone = linked_old_user.phone
      end
      @name = old_agency.company
      @siret = old_agency.siret
      @min_price = old_agency.min_price
      @pro_card = old_agency.pro_card
      @insurer_name = old_agency.insurer_name
      @postal_codes = old_agency.postal_codes
      puts "***********************************"
      puts @postal_codes
      puts "***********************************"
      new_password = Devise.friendly_token.first(12)
      @new_agency = Agency.create(postal_codes: @postal_codes,password: new_password, password_confirmation: new_password,phone: @phone.to_s, professional_card: @pro_card.to_s, siret: @siret.to_s, name: @name.to_s, source: "Old platform", city: @city.to_s, insurer_name: @insurer_name.to_s, partner: true, email: @email.to_s, min_price: @min_price.to_i)
    end
  end


end
