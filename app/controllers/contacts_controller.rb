class ContactsController < ApplicationController

  def create
    @contact = Contact.new(safe_params)
    if verify_recaptcha(model: @contact) && @contact.save
      puts "recaptcha ok !!"
      if params[:contact][:hunt]
        redirect_to hunt_path, notice: "Merci :-) Votre message a été transmis à nos castors qui reviennent vers vous très vite."
      else
        redirect_to contact_path, notice: "Merci :-) Votre message a été transmis à nos castors qui reviennent vers vous très vite."
      end
    else
      if params[:contact][:hunt]
        render "pages/hunt", alert: "Oupss !! il y a des erreurs dans le formulaire"
      else
        render "pages/contact", alert: "Oupss !! il y a des erreurs dans le formulaire"
      end
    end
  end

  private

  def safe_params
    params.require(:contact).permit(:firstname, :lastname, :email, :phone, :choice, :comment, :hunt)
  end

end
