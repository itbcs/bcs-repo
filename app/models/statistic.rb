class Statistic
  include Mongoid::Document
  include Mongoid::Timestamps

  field :result, type: Hash

  def self.demands_stats
    demands_count = Demand.count
    @results = {}
    @demand_param_array = Demand.all.last.fields.keys.reject!{|key| key == "modifier_id"}
    @demand_param_dates_sanitize = Demand.all.last.fields.keys.reject!{|key| key == "modifier_id"}.reject!{|key| key == "created_at"}.reject!{|key| key == "updated_at"}.reject!{|key| key == "old_platform_created_at"}
    @demand_param_array.each do |param|
      nil_count = Demand.where(param.to_sym => nil).size
      if !@results.has_key?(param.to_sym)
        @results[param.to_sym] = []
        @results[param.to_sym].push(nil_count)
      end
    end
    @demand_param_dates_sanitize.each do |param|
      empty_count = Demand.where(param.to_sym => "").size
      @results[param.to_sym].insert(-1,(empty_count))
    end
    @statistic = Statistic.new(result: @results)
    if @statistic.save
      @statistic.percent
      puts "Stat successfully created !!!!"
    end
  end


  def percent
    @sanitize_results = {}
    @results = self.result
    @results.each do |key,value|
      nil_value = value.first
      empty_string_value = value.last
      nil_percent = 100 - ((nil_value * 100) / Demand.count)
      empty_string_percent = 100 - ((empty_string_value * 100) / Demand.count)
      @sanitize_results[key.to_sym] = ["#{nil_percent}%","#{empty_string_percent}%"]
    end
    self.result = @sanitize_results
    self.save
  end

  def custom_percent(number1,number2)
    if number1 != 0
      val = (number1 * 100) / number2
      val = 100 - val
      return "#{val} %"
    else
      return "0 %"
    end
  end


end
