class Customers::SessionsController < Devise::SessionsController
  before_action :configure_sign_in_params, only: [:create]
  after_action :after_login, only: [:create]

  def new
    super
  end

  def create
    super
  end

  def destroy
    super
  end

  protected

  def after_login
    Visit.create!(customer_id: current_customer.id, customer_ip: current_customer.current_sign_in_ip, customer_email: current_customer.email)
    puts "**** Enter into after login devise controller ****"
  end

  def configure_sign_in_params
    devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute, :cgu])
  end

  def after_sign_in_path_for(resource)
    # customer_zone_path
    v2_customer_dashboard_path
  end

end
