var firstInput = $('.first-input-for-validation');
var secondInput = $('.last-input-for-validation');

// function handleEmailValidation(selector) {
//   var input = $(selector).find(".email-input");

//   var regex = /^(\w|\d|\S)+@+(\w|\d|\S)+[.]+(\b(fr)|\b(com)|\b(org)|\b(net)|\b(io)|\b(tv)|\b(pro)|\b(biz)|\b(eu)|\b(info))$/i;

//   var button = $(".with-validation");
//   var goodMessage = $(selector).find(".good-email-message");
//   var badMessage = $(selector).find(".bad-email-message");

//   $(input).keyup(function(){
//     if ($(this).val().match(regex)) {
//       $(button).attr("disabled", false);
//       $(button).removeClass("disabled-button");
//       $(goodMessage).fadeIn(1000);
//       $(badMessage).fadeOut(500);
//     }
//     else {
//       $(button).attr("disabled", true);
//       $(button).addClass("disabled-button");
//       $(goodMessage).fadeOut(1000);
//       $(badMessage).fadeIn(500);
//     }
//   })
// }
// handleEmailValidation(firstInput);
// handleEmailValidation(secondInput);

function handleEmailValidation(selector1, selector2) {
  var input1 = $(selector1).find(".email-input");
  var input2 = $(selector2).find(".email-input");
  $(input2).attr("disabled", true);

  var regex = /^(\w|\d|\S)+@+(\w|\d|\S)+[.]+(\b(fr)|\b(com)|\b(org)|\b(net)|\b(io)|\b(tv)|\b(pro)|\b(biz)|\b(eu)|\b(info))$/i;

  var button = $(".with-validation");
  $(button).attr("disabled", true);
  $(button).addClass("disabled-button");

  var badMessage1 = $(selector1).find(".bad-email-message");
  var badMessage2 = $(selector2).find(".bad-email-message");

  var option = false;

  $(input1).keyup(function(){
    if ($(this).val().match(regex)) {
      $(input2).attr("disabled", false)
      $(badMessage1).fadeOut(500);
      option = true;

      $(input2).keyup(function(){
        if ($(this).val().match(regex) && (option)) {
          $(button).attr("disabled", false);
          $(button).removeClass("disabled-button");
          $(badMessage2).fadeOut(500);
        }
        else {
          $(button).attr("disabled", true);
          $(button).addClass("disabled-button");
          $(badMessage2).fadeIn(500);
        }
      })
    }

    else {
      $(button).attr("disabled", true);
      $(button).addClass("disabled-button");
      $(badMessage1).fadeIn(500);
      option = false
    }
  })
}
handleEmailValidation(firstInput, secondInput);
