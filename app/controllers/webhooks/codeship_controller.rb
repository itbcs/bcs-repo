class Webhooks::CodeshipController < ActionController::API
  include ApplicationHelper
  def build
    event = JSON.parse(request.body.read)
    puts "---- webhooks ----"
    puts "----- BUILDS -----"
    puts event.inspect
    @webhook = Webhook.new(service: "#{controller_name}", action: "#{action_name}", description: "Code deploy service", logo: "codeship", content: event.to_h)
    if @webhook.save
      puts "Webhook saved !!"
      ActionCable.server.broadcast 'webhooks',
        content: @webhook.content.as_json,
        service: "#{@webhook.service.downcase.capitalize}",
        description: "#{@webhook.description}",
        event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
        logo: svg(@webhook.logo),
        filter_target: "#{controller_name.downcase}",
        dynamic_content: [{label: "Status",value: "#{@webhook.content['build']['status']}",html_class: "success"}, {label: "App",value: "#{@webhook.content['build']['project_name']}",html_class: "neutral"}, {label: "Branch",value: "#{@webhook.content['build']['branch']}"}, {label: "Message",value: "#{@webhook.content['build']['message']}"}],
        custom_action: "#{@webhook.action.capitalize}"
    end
    puts "----------------------------"
    render :json => {status: 200}
  end

end
