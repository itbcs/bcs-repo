class Api::Agency::BaseController < ApplicationController
  before_action :authenticate_agency_request
  attr_reader :current_agency

  private

  def authenticate_agency_request
    @current_agency = AuthorizeAgencyApiRequest.call(request.headers).result
    render json: { error: 'Not Authorized' }, status: 401 unless @current_agency
  end

end
