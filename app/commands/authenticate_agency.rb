class AuthenticateAgency
  prepend SimpleCommand

  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    JsonWebToken.encode(agency_id: agency.id) if agency
  end

  private

  attr_accessor :email, :password

  def agency
    agency = Agency.find_by(email: email)
    return agency if agency && Agency.authenticate(email,password)
    errors.add :user_authentication, 'invalid credentials'
    nil
  end
end
