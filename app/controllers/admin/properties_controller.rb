class Admin::PropertiesController < ApplicationController
  http_basic_authenticate_with name: ENV["HTTP_AUTH_LOGIN"], password: ENV["HTTP_AUTH_PWD"]

  def index
    # @properties = Property.all.limit(300)
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all.limit(100)
    @sponsorships = Sponsorship.all
    @paginate_properties = Property.all.where(offer_type: "Vente").page params[:page]
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/properties/index", layout: "admin"
  end

  def show
    @property = Property.find(params[:property])
    handle_property_photos_v2(@property)
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/properties/show", layout: "admin"
  end


  def handle_property_photos property
    @images = []
    fields = [:image_0, :image_1, :image_2, :image_3, :image_4, :image_5, :image_6, :image_7, :image_8, :image_9, :image_10, :image_11, :image_12, :image_13, :image_14, :image_15, :image_16, :image_17, :image_18, :image_19]
    fields.each do |field|
      image = property[field]
      (@images << image) if !image.empty?
    end
    return @images
  end


  def handle_property_photos_v2 property
    @images = []
    fields = [:img_1, :img_2, :img_3, :img_4, :img_5, :img_6, :img_7, :img_8]
    fields.each do |field|
      image = property.send("#{field}")
      if image
        url = property.send("#{field}_url")
        @images << url
      end
    end
    return @images.compact!
  end


  def add_agency_key_into_property
    @ref_count = 0
    @agency_count = 0
    Property.all.each do |prop|
      reference = prop[:reference_client]
      if reference
        @ref_count += 1
        agency = Agency.all.where(reference: reference.to_s).last
        if agency
          if !prop.agency
            @agency_count += 1
            prop.agency = agency
            prop.save
          end
        else
        end
      end
    end
    @ref_count
    @agency_count
    render "admin/properties/script", layout: "admin"
  end

end
