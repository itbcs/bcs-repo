# frozen_string_literal: true

# class Bcs_Advice
class BcsAdvice
  include Mongoid::Document
  #has_and_belongs_to_many :customers

  field :desc, type:  String
  field :name, type:  String
  field :lname, type: String
  field :advice_type, type: String
  field :advice_url, type: String
  field :tags, type: Array
end
