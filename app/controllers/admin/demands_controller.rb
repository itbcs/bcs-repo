require 'rest-client'

class Admin::DemandsController < ApplicationController
  http_basic_authenticate_with name: ENV["HTTP_AUTH_LOGIN"], password: ENV["HTTP_AUTH_PWD"]

  def generate_xls_file
    @old_demands_laravel = V2Demand.where(from_oldplatform: false)
    @custom_created_at = nil
    if params[:collection]
      if params[:collection] == "activated"
        @demands = V2Demand.where(activated: true).includes(:customer)
        @filename = "activated-demands"
      elsif params[:collection] == "pending"
        @demands = @old_demands_laravel.where(status: "pending").includes(:customer).select{|demand| demand.customer.valid?}
        @filename = "pending-demands"
      elsif params[:collection] == "deactivated"
        @demands = V2Demand.where(deactivated: true).includes(:customer)
        @filename = "deactivated-demands"
      elsif params[:collection] == "refused"
        @demands = @old_demands_laravel.where(refused: true).includes(:customer)
        @filename = "refused-demands"
      elsif params[:collection] == "orphan"
        @demands = @old_demands_laravel.includes(:customer).all.select{|demand| !demand.customer.valid?}
        @filename = "orphans-demands"
      elsif params[:collection] == "oldplatform"
        @demands = V2Demand.where(from_oldplatform: true).where(status: "pending").includes(:customer)
        @filename = "oldplatform-demands"
        @custom_created_at = true
      end
    end
    respond_to do |format|
      format.xlsx {
        response.headers['Content-Disposition'] = "attachment; filename=#{@filename}.xlsx"
      }
    end
  end

  def index
    @old_demands_laravel = Demand.where(from_oldplatform: false)
    @validated_demands = Demand.where(status: "success").includes(:customer)
    @activated_demands = Demand.where(activated: true).includes(:customer)
    @refused_demands = Demand.where(from_oldplatform: false).where(refused: true).includes(:customer)
    @deactivated_demands = Demand.where(deactivated: true).includes(:customer)
    @orphan_demands = Demand.where(from_oldplatform: false).includes(:customer).page params[:page]
    @pending_demands = Demand.where(from_oldplatform: false).where(status: "pending").includes(:customer)
    @today_demands = Demand.where(created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).includes(:customer)
    @old_platform_demands = Demand.where(from_oldplatform: true).where(status: "pending")
    render "admin/demands/active", layout: "admin"
  end

  def orphan
    @orphan_demands = V2Demand.where(status: {'$eq': nil}).order_by(created_at: :desc).page params[:page]
    render "admin/demands/orphan", layout: "admin"
  end

  def active
    if params[:success]
      @matching_success = []
      params[:success].each do |prop|
        @matching_success << Property.find(prop)
      end
    elsif params[:failed]
      @matching_failed = params[:failed]
    end
    @activated_demands = V2Demand.where(activated: true).includes(:customer).order_by(created_at: :desc).page params[:page]
    render "admin/demands/active", layout: "admin"
  end

  def inactive
    @deactivated_demands = V2Demand.where(deactivated: true).includes(:customer).order_by(created_at: :desc).page params[:page]
    render "admin/demands/inactive", layout: "admin"
  end

  def old
    @old_platform_demands = Demand.where(from_oldplatform: true).where(status: "pending").order_by(old_platform_created_at: :desc).page params[:page]
    render "admin/demands/old", layout: "admin"
  end

  def pending
    @pending_demands = V2Demand.where(from_oldplatform: false).where(status: "pending").includes(:customer).order_by(created_at: :desc).reject{|de| de.customer == nil}.select{|de| de.customer.email != ""}
    render "admin/demands/pending", layout: "admin"
  end

  def refuse
    @refused_demands = V2Demand.where(from_oldplatform: false).where(refused: true).includes(:customer).order_by(created_at: :desc).page params[:page]
    render "admin/demands/refuse", layout: "admin"
  end


  def update
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @customers = Customer.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    @demand = V2Demand.find(params[:id].to_s)
    @demand.update_attributes(safe_params)
    @filtered_agencies = Agency.all.keep_if{|agency| agency.v2_demand_ids.include?(@demand.id)}
    if @demand.save
      puts "Success save"
    else
      puts "Failed save"
    end

    result = ValidateDemand.new(@demand).call
    agencies_ids = result.data if result.success?

    puts agencies_ids

    status = handle_results(@demand, agencies_ids.uniq)

    if status.nil?
      redirect_to admin_demand_path(@demand), layout: "admin", notice: "Demande modifiée - Néanmoins aucune agence n'a été trouvée !!"
    else
      redirect_to admin_demand_path(@demand), layout: "admin", notice: "Demande modifiée - Bravo nous avons trouvés #{agencies_ids.uniq.size} agences !!"
    end

  end

  def show
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @demand = V2Demand.find(params[:id].to_s)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    @cities = {}


    @demand_last_circle = @demand.v2_map_circles.last
    if @demand_last_circle
      latlng = "#{@demand_last_circle.y},#{@demand_last_circle.x}"
      url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlng + "&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA"
      result = RestClient.get(url, {accept: :json})
      data = ActiveSupport::JSON.decode(result)
      @circle_address = data["results"][1]["formatted_address"]
    end

    if @demand.properties != []
      @propositions = []
      @demand.properties.each do |prop|
        @prop = V2DemandProperty.where(property_id: prop.id.to_s, v2_demand_id: @demand.id.to_s).last
        @customer_choice = CustomerChoice.where(property: prop, v2_demand: @demand).last
        @propositions << {property: prop, proposed: @prop, choice: @customer_choice, agency: prop.agency}
      end
    else
      @propositions = nil
    end

    @cities
    @filtered_agencies = Agency.all.keep_if{|agency| agency.v2_demand_ids.include?(@demand.id)}
    render "admin/demands/show", layout: "admin"
  end

  def destroy
    @demand = Demand.find(params[:id].to_s)
    @demand.destroy
    redirect_to admin_active_demands_path, notice: "Demande effacée"
  end

  def validate_demand
    @demand = V2Demand.find(params[:demand_id])
    result = ValidateDemand.new(@demand).call
    agencies_ids = result.data if result.success?
    status = handle_results(@demand, agencies_ids.uniq)
    if status.nil?
      redirect_to "/admin/demands/active", layout: "admin", notice: "Demande activée - Néanmoins aucune agence n'a été trouvée !!"
    else
      result = AutomaticDemandPropertyMatching.new(@demand).call
      properties = result.data if result.success?

      puts "PROPERTIES MATCHING SIZE"
      puts properties.size

      if properties.size > 0
        @matching_success = properties
        redirect_to admin_active_demands_path(success: @matching_success), layout: "admin", notice: "Demande activée - Bravo nous avons trouvés #{agencies_ids.uniq.size} agences !!"
      else
        @matching_failed = true
        redirect_to admin_active_demands_path(failed: agencies_ids.uniq.size), layout: "admin", notice: "Demande activée - Bravo nous avons trouvés #{agencies_ids.uniq.size} agences !!"
      end
    end
  end

  def deactivated_with_message
    current_id = params[:demand_id]
    @demand = V2Demand.find(current_id)
    render "admin/demands/deactivated-edit", layout: false
  end


  def deactivate_demand
    @demand = V2Demand.find(params[:demand_id])
    @demand.activated = false
    @demand.deactivated = true
    @demand.status = "pending"
    @associated_agencies = Agency.all.select{|agency| agency.demands.include?(@demand)}
    @associated_agencies.each do |agency|
      agency.demands.delete(@demand)
    end
    if @demand.save
      redirect_to "/admin/demands/active", layout: "admin", notice: "Demande désactivée"
    else
      redirect_to "/admin/demands/active", layout: "admin", alert: "Un problème est survenu"
    end
  end

  def deactivate_demand_with_message
    reason = params[:v2_demand][:deactivation_reason]
    comment = params[:v2_demand][:deactivation_comment]
    @demand = V2Demand.find(params[:v2_demand][:id])
    @demand.deactivation_reason = reason
    @demand.deactivation_comment = comment
    @demand.activated = false
    @demand.deactivated = true
    @demand.status = "deactivated"
    @associated_agencies = Agency.all.select{|agency| agency.demands.include?(@demand)}

    @associated_agencies.each do |agency|
      agency.demands.delete(@demand)
    end

    if @demand.save
      redirect_to "/admin/demands/active", layout: "admin", notice: "Demande désactivée"
    else
      redirect_to "/admin/demands/active", layout: "admin", alert: "Un problème est survenu"
    end
  end

  def refused_demand
    @demand = V2Demand.find(params[:demand_id])
    @demand.refused = true
    @demand.status = "refused"
    if @demand.save
      redirect_to "/admin/demands/active", layout: "admin", notice: "Demande refusée"
    else
      redirect_to "/admin/demands/active", layout: "admin", alert: "Un problème est survenu"
    end
  end

  def multiple_destroy
    @demands_to_delete_size = params[:demand_ids].size
    if params[:demand_ids]
      params[:demand_ids].each do |id|
        demand = Demand.find(id.to_s)
        demand.destroy
      end
    end
    redirect_to admin_active_demands_path, notice: "#{@demands_to_delete_size} Demandes éffacées !!!"
  end

  private

  def handle_results(demand, agencies_ids)
    unless agencies_ids.blank?
      demand.status = "success"
      demand.activated = true
      demand.deactivated = false
      demand.refused = false
      demand.activation_date = DateTime.now
      demand.save
      agencies_ids.each do |agency_id|
        agency = Agency.find(agency_id.to_s)
        agency.v2_demands.push(demand)
        AgencyMailer.agency_demand_matching_notif(agency).deliver
      end
    else
      demand.status = "success"
      demand.activated = true
      demand.deactivated = false
      demand.refused = false
      demand.save
      return nil
    end
  end

  def safe_params
    params.require(:demand).permit(budget:[], surface:[], postal_codes:[], search_zipcodes_area:[])
  end


end
