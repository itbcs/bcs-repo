class PropertiesController < ApplicationController

  def change_status
    @property = Property.find(params[:id])
    result = ChangePropertyStatus.new(@property).call
    if result.success?
      puts result.data
      puts "**** Result status ****"
      puts result.data.status
      puts "***********************"
    end
    redirect_to v2_dashboard_agency_properties_path
  end

  def create
    puts "**** AREA VALUE ****"
    puts params[:property][:area]
    puts "********************"
    @locality = params[:property][:locality]
    @zipcode = params[:property][:geoloc_zipcode]
    @area = params[:property][:area].to_s.gsub(/[[:space:]]/,'').to_i

    sanitize_params = safe_params.reject{|param| param == "pictures_attributes"}
    @property = Property.new(sanitize_params)
    pictures_safe_params = safe_params[:pictures_attributes]

    if pictures_safe_params
      pictures_safe_params.each do |param|
        @property.pictures.build(param)
      end
    end

    if @area
      @property.area = @area
    end

    if @zipcode == ""
      puts "**** GEOCODING WITH JUST A CITY ****"
      result = GeocodingWithoutZipcode.new(@property).call
      if result.success?
        puts "**** SERVICE RETURN ****"
        puts result.data
        puts "************************"
      end
    else
      @property.zipcode = @zipcode.to_s.strip
    end

    if @locality
      puts "**** LOCALITY PRESENT ****"
      puts @locality
      puts "**************************"
      @property.localisation = @locality.to_s.strip
      puts "**** PROPERTY INSPECT ****"
      puts @property.inspect
      puts "**************************"
    end

    if @property.save

      result = AutomaticPropertyCreationMatching.new(@property).call
      @matching_demands = result.data if result.success?


      empty_images = @property.pictures.select{|pict| pict.photo_url == nil}
      empty_images.each do |img|
        img.destroy
      end
      if @matching_demands != []
        @matching_demands.each do |demand|
          instance = V2Demand.find(demand)
          if instance
            CustomerMailer.customer_demand_matching(instance.customer).deliver
          end
        end
        Rails.configuration.admin_emails.each do |email|
          @matching_demands.each do |demand|
            instance = V2Demand.find(demand)
            if instance
              AdminMailer.admin_agency_demand_suggestion(current_agency,instance.customer,instance,email,"test").deliver
            end
          end
        end
        # redirect_to dashboard_agency_properties_path(demands: @matching_demands), notice: "#{@property.title} crée avec succès !"
        # render :json => {url: dashboard_agency_properties_path(demands: @matching_demands)}
        render :json => {url: v2_dashboard_agency_properties_path(demands: @matching_demands)}
      else
        # render :json => {url: dashboard_agency_properties_path(empty: @property)}
        render :json => {url: v2_dashboard_agency_properties_path(empty: @property)}
        # redirect_to dashboard_agency_properties_path(empty: @property), notice: "#{@property.title} crée avec succès !"
      end
    else
      # render "pages/agency_zone_create_property", alert: "Réglez les problèmes ci-dessus"
      render :json => {errors: @property.errors}
    end
  end

  def update
    sanitize_params = safe_params.reject{|param| param == "pictures_attributes"}
    @property = Property.find(params[:id])
    pictures_safe_params = safe_params[:pictures_attributes]

    if pictures_safe_params
      pictures_safe_params.each do |param|
        @property.pictures.build(param)
      end
    end

    @property.update(sanitize_params)
    empty_images = @property.pictures.select{|pict| pict.photo_url == nil}

    empty_images.each do |img|
      img.destroy
    end

    if @property.save
      redirect_to v2_dashboard_agency_properties_path, notice: "#{@property.title} mis à jour !!!"
    else
      redirect_to dashboard_agency_property_update_path(property_id: @property.id.to_s)
    end
  end

  def property_proposition
    @agency = Agency.find(params[:agency_id].to_s)
    @demand = Demand.find(params[:demand_id].to_s)
    @demand_properties = @demand.properties
    @already_proposed_properties = []

    @demand_properties.each do |prop|
      (@already_proposed_properties << prop) if @agency.properties.include?(prop)
    end

    @customer = @demand.customer
    @properties = @agency.properties
    @filtered_properties = @properties.reject{|prop| @already_proposed_properties.include?(prop)}
    render :property_proposition
  end

  def add_property_to_demand
    @prop_values = []
    if params[:property_id]
      @demand = V2Demand.find(params[:demand_id])
      params[:property_id].each do |prop_id|
        @property = Property.find(prop_id.to_s)
        @demand.properties.push(@property)
        V2DemandProperty.create(v2_demand: @demand, property_id: prop_id.to_s)
      end
      if @demand.save

        if @demand.properties != []
          @demand.properties.each do |prop|
            if prop.price
              good_price = prop.price
            elsif prop.prix
              good_price = prop.prix
            end

            if prop.titre
              good_title = prop.titre
            elsif prop.title
              good_title = prop.title
            end
            @prop_values << "#{prop.property_type} à #{good_price}"
          end
        end

        # CustomerMailer.customer_demand_matching(@demand.customer).deliver
        # params[:property_id].each do |prop_id|
        #   DemandProperty.create(demand_id: @demand.id.to_s, property_id: prop_id.to_s)
        # end
        Rails.configuration.admin_emails.each do |email|
          AdminMailer.admin_agency_demand_suggestion(current_agency,@demand.customer,@demand,email,@prop_values.join(" - ")).deliver
        end
        redirect_to dashboard_agency_demand_path(demand_id: @demand.id.to_s), notice: "Vos biens ont été envoyés au membre qui à effectué cette demande"
      else
        redirect_to dashboard_agency_demand_path(demand_id: @demand.id.to_s), alert: "Une erreur est survenue"
      end
    end
  end

  def ajax_photo_upload
    puts params
    puts params['picture_attributes']
    render json: {status: "success"}
  end

  private

  def safe_params
    params.require(:property).permit(:id, :fees_target, :agency_id, :localisation, :area, :price, :desc, :status, :title, :offer_type, :fees, :corps, :available_date, :property_type, :ground_space, :nb_room, :nb_sleeping_room, :exposition, :build_year, :country, :dpe, :ges, :city, :zipcode, :pdf_cache, :pdf, :geoloc_zipcode, :geoloc_address, :locality, :lng, :lat, :img_1, :img_2, :img_3, :img_3, :img_4, :img_5, :img_6, :img_7, :img_8, :img_1_cache, :img_2_cache, :img_3_cache, :img_4_cache, :img_5_cache, :img_6_cache, :img_7_cache, :img_8_cache, :property_house_type, :terrain_area, :property_flat_type, :state, :secteurs, pictures_attributes: [:photo_cache, :photo])
  end
end
