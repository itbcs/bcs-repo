class TestMailer < BaseMandrillMailer
  def welcome(customer)
    # customer = Customer.find(user_id)
    subject = "Bienvenue chez vous : votre recherche est active !"
    merge_vars = {
      "FIRST_NAME" => customer.first_name,
      "SIGN_IN_URL" => "https://bientotchezsoi.com/customers/connexion",
      "USER_URL" => "Test",
    }
    body = mandrill_template("[TRC] welcome_email", merge_vars)

    send_mail(customer.email, subject, body)
  end
end
