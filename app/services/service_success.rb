class ServiceSuccess
  def initialize data
    @data = data
  end

  def success?
    true
  end

  def data
    @data
  end
end
