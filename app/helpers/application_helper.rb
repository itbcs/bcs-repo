module ApplicationHelper
  include ActionView::Helpers::NumberHelper

  def dynamic_logo_for_blog_category(category)
    puts category.parameterize
    if category.parameterize == "conseils-deco"
      logo = "fireworks"
    elsif category.parameterize == "achat"
      logo = "customer-love"
    elsif category.parameterize == "transaction"
      logo = "case"
    elsif category.parameterize == "top5"
      logo = "clock"
    else
      logo = "clock"
    end
    logo
  end

  def dynamic_color_from_category(category)
    puts category.parameterize
    if category.parameterize == "conseils-deco"
      color = "rgb(245,245,245)"
    elsif category.parameterize == "achat"
      color = "rgb(235,235,235)"
    elsif category.parameterize == "transaction"
      color = "rgb(225,225,225)"
    elsif category.parameterize == "top5"
      color = "rgb(215,215,215)"
    end
    color
  end

  def compare_two_hashes(hash1, hash2)
    (hash1.keys | hash2.keys).select{ |key| hash1[key] != hash2[key] }
  end

  def handle_customer_rating(rating)
    if rating.to_i == 1
      result = [{index: 1, active: true},{index: 2, active: false},{index: 3, active: false},{index: 4, active: false},{index: 5, active: false}]
    elsif rating.to_i == 2
      result = [{index: 1, active: true},{index: 2, active: true},{index: 3, active: false},{index: 4, active: false},{index: 5, active: false}]
    elsif rating.to_i == 3
      result = [{index: 1, active: true},{index: 2, active: true},{index: 3, active: true},{index: 4, active: false},{index: 5, active: false}]
    elsif rating.to_i == 4
      result = [{index: 1, active: true},{index: 2, active: true},{index: 3, active: true},{index: 4, active: true},{index: 5, active: false}]
    elsif rating.to_i == 5
      result = [{index: 1, active: true},{index: 2, active: true},{index: 3, active: true},{index: 4, active: true},{index: 5, active: true}]
    end
    result
  end


  def handle_avatar(avatar)
    if avatar == "petit-prince"
      result = ("<div class='customer-avatar petit-prince'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "aladdin"
      result = ("<div class='customer-avatar aladdin'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "amelie-poulain"
      result = ("<div class='customer-avatar amelie-poulain'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "cesar"
      result = ("<div class='customer-avatar cesar'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "churchill"
      result = ("<div class='customer-avatar churchill'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "cleopatre"
      result = ("<div class='customer-avatar cleopatre'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "coco-chanel"
      result = ("<div class='customer-avatar coco-chanel'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "kurt-cobain"
      result = ("<div class='customer-avatar kurt-cobain'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "leia"
      result = ("<div class='customer-avatar leia'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "raiponce"
      result = ("<div class='customer-avatar raiponce'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "sissi"
      result = ("<div class='customer-avatar sissi'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    elsif avatar == "van-gogh"
      result = ("<div class='customer-avatar van-gogh'><div class='inline-svg'>#{svg(avatar)}</div></div>").html_safe
    end
  end


  def handle_api_results_count(collection_size)
    if collection_size
      if collection_size.to_i > 0 && collection_size.to_i <= 250
        value = "orange"
      elsif collection_size.to_i > 250 && collection_size.to_i <= 1000
        value = "blue"
      elsif collection_size.to_i > 1000 && collection_size.to_i <= 2000
        value = "deeppink"
      elsif collection_size.to_i > 2000
        value = "tomato"
      end
    else
      value = nil
    end
    return value
  end

  def prop_dpe(dpe)
    values = {1 => "A",
              2 => "B",
              3 => "C",
              4 => "D",
              5 => "E",
              6 => "F",
              7 => "G"
             }
    if dpe
      value = values[dpe.to_i]
    else
      value = nil
    end
    return value
  end

  def prop_exposition(exposition)
    values = {1 => "Sud",
              2 => "Ouest",
              3 => "Nord",
              4 => "Est",
              5 => "Sud-est",
              6 => "Sud-ouest",
              7 => "Nord-est",
              8 => "Nord-ouest"
             }
    if exposition
      value = values[exposition.to_i]
    else
      value = nil
    end
    return value
  end

  def french_human_counties(county)
    values = {"01" => "Ain",
              "02" => "Aisne",
              "03" => "Allier",
              "04" => "Alpes-de-haute-provence",
              "05" => "Hautes-alpes",
              "06" => "Alpes-Maritimes",
              "07" => "Ardèche",
              "08" => "Ardennes",
              "09" => "Ariège",
              "10" => "Aube",
              "11" => "Aude",
              "12" => "Aveyron",
              "13" => "Bouche-du-Rhône",
              "14" => "Calvados",
              "15" => "Cantal",
              "16" => "Charente",
              "17" => "Charente-Maritime",
              "18" => "Cher",
              "19" => "Corrèze",
              "21" => "Côte-d'or",
              "22" => "Côte-d'Armor",
              "23" => "Creuse",
              "24" => "Dordogne",
              "25" => "Doubs",
              "26" => "Drôme",
              "27" => "Eure",
              "28" => "Eure-et-loir",
              "29" => "Finistère",
              "2A" => "Corse-du-sud",
              "2B" => "Haute-corse",
              "30" => "Gard",
              "31" => "Haute-Garonne",
              "32" => "Gers",
              "33" => "Gironde",
              "34" => "Herault",
              "35" => "Ille-et-vilaine",
              "36" => "Indre",
              "37" => "Indre-et-loire",
              "38" => "Isère",
              "39" => "Jura",
              "40" => "Landes",
              "41" => "Loir-et-chair",
              "42" => "Loire",
              "43" => "Haute-Loire",
              "44" => "Loire-Atlantique",
              "45" => "Loiret",
              "46" => "Lot",
              "47" => "Lot-et-garonne",
              "48" => "Lozere",
              "49" => "Maine-et-Loire",
              "50" => "Manche",
              "51" => "Marne",
              "52" => "Haute-marne",
              "53" => "Mayenne",
              "54" => "Meurthe-et-moselle",
              "55" => "Meuse",
              "56" => "Morbihan",
              "57" => "Moselle",
              "58" => "Nièvre",
              "59" => "Nord",
              "60" => "Oise",
              "61" => "Orne",
              "62" => "Pas-de-calais",
              "63" => "Puy-de-Dôme",
              "64" => "Pyrénées-Atlantiques",
              "65" => "Hautes-Pyrénées",
              "66" => "Pyrénées-Orientales",
              "67" => "Bas-Rhin",
              "68" => "Haut-Rhin",
              "69" => "Rhône",
              "70" => "Haute-Saône",
              "71" => "Saône-et-Loire",
              "72" => "Sarthe",
              "73" => "Savoie",
              "74" => "Haute-Savoie",
              "75" => "Paris",
              "76" => "Seine-Maritime",
              "77" => "Seine-et-Marne",
              "78" => "Yvelines",
              "79" => "Deux-Sèvres",
              "80" => "Somme",
              "81" => "Tarn",
              "82" => "Tarn-et-Garonne",
              "83" => "Var",
              "84" => "Vaucluse",
              "85" => "Vendée",
              "86" => "Vienne",
              "87" => "Haute-Vienne",
              "88" => "Vosges",
              "89" => "Yonne",
              "90" => "Territoire de belfort",
              "91" => "Essonne",
              "92" => "Hauts-de-Seine",
              "93" => "Seine-Saint-Denis",
              "94" => "Val-de-Marne",
              "95" => "Val-d'Oise",
              "971" => "Guadeloupe",
              "972" => "Martinique",
              "973" => "Guyane",
              "974" => "La réunion",
              "976" => "Mayotte"
             }
    if county
      values[county.to_s]
    else
      nil
    end
  end

  def french_county_list
    list = ("01".."99").to_a
    list = ["Département"] + list
  end

  def prop_status_list_with_value
    list = [["Statut",""],
            ["En estimation",1],
            ["Actif",2],
            ["Sous offre",3],
            ["Sous compromis",4],
            ["Vendu",5],
            ["Mandat clos",6]]
  end

  def prop_type_list_with_value
    list = [["Type de bien",""],
            ["Une maison",1],
            ["Un appartement",2],
            ["Un studio",4],
            ["Un terrain",5],
            ["Un viager",7],
            ["Un mas",10],
            ["Une bastide",11],
            ["Un garage",15],
            ["Un parking",16],
            ["Un chalet",17],
            ["Un duplex",18],
            ["Autre",20],
            ["Un immeuble",21],
            ["Une propriété",22],
            ["Un commerce",23],
            ["Un cabanon",24],
            ["Une villa",25],
            ["Un rez de jardin",26],
            ["Un rez de villa",27],
            ["Un château",28],
            ["Une cave",29],
            ["Une ferme",30],
            ["Un loft",31],
            ["Une maison de maître",32],
            ["Une maison individuelle",33],
            ["Une Maison bi-familiale",34],
            ["Une fermette",35],
            ["Un bungalow",36],
            ["Un monastère",37],
            ["Une maison de ville",38],
            ["Une maison de village",39],
            ["Une chambre",40],
            ["Un triplex",41],
            ["Un penthouse",42],
            ["Un terrain à batir",43],
            ["Un terrain agricole",44],
            ["Un terrain de loisir",45],
            ["Un emplacement intérieur",46],
            ["Un parking ouvert",47],
            ["Un box",48],
            ["Un fond de commerce",231],
            ["Une cession de droit au bail",232],
            ["Des murs commerciaux",235],
            ["Des bureaux",2310],
            ["Un entrepot",2313],
            ["Un local commercial",2314],
            ["Des terrains",2315],
            ["Un local d'activité",2316],
            ["Un local professionnel",2317]]
  end

  def handle_heating(prop)
    values = []
    if heating_energy_humanized(prop["heating_energy"].to_i)
      values << {text: "Energie", content: heating_energy_humanized(prop["heating_energy"].to_i)}
    end
    if heating_type_humanized(prop['heating_type'].to_i)
      values << {text: "Type", content: heating_type_humanized(prop["heating_type"].to_i)}
    end
    if heating_format_humanized(prop['heating_format'].to_i)
      values << {text: "Format", content: heating_format_humanized(prop["heating_format"].to_i)}
    end
    if values.size != 0
      return values
    else
      return nil
    end
  end

  def handle_equipments(property)
    values = []
    if property["elevator"]
      values << {icon: "flux-elevator", text: "Ascenseur"}
    end
    if property["swimming_pool"]
      values << {icon: "flux-pool", text: "Piscine"}
    end
    if property["parking"]
      values << {icon: "flux-parking", text: "Parking"}
    end
    if property["terrace"]
      values << {icon: "flux-terrace", text: "Terrasse"}
    end
    if property["balcony"]
      values << {icon: "flux-balcony", text: "Balcon"}
    end
    if property["washroom"]
      values << {icon: "flux-washroom", text: "Buanderie"}
    end
    if values.size != 0
      return values
    else
      return nil
    end
  end

  def heating_type_humanized(heating_type)
    values = {1 => "radiateur",
              2 => "au sol",
              3 => "air pulse",
              4 => "convecteur",
              5 => "poele",
              6 => "panneaux rayonnant"
             }
    if heating_type
      values[heating_type.to_i]
    else
      nil
    end
  end


  def heating_format_humanized(heating_format)
    values = {1 => "collectif",
              2 => "individuel",
              3 => "central"
             }
    if heating_format
      values[heating_format.to_i]
    else
      nil
    end
  end


  def heating_energy_humanized(heating_energy)
    values = {1 => "gaz",
              2 => "electrique",
              3 => "fioul",
              4 => "charbon",
              5 => "bois",
              6 => "solaire",
              7 => "geothermie",
              8 => "pompe a chaleur",
              9 => "climatisation",
              10 => "mixte",
              11 => "sans",
              12 => "autre"
             }
    if heating_energy
      values[heating_energy.to_i]
    else
      nil
    end
  end


  def status_code_humanized(code)
    values = {1 => "En estimation",
              2 => "Actif",
              3 => "Sous offre",
              4 => "Sous compromis",
              5 => "Vendu",
              6 => "Mandat clos"
             }
    if code
      values[code]
    else
      nil
    end
  end

  def property_type_string(code)
    values = {1 => "Une maison",
              2 => "Un appartement",
              4 => "Un studio",
              5 => "Un terrain",
              7 => "Un viager",
              10 => "Un mas",
              11 => "Une bastide",
              15 => "Un garage",
              16 => "Un parking",
              17 => "Un chalet",
              18 => "Un duplex",
              20 => "Autre",
              21 => "Un immeuble",
              22 => "Une propriete",
              23 => "Un commerce",
              24 => "Un cabanon",
              25 => "Une villa",
              26 => "Un rez de jardin",
              27 => "Un rez de villa",
              28 => "Un château",
              29 => "Une cave",
              31 => "Un loft",
              30 => "Une ferme",
              32 => "Une maison de maître",
              33 => "Une maison individuelle",
              34 => "Une maison bi-familiale",
              35 => "Une fermette",
              36 => "Un bungalow",
              37 => "Un monastère",
              38 => "Une maison de ville",
              39 => "Une maison de village",
              40 => "Une chambre",
              41 => "Un triplex",
              42 => "Un penthouse",
              43 => "Un terrain à batir",
              44 => "Un terrain agricole",
              45 => "Un terrain de loisir",
              46 => "Un emplacement intérieur",
              47 => "Un parking ouvert",
              48 => "Un box",
              231 => "Un fonds de commerce",
              232 => "Une cession de droit au bail",
              235 => "Des murs commerciaux",
              2310 => "Des bureaux",
              2313 => "Un entrepôt",
              2314 => "Un local commerciale",
              2315 => "Des terrains",
              2316 => "Un local d'activité",
              2317 => "Un local professionnel"
             }
    if code
      values[code]
    else
      nil
    end
  end


  def property_type_color(code)
    values = {1 => "tomato",
              2 => "deepskyblue",
              4 => "springgreen",
              5 => "purple",
              7 => "orange",
              10 => "black",
              11 => "gray",
              15 => "deepskyblue",
              16 => "lightblue",
              17 => "aqua",
              18 => "purple",
              20 => "pink",
              21 => "orange",
              22 => "deepskyblue",
              23 => "lightblue",
              24 => "red",
              25 => "purple",
              26 => "tomato",
              27 => "gray",
              28 => "orange",
              29 => "aqua",
              31 => "forestgreen",
              30 => "coral",
              32 => "chartreuse",
              33 => "tomato",
              34 => "deepskyblue",
              35 => "forestgreen",
              36 => "gold",
              37 => "purple",
              38 => "red",
              39 => "blue",
              40 => "orange",
              41 => "black",
              42 => "deeppink",
              43 => "darkturquoise",
              44 => "tomato",
              45 => "lightblue",
              46 => "lightgreen",
              47 => "aqua",
              48 => "purple",
              231 => "gold",
              232 => "deeppink",
              235 => "deepskyblue",
              2310 => "orange",
              2313 => "coral",
              2314 => "chartreuse",
              2315 => "aliceblue",
              2316 => "gray",
              2317 => "limegreen"
             }
    if code
      values[code]
    else
      nil
    end
  end


  def front_validation_budget(value)
    sanitize_value = value.reject{|number| number.to_s.empty?}
    return sanitize_value.last.to_i
  end

  def format_condition_field_value(value)
    sanitize_data = value.reject{|number| number.to_s.empty?}
    return sanitize_data.map!{|number| number.to_s.strip.gsub(/[[:space:]]/,'').to_i}.last
  end


  def svg(name)
    file_path = "#{Rails.root}/app/assets/images/svg/#{name}.svg"
    return File.read(file_path).html_safe if File.exists?(file_path)
    '(not found)'
  end


  def currency_format(price)
    if price.to_s != "0"
      @new_price = price.strip.gsub(/[[:space:]]/,'')
      result = number_to_currency(@new_price.scan(/\d+/).join().to_i, local: :fr, precision: 0)
    else
      result = "Le prix est a 0 €"
    end
    return result
  end

  def demand_project_kind(demand_project)
    if demand_project
      demand_project = demand_project.to_i
      if demand_project == 7
        result = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/svgs/chatroom/chatroom-customer-avatar.svg"
      elsif demand_project == 8
        result = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/svgs/chatroom/chatroom-couple-avatar.svg"
      elsif demand_project == 9
        result = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/svgs/chatroom/chatroom-family-avatar.svg"
      elsif demand_project == 10
        result = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/svgs/chatroom/chatroom-family-avatar.svg"
      else
        result = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/svgs/chatroom/chatroom-customer-avatar.svg"
      end
    else
      result = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/svgs/chatroom/chatroom-customer-avatar.svg"
    end
    return result
  end

  def title(title)
    content_for(:title) {"Bientôt Chez Soi - #{title}"}
  end

  def meta_description(text)
    content_for(:meta_description) {text.html_safe}
  end

  def facebook_meta_url(url)
    content_for(:facebook_meta_url) {"https://bientotchezsoi.com#{url}"}
  end

  def facebook_meta_full_url(url)
    content_for(:facebook_meta_full_url) {"#{url}"}
  end

  def facebook_meta_image(image_url)
    content_for(:facebook_meta_image) {"#{image_url}"}
  end

  def facebook_meta_title(title)
    content_for(:facebook_meta_title) {title.html_safe}
  end

  def facebook_meta_description(description)
    content_for(:facebook_meta_description) {description.html_safe}
  end


  def twitter_meta_card(card)
    content_for(:twitter_meta_card) {card}
  end

  def twitter_meta_url(url)
    content_for(:twitter_meta_url) {url}
  end

  def twitter_meta_title(title)
    content_for(:twitter_meta_title) {title.html_safe}
  end

  def twitter_meta_description(desc)
    content_for(:twitter_meta_description) {desc.html_safe}
  end

  def twitter_meta_image(img_url)
    content_for(:twitter_meta_image) {img_url}
  end



  def agency_response_rate(active_demands_count, prop_sending_count)
    if active_demands_count != 0
      result = (prop_sending_count * 100) / active_demands_count
      return "Taux de réponse de #{result}%"
    else
      return "Taux de réponse de 0%"
    end
  end


  def enhanced_agency_response_rate(current_agency)
    @proposed_props = []
    @agency_demands = current_agency.v2_demands
    @agency_demands.each do |demand|
      @demand_props = demand.properties
      @demand_props.each do |demand_prop|
        if current_agency.properties.include?(demand_prop)
          @proposed_props << demand_prop
        end
      end
    end
    if @agency_demands.size != 0
      result = (@proposed_props.size * 100) / @agency_demands.size
    else
      result = 0
    end
    return {result: result, proposed_size: @proposed_props.size, demands_size: @agency_demands.size}
  end

  def agency_response_percentage(active_demands_count, prop_sending_count)
    if active_demands_count != 0
      result = (prop_sending_count * 100) / active_demands_count
      return result
    else
      return 0
    end
  end

  def format_number(number)
    if number != ""
      "#{number.gsub(',',' ')} €"
    end
  end

  def format_area(number)
    if number != ""
      "#{number} m²"
    end
  end

  def handle_array(array)
    @sanitized_array = array.reject{|field| (field == nil) || (field == "")}
    if @sanitized_array.size == 0
      result = "Vous avez passé cette question"
    elsif @sanitized_array.size == 1
      result = @sanitized_array.last
    elsif @sanitized_array.size > 1
      result = "#{@sanitized_array.first} #{@sanitized_array.last}"
    end
    return result
  end

  def admin_handle_property_type_color(property_type)

    if property_type == "[]"
      html = ("<div class='gray-text'>Inconnu</div>").html_safe
    elsif property_type == "Un appartement"
      html = ("<div class='red-text'>#{property_type}</div>").html_safe
    elsif property_type == "Une maison"
      html = ("<div class='green-text'>#{property_type}</div>").html_safe
    else
      html = ("<div class='gray-text'>#{property_type}</div>").html_safe
    end
    return html
  end

  def format_budget_spread_data(budget_array)
    sanitize_array = budget_array.reject{|budget| budget.empty?}
    if sanitize_array && sanitize_array.size > 0
      if sanitize_array.size == 1
        result = sanitize_array.first
      elsif sanitize_array.size == 2
        result = sanitize_array.last
      end
    end
    if result
      result.remove("\u00A0").to_i
    else
      "Non renseigné"
    end
  end


  def format_full_budget_spread_data(budget_array)
    sanitize_array = budget_array.reject{|budget| budget.empty?}
    if sanitize_array && sanitize_array.size > 0
      if sanitize_array.size == 1
        result = "#{number_with_delimiter(sanitize_array.first.to_i)} €"
      elsif sanitize_array.size == 2
        result = "Entre #{number_with_delimiter(sanitize_array.first.to_i, locale: :fr)} € et #{number_with_delimiter(sanitize_array.last.to_i, locale: :fr)} €"
      end
    end
    if result
      result
    else
      "Non renseigné"
    end
  end


  def generate_spread_order_id(demand)
    reference = demand.id.to_s.last(5).upcase
    result = "BCS_DEMAND_#{reference}"
  end

  def format_postal_codes_spread_data(postal_codes)
    if postal_codes
      if postal_codes.size == 1
        return "#{postal_codes.first}"
      elsif postal_codes.size == 2
        return "#{postal_codes.first} et #{postal_codes.last}"
      elsif postal_codes.size > 2
        return "#{postal_codes[0..-2].join(",")} et #{postal_codes.last}"
      end
    end
  end

  def format_equipements_spread_data(equipements)
    if equipements
      if equipements.size == 1
        return "#{equipements.first}"
      elsif equipements.size == 2
        return "#{equipements.first} et #{equipements.last}"
      elsif equipements.size > 2
        return "#{equipements[0..-2].join(",")} et #{equipements.last}"
      end
    end
  end

  def admin_surface_format(surface)
    if surface
      sanitize_array = surface.reject{|surface| surface.empty?}
      if sanitize_array && sanitize_array.size > 0
        if sanitize_array.size == 1
          result = "#{sanitize_array.first} m²"
        elsif sanitize_array.size == 2
          result = "Entre #{sanitize_array.first} m² et #{sanitize_array.last} m²"
        end
      elsif sanitize_array == []
        result = "non renseigné"
      end
    end
    result
  end


  def admin_budget_format(budget)
    if budget
      sanitize_array = budget.reject{|budget| budget.to_s.empty?}
      sanitize_array = sanitize_array.map!{|number| number.strip.gsub(/[[:space:]]/,'').gsub(",","")}
      if sanitize_array && sanitize_array.size > 0
        if sanitize_array.size == 1
          result = "#{number_with_delimiter(sanitize_array.first.to_i)} €"
        elsif sanitize_array.size == 2
          result = "Entre #{number_with_delimiter(sanitize_array.first.to_i)} € et #{number_with_delimiter(sanitize_array.last.to_i)} €"
        end
      end
    end
    result
  end


  def generate_merge_vars(key,value)
    email = key
    if value.size == 1
      @merge_vars = {"CUSTOMER_EMAIL" => "#{key}", "RESULT" => "Type de bien: #{value.first[2]}, Prix: #{value.first[0]}, Proposé depuis: #{value.first[3]}"}
    else
      @merge_vars = {"CUSTOMER_EMAIL" => "#{key}"}
      value.last(5).each_with_index do |item,index|
        @merge_vars["RESULT_#{index+1}"] = "Type de bien: #{item[2]}, Prix: #{item[0]}, Proposé depuis: #{item[3]}"
      end
    end
    return @merge_vars
  end


  def num_to_phone(num)
    if num.start_with?("+")
      result = "0#{num[3]} #{num[4..5]} #{num[6..7]} #{num[8..9]} #{num[10..11]}"
    else
      result = "#{num[0..1]} #{num[2..3]} #{num[4..5]} #{num[6..7]} #{num[8..9]}"
    end
    result
  end


  def remove_unbreakable_space(word)
    word.strip.gsub(/[[:space:]]/,'')
  end


end
