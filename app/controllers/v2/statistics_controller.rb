class V2::StatisticsController < ApplicationController

  def week_stats
    @last_week = DateTime.now.cweek - 1
    current_year = DateTime.now.year
    @wk_begin = Date.commercial(current_year, @last_week, 1)
    @wk_end = Date.commercial(current_year, @last_week, 7)
    @week_demands = V2Demand.where(created_at: @wk_begin.beginning_of_day..@wk_end.end_of_day).includes(:customer)
    render :week_stats
  end

  def cweek_diff(start_date, end_date)
    return if end_date < start_date
    cweek_diff = (end_date.cweek - start_date.cweek) + 1
    cwyear_diff = end_date.cwyear - start_date.cwyear
    cwyear_diff * 53 + cweek_diff - cwyear_diff
  end

end
