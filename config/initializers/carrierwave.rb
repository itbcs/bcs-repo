CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'
  config.fog_credentials = {
    provider:              'AWS',
    aws_access_key_id:     ENV["AWS_ACCESS_KEY"],
    aws_secret_access_key: ENV["AWS_SECRET_ACCESS"],
    region:                'eu-west-3'
  }
  config.fog_directory  = 'bientotchezsoi'
  config.fog_public     = false
  config.fog_attributes = { cache_control: "public, max-age=#{365.days.to_i}" }
  config.asset_host = "https://cdn.bientotchezsoi-dev.com"
end
