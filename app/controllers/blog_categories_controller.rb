class BlogCategoriesController < ApplicationController

  def show
    @blog_category = BlogCategory.find_by(lname: params[:lname])
    @last_category_post = @blog_category.posts.last
    @blog_categories = BlogCategory.all
    @last_five_posts = Post.all.desc('_id').limit(5)
    @tags = (Post.all.map {|po| po.tags.map{|t| t}}).flatten.uniq
  end

  private

  def blog_category_params
    params.require(:blog_category).permit(:name, :lname, :h1_content, :title, :desc, :content)
  end

end
