class ChatMessagesController < ApplicationController

  def create

    if params[:chat_message][:agency_ref]
      chatroom_id = params[:chat_message][:chatroom_id].to_s
      @chatroom = Chatroom.find(chatroom_id)
      @demand = @chatroom.v2_demand
      @property = @chatroom.property
      @customer = @chatroom.customer
      @customer_choice = @property.customer_choices.where(v2_demand_id: @demand.id).last
      chat_message = ChatMessage.new(message_params)

      check_phone_number(chat_message.content.to_s)
      check_email(chat_message.content.to_s)

      chat_message.customer = @customer
      chat_message.agency = current_agency
      chat_message.origin = "agency"
      chat_message.chatroom = @chatroom
      chat_message.seen_by_agency = true

      if customer_signed_in?
        @customer = current_customer
        chat_message.seen_by_customer = true
      end

      @chatroom.chat_messages.push(chat_message)
      @chatroom.save
      if chat_message.save
        if @customer_choice
          ActionCable.server.broadcast 'messages',
            message: chat_message.content,
            html_class: "agency-message",
            user: chat_message.agency.name,
            anonym: false
          head :ok
        else
          ActionCable.server.broadcast 'messages',
            message: chat_message.content,
            html_class: "agency-message",
            user: "L'agence",
            anonym: true
          head :ok
        end
      end
    else
      chatroom_id = params[:chat_message][:chatroom_id].to_s
      @chatroom = Chatroom.find(chatroom_id)
      @demand = @chatroom.v2_demand
      @property = @chatroom.property
      @chatroom.customer = current_customer
      @customer_choice = @property.customer_choices.where(v2_demand_id: @demand.id).last
      chat_message = ChatMessage.new(message_params)

      check_phone_number(chat_message.content.to_s)
      check_email(chat_message.content.to_s)

      chat_message.chatroom = @chatroom
      @customer = current_customer
      chat_message.customer = @customer
      chat_message.origin = "customer"
      chat_message.seen_by_customer = true

      if agency_signed_in?
        @agency = current_agency
        chat_message.seen_by_agency = true
      end

      @chatroom.chat_messages.push(chat_message)
      @chatroom.save
      @agency = Agency.find(params[:chat_message][:agency_id])
      @prop = Property.find(params[:chat_message][:property_id])
      if chat_message.save
        if @customer_choice
          ActionCable.server.broadcast 'messages',
            message: chat_message.content,
            html_class: "customer-message",
            user: chat_message.customer.first_name,
            anonym: false
          head :ok
        else
          ActionCable.server.broadcast 'messages',
            message: chat_message.content,
            html_class: "customer-message",
            user: "Le castor masqué",
            anonym: true
          head :ok
        end
      end
      sending_time = l(chat_message.created_at, format: :count)
      AgencyMailer.chatroom_message_notif(@agency,chat_message.content,@customer,@prop,sending_time).deliver
    end
  end

  private

  def message_params
    params.require(:chat_message).permit(:content, :chatroom_id)
  end

  def check_phone_number(str)
    re = /^
      (?:(?:\+|00)33|0)     # Dialing code
      \s*[1-9]              # First number (from 1 to 9)
      (?:[\s.-]*\d{2}){4}   # End of the phone number
      $/mix

    tempo_string = str
    if tempo_string.split().size == 1
      str.gsub!(re, "**********")
    else
      strings_to_delete = tempo_string.scan(/(?<=[ ])[\d \-+()]+$|(?<=[ ])[\d \-+()]+(?=[ ]\w)/)
      strings_to_delete.each do |wrong_string|
        str.gsub!(wrong_string, "**********")
      end
    end
  end


  def check_email(str)
    tempo_string = str
    strings_to_delete = tempo_string.scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/i)
    strings_to_delete.each do |wrong_string|
      str.gsub!(wrong_string, "**********")
    end
  end


end
