class ChatMessage
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :chatroom
  belongs_to :customer, optional: true
  belongs_to :agency, optional: true

  field :content, type: String
  field :origin, type: String

  field :seen_by_agency, type: Boolean, default: false
  field :seen_by_customer, type: Boolean, default: false

end
