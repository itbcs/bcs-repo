class V2Demand
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  include Mongoid::History::Trackable

  scope :activated, -> {where(activated: true)}

  track_history :on => [:fields],
                modifier_field_optional: true,
                track_update: true,
                track_create: false,
                track_destroy: false

  before_save :format_budget_field_on_update
  before_save :format_actual_housing_area
  before_save :format_ground_budget_field_on_update
  before_save :format_house_budget_field_on_update
  before_save :format_principal_space
  before_save :format_surface
  before_save :handle_multiple_values_budget

  belongs_to :customer
  has_and_belongs_to_many :properties
  has_many :demand_agency_views
  has_many :chatrooms
  has_many :customer_choices
  has_many :v2_demand_properties

  embeds_many :v2_map_circles
  embeds_many :v2_demand_comments

  mount_uploader :map_photo, MapUploader
  mount_uploader :browser_screenshot, BrowserScreenshotUploader

  field :project, type: String
  field :actual_situation, type: String
  field :property_type, type: String
  field :destination, type: String
  field :flat_state, type: String
  field :house_state, type: String
  field :travaux, type: Array
  field :budget, type: Array
  field :flat_budget, type: String
  field :terrain_budget, type: String
  field :house_budget, type: Array
  field :style, type: String
  field :surface, type: Array
  field :terrain_surface, type: String
  field :house_construction_surface, type: String
  field :rooms, type: String
  field :floor, type: String
  field :contact_constructor, type: String
  field :principal_space, type: String
  field :kitchen, type: String
  field :bathrooms, type: Integer
  field :more_rooms, type: Array
  field :equipments, type: Array
  field :accessibility, type: String
  field :flat_stairs, type: String
  field :house_stairs, type: String
  field :exposition, type: Array
  field :parking, type: Array
  field :security, type: Array
  field :heating, type: String
  field :heating_type, type: Array
  field :dpe, type: Array
  field :flat_exterior, type: Array
  field :house_exterior, type: Array
  field :age, type: String
  field :actual_pros, type: String
  field :actual_housing, type: String
  field :actual_housing_area, type: String
  field :installation_date, type: String
  field :last_precision, type: String
  field :heart_stroke, type: String
  field :criterias, type: Array
  field :firstname, type: String
  field :gender, type: String
  field :man_persona, type: String
  field :woman_persona, type: String
  field :email, type: String
  field :estimation_request, type: String
  field :energy_renovation, type: String
  field :school_type, type: Array
  field :visited_property_count, type: Integer
  field :banker_contact, type: String
  field :broker_contact, type: String
  field :search_time, type: String
  field :house_works, type: String
  field :personality, type: Array
  field :state, type: String
  field :garage, type: String


  field :search_formatted_area, type: String
  field :search_zipcodes_area, type: Array
  field :ground_budget, type: Array
  field :what_is_liked, type: String
  field :income, type: String

  field :completed, type: Boolean, default: false
  field :activated, type: Boolean, default: false
  field :deactivated, type: Boolean, default: false
  field :refused, type: Boolean, default: false
  field :status, type: String, default: "pending"
  field :sending_customer_recap_email, type: Boolean, default: false
  field :from_oldplatform, type: Boolean, default: false
  field :old_platform_created_at, type: Date
  field :activation_date, type: Time

  field :current_location, type: String
  field :current_address, type: String
  field :location_link, type: Array

  field :deactivation_reason, type: String
  field :deactivation_comment, type: String

  field :min_budget_int, type: Integer
  field :max_budget_int, type: Integer

  # CUSTOMIZE GETTERS
  # String or integer fields

  def self.personnality_hash
    @svg_for_men = { 'aladdin' => 'Aladdin',
                    'cesar' => 'Jules César',
                    'churchill' => 'Winston Churchill',
                    'kurt-cobain' => 'Kurt Cobain',
                    'petit-prince' => 'Le petit Prince',
                    'van-gogh' => 'Vincent van Gogh'
    }.to_a.sample

    @svg_for_women = { 'amelie-poulain' => 'Amélie Poulain',
                      'cleopatre' => 'Cléopatre',
                      'coco-chanel' => 'Coco Chanel',
                      'leia' => 'Leia',
                      'raiponce' => 'Raiponce',
                      'sissi' => 'Princesse Sissi'
    }.to_a.sample
  end


  def self.scoring_hash
    @scoring_hash = { "actual_situation" => { "auto_identifier_11" => 0,
                                              "auto_identifier_12" => 5,
                                              "auto_identifier_13" => 5,
                    },
                      "age" => { "auto_identifier_44" => 1,
                                  "auto_identifier_45" => 3,
                                  "auto_identifier_46" => 4,
                                  "auto_identifier_47" => 5
                      },
                      "search_time" => { "auto_identifier_24" => 1,
                                          "auto_identifier_25" => 3,
                                          "auto_identifier_26" => 4,
                                          "auto_identifier_27" => 5

                      },
                      "project" => { "auto_identifier_7" => 1,
                                      "auto_identifier_8" => 3,
                                      "auto_identifier_9" => 4,
                                      "auto_identifier_10" => 5

                      },
                      "created_at" => { "moins de 7 jours" =>  1,
                                        "de 8 à 21 jours" => 2,
                                        "de 22 à 35 jours" => 3,
                                        "plus de 36 jours" => 5

                      },
                      "last_log_in" => { "moins de 7 jours" =>  1,
                                        "de 8 à 21 jours" => 2,
                                        "de 22 à 35 jours" => 3,
                                        "plus de 36 jours" => 5

                      },
                      "installation_date" => { "auto_identifier_16" => 1,
                                                "auto_identifier_17" => 3,
                                                "auto_identifier_18" => 4,
                                                "auto_identifier_19" => 5
                      },
                      "visited_property_count" => { "auto_identifier_28" => 1,
                                                    "auto_identifier_29" => 3,
                                                    "auto_identifier_30" => 4,
                                                    "auto_identifier_31" => 5

                      },
                      "banker_contact" => { "auto_identifier_58" => 0,
                                            "auto_identifier_59" => 2,
                                            "auto_identifier_60" => 4
                      },
                      "broker_contact" => { "auto_identifier_61" => 0,
                                            "auto_identifier_62" => 2,
                                            "auto_identifier_63" => 4

                      },
                      "destination" => { "auto_identifier_61" => 5,
                                          "auto_identifier_62" => 3,
                                          "auto_identifier_63" => 3

                      },
                      "properties" => { "moins de 2 offres" => 1,
                                        "de 3 à 5 offres" => 2,
                                        "de 6 à 8 offres" => 3,
                                        "plus de 8 offres" => 5
                      },
                      "customer_choices" => { "0" => 2,
                                              "de 1 à 3 réactions" => 3,
                                              "de 4 à 6 réactions" => 4,
                                              "plus de 6 réactions" => 5
                      },
                      "personality" => { "auto_identifier_136" => 1,
                                          "auto_identifier_137" => 2,
                                          "auto_identifier_138" => 1,
                                          "auto_identifier_139" => 2,
                                          "auto_identifier_140" => 1,
                                          "auto_identifier_141" => 2,
                                          "auto_identifier_142" => 1,
                                          "auto_identifier_143" => 2,
                                          "auto_identifier_144" => 1,
                                          "auto_identifier_145" => 2,
                                          "auto_identifier_146" => 1,
                                          "auto_identifier_147" => 2,
                                          "auto_identifier_148" => 1,
                                          "auto_identifier_149" => 2,
                                          "auto_identifier_150" => 1,
                                          "auto_identifier_151" => 2
                      }
    }
  end


  def current_location
    if self[:current_location]
      V2Answer.find_by(auto_identifier: self[:current_location]).text
    else
      nil
    end
  end

  def contact_constructor
    if self[:contact_constructor]
      V2Answer.find_by(auto_identifier: self[:contact_constructor]).text
    else
      nil
    end
  end


  def principal_space
    if self[:principal_space]
      "#{ActionController::Base.helpers.number_with_delimiter(self[:principal_space].to_i, locale: :fr)} m²"
    else
      nil
    end
  end

  def garage
    if self[:garage]
      V2Answer.find_by(auto_identifier: self[:garage]).text
    else
      nil
    end
  end

  def income
    if self[:income]
      V2Answer.find_by(auto_identifier: self[:income]).text
    else
      nil
    end
  end

  def accessibility
    if self[:accessibility]
      V2Answer.find_by(auto_identifier: self[:accessibility]).text
    else
      nil
    end
  end


  def house_stairs
    if self[:house_stairs]
      V2Answer.find_by(auto_identifier: self[:house_stairs]).text
    else
      nil
    end
  end

  def flat_stairs
    if self[:flat_stairs]
      V2Answer.find_by(auto_identifier: self[:flat_stairs]).text
    else
      nil
    end
  end

  def floor
    if self[:floor]
      V2Answer.find_by(auto_identifier: self[:floor]).text
    else
      nil
    end
  end

  def banker_contact
    if self[:banker_contact]
      V2Answer.find_by(auto_identifier: self[:banker_contact]).text
    else
      nil
    end
  end

  def broker_contact
    if self[:broker_contact]
      V2Answer.find_by(auto_identifier: self[:broker_contact]).text
    else
      nil
    end
  end

  def actual_housing_area
    if self[:actual_housing_area]
      "#{ActionController::Base.helpers.number_with_delimiter(self[:actual_housing_area].to_i, locale: :fr)} m²"
    else
      nil
    end
  end

  def age
    if self[:age]
      V2Answer.find_by(auto_identifier: self[:age]).text
    else
      nil
    end
  end

  def installation_date
    if self[:installation_date]
      V2Answer.find_by(auto_identifier: self[:installation_date]).text
    else
      nil
    end
  end

  def project
    if self[:project]
      V2Answer.find_by(auto_identifier: self[:project]).text
    else
      nil
    end
  end

  def man_persona
    if self[:man_persona]
      V2Answer.find_by(auto_identifier: self[:man_persona]).image
    else
      nil
    end
  end

  def woman_persona
    if self[:woman_persona]
      V2Answer.find_by(auto_identifier: self[:woman_persona]).image
    else
      nil
    end
  end

  def actual_situation
    if self[:actual_situation]
      V2Answer.find_by(auto_identifier: self[:actual_situation]).text
    else
      nil
    end
  end

  def property_type
    if self[:property_type]
      V2Answer.find_by(auto_identifier: self[:property_type]).text
    else
      nil
    end
  end

  def is_a_house?
    if self[:property_type]
      self[:property_type].to_i == 21 ? true : false
    end
  end

  def is_a_flat?
    if self[:property_type]
      self[:property_type].to_i == 22 ? true : false
    end
  end

  def is_a_terrain?
    if self[:property_type]
      self[:property_type].to_i == 23 ? true : false
    end
  end

  def destination
    if self[:destination]
      V2Answer.find_by(auto_identifier: self[:destination]).text
    else
      nil
    end
  end

  def state
    if self[:state]
      if self[:property_type].to_i == 21
        V2Answer.find_by(auto_identifier: self[:state]).fem_text
      elsif self[:property_type].to_i == 22
        V2Answer.find_by(auto_identifier: self[:state]).text
      end
    else
      nil
    end
  end

  def heating
    if self[:heating]
      V2Answer.find_by(auto_identifier: self[:heating]).text
    else
      nil
    end
  end

  def style
    if self[:style]
      V2Answer.find_by(auto_identifier: self[:style]).text
    else
      nil
    end
  end

  def house_works
    if self[:house_works]
      V2Answer.find_by(auto_identifier: self[:house_works]).text
    else
      nil
    end
  end

  def energy_renovation
    if self[:energy_renovation]
      V2Answer.find_by(auto_identifier: self[:energy_renovation]).text
    else
      nil
    end
  end

  def kitchen
    if self[:kitchen]
      V2Answer.find_by(auto_identifier: self[:kitchen]).text
    else
      nil
    end
  end

  def gender
    if self[:gender]
      V2Answer.find_by(auto_identifier: self[:gender]).text
    else
      nil
    end
  end

  def destination
    if self[:destination]
      V2Answer.find_by(auto_identifier: self[:destination]).text
    else
      nil
    end
  end

  def actual_situation
    if self[:actual_situation]
      V2Answer.find_by(auto_identifier: self[:actual_situation]).text
    else
      nil
    end
  end

  def actual_housing
    if self[:actual_housing]
      V2Answer.find_by(auto_identifier: self[:actual_housing]).text
    else
      nil
    end
  end

  def bathrooms
    if self[:bathrooms]
      V2Answer.find_by(auto_identifier: self[:bathrooms]).text
    else
      nil
    end
  end

  def search_time
    if self[:search_time]
      V2Answer.find_by(auto_identifier: self[:search_time]).text
    else
      nil
    end
  end

  def visited_property_count
    if self[:visited_property_count]
      V2Answer.find_by(auto_identifier: self[:visited_property_count]).text
    else
      nil
    end
  end



  # CUSTOMIZE GETTERS ICON

  def budget_icon
    return "budget"
  end

  def criterias_icon
    return "important-fields"
  end


  # CUSTOMIZE GETTERS
  # Array fields

  def surface
    if self[:surface]
      self[:surface].reject!{|num| num.to_s.empty?}
      if self[:surface].size == 1
        return "#{ActionController::Base.helpers.number_with_delimiter(self[:surface].first.to_i, locale: :fr)} m²"
      elsif self[:surface].size == 2
        min_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:surface].first.to_i, locale: :fr)} m²"
        max_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:surface].last.to_i, locale: :fr)} m²"
        return "Entre #{min_value} et #{max_value}"
      end
    end
  end


  def budget
    if self[:budget]
      if self[:budget].size == 1
        return "#{ActionController::Base.helpers.number_with_delimiter(self[:budget].first.to_i, locale: :fr)}"
      elsif self[:budget].size == 2
        min_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:budget].first.to_i, locale: :fr)}"
        max_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:budget].last.to_i, locale: :fr)}"
        return "Entre #{min_value} et #{max_value}"
      end
    end
  end

  def integer_budget
    @values = []
    if self[:budget]
      if self[:budget].size == 1
        return self[:budget].first.to_i
      elsif self[:budget].size == 2
        @values << self[:budget].first.to_i
        @values << self[:budget].last.to_i
        return @values
      end
    else
      return nil
    end
  end

  def ground_budget
    if self[:ground_budget]
      if self[:ground_budget].size == 1
        return "#{ActionController::Base.helpers.number_with_delimiter(self[:ground_budget].first.to_i, locale: :fr)} €"
      elsif self[:ground_budget].size == 2
        min_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:budget].first.to_i, locale: :fr)}"
        max_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:budget].last.to_i, locale: :fr)}"
        return "Entre #{min_value} et #{max_value} €"
      end
    end
  end

  def house_budget
    if self[:house_budget]
      if self[:house_budget].size == 1
        return "#{ActionController::Base.helpers.number_with_delimiter(self[:house_budget].first.to_i, locale: :fr)} €"
      elsif self[:house_budget].size == 2
        min_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:budget].first.to_i, locale: :fr)}"
        max_value = "#{ActionController::Base.helpers.number_with_delimiter(self[:budget].last.to_i, locale: :fr)}"
        return "Entre #{min_value} et #{max_value} €"
      end
    end
  end

  def travaux
    if self[:travaux]
      answers = []
      self[:travaux].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text
      end
      return answers
    else
      nil
    end
  end

  def location_link
    if self[:location_link]
      answers = []
      self[:location_link].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text
      end
      return answers
    else
      nil
    end
  end

  def more_rooms
    if self[:more_rooms]
      answers = []
      self[:more_rooms].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text
      end
      return answers
    else
      nil
    end
  end

  def personality
    if self[:personality]
      answers = []
      self[:personality].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text
      end
      return answers
    else
      nil
    end
  end

  def school_type
    if self[:school_type]
      answers = []
      self[:school_type].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text
      end
      return answers
    else
      nil
    end
  end

  def dpe
    if self[:dpe]
      answers = []
      self[:dpe].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def heating_type
    if self[:heating_type]
      answers = []
      self[:heating_type].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def more_rooms
    if self[:more_rooms]
      answers = []
      self[:more_rooms].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end


  def criterias
    if self[:criterias]
      answers = []
      self[:criterias].each do |criteria|
        result = Question.where(demand_attribute: criteria).last
        if result
          answers << result.criteria_resume
        end
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end


  def criterias_array
    if self[:criterias]
      answers = []
      self[:criterias].each do |criteria|
        result = Question.where(demand_attribute: criteria).last
        if result
          answers << result.criteria_resume
        end
      end
      return answers
    else
      nil
    end
  end


  def personality
    if self[:personality]
      answers = []
      self[:personality].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def exposition
    if self[:exposition]
      answers = []
      self[:exposition].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def security
    if self[:security]
      answers = []
      self[:security].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def equipments
    if self[:equipments]
      answers = []
      self[:equipments].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end


  def flat_exterior
    if self[:flat_exterior]
      answers = []
      self[:flat_exterior].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def house_exterior
    puts self[:house_exterior]
    if self[:house_exterior]
      answers = []
      self[:house_exterior].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def parking
    if self[:parking]
      answers = []
      self[:parking].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def school_type
    if self[:school_type]
      answers = []
      self[:school_type].each do |value|
        answers << V2Answer.find_by(auto_identifier: value).text.downcase
      end
      if answers.size == 1
        return answers.last.capitalize
      elsif answers.size == 2
        return ("#{answers.first} et #{answers.last}").capitalize
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}").capitalize
      end
    else
      nil
    end
  end

  def search_zipcodes_area
    if self[:search_zipcodes_area]
      answers = []
      self[:search_zipcodes_area].each do |value|
        answers << value
      end
      if answers.size == 1
        return answers.last
      elsif answers.size == 2
        return ("#{answers.first} - #{answers.last}")
      elsif answers.size > 2
        return ("#{answers[0..-2].join(", ")} et #{answers.last}")
      end
    else
      nil
    end
  end



  # BEFORE_SAVE METHODS

  def handle_multiple_values_budget
    if self[:budget]
      if self[:budget].size == 1
        self[:min_budget_int] = self[:budget].first.to_s.strip.gsub(/[[:space:]]/,'').to_i
        self[:max_budget_int] = self[:budget].first.to_s.strip.gsub(/[[:space:]]/,'').to_i
      elsif self[:budget].size == 2
        self[:min_budget_int] = self[:budget].first.to_s.strip.gsub(/[[:space:]]/,'').to_i
        self[:max_budget_int] = self[:budget].last.to_s.strip.gsub(/[[:space:]]/,'').to_i
      end
    end
  end

  def format_principal_space
    if self[:principal_space]
      self[:principal_space].to_s.strip.gsub(/[[:space:]]/,'')
    end
  end


  def format_budget_field_on_update
    if self[:budget]
      puts "There is a budget #{budget}"
      self[:budget].reject!{|num| num.to_s.empty?}
      if self[:budget]
        self[:budget].map!{|number| number.strip.gsub("€","").gsub(/[[:space:]]/,'')}
      end
    end
  end

  def format_ground_budget_field_on_update
    if self[:ground_budget]
      self[:ground_budget].reject!{|num| num.to_s.empty?}
      if self[:ground_budget]
        self[:ground_budget].map!{|number| number.strip.gsub("€","").gsub(/[[:space:]]/,'')}
      end
    end
  end

  def format_house_budget_field_on_update
    if self[:house_budget]
      self[:house_budget].reject!{|num| num.to_s.empty?}
      if self[:house_budget]
        self[:house_budget].map!{|number| number.strip.gsub("€","").gsub(/[[:space:]]/,'')}
      end
    end
  end

  def format_surface
    if self[:surface]
      self[:surface].reject!{|num| num.to_s.empty?}
      if self[:surface]
        self[:surface].map!{|number| number.strip.gsub("€","").gsub(/[[:space:]]/,'')}
      end
    end
  end

  def format_actual_housing_area
    if actual_housing_area
      actual_housing_area.strip.gsub(/[[:space:]]/,'')
    end
  end


end
