module SitemapUtility

  def generate_blog_links
    add '/blog'
    BlogCategory.all.each do |category|
      category.posts.each do |post|
          puts "/blog/#{category.lname}/#{post.lname}"
          add "/blog/#{category.lname}/#{post.lname}"
      end
      puts "/blog/#{category.lname}"
      add "/blog/#{category.lname}"
    end
  end

  def generate_job_links
    add '/jobs'
    Job.all.each do |job|
      puts "/jobs/#{job.lname}"
      add "/jobs/#{job.lname}"
    end
  end

end
