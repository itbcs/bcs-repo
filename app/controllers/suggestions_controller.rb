class SuggestionsController < ApplicationController

  def create
    if params[:suggestion][:property_id]
      @prop = Property.find(params[:suggestion][:property_id])
    end

    @suggestion = Suggestion.create(suggestion_params)

    if params[:suggestion][:origin]
      @suggestion.origin = params[:suggestion][:origin]
    end

    if @prop
      if verify_recaptcha(model: @suggestion) && @suggestion.save
        redirect_to home_bcs_property_show_path(property_id: @prop.id.to_s), notice: "Merci :-) Votre message a été transmis à nos castors qui reviennent vers vous très vite."
      else
        redirect_to home_bcs_property_show_path(property_id: @prop.id.to_s), alert: "Oupss !! il y a des erreurs dans le formulaire"
      end
    else
      if verify_recaptcha(model: @suggestion) && @suggestion.save
        redirect_to root_path, notice: "Merci :-) Votre message a été transmis à nos castors qui reviennent vers vous très vite."
      else
        redirect_to root_path, alert: "Oupss !! il y a des erreurs dans le formulaire"
      end
    end

  end

  private

  def suggestion_params
    params.require(:suggestion).permit(:phone, :name, :email, :last_name, :message)
  end

end
