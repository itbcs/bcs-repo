class Score
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :customer
  field :count, type: Integer

end
