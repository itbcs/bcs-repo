class Api::Agency::AuthenticationController < Api::Agency::BaseController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_agency_request

  def authenticate
    command = AuthenticateAgency.call(params[:email], params[:password])
    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

end
