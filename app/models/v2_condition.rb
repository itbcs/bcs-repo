class V2Condition
  include Mongoid::Document
  embedded_in :question
  recursively_embeds_one

  field :demand_field, type: String
  field :demand_field_identifier, type: String
  field :operator, type: String
  field :value, type: String
  field :children_operator, type: String
  field :logic_condition, type: String
  field :ruler_survey, type: Integer
  field :journey, type: String
  field :hard_stock, type: Boolean
  field :parent_question_id, type: String

end
