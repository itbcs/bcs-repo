module PropertyHelper


  def property_status_format(status)
    if status || status != ""
      if status.to_i == 1
        result = "En cours d'estimation"
      elsif status.to_i == 2
        result = "Actif"
      elsif status.to_i == 3
        result = "Sous offre"
      elsif status.to_i == 4
        result = "Sous compromis"
      elsif status.to_i == 5
        result = "Vendu"
      elsif status.to_i == 6
        result = "Mandat clos"
      elsif status.to_i == 7
        result = "En vente"
      elsif status.to_i == 8
        result = "Exclusivité"
      end
      return result
    else
      return "Statut inconnu"
    end
  end


  def property_type_logo property_type
    if property_type
      case property_type.capitalize
      when "Rez de villa"
        logo = ("<i class='fa fa-leaf'></i>").html_safe
      when "Rez de jardin"
        logo = ("<i class='fa fa-leaf'></i>").html_safe
      when "Maison"
        logo = ("<i class='fa fa-home'></i>").html_safe
      when "Maison de village"
        logo = ("<i class='fa fa-home'></i>").html_safe
      when "Mas"
        logo = ("<i class='fa fa-flag'></i>").html_safe
      when "Loft"
        logo = ("<i class='fa fa-folder'></i>").html_safe
      when "Duplex"
        logo = ("<i class='fa fa-folder'></i>").html_safe
      when "Triplex"
        logo = ("<i class='fa fa-folder'></i>").html_safe
      when "Studio"
        logo = ("<i class='fa fa-home'></i>").html_safe
      when "Immeuble"
        logo = ("<i class='fa fa-building'></i>").html_safe
      when "Terrain"
        logo = ("<i class='fa fa-flag'></i>").html_safe
      when "Propriete"
        logo = ("<i class='fa fa-bug'></i>").html_safe
      when "Ferme"
        logo = ("<i class='fa fa-bolt'></i>").html_safe
      when "Chalet"
        logo = ("<i class='fa fa-bolt'></i>").html_safe
      when "Bastide"
        logo = ("<i class='fa fa-bolt'></i>").html_safe
      when "Villa"
        logo = ("<i class='fa fa-check'></i>").html_safe
      when "Appartement"
        logo = ("<i class='fa fa-building'></i>").html_safe
      when "Local commercial"
        logo = ("<i class='fa fa-bus'></i>").html_safe
      when "Bureaux"
        logo = ("<i class='fa fa-bus'></i>").html_safe
      when "Local professionnel"
        logo = ("<i class='fa fa-bus'></i>").html_safe
      when "Local d'activité"
        logo = ("<i class='fa fa-bus'></i>").html_safe
      when "Murs commerciaux"
        logo = ("<i class='fa fa-bus'></i>").html_safe
      when "Fonds de commerce"
        logo = ("<i class='fa fa-bus'></i>").html_safe
      when "Garage"
        logo = ("<i class='fa fa-car'></i>").html_safe
      when "Parking"
        logo = ("<i class='fa fa-car'></i>").html_safe
      when "Cave"
        logo = ("<i class='fa fa-car'></i>").html_safe
      when "Cession de droit au bail"
        logo = ("<i class='fa fa-car'></i>").html_safe
      when "Autre"
        logo = ("<i class='fa fa-folder'></i>").html_safe
      end
    else
      logo = ("<i class='fa fa-glass'></i>").html_safe
    end
    return logo
  end

  def offer_type_color offer_type
    if offer_type
      case offer_type.capitalize
      when "Vente"
        html = ("<div class='offer-type red'>#{offer_type.capitalize}</div>").html_safe
      when "Programme neuf"
        html = ("<div class='offer-type green'>#{offer_type.capitalize}</div>").html_safe
      when "Location"
        html = ("<div class='offer-type orange'>#{offer_type.capitalize}</div>").html_safe
      end
    else
      html = ("<div class='offer-type'>Type d'offre non renseigné</div>").html_safe
    end
    return html
  end

  def photo_check(photos)
    puts "*****************"
    puts photos
    puts "*****************"
    if photos == [] || photos == nil
      placeholder = "https://cdn.bientotchezsoi-dev.com/images/placeholder-4.jpg"
      return placeholder
    else
      return photos.last
      raise
    end
  end


  def handle_dpe_value(prop_dpe)
    values = ["A","B","C","D","E","F","G"]
    results = []
    values.each do |val|
      if val == prop_dpe.to_s.upcase
        results << "<div class='dpe-class active level-#{prop_dpe.downcase}'></div>"
      else
        results << "<div class='dpe-class level-#{prop_dpe.downcase}'></div>"
      end
    end
    block = "<div class='dpe-block'></div>"
  end

  def handle_ges_value(prop_ges)
    values = ["A","B","C","D","E","F","G"]
    results = []
    values.each do |val|
      if val == prop_ges.to_s.upcase
        results << "<div class='ges-class active level-#{prop_ges.downcase}'></div>"
      else
        results << "<div class='ges-class level-#{prop_ges.downcase}'></div>"
      end
    end
    block = "<div class='ges-block'></div>"
  end

  def handle_html_class(energy_type)
  end


  def property_matching(property, important_fields)
    @percent_count = 0
    @attributes = property.attributes.to_hash
    if important_fields != []
      important_fields.each do |field|
        case field.downcase.to_s
        when "localisation"
          if @attributes["city"] == @demand.localisation
            @percent_count += 1
          end
        when "zone de recherche"
        when "etat de la maison"
        when "espace principal"
        when "escalier pour la maison"
        when "etage"
        when "projet"
        when "dpe"
        when "accessibilité"
        when "escalier pour la maison"
        when "budget"
        when "style"
        when "surface"
          if @attributes["surface"] == @demand.surface
            @percent_count += 1
          end
        when "chambres"
          if @attributes["rooms"] == @demand.rooms
            @percent_count += 1
          end
        when "equipements"
        when "bien recherché"
        when "situation actuelle"
        when "age"
        when "parking"
        when "chauffage"
        when "accès appartement"
        when "sécurité"
        when "travaux"
        end
      end
    end
    result = (@percent_count.to_i * 100) / important_fields.size
  end

end
