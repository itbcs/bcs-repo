function debounce(func, wait = 20, immediate = true) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

const sliderElements = document.querySelectorAll('.slide-in');
const displayerElements = document.querySelectorAll('.display-in');


function checkSlide() {
  sliderElements.forEach(element => {
    const slideInAt = (window.scrollY + window.innerHeight) - element.offsetHeight / 2;
    const elementBottom = element.offsetTop + element.offsetHeight;

    const isHalfShown = slideInAt > element.offsetTop;
    const isNotScrolledPast = window.scrollY < elementBottom;
    if (isHalfShown && isNotScrolledPast) {
      element.classList.add('active-on-slide-in');
    } else {
      element.classList.remove('active-on-slide-in');
    }
  });
}

window.addEventListener('scroll', debounce(checkSlide));

function checkDisplay() {
  displayerElements.forEach(element => {
    const slideInAt = (window.scrollY + window.innerHeight) - element.offsetHeight / 2;
    const elementBottom = element.offsetTop + element.offsetHeight;

    const isHalfShown = slideInAt > element.offsetTop;
    const isNotScrolledPast = window.scrollY < elementBottom;
    if (isHalfShown && isNotScrolledPast) {
      element.classList.add('active-on-display-in');
    } else {
      element.classList.remove('active-on-display-in');
    }
  });
}

window.addEventListener('scroll', debounce(checkDisplay));
