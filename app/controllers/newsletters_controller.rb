class NewslettersController < ApplicationController

  def create
    @newsletter = Newsletter.new(safe_params)
    if verify_recaptcha(model: @newsletter) && @newsletter.save
      redirect_to root_path, notice: "Vous êtes inscrit à notre newsletter"
    else
      render root_path, alert: "Problème pour l'inscription à la newsletter"
    end
  end

  private

  def safe_params
    params.require(:newsletter).permit(:email)
  end

end
