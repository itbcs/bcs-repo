class CustomerChoice
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :property
  belongs_to :demand, optional: true
  belongs_to :v2_demand, optional: true

  before_save :sanitize_comment_param

  field :choice, type: String
  field :negative_comment, type: String
  field :positive_comment, type: String
  field :comment, type: String
  field :rating, type: Integer

  def self.one_day_old_positive_choices
    Time.zone.now.beginning_of_day..Time.zone.now.end_of_day
    CustomerChoice.where(created_at: {'$gte': (Time.zone.now.beginning_of_day - 1.day),'$lte': Time.zone.now.end_of_day}).where(choice: {'$ne': "Je n'aime pas"})
  end


  def sanitize_comment_param
    if comment.to_s.empty?
      puts "********* sanitize comment param *********"
      puts "---- Genuine param ----"
      puts comment.inspect
      puts "******************************************"
      comment = nil
      puts "---- Sanitize param ----"
      puts comment
    else
      puts comment.inspect
    end
  end

end
