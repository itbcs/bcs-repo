class ZipParser

  def get_file(ftp)
    ftp.chdir("./")
    zip_files = ftp.nlst('*.zip')
    zip_files.first(30).each do |filename|
      binary_file = TempFile.new(ftp.getbinaryfile(filename))
      Zip::File.open(binary_file) do |zip_file|
        zip_file.each do |entry|
          content = entry.get_input_stream.read
          xml_doc  = Nokogiri::XML(content)
          XmlParser.new().format_xml(xml_doc,entry)
        end
      end
    end
  end

end
