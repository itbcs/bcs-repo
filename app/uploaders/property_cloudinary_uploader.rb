class PropertyCloudinaryUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  def public_id
    puts "******** Cloudinary uploader ********"
    puts model.property_type
    puts model.agency.name
    puts "*************************************"
    if model.agency.name
      @agency_name = model.agency.name.strip.parameterize
    else
      @agency_name = model.agency.id.to_s
    end

    if model.property_type
      @prop_type = model.property_type.strip.parameterize
    elsif model.type_bien
      @prop_type = model.type_bien.strip.parameterize
    end

    return "Carrierwave-uploads/Biens-immobiliers/Agence-#{@agency_name}/#{@prop_type}-#{model.id.to_s}/image-#{SecureRandom.hex(8)}"
  end

end
