class Access::PropertiesController < ApplicationController

  def show
    if params[:id] && params[:demand_id]
      @demand = V2Demand.find(params[:demand_id])
      @property = Property.find(params[:id])
      @customer = @demand.customer
    end
    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]
    @imgs_attrs.each do |field|
      puts @property.send("#{field}")
      if !@property.send("#{field}").nil?
        @present_imgs << (field.to_s.remove("img_").to_i)
      end
    end
    @present_imgs
    @chatroom = Chatroom.where(property_id: @property.id.to_s).where(customer_id: @customer.id.to_s).last
    @customer_choice = @property.customer_choices.where(v2_demand: @demand).last
    render :show
  end

  def success
    render :success
  end

  def service_test
    render :service_test
  end


  def handle_properties(properties,demand_id,firstname)
    vars = {}
    if properties != []
      properties.to_a.last(10).each_with_index do |prop,index|
        vars["PROP_#{index+1}"] = prop.id.to_s
        if prop.img_1_url
          vars["PROP_#{index+1}_IMG"] = prop.img_1_url
        else
          vars["PROP_#{index+1}_IMG"] = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/pictos/kenotte/Castor-Inspecteur.svg"
        end
        vars["PROP_#{index+1}_LINK"] = "https://bientotchezsoi.com/access/properties/#{prop.id.to_s}?demand_id=#{demand_id}"
        vars["PROP_#{index+1}_ROOMS"] = prop.nb_room
        vars["PROP_#{index+1}_LOCALISATION"] = prop.localisation
        vars["PROP_#{index+1}_PRICE"] = prop.price
        vars["PROP_#{index+1}_TITLE"] = prop.title
        vars["FIRST_NAME"] = firstname
      end
    end
    return vars
  end


  def all_offers_without_choice
    @demands = V2Demand.where(activated: true)
    @demands.each do |demand|
      @email = demand.customer.email
      @firstname = demand.customer.first_name
      if demand.properties?
        result = OfferWithoutChoice.new(demand).call
        if result.success?
          if result.data != []
            @results = result.data
            @vars = handle_properties(@results, demand.id.to_s, @firstname)
            CustomerMailer.offers_without_choice(@email,demand.id.to_s,@vars,@firstname).deliver
          end
        end
      end
    end
    redirect_to root_path
  end

  # def handle_demands_budget_to_integer
  #   @demands = V2Demand.where(status: {"$ne" => nil})
  #   puts "**** Handle demands budget to integer ****"
  #   @demands.each do |demand|
  #     if !demand[:max_budget_int] && !demand[:min_budget_int]
  #       if demand[:budget]
  #         if demand[:budget].size == 1
  #           demand[:min_budget_int] = demand[:budget].first.to_s.strip.gsub(/[[:space:]]/,'').to_i
  #           demand[:max_budget_int] = demand[:budget].first.to_s.strip.gsub(/[[:space:]]/,'').to_i
  #         elsif demand[:budget].size == 2
  #           demand[:min_budget_int] = demand[:budget].first.to_s.strip.gsub(/[[:space:]]/,'').to_i
  #           demand[:max_budget_int] = demand[:budget].last.to_s.strip.gsub(/[[:space:]]/,'').to_i
  #         end
  #       end
  #       if demand.save
  #         puts "Demand successfully saved !!! - with: #{demand.min_budget_int} / #{demand.max_budget_int}"
  #       end
  #     else
  #       puts "**** Demand already processed for integer budget"
  #       puts "#{demand.min_budget_int} - #{demand.max_budget_int}"
  #     end
  #   end
  #   render :service_test
  # end

  def handle_demands_budget_to_integer
    @chatrooms = Chatroom.all.select{|cr| cr.property == nil}
    @chatrooms.each do |chatroom|
      chatroom.destroy
    end
    render :service_test
  end

end
