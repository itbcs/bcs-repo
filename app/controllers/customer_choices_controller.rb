class CustomerChoicesController < ApplicationController

  def hate
    @demand = V2Demand.find(params[:demand_id])
    @property = Property.find(params[:property_id])
    @choice = CustomerChoice.new()
    @choice.choice = "Je n'aime pas"

    if params[:comment].to_s.empty?
      @choice.comment = nil
    else
      @choice.comment = params[:comment]
    end

    if params[:rating].to_s.empty?
      @choice.rating = nil
    else
      @choice.rating = params[:rating].to_i
    end

    @demand.customer_choices.push(@choice)
    @property.customer_choices.push(@choice)
    @demand.save
    @property.save
    if @choice.save
      AgencyMailer.customer_hate(@property.agency,@demand.customer,@property,@choice.choice,@choice.comment,@demand).deliver
      Rails.configuration.admin_emails.each do |email|
        AdminMailer.agency_dislike(email,@property.agency,@demand.customer,@property,@choice.choice,@choice.comment).deliver
      end

      if params[:no_auth]
        redirect_to access_success_path
      else
        redirect_to dashboard_customer_property_path(property_id: @property.id.to_s), notice: "Votre choix à bien été pris en compte."
      end

    else
      render dashboard_customer_property_path(property_id: @property.id.to_s), alert: "Oupss !! Un problème est survenu."
    end
  end

  def like
    @demand = V2Demand.find(params[:demand_id])
    @property = Property.find(params[:property_id])
    @agency = @property.agency
    @choice = CustomerChoice.new()
    @choice.choice = "J'aime"

    if params[:comment].to_s.empty?
      @choice.comment = nil
    else
      @choice.comment = params[:comment]
    end

    if params[:rating].to_s.empty?
      @choice.rating = nil
    else
      @choice.rating = params[:rating].to_i
    end

    @demand.customer_choices.push(@choice)
    @property.customer_choices.push(@choice)
    @demand.save
    @property.save
    if @choice.save
      AgencyMailer.customer_like(@agency,@demand.customer,@choice.comment,@demand,@property).deliver
      Rails.configuration.admin_emails.each do |email|
        AdminMailer.agency_like(email,@property.agency,@demand.customer,@property,@choice.choice,@choice.comment).deliver
      end

      if params[:no_auth]
        redirect_to access_success_path
      else
        redirect_to v2_customer_dashboard_property_path(property_id: @property.id.to_s), notice: "Votre choix à bien été pris en compte."
      end

    else
      render v2_customer_dashboard_property_path(property_id: @property.id.to_s), alert: "Oupss !! Un problème est survenu."
    end
  end

  def love
    @demand = V2Demand.find(params[:demand_id])
    @property = Property.find(params[:property_id])
    @choice = CustomerChoice.new()
    @agency = @property.agency
    @choice.choice = "J'adore"

    if params[:comment].to_s.empty?
      @choice.comment = nil
    else
      @choice.comment = params[:comment]
    end

    @demand.customer_choices.push(@choice)
    @property.customer_choices.push(@choice)
    @demand.save
    @property.save
    if @choice.save
      AgencyMailer.customer_like(@agency,@demand.customer,@choice.comment,@demand,@property).deliver
      Rails.configuration.admin_emails.each do |email|
        AdminMailer.agency_like(email,@property.agency,@demand.customer,@property,@choice.choice,@choice.comment).deliver
      end

      if params[:no_auth]
        redirect_to access_success_path
      else
        redirect_to v2_customer_dashboard_property_path(property_id: @property.id.to_s), notice: "Votre choix à bien été pris en compte."
      end

    else
      render v2_customer_dashboard_property_path(property_id: @property.id.to_s), alert: "Oupss !! Un problème est survenu."
    end
  end

end
