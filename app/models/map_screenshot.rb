class MapScreenshot
  require "net/http"
  require "net/https"
  require "uri"
  require "json"

  SCREENSHOT_API_KEY = "927de22d-378f-4710-9087-25f827f45b2c"

  def begin_capture(url,viewport,fullpage,webdriver,javascript)
    server_url = "https://api.screenshotapi.io/capture"
    puts "Sending request: #{server_url}"
    uri = URI.parse(server_url)
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(server_url, initheader = {'apikey' => SCREENSHOT_API_KEY})
    req.set_form_data({"url" => url, "viewport" => viewport, "fullpage" => fullpage, "webdriver" => webdriver, "javascript" => javascript})

    result = https.request(req)
    #{"status":"ready","key":"f469a4c54b4852b046c6f210935679ae"}
    json_results = JSON.parse(result.body)
    key = json_results["key"]
    puts "Capture request key is #{key}"
    return key
  end

  RetrieveResult = Struct.new(:success, :bytes)

  def try_retrieve(key)
    server_url = "https://api.screenshotapi.io/retrieve?key=" + key
    puts "Trying to retrieve: " + server_url

    uri = URI.parse(server_url)
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Get.new(server_url, initheader = {'apikey' => SCREENSHOT_API_KEY})
    result = https.request(req)
    json_results = JSON.parse(result.body)
    #{ "status":"ready","imageUrl":"http://screenshotapi.s3.amazonaws.com/captures/f469a4c54b4852b046c6f210935679ae.png"}
    return_result =  RetrieveResult.new

    if json_results["status"] == "ready"
      puts "Downloading image: " + json_results["imageUrl"]
      @image_url = json_results["imageUrl"]
      return_result.success = true
      image_result = Net::HTTP.get_response(URI.parse(json_results["imageUrl"]))
      return_result.bytes =  image_result.body
    else
      return_result.success = false
    end

    return return_result
  end


  def map_generation(demand_url, demand_id)
    formatted_url = "https://bientotchezsoi.com" + demand_url.to_s
    key = begin_capture(formatted_url,"1200x800","true","firefox","true")
    timeout = 120
    t_counter = 0
    t_count_increm = 3
    while true do
      return_result = try_retrieve(key)
      if return_result.success
        puts "Saving screenshot to the empty void of space !!!"
        outfile = File.new("demand-map-#{demand_id}-#{rand(0..100)}.png", 'wb')
        outfile.puts(return_result.bytes)
        @demand = Demand.find(demand_id)
        @demand.map_photo = File.open(outfile)
        @demand.save
        outfile.close
        break
      else
        t_counter += t_count_increm
        puts "Screenshot not yet ready.. waiting for: " + t_count_increm.to_s + " seconds."
        sleep(t_count_increm)
        if t_counter > timeout
          puts "Timed out while trying to retrieve: " + key
          break
        end
      end
    end
    @image_url
  end

end
