module WebhookHelper

  def french_phone(phone)
    raw_phone = Phonelib.parse(phone)
    raw_phone.national
  end

  def sms_price(price)
    "#{price.to_f.round(2)} €"
  end

  def age_calculation(time)
    (Time.now.to_s(:number).to_i - time.to_time.to_s(:number).to_i)/10e9.to_i
  end

end
