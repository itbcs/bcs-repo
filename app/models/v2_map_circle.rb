class V2MapCircle
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  embedded_in :v2_demand

  field :x, type: Float
  field :y, type: Float
  field :radius, type: Float
  field :circle_id, type: Integer
  field :layer_id, type: Integer
  field :auto_id, type: Integer

end
