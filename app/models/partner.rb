class Partner
  include Mongoid::Document
  include Mongoid::Timestamps

  before_save :sanitize_array

  mount_uploader :logo, PartnerLogoUploader

  field :name, type: String
  field :content, type: String
  field :widget_url, type: String
  field :api_url, type: String
  field :service_url, type: String
  field :iframe_url, type: String
  field :service_identifier, type: String
  field :service_password, type: String
  field :technology, type: String
  field :javascript_code, type: String
  field :widget_html_target, type: String
  field :targets, type: Array

  protected

  def sanitize_array
    targets.reject!{|t| t.empty?}
  end

end
