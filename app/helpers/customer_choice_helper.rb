module CustomerChoiceHelper

  def handle_customer_choice(customer_choice)
    if customer_choice.downcase == "je n'aime pas"
      icon = "<div class='icon-prop-delete icon-gray fonticon'></div>"
      text = "<div class='customer-choice-text'>Vous n'aimez pas ce bien !!</div>"
      result = ("<div class='customer-choice-recap'>#{icon}#{text}</div>").html_safe
    elsif customer_choice.downcase == "j'aime"
      icon = "<div class='icon-prop-plain-heart icon-green fonticon'></div>"
      text = "<div class='customer-choice-text'>Vous aimez ce bien !!</div>"
      result = ("<div class='customer-choice-recap'>#{icon}#{text}</div>").html_safe
    elsif customer_choice.downcase == "j'adore"
      icon = "<div class='icon-prop-fire icon-red fonticon'></div>"
      text = "<div class='customer-choice-text'>Vous adorez ce bien !!</div>"
      result = ("<div class='customer-choice-recap'>#{icon}#{text}</div>").html_safe
    end
    return result
  end

  def handle_customer_choice_agency_side(customer_choice)
    if customer_choice.downcase == "je n'aime pas"
      icon = generate_choice_svg("customer-hate", "black")
      text = "<div class='customer-choice-text'>Le membre n'aime pas ce bien !!</div>"
      result = ("<div class='customer-choice-recap'>#{icon}#{text}</div>").html_safe
    elsif customer_choice.downcase == "j'aime"
      icon = generate_choice_svg("customer-like", "green-text")
      text = "<div class='customer-choice-text'>Le membre aime ce bien !!</div>"
      result = ("<div class='customer-choice-recap'>#{icon}#{text}</div>").html_safe
    elsif customer_choice.downcase == "j'adore"
      icon = generate_choice_svg("customer-love", "red")
      text = "<div class='customer-choice-text'>Le membre adore ce bien !!</div>"
      result = ("<div class='customer-choice-recap'>#{icon}#{text}</div>").html_safe
    end
    return result
  end

  def handle_customer_response_waiting
    icon = "<div class='icon-clock icon-green fonticon'></div>"
    text = "<div class='customer-choice-text'>En attente de réponse !!</div>"
    result = ("<div class='customer-choice-recap'>#{icon}#{text}</div>").html_safe
  end

  def generate_choice_svg(filename, color)
    return ("<div class='inline-svg #{color} small-icon mr-10'>" + svg("#{filename}") + "</div>").html_safe
  end

end
