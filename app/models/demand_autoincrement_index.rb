class DemandAutoincrementIndex
  include Mongoid::Document
  include Mongoid::Timestamps

  field :property_type, type: String
  field :destination_type, type: String
  field :customer_type, type: Integer
  field :last_index, type: Integer, default: 1
  field :zipcode, type: Integer
  field :current_year, type: Integer

end
