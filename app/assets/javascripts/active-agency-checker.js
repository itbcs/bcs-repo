function handleAgencyStatus() {
  var agencyStatus = $(".activated-agency-checker").data("agency-checker");
  if (agencyStatus === "active") {
    console.log("Active agency");
  } else if (agencyStatus === "inactive") {
    console.log("Inactive agency");
    $("#agency-status-modal").modal("show");
  }
}


handleAgencyStatus();
