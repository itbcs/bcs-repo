class MatchingAlgoTest

  def initialize(demand)
    @demand = demand
    @errors = []
    @matching = []
  end

  def call
    automatic_matching
    service_success
  end

  def automatic_matching

    puts "**** Enter into automatic demand property matching ****"
    puts @demand.inspect
    puts "*******************************************************"

    if @demand[:property_type] == "21"
      @type = "Maison"
    elsif @demand[:property_type] == "22"
      @type = "Appartement"
    elsif @demand.property_type == "23"
      @type = "Terrain"
    end

    if @demand[:budget].size == 1
      result = handle_min_budget(@demand[:budget])
      @min_budget = result[:min]
      @max_budget = result[:max]
    elsif @demand[:budget].size == 2
      result = handle_max_budget(@demand[:budget])
      @min_budget = result[:min]
      @max_budget = result[:max]
    end


    puts "$$$$ BUDGET RANGE $$$$"
    puts @min_budget
    puts @max_budget
    puts "$$$$$$$$$$$$$$$$$$$$$$"

    results = Property.unsold.where(property_type: @type).where(:zipcode.in => @demand[:search_zipcodes_area]).where(price_int: {'$gte': @min_budget,'$lte': @max_budget})
    # results = Property.where(property_type: @type).where(:zipcode.in => @demand[:search_zipcodes_area])
    puts "MATCHING COUNT"
    puts "Biens trouvés: #{results.size}"

    if results != []
      results.each do |property|
        @matching << property
      end
    end

  end

  private

  def handle_min_budget(budget_array)
    min_budget = budget_array.first.to_i
    delta = (min_budget * 0.15).to_i
    min_budget = (min_budget - delta).to_i
    max_budget = (min_budget + delta).to_i
    return {min: min_budget, max: max_budget}
  end

  def handle_max_budget(budget_array)
    min_budget = budget_array.first.to_i
    max_budget = budget_array.last.to_i
    delta = (min_budget * 0.15).to_i
    min_budget = (min_budget - delta).to_i
    max_budget = (max_budget + delta).to_i
    return {min: min_budget, max: max_budget}
  end

  def service_success
    ServiceSuccess.new(@matching)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
