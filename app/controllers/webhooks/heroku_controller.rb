class Webhooks::HerokuController < ActionController::API
  include ApplicationHelper
  def builds
    event = JSON.parse(request.body.read)
    puts "---- webhooks ----"
    puts "----- BUILDS -----"
    puts event.inspect
    @webhook = Webhook.new(service: "#{controller_name}", action: "#{action_name}", description: "Web hosting service", logo: "heroku", content: event.to_h)
    if @webhook.save
      puts "Webhook saved !!"
      ActionCable.server.broadcast 'webhooks',
        content: @webhook.content.as_json,
        service: "#{@webhook.service.downcase.capitalize}",
        description: "#{@webhook.description}",
        event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
        logo: svg(@webhook.logo),
        filter_target: "#{controller_name.downcase}",
        dynamic_content: [{label: "Status",value: "#{@webhook.content['data']['status']}",html_class: "success"}, {label: "App",value: "#{@webhook.content['data']['app']['name']}",html_class: "neutral"}, {label: "Version",value: "#{@webhook.content['data']['version']}"}, {label: "Commit",value: "#{@webhook.content['data']['slug']['commit_description']}"}],
        custom_action: "#{@webhook.action.capitalize}"
    end
    puts "----------------------------"
    render :json => {status: 200}
  end

  def releases
    event = JSON.parse(request.body.read)
    puts "---- webhooks ----"
    puts "--------- RELEASES ---------"
    puts event.inspect
    @webhook = Webhook.new(service: "#{controller_name}", action: "#{action_name}", description: "Web hosting service", logo: "heroku", content: event.to_h)
    if @webhook.save
      puts "Webhook saved !!"
    end
    puts "----------------------------"
    render :json => {status: 200}
  end

  def app
    event = JSON.parse(request.body.read)
    puts "---- webhooks ----"
    puts "------ APP -------"
    puts event.inspect
    @webhook = Webhook.new(service: "#{controller_name}", action: "#{action_name}", description: "Web hosting service", logo: "heroku", content: event.to_h)
    if @webhook.save
      puts "Webhook saved !!"
    end
    puts "----------------------------"
    render :json => {status: 200}
  end

end
