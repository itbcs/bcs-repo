class AristidMailer < BaseMandrillMailer
  include Resque::Mailer

  def customer_initial_request(email,url,agency_name,phone)
    subject = "Commencez votre recherche immobilière"
    merge_vars = {
      "AGENCY" => agency_name,
      "AGENCYLINK" => url,
      "AGENCYTEL" => phone,
    }
    body = mandrill_template("trc-aristid-agency", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(email, subject, body, from, replyto)
  end

  def customer_new_survey(customer,webhook)
    puts "---- INSIDE CUSTOMER ARISTID ----"
    puts customer.inspect
    puts webhook.inspect
    puts customer["firstname"]
    puts customer["email"]
    subject = "Récapitulatif de votre recherche immobilière"
    merge_vars = {
      "FIRSTNAME" => customer["firstname"],
    }
    body = mandrill_template("trc-aristid-customer", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    pdf_result = generate_customer_pdf(webhook)
    file = pdf_result.first
    filename = pdf_result.last
    email = customer["email"]
    send_mail_with_attachments(email, subject, body, from, replyto, file, filename)
  end

  def agency_new_survey(webhook,email)
    puts "---- INSIDE AGENCY ARISTID ----"
    subject = "Un nouvel acquéreur est en rercherche de bien"
    merge_vars = {
      "FIRSTNAME" => webhook["content"]["customer"]["firstname"],
    }
    body = mandrill_template("tra-aristid-agency", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    pdf_result = generate_agency_pdf(webhook)
    file = pdf_result.first
    filename = pdf_result.last
    send_mail_with_attachments(email, subject, body, from, replyto, file, filename)
  end

  def generate_customer_pdf(webhook)
    filename = "recapitulatif_" + rand(1..1000000).to_s
    formatted_phone = phone_human_display(webhook["content"]["customer"]["phone_number"])
    formatted_budget = budget_human_display(webhook["content"]["survey"]["budget"])
    pdf = ApplicationController.new.render_to_string(layout: "pdf.html.haml", pdf: "aristid-test", template: "pdfs/aristid_survey.html.haml", encoding: "UTF-8", locals: {webhook: webhook, formatted_phone: formatted_phone, formatted_budget: formatted_budget})
    file = File.open("#{filename}.pdf", "w")
    file_path = File.join("#{filename}.pdf")
    file.puts(pdf.force_encoding("UTF-8"))
    file.close
    return [file_path,filename]
  end

  def generate_agency_pdf(webhook)
    filename = "recapitulatif_" + rand(1..1000000).to_s
    formatted_phone = phone_human_display(webhook["content"]["customer"]["phone_number"])
    formatted_budget = budget_human_display(webhook["content"]["survey"]["budget"])
    pdf = ApplicationController.new.render_to_string(layout: "pdf.html.haml", pdf: "aristid-test", template: "pdfs/aristid_survey_agency.html.haml", encoding: "UTF-8", locals: {webhook: webhook, formatted_phone: formatted_phone, formatted_budget: formatted_budget})
    file = File.open("#{filename}.pdf", "w")
    file_path = File.join("#{filename}.pdf")
    file.puts(pdf.force_encoding("UTF-8"))
    file.close
    return [file_path,filename]
  end

  def phone_human_display(phone)
    if Phonelib.valid?(phone)
      raw_phone = Phonelib.parse(phone)
      @formatted_phone = raw_phone.national
    else
      @formatted_phone = phone
    end
  end

  def budget_human_display(budget)
    sanitized_budget = budget.reject(&:blank?)
    if sanitized_budget != []
      "#{ActionController::Base.helpers.number_with_delimiter(sanitized_budget.first.to_i, locale: :fr)} €"
    else
      puts "No budget for this survey - an error occured !!"
    end
  end

end
