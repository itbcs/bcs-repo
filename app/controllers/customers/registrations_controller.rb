class Customers::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]

  def new
    super
  end

  # def create
  #   super
  # end

  def create
    build_resource(sign_up_params)
    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        # respond_with resource, location: after_sign_up_path_for(resource)
        render :json => {url: after_sign_up_path_for(resource)}
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        # respond_with resource, location: after_inactive_sign_up_path_for(resource)
        render :json => {connected: "Customer connected !!"}
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      render :json => {errors: resource.errors}
    end
  end

  def edit
    puts resource
    @demand = resource.v2_demands.last
    super
  end

  def update
    super
  end

  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute, :last_name, :first_name, :city, :country, :phone, :cgu, :promo_code])
  end

  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:attribute, :last_name, :first_name, :city, :country, :phone, :cgu, :promo_code])
  end

  def after_sign_up_path_for(resource)
    # customer_zone_path
    v2_customer_dashboard_path
  end

  def after_update_path_for(resource)
    # customer_zone_path
    v2_customer_dashboard_path
  end

end
