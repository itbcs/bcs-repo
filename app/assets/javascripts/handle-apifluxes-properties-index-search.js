function apifluxesPropsIndexSearch(){
  $("form.apifluxes-properties--search").submit(function(event){
    event.preventDefault();
    console.log(event);
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    $.ajax({
      url: url,
      processData: false,
      contentType: false,
      type: method,
      data: new FormData(this),
      beforeSend: function() {
        $("section.apifluxes-ajax-loading").removeClass("hidden");
        console.log("Before send action");
      },
      success: function(data) {
        console.log(data);
        $("section.apifluxes-ajax-loading").addClass("hidden");
        var rightWrapper = $(data).find("section.right-wrapper");
        $("section.right-wrapper").replaceWith(rightWrapper);
      },
      error: function(data) {
        alert("Ajax error call !!!");
      },
      complete: function(data) {
        console.log("Inside complete ajax callback");
        apifluxesPropsIndexPaginationLink();
        apifluxesAjaxPropertyShow();
      }
    });
  });
}


function apifluxesPropsIndexPaginationLink(){
  $(".pagination-ajax-link").click(function(event){
    console.log(this);
    var url = $(this).attr('href');
    var method = $(this).data('method');
    var text = $(this).text();
    $.ajax({
      url: url,
      processData: false,
      contentType: false,
      type: method,
      beforeSend: function() {
        $("section.apifluxes-ajax-loading-pagination").removeClass("hidden");
        $(".pagination-loading-text").text("Loading page " + text);
        console.log("Before send action");
      },
      success: function(data) {
        console.log(data);
        $("section.apifluxes-ajax-loading-pagination").addClass("hidden");
        var rightWrapper = $(data).find("section.right-wrapper");
        $("section.right-wrapper").replaceWith(rightWrapper);
      },
      error: function(data) {
        alert("Ajax error call !!!");
      },
      complete: function(data) {
        console.log("Inside complete ajax callback");
        apifluxesPropsIndexPaginationLink();
        apifluxesAjaxPropertyShow();
      }
    });
  });
}


function apifluxesAjaxPropertyShow() {
  var showAjaxLinks = document.querySelectorAll(".custom-ajax-link");
  console.log(showAjaxLinks.length);
  showAjaxLinks.forEach(function(item) {
    var url = $(item).data('url');
    var method = $(item).data('method');
    var text = $(item).data("title");
    console.log(url);
    item.addEventListener("click",function(e) {
      $.ajax({
        url: url,
        processData: false,
        contentType: false,
        type: method,
        beforeSend: function() {
          $("section.apifluxes-ajax-loading-pagination").removeClass("hidden");
          $(".pagination-loading-text").text("Loading page " + text);
          console.log("Before send action");
        },
        success: function(data) {
          console.log(data);
          $("section.apifluxes-ajax-loading-pagination").addClass("hidden");
          $("section.apifluxes-properties--index").append(data);
        },
        error: function(data) {
          alert("Ajax error call !!!");
        },
        complete: function(data) {
          console.log("Inside complete ajax callback");
          handleShowSectionCloser();
        }
      });
    })
  });
}



function handleShowSectionCloser() {
  var customSectionCloser = document.querySelector(".custom-section-closer");
  customSectionCloser.addEventListener("click", function(e) {
    document.querySelector("section.show-wrapper").remove();
  });
}




apifluxesPropsIndexSearch();
