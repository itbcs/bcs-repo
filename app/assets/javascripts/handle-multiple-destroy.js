var countChecked = function() {
  if ($(this).is(':checked')){
    var fullTr = $(this).parent().parent().parent();
    $(fullTr).addClass("colored-tr");
  } else {
    var fullTr = $(this).parent().parent().parent();
    $(fullTr).removeClass("colored-tr");
  }
  var n = $( "input:checked" ).length;
  if (n === 0){
    $('.multiple-delete-size-container').text("");
    console.log("O checked input");
  } else {
    $(".multiple-delete-size-container").text(n + (n === 1 ? " demande à effacer" : " demandes à effacer"));
  }
};

// $("input[type=checkbox]").on("click", countChecked);
