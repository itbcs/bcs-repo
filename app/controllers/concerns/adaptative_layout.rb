module AdaptativeLayout

  def self.views_without_navbar
    ["apifluxes/properties","apifluxes/agencies","employees/sessions","employees/registrations"]
  end

  def self.views_without_footer
    ["apifluxes/properties","apifluxes/agencies","v2/new-survey","employees/sessions","employees/registrations"]
  end

end
