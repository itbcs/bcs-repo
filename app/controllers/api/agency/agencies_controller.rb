class Api::Agency::AgenciesController < Api::Agency::BaseController

  def show
    if current_agency
      puts '---- Inside show action (Agency) ----'
      puts current_agency.inspect
    end
  end

  def status
    @boolean_fields = generate_boolean_fields
    if current_agency
      puts '---- Inside status action (Agency) ----'
    end
    render json: {status: "connected", connected_at: "#{current_agency.current_sign_in_at}", email: current_agency.email, services: @boolean_fields}.as_json, status: :ok
  end

  private

  def generate_boolean_fields
    if current_agency
      current_agency.attributes.extract!("partner","activated","is_renter","active_renter").as_json
    end
  end

end
