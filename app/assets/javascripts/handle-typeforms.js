function closeTypeformModal() {
  $("#demand-modifier-modal.custom-modal-container").addClass("hidden");
  $("#typeform-modifier-form-modal.custom-modal-container").removeClass("hidden");
}

function closeDeactivateTypeformModal() {
  $("#demand-deactivate-modal.custom-modal-container").addClass("hidden");
  $("#typeform-deactivate-form-modal.custom-modal-container").removeClass("hidden");
}




function handleTypeform() {
  var embedElement = document.querySelector('#demand-modifier-modal .custom-modal-content')
  if (embedElement) {
    typeformEmbed.makeWidget(
      embedElement,
      'https://bientotchezsoi.typeform.com/to/big4qV' + "?firstname=" + gon.firstname + "&email=" + gon.email + "&id_demande=" + gon.id_demande + "&lastname=" + gon.lastname,
      {
        hideHeaders: false,
        hideFooter: false,
        buttonText: "Take the survey!",
        onSubmit: function () {
          handleTypeform();
          setTimeout(closeTypeformModal,1000);
        }
      }
    )
  }
}


function handleDeactivateTypeform() {
  var embedElement = document.querySelector('#demand-deactivate-modal .custom-modal-content')
  if (embedElement) {
    typeformEmbed.makeWidget(
      embedElement,
      'https://bientotchezsoi.typeform.com/to/wkqEAO' + "?firstname=" + gon.firstname + "&email=" + gon.email + "&id_demande=" + gon.id_demande + "&lastname=" + gon.lastname,
      {
        hideHeaders: false,
        hideFooter: false,
        buttonText: "Take the survey!",
        onSubmit: function () {
          handleDeactivateTypeform();
          setTimeout(closeDeactivateTypeformModal,1000);
        }
      }
    )
  }
}


$(document).ready(function(){
  handleTypeform();
})

$(document).ready(function(){
  handleDeactivateTypeform();
})




