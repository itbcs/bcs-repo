class PdfProperty
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :agency

  field :name, type: String
  field :status, type: String
  mount_uploader :photo, ImageUploader

end
