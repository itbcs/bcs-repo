# frozen_string_literal: true

# class BlogCategory
class BlogCategory
  include Mongoid::Document
  has_many :posts

  field :name, type: String
  field :lname, type: String
  field :h1_content, type: String
  field :title, type: String
  field :desc, type: String
  field :content, type: String
end
