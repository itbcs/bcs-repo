class Estimation
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  field :localisation, type: String
  field :property_type, type: String
  field :living_space, type: String
  field :quality, type: String
  field :email, type: String
  field :firstname, type: String
  field :lastname, type: String
  field :phone, type: String
  field :comment, type: String
  field :results, type: Hash
  field :estimation_value, type: String
  field :contact_require, type: Boolean

  # validates_presence_of :localisation, :property_type, :living_space, :quality
  validates_presence_of :email, :firstname, :lastname

end
