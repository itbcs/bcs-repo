class CandidaciesController < ApplicationController

  def create
    @candidacy = Candidacy.new(safe_params)
    if @candidacy.save
      redirect_to jobs_path, notice: "Votre demand à été prise en compte !!"
    else
      render jobs_path, alert: "Un problème est survenu !!"
    end
  end

  private

  def safe_params
    params.require(:candidacy).permit(:firstname, :lastname, :email, :phone, :jobname, :message)
  end

end
