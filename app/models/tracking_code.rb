# frozen_string_literal: true

# class TrackingCode
class TrackingCode
  include Mongoid::Document
  has_many :customers

  field :code, type: String
  field :start_date, type: Integer
  field :end_date, type: String
  field :status, type: String
end
