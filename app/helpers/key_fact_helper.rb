module KeyFactHelper

  def keyfact_icon_type(keyfact)
    if keyfact.number_type == "duration"
      html = "<div class='three-right-top'>" + "<div class='logo'>" + "#{generate_image_tag}" + "</div>" + "<div class='number'>" + "#{keyfact.number}" + "</div>" + "</div>" + "<div class='three-right-body'>" + keyfact.text + "</div>"
    elsif keyfact.number_type == "quantity"
      html = "<div class='three-right-top'>" + "<div class='number'>" + "#{keyfact.number}" + "</div>" + "</div>" + "<div class='three-right-body'>" + keyfact.text + "</div>"
    elsif keyfact.number_type == "percent"
      html = "<div class='svg-container'>" + "<div class='svg-container-value' data-value='#{keyfact.number}'>#{keyfact.number} %</div>" + generate_svg + "</div>" + "<div class='three-left-body'>" + keyfact.text + "</div>"
    elsif keyfact.number_type.downcase == "durée"
      html = "<div class='three-right-top'>" + "<div class='logo'>" + "#{generate_image_tag(keyfact.photo_url)}" + "</div>" + "<div class='number'>" + "#{keyfact.number}" + "</div>" + "</div>" + "<div class='three-right-body'>" + keyfact.text + "</div>"
    elsif keyfact.number_type.downcase == "quantité"
      html = "<div class='three-right-top'>" + "<div class='logo'>" + "#{generate_image_tag(keyfact.photo_url)}" + "</div>" + "<div class='number'>" + "#{keyfact.number}" + "</div>" + "</div>" + "<div class='three-right-body'>" + keyfact.text + "</div>"
    elsif keyfact.number_type.downcase == "pourcentage"
      html = "<div class='three-right-top'>" + "<div class='logo'>" + "#{generate_image_tag(keyfact.photo_url)}" + "</div>" + "<div class='number'>" + "#{keyfact.number}" + "</div>" + "</div>" + "<div class='three-right-body'>" + keyfact.text + "</div>"
    end
    return html.html_safe
  end

  def generate_svg
    svg = "<svg viewbox='-1 -1 34 34' xmlns='http://w3.org/2000/svg'>" + "<circle class='progress-bar__background' cx='16' cy='16' r='15.9155'></circle>" + "<circle class='progress-bar__progress js-progress-bar' cx='16' cy='16' r='15.9155'></circle>" + "</svg>"
    return svg
  end

  def generate_image_tag(photo_url = nil)
    if photo_url
      image = ActionController::Base.helpers.image_tag("#{photo_url}", width: "50")
    else
      image = ActionController::Base.helpers.image_tag("https://cdn.bientotchezsoi-dev.com/images/chiffre-BCS.svg", width: "50")
    end
    return image
  end

end


