require 'rubygems'
require 'sitemap_generator'

SitemapGenerator::Sitemap.default_host = ENV['SITE_URL']
SitemapGenerator::Interpreter.send :include, SitemapUtility
SitemapGenerator::Sitemap.create do
    add '/'
    add '/mon-compte'
    add '/mon-agence'
    add '/jobs'
    add '/je-cherche/map'
    add '/je-cherche/questionnaire'
    add '/je-cherche'
    add '/je-vends'
    add '/je-m-informe'
    add '/je-construis'
    add '/contact'
    generate_blog_links
    generate_job_links
end
#SitemapGenerator::Sitemap.ping_search_engines
