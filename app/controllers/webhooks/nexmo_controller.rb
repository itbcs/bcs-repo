class Webhooks::NexmoController < ActionController::API
  include ApplicationHelper
  def delivery
    if params
      event = params.as_json.to_h
      puts "---- webhooks ----"
      puts "----- BUILDS -----"
      @webhook = Webhook.new(service: "#{controller_name}", action: "#{action_name}", description: "Sms sending service", logo: "nexmo", content: event.to_h)
      if @webhook.save
        puts "Webhook saved !!"
        ActionCable.server.broadcast 'webhooks',
          content: @webhook.content.as_json,
          service: "#{@webhook.service.downcase.capitalize}",
          description: "#{@webhook.description}",
          event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
          logo: svg(@webhook.logo),
          filter_target: "#{controller_name.downcase}",
          custom_action: "#{@webhook.action.capitalize}"
      end
      puts "----------------------------"
    end
    render :json => {status: 200}
  end

end
