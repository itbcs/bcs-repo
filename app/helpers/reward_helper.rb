module RewardHelper

  def avatar_list(score)
    avatars = [["https://cdn.bientotchezsoi-dev.com/images/castoriginal.svg", "Vous êtes déjà quelqu'un de très spécial pour nous", "Cast'Original"],
                ["https://cdn.bientotchezsoi-dev.com/images/castorpailleur.svg", "On a les bases pour vous trouver la pépite… ", "Cast'Orpailleur"],
                ["https://cdn.bientotchezsoi-dev.com/images/castorsuper.svg", "Bravo, vous avez atteint le niveau suprême !", "CastorSuper"],
                ["https://cdn.bientotchezsoi-dev.com/images/castorfevre.svg", "Une recherche bien taillée, au moins 36 carats !", "Cast'Orfevre"],
                ["https://cdn.bientotchezsoi-dev.com/images/castoracle.svg", "Je vois…. Une maison, très bientôt !", "Cast'Oracle"]]
    if score == 0
      active_index = 0
    elsif score == 50
      active_index = 1
    elsif score == 100
      active_index = 2
    elsif score == 150
      active_index = 3
    elsif score > 150
      active_index = 4
    end

    @test = []

    avatars.each_with_index do |avatar, index|
      if index == active_index
        @test << [ActionController::Base.helpers.image_tag(avatar.first, class: 'rank-avatar active'), avatar[1], avatar.last]
      end
    end
    return @test
  end

end


