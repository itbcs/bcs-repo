# frozen_string_literal: true

# class Demands
class Demand
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  include Mongoid::History::Trackable

  track_history :on => [:fields],
                modifier_field_optional: true,
                track_update: true,
                track_create: false,
                track_destroy: false


  belongs_to :customer
  has_and_belongs_to_many :properties
  has_many :customer_advices
  has_many :customer_choices
  has_many :demand_agency_views
  has_many :chatrooms
  has_many :demand_properties

  embeds_many :map_circles
  embeds_many :demand_comments
  mount_uploader :map_photo, MapUploader
  mount_uploader :browser_screenshot, BrowserScreenshotUploader

  field :banker_contact, type: String
  field :broker_contact, type: String
  field :completed, type: Boolean, default: false
  field :activated, type: Boolean, default: false
  field :deactivated, type: Boolean, default: false
  field :deactivation_reason, type: String
  field :deactivation_comment, type: String
  field :refused, type: Boolean, default: false
  field :old_platform_user_id, type: Integer
  field :localisation, type:  String
  field :postal_codes, type:  Array
  field :perimeter, type: String
  field :project, type: String
  field :actual_situation, type: String
  field :property_type, type: String
  field :destination, type: String
  field :flat_state, type: String
  field :house_state, type: String
  field :travaux, type: Array
  field :budget, type: Array
  field :flat_budget, type: Array
  field :terrain_budget, type: Array
  field :house_budget, type: Array
  field :style, type: Array
  field :surface, type: Array
  field :terrain_surface, type: Array
  field :house_construction_surface, type: Array
  field :rooms, type: String
  field :floor, type: String
  field :contact_constructor, type: String
  field :principal_space, type: Array
  field :kitchen, type: String
  field :bathrooms, type: String
  field :more_rooms, type: Array
  field :equipements, type: Array
  field :accessibility, type: Array
  field :flat_stairs, type: Array
  field :house_stairs, type: Array
  field :exposition, type: Array
  field :parking, type: Array
  field :security, type: Array
  field :heating, type: Array
  field :heating_type, type: Array
  field :dpe, type: Array
  field :flat_exterior, type: Array
  field :house_exterior, type: Array
  field :age, type: String
  field :actual_pros, type: Array
  field :actual_housing, type: String
  field :actual_housing_area, type: Array
  field :installation_date, type: String
  field :last_precision, type: Array
  field :heart_stroke, type: Array
  field :status, type: String, default: "pending"
  field :comments, type: String
  field :question_position, type: Integer
  field :important_fields, type: Array
  field :localisation_lat, type: Float
  field :localisation_lng, type: Float
  field :sending_customer_notification, type: Boolean, default: false
  field :fetch_map_photo, type: Boolean, default: false
  field :old_platform_created_at, type: Date
  field :from_oldplatform, type: Boolean, default: false
  field :sending_customer_recap_email, type: Boolean, default: false
  field :updated_by_customer, type: Boolean, default: false

  def validate?
    self.status == "success"
  end

  def failed?
    self.status == "fail"
  end

  def pending?
    self.status == "pending"
  end



  def self.unviewed_offers
    @final_results = {}
    @results = {}
    @demands_with_props = Demand.where(activated: true)
    @demands_with_props.each do |demand|
      @results[demand.id.to_s] = []
      if demand.properties.any?
        demand.properties.each do |prop|
          if !CustomerChoice.where(property_id: prop.id.to_s).where(demand_id: demand.id.to_s).last
            @results[demand.id.to_s] << prop.id.to_s
          end
        end
      end
    end
    @results.each do |key,value|
      if value != []
        @demand = Demand.find(key)
        @customer_name = Demand.find(key).customer.first_name
        @customer_email = Demand.find(key).customer.email
        @link = "https://bientotchezsoi.com/customers/connexion"
        @dates = []
        value.each do |prop|
          @prop = Property.find(prop)
          if @prop.price
            good_price = @prop.price
          elsif @prop.prix
            good_price = @prop.prix
          end

          @demand_property = DemandProperty.where(demand_id: @demand.id.to_s).where(property_id: @prop.id.to_s).last

          if @demand_property
            @proposed_date = @demand_property.created_at
            time_difference = (Date.today - @proposed_date.to_date).to_i
            if time_difference <= 3
              puts time_difference
              @dates << ["#{ActionController::Base.helpers.number_with_delimiter(good_price.strip.gsub(/[[:space:]]/,'').to_i, locale: :fr)} €", I18n.localize(@proposed_date, format: :medium), @prop.property_type, "#{time_difference} Jours", @link, @customer_name]
            end
          end
        end

        @final_results[@customer_email] = @dates
      end
    end
    @filtered_results = {}

    @final_results.each do |key,value|
      if value != []
        @filtered_results[key] = value
      end
    end

    # Normally return @filtered_results
    # return @filtered_results
    return @filtered_results
  end


end
