class CustomerGeolocSearchArea

  def initialize(geoloc_params)
    @params = geoloc_params
    @zipcodes = []
    @cities = []
    @fullresults = []
    @cities_zipcodes = []
    @errors = []
  end

  def call
    handle_areas
    service_success
  end

  private

  def handle_areas
    @params.to_a.each do |placeid|
      response = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?place_id=#{placeid}&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA")
      @result = (JSON.parse(response.body.to_s)) if response.code == 200
      puts "---- RESULTS ----"
      puts @result
      puts "-----------------"
      @fullresults << @result.to_h

      puts "---- RESULT ADDRESS COMPONENTS ----"
      address_component = @result["results"][0]["address_components"]
      geometry = @result["results"][0]["geometry"]
      puts address_component
      puts "-----------------------------------"

      @temp_hash = {}

      if geometry
        geometry.each do |hashed_values|
          puts "---- GEOMETRY ----"
          puts hashed_values
          puts "------------------"
          if hashed_values.first == "location"
            @lat = hashed_values.last["lat"]
            @lng = hashed_values.last["lng"]
            @temp_hash[:lat] = @lat
            @temp_hash[:lng] = @lng
          end
        end
      end

      if address_component
        address_component.each do |hashed_values|
          if hashed_values["types"] == ["postal_code"]
            @zipcodes << hashed_values["short_name"]
            @temp_hash[:zipcode] = hashed_values["short_name"]
          elsif hashed_values["types"] == ["locality", "political"]
            @cities << hashed_values["short_name"]
            @temp_hash[:city] = hashed_values["short_name"]
          elsif hashed_values["types"] == ["administrative_area_level_1", "political"]
            if hashed_values["short_name"].to_s.downcase == "brittany"
              @temp_hash[:district] = "Bretagne"
            else
              @temp_hash[:district] = hashed_values["short_name"]
            end
          elsif hashed_values["types"] == ["administrative_area_level_2", "political"]
            @temp_hash[:department] = hashed_values["short_name"]
          elsif hashed_values["types"] == ["street_number"]
            @temp_hash[:street_number] = hashed_values["short_name"]
          elsif hashed_values["types"] == ["route"]
            @temp_hash[:street_name] = hashed_values["short_name"]
          else
            puts "There is no zipcode in this query !!!!"
          end
        end
        @cities_zipcodes << @temp_hash
      end
    end
    puts "---- FULL RESULTS ----"
    puts "Taille de la réponse: #{@fullresults.size}"
    puts @fullresults
    puts "----------------------"

    puts "---- ZIPCODES ----"
    puts @zipcodes.inspect
    puts "------------------"

    puts "---- CITIES ----"
    puts @cities.inspect
    puts "----------------"

    puts "---- CITIES PLUS ZIPCODES ----"
    puts @cities_zipcodes.inspect
    puts "------------------------------"

    puts "---- UNIQ ZIPCODES ----"
    puts @zipcodes.uniq.inspect
    puts "------------------"
  end

  def service_success
    ServiceSuccess.new(@cities_zipcodes)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
