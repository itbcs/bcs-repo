# frozen_string_literal: true

Rails.application.configure do

  GoogleTagManager.gtm_id = "GTM-P7QGJW3"
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # config.action_cable.url = "ws://localhost:3000/cable"
  config.web_socket_server_url = "wss://bientotchezsoi-dev.herokuapp.com/cable"
  config.action_cable.allowed_request_origins = ['https://bientotchezsoi-dev.herokuapp.com']

  # Enable/disable caching. By default caching is disabled.
  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.seconds.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  config.action_mailer.perform_caching = false
  config.action_mailer.perform_deliveries = true

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log
  config.active_job.queue_adapter     = :resque
  #config.action_mailer.default_url_options = { :host => "https://bientotchezsoi-dev.herokuapp.com/" }

  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address:              ENV.fetch("SMTP_MANDRILL_ADDRESS"),
    port:                 587,
    domain:               ENV.fetch("SMTP_MANDRILL_DOMAIN"),
    user_name:            ENV.fetch("SMTP_MANDRILL_USERNAME"),
    password:             ENV.fetch("SMTP_MANDRILL_PASSWORD"),
    authentication:       'plain',
    enable_starttls_auto: true  }
  config.action_mailer.default_url_options = { host: ENV["SMTP_MANDRILL_DOMAIN"] }


  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
end
