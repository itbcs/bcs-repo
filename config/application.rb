# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
# require "active_record/railtie"
require "action_cable/engine"
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'sprockets/railtie'
require 'rails/test_unit/railtie'
require 'i18n/backend/fallbacks'


# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Bcs
  class Application < Rails::Application
    config.load_defaults 5.1
    config.action_view.embed_authenticity_token_in_remote_forms = true

    config.autoload_paths << Rails.root.join('lib')

    config.middleware.use Rack::Deflater
    config.time_zone = 'Europe/Paris'

    config.exceptions_app = self.routes
    # change i18n local
    config.i18n.default_locale = :fr

    config.admin_emails = ["annegaelle@bientotchezsoi.com","laure@bientotchezsoi.com","philippe@bientotchezsoi.com","aurelien@bientotchezsoi.com"]
    #TODO: A rendre generique

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)
    config.i18n.fallbacks = {'fr' => 'en'}
  end
end
