class V2::CustomersController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_customer!, only: [:home]

  def journey
    @customer = current_customer
    @demand = @customer.v2_demands.last
  end

  def selection
    @customer = current_customer
    @demand = @customer.v2_demands.last
    @props_with_choices = @demand.customer_choices.where(choice: {"$ne" => "Je n'aime pas"})
    if @props_with_choices.size > 0
      puts "There is some positive choices for #{@customer.email} demand"
    else
      puts "There is no positive choices for #{@customer.email} demand"
    end
  end

  def advices
    @customer = current_customer
    @demand = @customer.v2_demands.last
    @target_posts = @demand ? handle_customer_blog_posts(@customer) : Post.all
    if unseen_messages_count != nil
      @count = unseen_messages_count
    end
  end

  def handle_customer_blog_posts(customer)
    if customer.status
      puts "**** CUSTOMER STATUS ****"
      puts customer.status
      puts "*************************"
      @posts = Post.where(targets: { '$regex': customer.status })
    else
      @posts = Post.all
    end
    @targets = @posts.to_a.map{|post| post.targets}
    puts "**** TARGETS ****"
    puts @targets.inspect
    @posts
  end


  def my_little_mores
    @demand = current_customer.v2_demands.last

    result = HandleBadges.new(@demand).call
    if result.success?
      @badges = result.data
      puts "**** Handle badges service ****"
      puts @badges
      puts "*******************************"
    end

    if result.success?
      new_badge_result = AddBadgesToCustomer.new(current_customer, result.data).call
      if new_badge_result.success?
        if new_badge_result.data != []
          @new_badges = new_badge_result.data
        end
      end
    end

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

  end

  def messages
    @customer = current_customer
    @demand = @customer.v2_demands.last
    @customer_chatrooms = Chatroom.where(customer_id: @customer.id.to_s).to_a.select{|cr| cr.chat_messages.size != 0 }

    @unseen_messages = {}
    @customer_chatrooms.each do |chatroom|
      unseen_messages = chatroom.chat_messages.where(seen_by_customer: false)
      if unseen_messages.to_a != []
        @unseen_messages[chatroom] = []
        @unseen_messages[chatroom] << unseen_messages.to_a
      else
        puts "Empty results"
      end
    end

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    puts @unseen_messages
    render :messages
  end

  def home
    @post = Post.all.last
    if session[:bcs_customer_id]
      @temporary_customer = Customer.find(session[:bcs_customer_id])
      if @temporary_customer
        if @temporary_customer.v2_demands.any?
          @demand = @temporary_customer.v2_demands.last.clone
          @demand.remote_browser_screenshot_url = @temporary_customer.v2_demands.last.map_photo_url
          @customer = current_customer
          @customer.v2_demands.push(@demand)
          @demand.save
          @customer.save

          Rails.configuration.admin_emails.each do |email|
            AdminMailer.new_demand_recap(@demand,@customer,email,l(@demand.created_at, format: :medium),l(@demand.created_at, format: :count)).deliver
          end

          session.delete(:bcs_customer_id)
        else
          session.delete(:bcs_customer_id)
          @customer = current_customer
        end
      else
        session.delete(:bcs_customer_id)
        @customer = current_customer
        @demand = @customer.v2_demands.last
        @properties = @demand.properties

        customer_properties_images(@properties)
      end
    else
      @customer = current_customer
      @demand = @customer.v2_demands.last
      @properties = @demand.properties

      customer_properties_images(@properties)
    end

    @man = ["petit-prince", "aladdin", "cesar", "churchill", "kurt-cobain", "van-gogh"].sample
    @woman = ["amelie-poulain", "cleopatre", "coco-chanel", "leia", "raiponce", "sissi"].sample

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    result = HandleBadges.new(@demand).call
    if result.success?
      @badges = result.data
      puts "**** Handle badges service ****"
      puts @badges
      puts "*******************************"
    end

    if result.success?
      new_badge_result = AddBadgesToCustomer.new(current_customer, result.data).call
      if new_badge_result.success?
        if new_badge_result.data != []
          @new_badges = new_badge_result.data
        end
      end
    end

    @target_posts = @demand ? handle_customer_blog_posts(@customer) : Post.all
    # @loc_id = "dDUH3i"
    # response = RestClient.get("https://api.typeform.com/forms/#{@loc_id}/responses?completed=true", {Authorization: "Bearer GwcruNPuS6UEzxi3iSxZPDXsYx3J2Ux12m5MRvrRbGdb"})
    # puts "$$$$ API TYPEFORM $$$$"
    # JSON.parse(response.body)["items"].each do |form|
    #   puts "§§§§ API RESPONSES ITEM §§§§"
    #   form.to_h.each do |key,value|
    #     if key == "hidden"
    #       puts "HIDDEN FIELDS --> #{value}"
    #     end
    #     if key == "answers"
    #       value.each do |key1,value1|
    #         puts "ANSWERS --> #{key1} --> #{value1}"
    #       end
    #     end
    #   end
    # end

    gon.push({
      firstname: "#{@customer.first_name}",
      email: "#{@customer.email}",
      lastname: "#{@customer.last_name}",
      id_demande: "#{@demand.id.to_s}"
    })

    if @customer.badges
      @customer_points = 0
      @customer.badges.each do |badge_hash|
        @customer_points += badge_hash["points"].to_i
      end
    end

    render :home
  end

  def properties
    @customer = current_customer
    @demand = @customer.v2_demands.last
    @properties = @demand.properties

    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    @properties.each do |prop|
      @imgs_attrs.each do |field|
        puts prop.send("#{field}")
        if !prop.send("#{field}").nil?
          @present_imgs << (field.to_s.remove("img_").to_i)
        end
      end
      @customer_choices = prop.customer_choices.where(demand_id: @demand.id).last
      @present_imgs
    end

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    render :properties
  end

  def customer_unlike_choice_ajax
    @demand = V2Demand.find(params[:demand_id])
    @property = Property.find(params[:property_id])

    puts "***** PARAMS *****"
    puts params
    puts "******************"


    render partial: "v2/customers/partials/properties_unlike_ajaxify"
  end

  def customer_like_choice_ajax
    @demand = V2Demand.find(params[:demand_id])
    @property = Property.find(params[:property_id])


    puts "***** PARAMS *****"
    puts params
    puts "******************"


    render partial: "v2/customers/partials/properties_like_ajaxify"
  end

  def property
    @customer = current_customer
    @demand = @customer.v2_demands.last
    @property = Property.find(params[:property_id])
    @agency = @property.agency
    @papernest = Partner.where(name: "Papernest").last
    @needhelp = Partner.where(name: "needhelp").last

    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    @imgs_attrs.each do |field|
      puts @property.send("#{field}")
      if !@property.send("#{field}").nil?
        @present_imgs << (field.to_s.remove("img_").to_i)
      end
    end
    @present_imgs

    @chatroom = Chatroom.where(property_id: @property.id.to_s).where(customer_id: @customer.id.to_s).last
    @customer_choices = @property.customer_choices.where(demand_id: @demand.id).last

    gon.push({
      lat: @property.lat,
      lng: @property.lng,
      title: @property.title,
    })

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    if unseen_property_messages_count(@property).size > 0
      @prop_messages_count = unseen_property_messages_count(@property)
    end


    @photos = handle_property_photos(@property)
    @demand = @customer.v2_demands.last
    @unseen_customer_messages = []
    @unseen_agency_messages = []

    @customer_choice = @property.customer_choices.where(v2_demand_id: @demand.id).last

    if @customer_choice
      puts "***********************"
      puts "Customer choice present"
      puts "***********************"
    else
      puts "***********************"
      puts "Customer choice not present"
      puts "***********************"
    end


    if @chatroom
      if @chatroom.chat_messages != []
        if customer_signed_in?
          handle_unseen_customer_messages(@chatroom.chat_messages)
        end
        if agency_signed_in?
          handle_unseen_agency_messages(@chatroom.chat_messages)
        end
      end
    else
      @chatroom = Chatroom.create(property: @property, customer: @customer, agency: @property.agency, v2_demand: @demand)
    end

    @messages = @chatroom.chat_messages
    @chatroom.save
    @chat_message = ChatMessage.new

    if params[:redirect_with_tab_target]
      @tab_target = true
    end

    @property_customer_view = PropertyCustomerView.where(property: @property).where(customer: @customer).last
    if @property_customer_view
      puts "The customer already seen this page"
    else
      @new_property_customer_view = PropertyCustomerView.new(property: @property, customer: @customer, view: true)
      if @new_property_customer_view.save
        puts "Successfully saved customer prop view"
        Rails.configuration.admin_emails.each do |email|
          AdminMailer.admin_customer_property_view(@property.agency,@customer,@property,email).deliver
        end

        event = {agency_name: @property.agency.name, customer_fullname: "#{@customer.first_name} #{@customer.last_name}", property_title: @property.title}
        @webhook = Webhook.new(service: "BientotChezSoi", action: "Consultation d'un bien", description: "Action interne", logo: "kenotte-avatar", content: event.to_h)
        if @webhook.save
          ActionCable.server.broadcast 'webhooks',
            content: @webhook.content.as_json,
            service: "#{@webhook.service.downcase.capitalize}",
            description: "#{@webhook.description}",
            event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
            logo: svg(@webhook.logo),
            dynamic_content: [{label: "Agence",value: "#{@webhook.content['agency_name']}"}, {label: "Membre",value: "#{@webhook.content['customer_fullname']}"}, {label: "Bien consulté",value: "#{@webhook.content['property_title']}"}],
            filter_target: "bientotchezsoi",
            custom_action: "#{@webhook.action.capitalize}"
        end
      end
      puts "The customer doesn't see this page"
    end


    render :property
  end

  private

  def unseen_property_messages_count(property)
    @count = 0
    @customer = current_customer
    @property_chatroom = Chatroom.where(customer_id: @customer.id.to_s).where(property_id: property.id.to_s).last

    if @property_chatroom
      unseen_messages = @property_chatroom.chat_messages.where(seen_by_customer: false)
      if unseen_messages.to_a != []
        @count = unseen_messages.to_a.size
      else
        @count = 0
      end
    end
    return @count
  end

  def unseen_messages_count
    @count = 0
    @customer = current_customer
    @customer_chatrooms = Chatroom.where(customer_id: @customer.id.to_s)

    @unseen_messages = {}
    @customer_chatrooms.each do |chatroom|
      unseen_messages = chatroom.chat_messages.where(seen_by_customer: false)
      if unseen_messages.to_a != []
        @unseen_messages[chatroom] = []
        @unseen_messages[chatroom] << unseen_messages.to_a
      end
    end

    @unseen_messages.each do |chatroom,chat_messages|
      chat_messages.each do |chatroom_message|
        chatroom_message.each do |msg|
          if msg.seen_by_customer == false
            @count += 1
          end
        end
      end
    end

    if @count == 0
      return nil
    else
      return @count
    end
  end

  def customer_properties_images(properties)
    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    properties.each do |prop|
      @imgs_attrs.each do |field|
        puts prop.send("#{field}")
        if !prop.send("#{field}").nil?
          @present_imgs << (field.to_s.remove("img_").to_i)
        end
      end
      @present_imgs
    end
  end



  def handle_unseen_customer_messages(messages)
    results = []
    messages.each do |mess|
      mess.seen_by_customer = true
      mess.save
    end
  end

  def handle_unseen_agency_messages(messages)
    results = []
    messages.each do |mess|
      mess.seen_by_agency = true
      mess.save
    end
  end

  def handle_property_photos(property)
    results = []
    fields = [:img_1_url, :img_2_url, :img_3_url, :img_4_url, :img_5_url, :img_6_url, :img_7_url, :img_8_url]
    fields.each do |field|
      remote_photo = property.send("#{field.to_s}")
      if remote_photo
        results << remote_photo
      end
    end
    return results
  end




end
