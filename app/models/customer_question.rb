class CustomerQuestion
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :customer

  field :message, type: String

end
