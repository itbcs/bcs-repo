function formatNumericInput() {
  $(".numeric-formating-input").keyup(function() {
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
      return;
    }
    var input = $(this).val();
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt( input, 10 ) : 0;
    $(this).val( function() {
      return ( input === 0 ) ? "" : input.toLocaleString("fr-FR");
    });
  });
}



function v2HintStepper(position, questions) {
  var v2FirstStep = $('.item-step-25');
  var v2SecondStep = $('.item-step-50');
  var v2ThirdStep = $('.item-step-75');

  var v2FirstHint = $('.item-step-25-hint');
  var v2SecondHint = $('.item-step-50-hint');
  var v2ThirdHint = $('.item-step-75-hint');

  var result = Math.round(((position / questions) * 100));

  if (result > 25 && result <= 50){
    v2FirstStep.removeClass('hidden');
  }
  else if (result > 50 && result <= 75){
    v2FirstStep.removeClass('hidden').addClass('opacity-background-5');
    v2FirstHint.addClass('hidden');

    v2SecondStep.removeClass('hidden');
  }
  else if (result > 75) {
    v2FirstStep.removeClass('hidden').addClass('opacity-background-5');
    v2FirstHint.addClass('hidden');

    v2SecondStep.removeClass('hidden').addClass('opacity-background-5');
    v2SecondHint.addClass('hidden');

    v2ThirdStep.removeClass('hidden');
  }
}





function v2RemovePastSurvey(position) {
  $("#question-form-" + position.toString()).remove();
}

function v2AddNextSurvey(innerHTML) {
  var demandContainer = $('.v2-questions-container');
  var currentSurvey = $(innerHTML).find('.question-form');
  $(currentSurvey).appendTo(demandContainer).fadeIn("slow");
}

function v2ClickCounterSurvey(position) {
  var buttonPositive = $('.positive-operator');
  var buttonNegative = $('.negative-operator');
  var plus = $('.positive-operator').find('g');
  var moins = $('.negative-operator').find('path');
  var count = 0;

  console.log(plus, moins);

  buttonPositive.on('touchend click', function(){
    if(count < 5) {
      count += 1;
      $(`#${parseInt(position)+1}-1`).val(count);
    } else {
      return;
    }
    plus.addClass('opacity-background-7');
    setTimeout(function(){plus.removeClass('opacity-background-7');}, 150)
  });

  buttonNegative.on('touchend click', function(){
    if(count > 0) {
      count -= 1;
      $(`#${parseInt(position)+1}-1`).val(count);
    } else {
      return;
    }
    moins.addClass('opacity-background-7');
    setTimeout(function(){moins.removeClass('opacity-background-7');}, 150)
  });
}

function v2FocusTextInputSurvey(position) {
  var textInputOne = $(`#${parseInt(position)+1}-1`);
  var textInputTwo = $(`#${parseInt(position)+1}-2`);

  textInputOne.on('focus', function(){
    $(this).addClass('border-green');
  });
  textInputOne.on('blur', function(){
    $(this).removeClass('border-green');
  });

  textInputTwo.on('focus', function(){
    $(this).addClass('border-green');
  });
  textInputTwo.on('blur', function(){
    $(this).removeClass('border-green');
  });

  textInputTwo.prop('disabled', true);
  textInputOne.keyup(function(){
    textInputTwo.prop('disabled', this.value == "" ? true : false);
  });

  textInputTwo.keyup(function(){
    if (textInputTwo.val() == "123456"){
      $('.kenotte-container-advice').removeClass('hidden');
    } else {
      $('.kenotte-container-advice').addClass('hidden');
    }
  });
}

function v2SurveyCourseStatus(innerHTML){
  var v2CurrentCourseStatus = $(innerHTML).find('.v2-stepper');
  var v2StepperContainer = $('.v2-stepper-container');
  var houseSetter = $(innerHTML).find('#house-stepper');
  var searchSetter = $(innerHTML).find('#search-stepper');

  v2StepperContainer.empty();
  $(v2CurrentCourseStatus).appendTo(v2StepperContainer);
  $(houseSetter).appendTo(v2StepperContainer);
  $(searchSetter).appendTo(v2StepperContainer);
}

function handleNextSurveySuccess(innerHTML, surveyPosition, surveyQuestions) {
  v2RemovePastSurvey(surveyPosition);
  v2AddNextSurvey(innerHTML);
  v2ClickCounterSurvey(surveyPosition);
  // v2FocusTextInputSurvey(surveyPosition);
  // v2SurveyCourseStatus(innerHTML);
  formatNumericInput();
  v2HintStepper(surveyPosition, surveyQuestions);
}




function handleAjaxFormRequest() {
  $(".question-form").submit(function(event) {
    event.preventDefault();
    var form = $(this);
    var url = $(form).attr('action');
    var method = $(form).attr('method');
    var position = $(form).find('input[name="position"]').val();
    var questions = $(form).find('input[name="questions"]').val();

    $.ajax({
      url: url,
      type: method,
      data: $(form).serialize(),
      success: function(data) {
        handleNextSurveySuccess(data,position,questions);
        handleAjaxFormRequest();
      },
      error: function(data) {
        alert("Fatal error");
      }
    });
  });
}


handleAjaxFormRequest();
