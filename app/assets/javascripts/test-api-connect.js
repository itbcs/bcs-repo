function testApiConnect() {
  $("form.connect").submit(function(event){
    event.preventDefault();
    console.log(event);
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    var datas = $form.serialize();
    $.ajax({
      url: url,
      processData: false,
      contentType: false,
      type: method,
      data: datas,
      success: function(data) {
        console.log(data);
        var submitButton = document.querySelector(".submit-btn");
        submitButton.removeAttribute("disabled");
        if (data.authenticate === true) {
          handleSuccessConnect(data);
        } else if (data.authenticate === false) {
          alert(data.error);
        }
      },
      error: function(data) {
        alert("Ajax error call !!!");
      }
    });
  });
}

function handleSuccessConnect(data) {
  var agencyName = data.agency.name
  var currentForm = document.querySelector("form.connect");
  var formHeader = document.querySelector("h3.form-header-title");
  formHeader.remove();
  currentForm.remove();
  var successLink = document.createElement("a");
  successLink.setAttribute("href","http://localhost:3000/");
  successLink.textContent = "Mon dashboard";
  successLink.classList.add("modern-blue-btn");
  var domElement = document.createElement("div");
  var domElementChild = document.createElement("div");
  domElementChild.classList.add("success-text");
  domElementChild.textContent = `Vous êtes connecté en tant que ${agencyName}`;
  domElement.classList.add("success-authentification");
  domElement.appendChild(domElementChild);
  var receiver = document.querySelector("section.test-api-connect");
  receiver.appendChild(domElement);
  receiver.appendChild(successLink);
}

testApiConnect();
