$('button[data-dismiss="dpe-modal"]').click(function() {
  $("#energies-dpe").modal('hide');
});

$('button[data-dismiss="ges-modal"]').click(function() {
  $("#energies-ges").modal('hide');
});

$('button[data-dismiss="agency-connection-modal"]').click(function() {
  $("#agency-connection").modal('hide');
});

$('button[data-dismiss="agency-status-modal"]').click(function() {
  $("#agency-status-modal").modal('hide');
});

$(".modal-dialog.agency-status").click(function() {
  $("#agency-status-modal").modal('hide');
})
