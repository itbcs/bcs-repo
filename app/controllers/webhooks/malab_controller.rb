class Webhooks::MalabController < ActionController::API

  def submit
    event = JSON.parse(request.body.read)
    @webhook = Webhook.new(service: "Malab", description: "intern service", logo: "typeform", action: "#{action_name}", content: event.to_h)
    if @webhook.save
      puts "Malab typeform successfully saved !!"
    end
    render :json => {status: 200}
  end

end
