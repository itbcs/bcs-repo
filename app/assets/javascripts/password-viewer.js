function handlePasswordViewers() {
  var container = document.querySelector(".password-viewer-container");
  var containerText = document.querySelector(".password-viewer-text");
  var field = document.getElementById("employee_password");
  if (container) {
    container.addEventListener("click",function(){
      if (containerText.textContent === "Cacher le mot de passe") {
        containerText.textContent = "Voir le mot de passe";
      } else {
        containerText.textContent = "Cacher le mot de passe";
      }
      container.classList.toggle("disabled");
      if (field.type === "password") {
        field.type = "text";
      } else {
        field.type = "password";
      }
      field.classList.toggle("active");
    });
  }
}


handlePasswordViewers();
