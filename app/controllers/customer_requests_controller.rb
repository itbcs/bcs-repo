class CustomerRequestsController < ApplicationController

  def create
    @customer_request = CustomerRequest.new(safe_params)
    if current_customer
      if current_customer.pdf_customer_guide_request == false
        current_customer.pdf_customer_guide_request = true
        current_customer.save
      end
    end
    if @customer_request.save
      if params[:action_origin] == "home"
        redirect_to root_path, notice: "Ok, le message est partis"
      elsif params[:action_origin] == "dashboard"
        redirect_to v2_customer_dashboard_path, notice: "Ok, le message est partis"
      end
    else
      if params[:action_origin] == "home"
        render root_path, alert: "Désolé, un problème est survenu"
      elsif params[:action_origin] == "dashboard"
        render v2_customer_dashboard_path, alert: "Désolé, un problème est survenu"
      end
    end
  end

  private

  def safe_params
    params.require(:customer_request).permit(:email,:customer_action)
  end

end
