class CustomerQuestionsController < ApplicationController

  def create
    @customer_question = current_customer.customer_questions.build(safe_params)
    @message = @customer_question.message
    if @customer_question.save
      Rails.configuration.admin_emails.each do |email|
        AdminMailer.customer_question(@message,current_customer,email).deliver
      end
      redirect_to customer_zone_path, notice: "Merci, nous avons pris votre question en compte !!"
    else
      render customer_zone_path, alert: "Un problème est survenu !!"
    end
  end

  private

  def safe_params
    params.require(:customer_question).permit(:message, :customer)
  end

end
