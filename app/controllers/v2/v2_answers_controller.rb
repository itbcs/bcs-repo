class V2::V2AnswersController < ApplicationController

  def edit
    @answer = V2Answer.find(params[:id])
    render :edit
  end

  def update
    @answer = V2Answer.find(params[:id])
    if @answer.update_attributes(safe_params)
      puts "Answer successfully update"
      puts @answer.inspect
      redirect_to v2_demand_view_path, notice: "Réponse modifiée avec succès !!"
    else
      render :update, alert: "Une erreur est survenue !!"
    end
  end

  private

  def safe_params
    params.require(:v2_answer).permit(:answer_type, :text, :unit, :input_hint, :input_format, :demand_attribute, :order, :fem_text, :slug, :score)
  end

end
