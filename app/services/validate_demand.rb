class ValidateDemand

  def initialize demand
    @demand = demand
    @errors = []
    @matching = []
    @matching_instances = []
  end

  def call
    # agency_matching
    perf_matching
    service_success
  end

  private

  def perf_matching
    integerify_postal_codes = @demand[:search_zipcodes_area].map{|pc| pc.to_s}
    results = Agency.where(activated: true).where(:catchment_area.in => integerify_postal_codes)
    min_budget = @demand[:budget].first
    if min_budget != 0
      final_results = results.where(min_price: {'$lte' => min_budget})
    else
      final_results = results
    end
    @matching = final_results.map{|agency| agency.id}
    @matching
  end

  # def agency_matching
  #   real_budget = format_budget(@demand.budget)
  #   @demand.postal_codes.each do |zipcode|
  #     Agency.where(partner: true).keep_if{|a| a.postal_codes.include?(zipcode.to_s)}.each do |agency|
  #       (@matching << agency.id)
  #       (@matching_instances << agency)
  #     end
  #   end
  #   if @matching_instances != []
  #     @priced_filtered = @matching_instances.reject{|agency| agency.min_price > real_budget}
  #     if @priced_filtered != []
  #       @priced_filtered.map!{|agency| agency.id}
  #     end
  #   else
  #     puts "Tableau vide !!!"
  #   end
  #   @priced_filtered
  # end

  # def format_budget budget
  #   if budget.is_a?(Array)
  #     budget.first.gsub(",","").to_i
  #   else
  #     return budget.gsub(",","").to_i
  #   end
  # end

  def check_budget_format budget_collection
    if budget_collection
      budget_collection = budget_collection.select{|number| number != ""}
      if budget_collection.is_a?(Array) && budget_collection != []
        min_budget = budget_collection.first.to_s.strip.gsub(/[[:space:]]/,'').gsub(",","").to_i
      end
    end
    min_budget ? min_budget : 0
  end

  def service_success
    ServiceSuccess.new(@matching)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
