class Employee
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  validates_inclusion_of :role, in: EmployeeRole.list



  ## Database authenticatable
  field :email, type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token, type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count, type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at, type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip, type: String

  field :firstname, type: String
  field :lastname, type: String
  field :password, type: String
  field :role, type: String


end
