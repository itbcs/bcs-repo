class Webhooks::PagesController < ApplicationController

  def list
    @webhooks = Webhook.where(service: "heroku")
    @webhook = Webhook.find("5dcacf46b475c7000772b8d6")
    @webhook.malab_sanitize_infos
  end

  def malab_view
    @webhook = Webhook.where(service: "Malab").last
    @infos = @webhook.malab_sanitize_infos
  end

  def typeform_list
    @webhooks = Webhook.where(created_at: {"$gte": 1.day.ago})
  end

  def visits
    @visits = Visit.all
    @test = uniq_customer_visits
  end

  def uniq_customer_visits
    results = {}
    Visit.all.each do |visit|
      if !results.has_key?(visit.customer_email)
        results[visit.customer_email] = 1
      else
        results[visit.customer_email] += 1
      end
    end
    return results
  end

end
