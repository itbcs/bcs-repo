var place;


function initializeAutocomplete(id) {
  var element = document.getElementById(id);
  var options = {types: ['geocode'], componentRestrictions: {country: "fr"}};
  if (element) {
    var autocomplete = new google.maps.places.Autocomplete(element, options);
    google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChanged);
  }
}

function initializeAutocompleteWithClass(className) {
  var nodes = document.querySelectorAll(className);
  var element = nodes[nodes.length - 1];
  var options = {types: ['geocode'], componentRestrictions: {country: "fr"}};
  if (element) {
    var autocomplete = new google.maps.places.Autocomplete(element, options);
    google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChangedMultipleInputs);
  }
}

function onPlaceChanged() {
  place = this.getPlace();
  var zipCode = place["adr_address"];
  var javascriptObject = $.parseHTML(zipCode);

  for (var [key,value] of Object.entries(javascriptObject)) {
    if ($(value).filter(".postal-code").length > 0) {
      var formattedZipcodeV2 = $(value).filter(".postal-code");
    }
    if ($(value).filter(".street-address").length > 0) {
      var geolocAddress = $(value).filter(".street-address");
    }
  }

  var lat = place.geometry.location.lat();
  var lng = place.geometry.location.lng();
  $('#lat').val(lat);
  $('#lng').val(lng);

  if (formattedZipcodeV2) {
    console.log("Geoloc zipcode present !!");
    $("#geoloc_zipcode").val(formattedZipcodeV2.text());
  } else {
    console.log("Attention - Adresse pas assez précise - Code postal non trouvé");
  }

  if (geolocAddress) {
    console.log(geolocAddress.text());
    $("#geoloc_address").val(geolocAddress.text());
  }

  $('#geoloc-zipcode').val(formattedZipcodeV2);
  for (var i in place.address_components) {
    var component = place.address_components[i];
    for (var j in component.types) {
      var type_element = document.getElementById(component.types[j]);
      if (type_element) {
        type_element.value = component.long_name;
      }
    }
  }
}





function onPlaceChangedMultipleInputs() {
  place = this.getPlace();
  var zipCode = place["adr_address"];
  var javascriptObject = $.parseHTML(zipCode);

  for (var [key,value] of Object.entries(javascriptObject)) {
    console.log(value);
    if ($(value).filter(".postal-code").length > 0) {
      var formattedZipcodeV2 = $(value).filter(".postal-code");
    }
    if ($(value).filter(".street-address").length > 0) {
      var geolocAddress = $(value).filter(".street-address");
    }
    if ($(value).filter(".locality").length > 0) {
      var geolocLocality = $(value).filter(".locality");
    }
  }

  var lat = place.geometry.location.lat();
  var lng = place.geometry.location.lng();
  $('input.lat').last().val(lat);
  $('input.lng').last().val(lng);

  if (formattedZipcodeV2) {
    console.log("Geoloc zipcode present !!");
    $(".geoloc_zipcode").last().val(formattedZipcodeV2.text());
  } else {
    console.log("Attention - Adresse pas assez précise - Code postal non trouvé");
    var sentence = $("<div class='input-sentence'><i class='fa fa-warning'></i>Code postal introuvable</div>")
    $(".all-inputs-item").last().append(sentence);
    $(".dynamic-geoloc").last().val("");
    $(".dynamic-geoloc").focus(function(){
      $(".input-sentence").remove();
    });
  }

  if (geolocAddress) {
    console.log(geolocAddress.text());
    $(".geoloc_address").last().val(geolocAddress.text());
  }

  if (geolocLocality) {
    console.log(geolocLocality.text());
    $(".geoloc_locality").last().val(geolocLocality.text());
  }

  $('#geoloc-zipcode').val(formattedZipcodeV2);
  for (var i in place.address_components) {
    var component = place.address_components[i];
    for (var j in component.types) {
      var type_element = document.getElementById(component.types[j]);
      if (type_element) {
        type_element.value = component.long_name;
      }
    }
  }
}






google.maps.event.addDomListener(window, 'load', function() {
  initializeAutocomplete('user_input_autocomplete_address');
});

google.maps.event.addDomListener(window, 'load', function() {
  initializeAutocomplete('estimation-place-input');
});

google.maps.event.addDomListener(window, 'load', function() {
  initializeAutocomplete('property_localisation');
});

google.maps.event.addDomListener(window, 'load', function() {
  initializeAutocomplete('estimation-geoloc-input');
});




$('#initial_location').on('keyup keypress', function(e) {
 var keyCode = e.keyCode || e.which;
 if (keyCode === 13) {
   e.preventDefault();
   return false;
 }
});

$('.user-choice-submit').click(function(event) {
  if (!place) {
    event.preventDefault();
    $('#user_input_autocomplete_address').val("").attr("placeholder", "Retentez votre chance ...");
    if ($(".empty-input-alert").length === 0) {
      var formContainer = $(".inline-form");
      var textDiv = "<small class='empty-input-alert'><i class='fa fa-rocket'></i>Oupss !! Ville inconnue à notre connaissance ...</small>";
      $(textDiv).appendTo(formContainer);
    }
  }
  var inputValue = $('#user_input_autocomplete_address').val();
  if ($('#user_input_autocomplete_address').val() === "") {
    event.preventDefault();
    $('#user_input_autocomplete_address').attr("placeholder", "Retentez votre chance ...");
    if ($(".empty-input-alert").length === 0) {
      var formContainer = $(".inline-form");
      var textDiv = "<small class='empty-input-alert'><i class='fa fa-rocket'></i>Oupss !! Ville inconnue à notre connaissance ...</small>";
      $(textDiv).appendTo(formContainer);
    }
  }

});


$('#user_input_autocomplete_address').focus(function(){
  if ($(".empty-input-alert").length != 0) {
    $(".empty-input-alert").remove();
  }
});
