module PdfHelper

  def generate_reference(demand)
    creation_date = l(demand.created_at, format: :medium)
    reference = demand.id.to_s.last(5).upcase
    result = "Ref: BCS_#{reference} créee le #{creation_date}"
  end

  def generate_spread_order_id(demand)
    reference = demand.id.to_s.last(5).upcase
    result = "BCS_DEMAND_#{reference}"
  end

  def generate_only_reference(demand_id)
    ref = "BCS_#{demand_id.to_s.last(5).upcase}"
  end

  def format_number(number)
    if number != ""
      "#{number.gsub(',',' ')} €"
    end
  end

  def format_area(number)
    if number != ""
      "#{number} m²"
    end
  end

  def age_calculation(time)
    (Time.now.to_s(:number).to_i - time.to_time.to_s(:number).to_i)/10e9.to_i
  end

end
