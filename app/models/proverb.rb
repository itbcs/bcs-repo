class Proverb
  include Mongoid::Document
  include Mongoid::Timestamps

  field :active_date, type: Date
  field :sentence, type: String

end
