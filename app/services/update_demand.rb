class UpdateDemand
  def initialize survey, demand, answer_ids, answers_values
    @survey = survey
    @demand = demand
    @answer_ids = answer_ids
    @answer_values = answers_values
    @answers = []
    @errors = []
  end

  def call
    initialize_user_answers
    update_demand_with_user_answers
    service_success
  end

  private

  def update_demand_with_user_answers
    if @answers.size == 1
      if (@answers.last).is_a?(Array)
        @demand[@survey.demand_attribute.to_sym] = @answers.last
      else
        @demand[@survey.demand_attribute.to_sym] = @answers.last.text
      end
    else
      value = []
      @answers.each do |answer|
        value << answer.text
      end
      @demand[@survey.demand_attribute.to_sym] = value
    end
    @demand.save
  end

  def initialize_user_answers
    if @answer_values
      @answers << @answer_values
    elsif @answer_values && @answer_ids
    elsif @answer_ids
      if @survey.answer_type == :checkbox.to_s
        @answers_array = []
        @survey.answers.each do |answer|
          @answer_ids.each do |id|
            if(answer.id.to_s == id)
              @answers_array << answer.text
            end
          end
        end
        @answers << @answers_array
      else
        @survey.answers.each do |answer|
          @answer_ids.each do |id|
            if(answer.id.to_s == id)
              @answers << answer
            end
          end
        end
      end
    end
  end

  def service_success
    ServiceSuccess.new(@demand)
  end

  def service_error
    ServiceError.new(@errors)
  end
end
