class PressRelease
  include Mongoid::Document
  include Mongoid::Timestamps
  mount_uploader :pdf, ImageUploader
  mount_uploader :photo, ImageUploader

  field :title, type: String
  field :content, type: String

end
