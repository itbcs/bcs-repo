class V2::PagesController < ApplicationController
  include ApplicationHelper
  require "csv"


  def test_api_connect
  end

  def original_demand_keys
    values = ["_id", "created_at", "updated_at", "version", "modifier_id", "customer_id", "property_ids", "map_photo", "browser_screenshot", "banker_contact", "broker_contact", "completed", "activated", "deactivated", "deactivation_reason", "deactivation_comment", "refused", "old_platform_user_id", "localisation", "postal_codes", "perimeter", "project", "actual_situation", "property_type", "destination", "flat_state", "house_state", "travaux", "budget", "flat_budget", "terrain_budget", "house_budget", "style", "surface", "terrain_surface", "house_construction_surface", "rooms", "floor", "contact_constructor", "principal_space", "kitchen", "bathrooms", "more_rooms", "equipements", "accessibility", "flat_stairs", "house_stairs", "exposition", "parking", "security", "heating", "heating_type", "dpe", "flat_exterior", "house_exterior", "age", "actual_pros", "actual_housing", "actual_housing_area", "installation_date", "last_precision", "heart_stroke", "status", "comments", "question_position", "important_fields", "localisation_lat", "localisation_lng", "sending_customer_notification", "fetch_map_photo", "old_platform_created_at", "from_oldplatform", "sending_customer_recap_email", "updated_by_customer"]
  end

  def new_demand_keys
    values = ["_id", "created_at", "updated_at", "version", "modifier_id", "customer_id", "property_ids", "map_photo", "browser_screenshot", "project", "actual_situation", "property_type", "destination", "flat_state", "house_state", "travaux", "budget", "flat_budget", "terrain_budget", "house_budget", "style", "surface", "terrain_surface", "house_construction_surface", "rooms", "floor", "contact_constructor", "principal_space", "kitchen", "bathrooms", "more_rooms", "equipments", "accessibility", "flat_stairs", "house_stairs", "exposition", "parking", "security", "heating", "heating_type", "dpe", "flat_exterior", "house_exterior", "age", "actual_pros", "actual_housing", "actual_housing_area", "installation_date", "last_precision", "heart_stroke", "criterias", "firstname", "gender", "man_persona", "woman_persona", "email", "estimation_request", "energy_renovation", "school_type", "visited_property_count", "banker_contact", "broker_contact", "search_time", "house_works", "personality", "state", "garage", "search_formatted_area", "search_zipcodes_area", "ground_budget", "what_is_liked", "income", "current_address", "completed", "activated", "deactivated", "refused", "status", "sending_customer_recap_email", "from_oldplatform", "activation_date"]
  end


  def website_scrapper(url)
    @doc = Nokogiri::HTML(open("#{url}", ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE))
  end

  def generate_new_survey_recap
    @questions = Question.all.sort_by{|question| question.position}
    @header = ["Question", "Réponses"]
    CSV.open("answers-recap.csv", "wb", {col_sep: ";"}) do |csv|
      csv << @header
      @questions.each do |question|
        @values = []
        @values << question.label
        @answers = []
        question.v2_answers.each do |answer|
          if answer.text
            @answers << [answer.text, answer.auto_identifier]
          else
            @answers << ["Champ libre", answer.auto_identifier]
          end
        end
        @values << @answers
        csv << @values
      end
    end
  end

  def formatted_property_price
    @agencies = Agency.where(activated: true)
    @agencies.each do |agency|
      if agency.properties?
        agency.properties.each do |prop|
          if prop.price
            puts "@@@@@@ PROPERTY PRICE @@@@@"
            puts prop.price
            puts prop.price.inspect
            puts "---- TR Method for String ----"
            puts prop.price.tr('^0-9', '')
            @integer_price = prop.price.tr('^0-9', '').to_i
            puts "------------------------------"
          elsif prop.prix
            puts "@@@@@@ PROPERTY PRIX @@@@@"
            puts prop.prix
            puts prop.prix.inspect
            puts "---- TR Method for String ----"
            @integer_price = prop.prix.to_s.tr('^0-9', '')
            puts prop.prix.to_s.tr('^0-9', '').to_i
            puts "------------------------------"
          else
            puts "@@@@@@ PRICE IS NIL @@@@@"
            puts prop.inspect
            @integer_price = 0
          end
          prop.price_int = @integer_price
          # if prop.save(validate: false)
          #   puts "Property successfully saved with a price of #{@integer_price}!"
          # else
          #   puts "---- ERROR MESSAGE ----"
          #   puts prop.errors.messages
          #   puts "Oupsss, an error occured !"
          # end
          puts prop.inspect
        end
      end
    end
  end

  # def demand_stats
  #   @active_demands = V2Demand.all.to_a.last(100)
  #   @oldplatform_demands = Demand.where(from_oldplatform: true).where(status: "pending").includes(:customer)
  #   @orphan_demands = Demand.where(from_oldplatform: false).includes(:customer).all.select{|demand| !demand.customer.valid?}
  #   @refused_demands = Demand.where(from_oldplatform: false).where(refused: true).includes(:customer)
  #   @pending_demands = Demand.where(from_oldplatform: false).where(status: "pending").includes(:customer).select{|demand| demand.customer.valid?}
  #   @deactivated_demands = Demand.where(deactivated: true).includes(:customer)
  #   @fields = V2Demand.all.last.fields.keys.to_a
  #   CSV.open("new-demands-test-v2.csv", "wb", {col_sep: ";", encoding: "iso-8859-1:UTF-8"}) do |csv|
  #     csv << @fields
  #     @active_demands.each do |demand|
  #       @values = []
  #       demand.fields.keys.each do |key|
  #         puts "$$$$ CSV KEY $$$$"
  #         puts key
  #         puts "$$$$$$$$$$$$$$$$$"
  #         if key.to_sym == :budget
  #           if demand[:budget]
  #             @values << demand[:budget]
  #           else
  #             @values << "NULL"
  #           end
  #         elsif key.to_sym == :surface
  #           if demand[:surface]
  #             @values << demand[:surface]
  #           else
  #             @values << "NULL"
  #           end
  #         elsif key.to_sym == :actual_housing_area
  #           if demand[:actual_housing_area]
  #             @values << demand[:actual_housing_area]
  #           else
  #             @values << "NULL"
  #           end
  #         elsif key.to_sym == :principal_space
  #           if demand[:principal_space]
  #             @values << demand[:principal_space]
  #           else
  #             @values << "NULL"
  #           end
  #         else
  #           if demand.send("#{key}")
  #             @values << demand.send("#{key}")
  #           else
  #             @values << "NULL"
  #           end
  #         end
  #       end
  #       csv << @values
  #     end
  #   end
  #   redirect_to v2_start_path, notice: "Successfull csv generation"
  # end

  def demand_stats
    @active_demands = V2Demand.where(activated: true)
    @header = ["id", "created_at", "updated_at", "customer_id", "proposed_properties_count", "project", "property_type", "budget_mini", "budget_maxi", "surface_mini", "surface_maxi", "email", "principal_space", "rooms_count", "firstname", "gender", "search_zipcodes_area", "status", "activated", "deactivated", "refused", "kitchen", "destination", "actual_situation", "dpe", "exposition", "security", "parking", "heating", "heating_type", "age", "actual_housing", "criterias", "banker_contact", "broker_contact"]
    CSV.open("v2-demands-stats-v8.csv", "wb", {col_sep: ";", encoding: "iso-8859-1:UTF-8"}) do |csv|
      csv << @header
      @active_demands.each do |demand|
        id = demand.id.to_s
        created_at = demand.created_at
        updated_at = demand.updated_at
        customer = demand.customer ? demand.customer.id.to_s : "NULL"
        properties = demand.properties.any? ? demand.properties.count : "NULL"
        project = demand.project ? demand.project : "NULL"
        property_type = demand.property_type ? demand.property_type : "NULL"
        email = demand.email ? demand.email : "NULL"
        rooms = demand.rooms ? demand.rooms.to_i : "NULL"
        firstname = demand.firstname ? demand.firstname : "NULL"
        gender = demand.gender ? demand.gender : "NULL"
        status = demand.status ? demand.status : "NULL"
        activated = demand.activated ? demand.activated : false
        deactivated = demand.deactivated ? demand.deactivated : false
        refused = demand.refused ? demand.refused : false
        kitchen = demand.kitchen ? demand.kitchen : "NULL"
        destination = demand.destination ? demand.destination : "NULL"
        actual_situation = demand.actual_situation ? demand.actual_situation : "NULL"
        dpe = demand.dpe ? demand.dpe : "NULL"
        exposition = demand.exposition ? demand.exposition : "NULL"
        security = demand.security ? demand.security : "NULL"
        parking = demand.parking ? demand.parking : "NULL"
        heating = demand.heating ? demand.heating : "NULL"
        heating_type = demand.heating_type ? demand.heating_type : "NULL"
        age = demand.age ? demand.age : "NULL"
        actual_housing = demand.actual_housing ? demand.actual_housing : "NULL"
        criterias = demand.criterias ? demand.criterias : "NULL"
        banker_contact = demand.banker_contact ? demand.banker_contact : "NULL"
        broker_contact = demand.broker_contact ? demand.broker_contact : "NULL"

        puts "**** BUDGET ****"
        puts demand[:budget].inspect
        puts "****************"
        if demand[:budget]
          if demand[:budget].size == 1
            budget_mini = demand[:budget].first.to_i
            budget_maxi = "NULL"
          elsif demand[:budget].size == 2
            budget_mini = demand[:budget].first.to_i
            budget_maxi = demand[:budget].last.to_i
          end
        else
          budget_maxi = "NULL"
          budget_mini = "NULL"
        end


        puts "**** SURFACE ****"
        puts demand[:surface].inspect
        puts "*****************"
        if demand[:surface]
          if demand[:surface].size == 1
            surface_mini = demand[:surface].first.to_i
            surface_maxi = "NULL"
          elsif demand[:surface].size == 2
            surface_mini = demand[:surface].first.to_i
            surface_maxi = demand[:surface].last.to_i
          end
        else
          surface_maxi = "NULL"
          surface_mini = "NULL"
        end

        puts "**** PRINCIPAL SPACE ****"
        if demand[:principal_space]
          principal_space = demand[:principal_space].to_i
        else
          principal_space = "NULL"
        end
        puts "*************************"


        puts "**** SEARCH ZIPCODES AREA ****"
        if demand[:search_zipcodes_area]
          search_zipcodes_area = demand[:search_zipcodes_area].first(9).join("'")
        else
          search_zipcodes_area = "NULL"
        end
        puts "******************************"


        values = [id,created_at,updated_at,customer,properties,project,property_type,budget_mini,budget_maxi,surface_mini,surface_maxi,email,principal_space,rooms,firstname,gender,search_zipcodes_area,status,activated,deactivated,refused,kitchen,destination,actual_situation,dpe,exposition,security,parking,heating,heating_type,age,actual_housing,criterias,banker_contact,broker_contact]
        csv << values
      end
    end
    redirect_to v2_start_path, notice: "Successfull csv generation"
  end

  def push_demands_for_arthur
    @agency = Agency.find("5c7551091748c04d02852bd7")
    @demands = ["5cc1bb86a1c4330a6cde6ff3","5cc1bb7ca1c4330a6cde6ff1","5cc1bb39a1c4330a6cde6fef"]
    @demands.each do |demand_id|
      demand_instance = V2Demand.find(demand_id)
      @agency.v2_demands.push(demand_instance)
    end
  end

  # def migrations
  #   @deactivated_demands = Demand.where(deactivated: true).includes(:customer)
  #   @refused_demands = Demand.where(from_oldplatform: false).where(refused: true).includes(:customer)
  #   @active_demands = Demand.where(activated: true)
  #   @pending_demands = Demand.where(from_oldplatform: false).where(status: "pending").includes(:customer).select{|demand| demand.customer.valid?}
  #   @values = []
  #   @count = 0
  #   @pending_demands.each do |active_demand|
  #     result = OldDemandsMigration.new(active_demand).call

  #     if result.success?
  #       @count += 1
  #       puts "Demand-#{@count} / V2 Demand migration service succesfull !!"
  #     end

  #     demand = result.data if result.success?
  #     @values << demand
  #   end
  #   redirect_to v2_start_path, notice: "Successfull migrations"
  # end

  # def migrations
  #   @active_demands = V2Demand.where(activated: true)
  #   @active_demands.each do |demand|
  #     result = OldDemandsAgencyLink.new(demand).call
  #     if result.success?
  #       puts "Success agency link service"
  #     else
  #       puts "Failed agency link service"
  #     end
  #   end
  #   redirect_to v2_start_path, notice: "Failed migrations"
  # end

  # def migrations
  #   @active_demands = V2Demand.where(activated: true)
  #   @active_demands.each do |demand|
  #     result = ValidateDemand.new(demand).call
  #     agencies_ids = result.data if result.success?
  #     status = handle_results(demand,agencies_ids.uniq)
  #     if status.nil?
  #       puts "failed"
  #     else
  #       puts "success"
  #     end
  #   end
  #   redirect_to v2_start_path, notice: "Migrations succeed"
  # end

  def test_new_survey
    render :test_new_survey
  end

  # def migrations
  #   V2Demand.where(from_oldplatform: false).where(status: "pending").select{|demand| demand.customer.valid?}.each_with_index do |demand,index|
  #     puts "ORIGINAL CRITERIAS"
  #     puts demand[:criterias].inspect
  #     puts demand[:criterias].class
  #     puts "€€€€€€€€€€€€€€€€€€"
  #     if demand[:criterias]
  #       if demand[:criterias].empty?
  #         @result = nil
  #       else
  #         @result = []
  #         demand[:criterias].each do |criteria|
  #           @result << handle_criteria_format(criteria)
  #         end
  #       end
  #     else
  #       @result = nil
  #     end
  #     puts "**** Demand-#{index+1} ****"
  #     if @result
  #       puts @result.uniq
  #       puts @result.uniq.inspect
  #       demand.criterias = @result.uniq
  #       if demand.save
  #         puts "CRITERIAS ON"
  #         puts demand.inspect
  #       end
  #     else
  #       puts @result.inspect
  #       demand.criterias = nil
  #       if demand.save
  #         puts "CRITERIAS OFF"
  #         puts demand.inspect
  #       end
  #     end
  #     puts "***************************"
  #   end
  #   redirect_to v2_start_path, notice: "Migrations succeed"
  # end

  # def migrations
  #   @demands = V2Demand.where(activated: true)
  #   @demands.each do |demand|
  #     if !demand.completed
  #       puts demand.completed
  #     end
  #   end
  #   redirect_to v2_start_path, notice: "Migrations succeed"
  # end

  def migrations
    # response = RestClient.get('https://www.century21.fr/annonces/achat-maison-appartement/v-bordeaux/alentours-10/s-0-/st-0-/b-0-/page-1/')
    # @doc = website_scrapper("https://www.century21.fr/annonces/achat-maison-appartement/v-bordeaux/alentours-10/s-0-/st-0-/b-0-/page-1/")
    # @doc = response.body
    @choices = CustomerChoice.one_day_old_positive_choices
    render :migrations
  end

  def handle_criteria_format(criteria)
    if criteria.parameterize == "localisation"
      @criteria = "search_formatted_area"
    elsif criteria.parameterize == "budget"
      @criteria = "budget"
    elsif criteria.parameterize == "surface"
      @criteria = "surface"
    elsif criteria.parameterize == "parking"
      @criteria = "parking"
    elsif criteria.parameterize == "exterieur-de-l-appartement"
      @criteria = "flat_exterior"
    elsif criteria.parameterize == "bien-recherche"
      @criteria = "property_type"
    elsif criteria.parameterize == "espace-principal"
      @criteria = "principal_space"
    elsif criteria.parameterize == "exterieur-de-votre-maison"
      @criteria = "house_exterior"
    elsif criteria.parameterize == "pieces-supplementaires"
      @criteria = "more_rooms"
    elsif criteria.parameterize == "chambres"
      @criteria = "rooms"
    elsif criteria.parameterize == "accessibilite"
      @criteria = "accessibility"
    elsif criteria.parameterize == "zone-de-recherche"
      @criteria = "search_formatted_area"
    elsif criteria.parameterize == "style"
      @criteria = "style"
    elsif criteria.parameterize == "travaux"
      @criteria = "house_works"
    elsif criteria.parameterize == "exposition"
      @criteria = "exposition"
    elsif criteria.parameterize == "cuisine"
      @criteria = "kitchen"
    elsif criteria.parameterize == "etat-de-la-maison"
      @criteria = "state"
    elsif criteria.parameterize == "escalier-pour-la-maison"
      @criteria = "house_stairs"
    elsif criteria.parameterize == "acces-appartement"
      @criteria = "accessibility"
    elsif criteria.parameterize == "securite"
      @criteria = "security"
    elsif criteria.parameterize == "surface-du-terrain"
      @criteria = "terrain_surface"
    elsif criteria.parameterize == "surface-de-la-maison-construction"
      @criteria = "house_construction_surface"
    elsif criteria.parameterize == "type-de-chauffage"
      @criteria = "heating_type"
    elsif criteria.parameterize == "budget-pour-le-terrain"
      @criteria = "terrain_budget"
    elsif criteria.parameterize == "equipements"
      @criteria = "equipments"
    elsif criteria.parameterize == "salles-de-bain"
      @criteria = "bathrooms"
    elsif criteria.parameterize == "mon-banquier-est-il-au-courant"
      @criteria = "banker_contact"
    elsif criteria.parameterize == "ai-je-rencontre-un-courtier"
      @criteria = "broker_contact"
    elsif criteria.parameterize == "etat-de-l-appartement"
      @criteria = "state"
    elsif criteria.parameterize == "chauffage"
      @criteria = "heating"
    elsif criteria.parameterize == "dpe"
      @criteria = "dpe"
    elsif criteria.parameterize == "etat"
      @criteria = "state"
    elsif criteria.parameterize == "exterieur"
      @criteria = "house_exterior"
    else
      @criteria = nil
    end
    return @criteria
  end

  def map
    if customer_signed_in? && current_customer.v2_demands.any?
      gon.push({
        lat: 44.836151,
        lng: -0.580816,
        origin: "restricted",
        city: "Bordeaux"
      })
      render "map/restricted_demand_map", layout: 'map'
    else
      create_empty_demand
      gon.push({
        lat: params[:lat],
        lng: params[:lng],
        city: params[:locality].to_s,
        demand: @demand.id.to_s
      })
      render :map, layout: 'map'
    end
  end

  def flatten_hash(hash)
    hash.each_with_object({}) do |(k, v), h|
      if v.is_a? Hash
        flatten_hash(v).map do |h_k, h_v|
          h["#{h_k}"] = h_v
        end
      else
        h[k] = v
      end
     end
  end

  def start
    @current_page = request.parameters[:page]
    @request = request.parameters.except!(:utf8, :button, :controller, :action).delete_if{|key,value| value == ""}
    if session[:bcs_customer_id]
      session.delete(:bcs_customer_id)
    end

    if params["page"]
      sanitize_params = flatten_hash(@request)
      @request = flatten_hash(@request)
      puts "**** REQUEST ****"
      puts @request
      puts "*****************"
    else
      sanitize_params = {}
      params.each do |key,value|
        if value != ""
          sanitize_params[key.to_sym] = value
        end
      end
    end

    puts "**** SANITIZE PARAMS ****"
    puts sanitize_params.except!(:utf8, :button, :controller, :action, "page", "api_key")
    puts "*************************"

    @start_url = "https://bcs-api.herokuapp.com/properties"
    sanitize_params.each_with_index do |(key,value),index|
      if index == 0
        if key != "page" || key != "api_key"
          puts key
          result = generate_api_param_request(key,value,true)
          puts result
          @start_url = @start_url + result
        end
      else
        if key != "page" || key != "api_key"
          puts key
          result = generate_api_param_request(key,value,false)
          puts result
          @start_url = @start_url + result
        end
      end
    end

    puts "**** TEST ON DYNAMIC PARAMS ITERATION ****"
    if sanitize_params.size == 0
      @start_url = @start_url + "?api_key=ea5f04afe43b502a653a9a27b3aa9688"

      if params["page"]
        @start_url_first_page = @start_url + "&api_key=ea5f04afe43b502a653a9a27b3aa9688" + "&page=#{params['page']}"
      else
        @start_url_first_page = @start_url + "&api_key=ea5f04afe43b502a653a9a27b3aa9688" + "&page=1"
      end

    else
      @start_url = @start_url + "&api_key=ea5f04afe43b502a653a9a27b3aa9688"

      if params["page"]
        @start_url_first_page = @start_url + "&api_key=ea5f04afe43b502a653a9a27b3aa9688" + "&page=#{params['page']}"
      else
        @start_url_first_page = @start_url + "&api_key=ea5f04afe43b502a653a9a27b3aa9688" + "&page=1"
      end

      @sanitize_params = sanitize_params

      puts @sanitize_params.inspect
      @french_params = french_params_keys(@sanitize_params)
    end
    puts @start_url
    puts "******************************************"

    # if sanitize_params[:status] && sanitize_params[:type] && sanitize_params[:county] && sanitize_params[:min_price] && sanitize_params[:max_price]
    #   @api_url = "https://bcs-api.herokuapp.com/properties?status=#{sanitize_params[:status]}&type=#{sanitize_params[:type]}&county=#{sanitize_params[:county]}&min_price=#{sanitize_params[:min_price]}&max_price=#{sanitize_params[:max_price]}&api_key=ea5f04afe43b502a653a9a27b3aa9688"
    # elsif sanitize_params[:county]
    #   @api_url = "https://bcs-api.herokuapp.com/properties?county=#{sanitize_params[:county]}&api_key=ea5f04afe43b502a653a9a27b3aa9688"
    # elsif sanitize_params[:status]
    #   @api_url = "https://bcs-api.herokuapp.com/properties?status=#{sanitize_params[:status]}&api_key=ea5f04afe43b502a653a9a27b3aa9688"
    # elsif sanitize_params[:type]
    #   @api_url = "https://bcs-api.herokuapp.com/properties?type=#{sanitize_params[:type]}&api_key=ea5f04afe43b502a653a9a27b3aa9688"
    # else
    #   @api_url = "https://bcs-api.herokuapp.com/properties?county=33&api_key=ea5f04afe43b502a653a9a27b3aa9688"
    # end
    if params["details"]
      @remote_prop = RestClient.get("https://bcs-api.herokuapp.com/properties/#{params['property_id']}?api_key=ea5f04afe43b502a653a9a27b3aa9688")
      if @remote_prop.code == 200
        @request_code_status = 200
        puts "HTTP CODE RETURN - 200"
        @prop_response = JSON.parse(@remote_prop.body.to_s)
      elsif @remote_prop.code == 204
        @request_code_status = 204
        puts "HTTP CODE RETURN - 204"
        @prop_response = [@remote_prop.body.to_s]
      elsif @remote_prop.code == 401
        @request_code_status = 401
        puts "HTTP CODE RETURN - 401"
        @prop_response = [@remote_prop.body.to_s]
      else
        puts "HTTP CODE RETURN"
        @prop_response = JSON.parse(@remote_prop.body.to_s)
      end
    end

    @api_response = RestClient.get(@start_url_first_page)
    if @api_response.code == 200
      @request_code_status = 200
      puts "HTTP CODE RETURN - 200"
      @response = JSON.parse(@api_response.body.to_s)
    elsif @api_response.code == 204
      @request_code_status = 204
      puts "HTTP CODE RETURN - 204"
      @response = [@api_response.body.to_s]
    elsif @api_response.code == 401
      @request_code_status = 401
      puts "HTTP CODE RETURN - 401"
      @response = [@api_response.body.to_s]
    else
      puts "HTTP CODE RETURN"
      @response = JSON.parse(@api_response.body.to_s)
    end


    @api_response_fullsize = RestClient.get(@start_url)
    if @api_response_fullsize.code == 200
      puts "HTTP CODE RETURN - 200"
      @fullsize_response = JSON.parse(@api_response_fullsize.body.to_s)
    elsif @api_response_fullsize.code == 204
      puts "HTTP CODE RETURN - 204"
      @fullsize_response = [@api_response_fullsize.body.to_s]
    elsif @api_response_fullsize.code == 401
      puts "HTTP CODE RETURN - 401"
      @fullsize_response = [@api_response_fullsize.body.to_s]
    else
      puts "HTTP CODE RETURN"
      @fullsize_response = JSON.parse(@api_response_fullsize.body.to_s)
    end

    if @fullsize_response.size != 0
      puts "**** FULLSIZE RESPONSE ****"
      puts @fullsize_response.size
      puts (@fullsize_response.size.to_f / 25.0).ceil
      @number_of_pages = (@fullsize_response.size.to_f / 25.0).ceil
    else
      puts "**** FULLSIZE RESPONSE == 0 ****"
      puts @fullsize_response.inspect
      puts "********************************"
    end
    render :start
  end

  def french_params_keys(sanitize_params)
    french_params_with_icons = []
    french_params = {}
    sanitize_params.each do |key,value|
      if key.to_sym == :city
        french_params_with_icons << {"class" => "black", "origin_key" => "Ville", "origin_value" => value, "icon" => "api-city"}
        french_params["Ville"] = value
      elsif key.to_sym == :min_price
        french_params_with_icons << {"class" => "green", "origin_key" => "Prix min", "origin_value" => value, "icon" => "api-price"}
        french_params["Prix min"] = value
      elsif key.to_sym == :max_price
        french_params_with_icons << {"class" => "red", "origin_key" => "Prix max", "origin_value" => value, "icon" => "api-price"}
        french_params["Prix max"] = value
      elsif key.to_sym == :min_area
        french_params_with_icons << {"class" => "green", "origin_key" => "Surface min", "origin_value" => value, "icon" => "api-area"}
        french_params["Surface min"] = value
      elsif key.to_sym == :max_area
        french_params_with_icons << {"class" => "red", "origin_key" => "Surface max", "origin_value" => value, "icon" => "api-area"}
        french_params["Surface max"] = value
      elsif key.to_sym == :zipcode
        french_params_with_icons << {"class" => "black", "origin_key" => "Code postal", "origin_value" => value, "icon" => "api-city"}
        french_params["Code postal"] = value
      elsif key.to_sym == :county
        french_params_with_icons << {"class" => "black", "origin_key" => "Département", "origin_value" => value, "icon" => "api-city"}
        french_params["Département"] = value
      elsif key.to_sym == :status
        french_params_with_icons << {"class" => "black", "origin_key" => "Statut", "origin_value" => value, "icon" => "api-city"}
        french_params["Statut"] = value
      elsif key.to_sym == :type
        french_params_with_icons << {"class" => "black", "origin_key" => "Type", "origin_value" => value, "icon" => "api-type"}
        french_params["Type"] = value
      end
    end
    puts "**** FRENCH PARAMS WITH ICONS ****"
    puts french_params_with_icons
    puts "**********************************"
    return french_params_with_icons
  end

  def generate_api_param_request(key,value,first_item)
    if first_item
      param_part = "?#{key.to_s}=#{value.parameterize}"
    else
      param_part = "&#{key.to_s}=#{value.parameterize}"
    end
  end

  def criterias
    @current_question = Question.all.last
    demand_attribute = "criterias"
    answer_values = params[:criterias].to_a
    @demand = V2Demand.find(params[:demand_id])
    UpdateNewDemand.new(@current_question, @demand, demand_attribute, answer_values).call
    # redirect_to v2_summary_path(demand_id: @demand.id.to_s), notice: "Bravo, vous avez completé votre demande"
    # redirect_to v2_customer_dashboard_path, notice: "Vous devez être inscrit pour aller plus loin !"
    render :json => {criterias: answer_values.size}
  end

  def summary
    @demand = V2Demand.find(params[:demand_id])
    @hashes = {}
    # @values = @demand.attributes.map{|attribute| V2Answer.find_by(auto_identifier: attribute.last)}.compact.map{|answer| answer.text}
    @demand.attributes.except("_id", "created_at", "updated_at", "customer_id", "v2_map_circles", "map_photo").each_with_index do |attribute,index|
      value = @demand.send("#{attribute.first}")
      if value
        @hashes[index + 1] = {value: value}
      end
      question = Question.where(demand_attribute: attribute.first).last
      if question && value
        @hashes[index + 1]["label".to_sym] = question.label_resume
        if question.image
          @hashes[index + 1]["image".to_sym] = question.image
        else
          if attribute.last.is_a?(Array)
            answer = V2Answer.where(auto_identifier: attribute.last.last.to_i).last
          else
            answer = V2Answer.where(auto_identifier: attribute.last.to_i).last
          end
          if answer
            if answer.image
              @hashes[index + 1]["image".to_sym] = answer.image
            else
              @hashes[index + 1]["image".to_sym] = "man"
            end
          else
            @hashes[index + 1]["image".to_sym] = "man"
          end
        end
      else
        puts "******** CRITERIAS ********"
        puts attribute.inspect
        puts "***************************"
        if attribute.first.to_sym == :criterias
          criterias = @demand.criterias
          @hashes[index + 1] = {value: criterias}
          @hashes[index + 1]["label".to_sym] = "Mes critères importants"
          @hashes[index + 1]["image".to_sym] = "important-fields"
        end
        if attribute.first.to_sym == :search_zipcodes_area
          zipcodes = @demand.search_zipcodes_area
          @hashes[index + 1] = {value: zipcodes}
          @hashes[index + 1]["label".to_sym] = "Ma zone de recherche"
          @hashes[index + 1]["image".to_sym] = "map-marker"
        end
        if attribute.first.to_sym == :search_formatted_area
          search_zone = @demand.search_formatted_area
          @hashes[index + 1] = {value: search_zone}
          @hashes[index + 1]["label".to_sym] = "Ma zone de recherche"
          @hashes[index + 1]["image".to_sym] = "map-marker"
        end
      end
    end
    render :summary
  end

  def test_next_question
    demand_attribute = params["demand_field"]
    answer_values = params["#{demand_attribute}"]
    current_position = params["position"].to_i
    @demand = V2Demand.find(params["demand_id"])
    @question = V2Question.where(position: current_position + 1).last
    @v2_survey_course_status = v2_survey_course_status(current_position)
    @v2_questions = V2Question.all.size.to_f
    render partial: "v2/pages/partials/test_question_item"
  end

  def v2_survey_course_status(position)
    question_count = Question.all.size.to_f
    ((position.to_f / question_count) * 100).round(0)
  end


  def new_survey
    # generate_new_survey_models
    @demand = V2Demand.find(params[:demand_id])
    @question = Question.where(position: 1).last
    @v2_survey_course_status = 0
    @questions = Question.all.size.to_f

    if @question.sentence
      @sentence = @question.sentence
    end
    render :new_survey
  end

  def demand_view
    # generate_new_survey_models
    @questions = Question.all
    @conditions = Question.all.map{|question| question.v2_condition if question.v2_condition}
    # @results = old_demand_criterias
    generate_decision_tree(@questions)
    # display_current_demands_closer_reasons
    render :demand_view
  end

  def display_current_demands_closer_reasons
    @demands = V2Demand.where(activated: true)
    puts "**** LOCATION LINK ****"
    @demands.each do |demand|
      if demand[:location_link] != nil
        puts "Field present in demand"
        puts demand[:location_link].inspect
        if demand[:location_link].is_a?(String)
          puts "String save in db"
          demand.location_link = [demand[:location_link]]
          if demand.save
            puts "Demand successfully saved !!!"
          end
        elsif demand[:location_link].is_a?(Array)
          puts "Array save in db"
          demand.location_link
        end
      else
        puts "Field equal nil"
      end
    end
    puts "***********************"
  end

  def generate_decision_tree(questions)
    @conditions = (Question.where(:v2_condition.not => {"$size" => 0})).all.map{|question| question.v2_condition}.compact
    @results = []
    @tree = {}
    @key = 0
    start_hash = {key: @key, name: "Questionnaire", gender: "TITLE"}
    @results << start_hash
    questions.sort_by{|question| question.position}.each_with_index do |question,index|

      puts "******* QUESTION ID *******"
      puts question.id.to_s
      puts "***************************"
      @impacted_questions_conditons = []

      @conditions.each do |condition|
        if condition.parent_question_id == question.id.to_s
          @impacted_questions_conditons << condition.question.id.to_s
        end
      end

      if @impacted_questions_conditons != []
        @tree[index + 1] = @impacted_questions_conditons.sort_by!{|question| Question.find(question).position.to_i}
      else
        @tree[index + 1] = nil
      end


      # @key += 1
      # if index == 0
      #   empty_hash = {key: @key, parent: (@key - 1), name: "Q#{question.position}", gender: "QUESTION", answers: question.v2_answers.size}
      #   @results << empty_hash.to_h
      # else
      #   @tree.each do |key,value|
      #     if value == nil
      #       empty_hash = {key: index + 1, parent: (@key - 1), name: "Q#{question.position}", gender: "QUESTION", answers: question.v2_answers.size}
      #     else
      #       @subkey = 0
      #       value.each do |val|
      #         @subkey += 1
      #         empty_hash = {key: @key, parent: index + 1, name: "Q#{question.position}", gender: "PATH1", answers: question.v2_answers.size, condition: "oui"}
      #       end
      #     end
      #   end
      #   @results << empty_hash.to_h
      # end


    end


    puts @tree


    @alt_results = []
    empty_hash = {key: 0, name: "Survey", gender: "QUESTION"}
    @alt_results << empty_hash.to_h

    @tree.each do |key,value|
      if key.to_i == 1
        if value == nil
          empty_hash = {key: key.to_i, parent: 0, name: "Q#{key}", gender: "QUESTION", answers: 3}
        else
          @tempo_hashes = []
          question_hash = {key: key.to_i, parent: 0, name: "Q#{key}", gender: "QUESTION", answers: 3}
          @tempo_hashes << question_hash
          value.each do |val|
            empty_hash = {key: Question.find(val).position.to_i, parent: key.to_i, name: "Q#{Question.find(val).position.to_i}", gender: "PATH1", answers: 3, condition: "oui"}
            @tempo_hashes << empty_hash.to_h
          end
        end
      else
        if value == nil
          empty_hash = {key: key.to_i, parent: key.to_i - 1, name: "Q#{key}", gender: "QUESTION", answers: 3}
        else
          @tempo_hashes = []
          question_hash = {key: key.to_i, parent: Question.find(value.last.to_s).position.to_i, name: "Q#{key}", gender: "QUESTION", answers: 3}
          @tempo_hashes << question_hash
          value.each do |val|
            empty_hash = {key: Question.find(val.to_s).position.to_i, parent: key.to_i, name: "Q#{Question.find(val).position.to_i}", gender: "PATH1", answers: 3, condition: "oui"}
            @tempo_hashes << empty_hash.to_h
          end
        end
      end

      if @tempo_hashes
        @tempo_hashes.each do |item|
          @alt_results << item
        end
      else
        @alt_results << empty_hash.to_h
      end

    end


    @alt_results.to_a
    gon.push({
      datas: @alt_results
    })
  end

  def new_question
    @question = Question.new(question_safe_params)
    @position = @question.position.to_i
    @answer = V2Answer.new(answer_safe_params)
    @answer.question = @question
    if @answer.save
      puts "Answer saved"
    else
      puts "Answer not saved"
    end
    if @question.save
      @question.move_to! @position
      redirect_to v2_demand_view_path, notice: "Question créee avec succès"
    end
  end


  def orderable
    @question = Question.find(params[:question_id])
    original_position = params[:original_position].to_i
    new_position = params[:question][:position].to_i
    puts "******* POSITIONS CHANGES *******"
    puts "original: #{original_position}"
    puts "new: #{new_position}"
    puts "*********************************"
    @question.move_to! new_position
    redirect_to v2_demand_view_path, notice: "Change questions positions"
  end

  def old_demand_criterias
    @results = {}
    @demands = Demand.where(activated: true)
    @demands.each do |demand|
      @criterias = demand.important_fields
      if @criterias != []
        @criterias.each do |criteria|
          if @results.has_key?(criteria)
            @results[criteria] += 1
          else
            @results[criteria] = 1
          end
        end
      end
    end
    return @results
  end

  def next_question_on_skip
    @params = params
    @demand = V2Demand.find(params["demand_id"])
    current_position = params["position"].to_i
    @current_question = Question.where(position: current_position).last

    result = GetNextQuestion.new(@current_question,@demand,current_position).call
    @question = result.data if result.success?

    @v2_survey_course_status = v2_survey_course_status(current_position)
    @questions = Question.all.size.to_f

    if @question.has_sentence
      @sentence = @question.sentence
      render partial: "v2/pages/partials/question_with_sentence"
    else
      render partial: "v2/pages/partials/question_item"
    end
  end

  def last_question
    if params["radio_pair"]
      demand_attribute = params["radio_pair"]
      answer_values = []
      personality_params = ["personality_1","personality_2","personality_3","personality_4","personality_5","personality_6","personality_7","personality_8"]
      personality_params.each do |param_hash|
        if params.keys.include?(param_hash)
          answer_values << params["#{param_hash}"]
        end
      end
    else
      demand_attribute = params["demand_field"]
      answer_values = params["#{demand_attribute}"]
    end
    current_position = params["position"].to_i
    @demand = V2Demand.find(params["demand_id"])
    @current_question = Question.where(position: current_position).last

    if current_position != Question.all.size
      UpdateNewDemand.new(@current_question, @demand, demand_attribute, answer_values).call
    end

    puts "*************"
    puts @demand.property_type
    puts "*************"

    if @demand.property_type
      if @demand.property_type.split().last.downcase.to_sym == :appartement
        @major_fields = {"state": "Etat", "style": "Style", "rooms": "Chambres", "budget": "Budget", "surface": "Surface", search_formatted_area: "Zone de recherche", more_rooms: "Pièces supplémentaires", kitchen: "Cuisine", bathrooms: "Salle de bain", flat_exterior: "Extérieur", parking: "Parking"}
        @minor_fields = {"accessibility": "Accessibilité", "exposition": "Exposition"}
      elsif @demand.property_type.split().last.downcase.to_sym == :maison
        @major_fields = {"state": "Etat", "style": "Style", "rooms": "Chambres", "budget": "Budget", "surface": "Surface", search_formatted_area: "Zone de recherche", more_rooms: "Pièces supplémentaires", kitchen: "Cuisine", bathrooms: "Salle de bain", house_exterior: "Extérieur", parking: "Parking", garage: "Garage"}
        @minor_fields = {"house_works": "Travaux", "accessibility": "Accessibilité", "exposition": "Exposition"}
      elsif @demand.property_type.split().last.downcase.to_sym == :terrain
        @major_fields = {"ground_budget": "Budget pour le terrain", "house_budget": "Budget pour la maison", "floor": "Maison avec étage", "surface": "Surface", search_formatted_area: "Zone de recherche"}
      end
    else
      @major_fields = {"budget": "Budget", "broker_contact": "Contact avec un courtier", "banker_contact": "Contact avec un banquier"}
    end
    render partial: "v2/pages/partials/criterias"
  end

  def next_question
    if params[:position].to_i == Question.all.size.to_i
      puts "**** next question params ****"
      puts params[:position].to_i
      puts Question.all.size.to_i
      puts "******************************"
    else
      puts "**** next question params ****"
      puts params[:position].to_i
      puts Question.all.size.to_i
      puts "******************************"
    end
    @params = params
    puts "************ Ajax serialize params **********"
    puts @params
    puts "*********************************************"


    if params["radio_pair"]
      demand_attribute = params["radio_pair"]
      answer_values = []
      personality_params = ["personality_1","personality_2","personality_3","personality_4","personality_5","personality_6","personality_7","personality_8"]
      personality_params.each do |param_hash|
        if params.keys.include?(param_hash)
          answer_values << params["#{param_hash}"]
        end
      end
    else
      demand_attribute = params["demand_field"]
      answer_values = params["#{demand_attribute}"]
    end

    current_position = params["position"].to_i
    @demand = V2Demand.find(params["demand_id"])

    @v2_survey_course_status = v2_survey_course_status(current_position)
    @questions = Question.all.size.to_f

    @current_question = Question.where(position: current_position).last
    UpdateNewDemand.new(@current_question, @demand, demand_attribute, answer_values).call
    result = GetNextQuestion.new(@current_question,@demand,current_position).call
    @question = result.data if result.success?


    if @question.client_request
      @radio_fields = @question.v2_answers.where(answer_type: "radio")
      @text_fields = @question.v2_answers.where(answer_type: "text")
    end

    if @question.sentence
      @sentence = @question.sentence
      render partial: "v2/pages/partials/question_with_sentence"
    else
      render partial: "v2/pages/partials/question_item"
    end
  end


  def multi_circle_batch_constructor
    @demand = Customer.find(session[:bcs_customer_id].to_s).v2_demands.last
    @only_cities = []
    @only_zipcodes = []
    @circle_objects = {}
    @string_results = []
    @coordinates = []
    @concatenate_radius = 0
    @message = "OK"
    @result_as_array = []

    if @demand
      @demand.v2_map_circles.destroy_all
    end

    params["circles"].each do |key,value|
      if ((value["radius"].to_f) / 1000) < 1.10
        retrieve_radius = (value["radius"].to_f / 1000).round(3)
        retrieve_radius = (retrieve_radius * 2.0).round(3)
      else
        retrieve_radius = (value["radius"].to_f / 1000).round(3)
      end
      retrieve_lat = value["lat"]
      retrieve_lng = value["lng"]
      @circle = V2MapCircle.new(x: retrieve_lng, y: retrieve_lat, radius: retrieve_radius)
      @demand.v2_map_circles.push(@circle)
      @concatenate_radius += retrieve_radius.to_f
    end

    @demand.save

    # puts "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
    # puts "#{@demand.map_circles.size} Cercle(s)"
    # puts "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
    @demand.v2_map_circles.each_with_index do |circle,index|
      @circle_objects[index + 1] = {circle_id: "#{index + 1}", x: "#{circle.x}", y: "#{circle.y}", radius: "#{circle.radius}"}.to_json
      meters_in_km = circle.radius

      begin
        circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{circle.y}&lng=#{circle.x}&radius=#{meters_in_km}&username=agaloppe84").body
        if JSON.parse(circle_influence_zone.to_s) == {"geonames"=>[]}
          puts "Old circle radius: #{meters_in_km}"
          meters_in_km = (meters_in_km * 1).round(2)
          puts "New circle radius: #{meters_in_km}"
          circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{circle.y}&lng=#{circle.x}&radius=#{meters_in_km}&username=agaloppe84").body
          if JSON.parse(circle_influence_zone.to_s) == {"geonames"=>[]}
            puts "Nested API call - 2nd times"
            puts "Nested - Old circle radius: #{meters_in_km}"
            meters_in_km = (meters_in_km * 2.5).round(2)
            puts "Nested - New circle radius: #{meters_in_km}"
            circle_influence_zone = RestClient.get("http://api.geonames.org/findNearbyPlaceNameJSON?lat=#{circle.y}&lng=#{circle.x}&radius=#{meters_in_km}&username=agaloppe84").body
          end
        end
      rescue RestClient::ServiceUnavailable
        @message = "Service non disponible"

        # Recursive method
        multi_circle_batch_constructor
      end

      @json = JSON.parse(circle_influence_zone.to_s)
      @json.each do |api_return|
        api_return.last.each do |element|
          distance = element["distance"]
          lng = element["lng"]
          lat = element["lat"]
          gps_coordinate = "#{lat},#{lng}"
          cityname = element["name"].parameterize
          @string_results << " (#{distance}(Km) - #{cityname}) "
          @only_cities << cityname
          current_element_hash = {}
          current_element_hash[:coordinate] = gps_coordinate
          current_element_hash[:district] = cityname
          @coordinates << current_element_hash
        end
      end
    end
    # puts @string_results
    # puts @only_cities

    @coordinates.each do |coordinate|
      zipcode_finder = RestClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + coordinate[:coordinate] + "&key=AIzaSyCdpP4BuVUKk75HM7NENKCUgo7jwFMwQwA").body
      @zipcode_json = JSON.parse(zipcode_finder.to_s)
      components = @zipcode_json["results"][0]["address_components"]
      @result_as_hash = {}
      components.each do |compo|
        if compo["types"][0] === "postal_code"
          @only_zipcodes << compo["short_name"]
          @result_as_hash["zipcode"] = compo["short_name"]
        end
        if compo["types"][0] === "locality"
          @result_as_hash["locality"] = compo["short_name"]
        end
      end

      if (@result_as_hash["locality"].downcase.parameterize) != (coordinate[:district].downcase.parameterize)
        @result_as_hash["district"] = coordinate[:district].downcase.capitalize.gsub("-"," ")
      end

      @result_as_array << @result_as_hash
    end

    @uniq_hashed_results = @result_as_array.uniq{|h| h["zipcode"]}

    puts "UNIQ HASHED RESULTS"
    puts @uniq_hashed_results
    puts @uniq_hashed_results.inspect

    @zipcodes_to_append = []
    @uniq_hashed_results.each do |result_hash_values|
      @zipcodes_to_append << result_hash_values["zipcode"]
    end
    add_postal_codes_to_demand(@demand.id.to_s,@zipcodes_to_append)
    # add_locality_to_demand(@demand.id.to_s,@uniq_hashed_results.first["locality"])
    new_version_add_locality_to_demand(@demand.id.to_s, @uniq_hashed_results)

    puts @demand.inspect

    if @uniq_hashed_results != []
      render json: {message: @message, radius: @concatenate_radius.to_i, cities: @only_cities, fullresponse: @string_results, arrayresponse: @uniq_hashed_results, demandid: @demand.id.to_s}
    elsif @uniq_hashed_results == []
      render json: {message: "empty"}
    end
  end


  private

  def question_safe_params
    params.require(:question).permit(:demand_attribute, :label, :position, :question_type, :required)
  end

  def answer_safe_params
    params.require(:v2_answers_attributes).permit(:answer_type, :text, :order)
  end

  def add_postal_codes_to_demand(demand_id,results)
    @demand = V2Demand.find(demand_id)
    @demand.search_zipcodes_area = results.to_a
    @demand.save
    # event = {zone: results.to_a}
    # @webhook = Webhook.new(service: "BientotChezSoi", action: "Recherche sur la map", description: "Action interne", logo: "kenotte-avatar", content: event.to_h)
    # if @webhook.save
    #   puts "Webhook saved !!"
    #   ActionCable.server.broadcast 'webhooks',
    #     content: @webhook.content.as_json,
    #     service: "#{@webhook.service.downcase.capitalize}",
    #     description: "#{@webhook.description}",
    #     event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
    #     logo: svg(@webhook.logo),
    #     filter_target: "bientotchezsoi",
    #     dynamic_content: [{label: "Zone",value: "#{@webhook.content['zone'].join(" - ")}", html_class: "neutral"}],
    #     custom_action: "#{@webhook.action.capitalize}"
    # end
  end

  def new_version_add_locality_to_demand(demand_id, hashed_results)
    hashed_return = {}
    hashed_results.each do |item|
      city = item["locality"]
      zipcode = item["zipcode"]
      if !hashed_return.has_key?(city)
        hashed_return[city] = []
        hashed_return[city].push(zipcode)
      else
        hashed_return[city].push(zipcode)
      end
    end
    @full_formatted_localisation = []
    hashed_return.each do |key,value|
      if value.size == 1
        fullstring_array = "#{key} (#{value[0]})"
      elsif value.size == 2
        fullstring_array = "#{key} (#{value[0]} et #{value[-1]})"
      elsif value.size > 2
        fullstring_array = "#{key} (#{value[0..-2].join(',')} et #{value[-1]})"
      end
      @full_formatted_localisation << fullstring_array
    end
    if @full_formatted_localisation.size == 1
      @string_test = "#{@full_formatted_localisation.first}"
    else
      @string_test = "#{@full_formatted_localisation[0..-2].join(', ')} et #{@full_formatted_localisation.last}"
    end
    @demand = V2Demand.find(demand_id.to_s)
    @demand.search_formatted_area = @string_test
    @demand.save
  end

  def create_empty_demand
    if session[:bcs_customer_id]
      @customer = Customer.find(session[:bcs_customer_id])
      if @customer.v2_demands.any?
        @demand = @customer.v2_demands.last
      else
        @demand = V2Demand.new()
        @demand.customer = @customer
        @demand.save
      end
    else
      @demand = V2Demand.new()
      @customer = Customer.new()
      @demand.customer = @customer
      @demand.save
      @customer.save(validate: false)
      session[:bcs_customer_id] = @customer.id.to_s
    end
  end

  def handle_results(demand,agencies_ids)
    unless agencies_ids.blank?
      agencies_ids.each do |agency_id|
        agency = Agency.find(agency_id.to_s)
        agency.v2_demands.push(demand)
      end
    else
      return nil
    end
  end

  private

  def safe_params
    params.permit(:query, :page)
  end

end
