var parc_relais = {
"type": "FeatureCollection",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "NOM": "Arts et MÃ©tiers", "NBPLACE": "600 places" }, "geometry": { "type": "Point", "coordinates": [ -0.60108049, 44.80559277, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Carle Vernet", "NBPLACE": "200 places" }, "geometry": { "type": "Point", "coordinates": [ -0.55066979, 44.82005562, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "SÃ©guinaud (1)", "NBPLACE": "151 places" }, "geometry": { "type": "Point", "coordinates": [ -0.51743173, 44.88869647, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Galin", "NBPLACE": "440 places" }, "geometry": { "type": "Point", "coordinates": [ -0.54657646, 44.84970048, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Stalingrad", "NBPLACE": "250 places" }, "geometry": { "type": "Point", "coordinates": [ -0.55735536, 44.8384068, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "ButtiniÃ¨re", "NBPLACE": "600 places" }, "geometry": { "type": "Point", "coordinates": [ -0.52453627, 44.86468886, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Unitec", "NBPLACE": "249 places" }, "geometry": { "type": "Point", "coordinates": [ -0.62231336, 44.79461951, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Quatre Chemins", "NBPLACE": "398 places" }, "geometry": { "type": "Point", "coordinates": [ -0.64597733, 44.83201381, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Bougnard", "NBPLACE": "187 places" }, "geometry": { "type": "Point", "coordinates": [ -0.63425104, 44.79381407, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Les Lauriers", "NBPLACE": "185 places" }, "geometry": { "type": "Point", "coordinates": [ -0.51878698, 44.87819888, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Floirac Dravemont", "NBPLACE": "46 places" }, "geometry": { "type": "Point", "coordinates": [ -0.51038176, 44.8450496, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Pessac Centre", "NBPLACE": "84 places" }, "geometry": { "type": "Point", "coordinates": [ -0.63230893, 44.80484902, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "SÃ©guinaud (2)", "NBPLACE": "248 places" }, "geometry": { "type": "Point", "coordinates": [ -0.51821674, 44.88776062, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Porte de Bordeaux", "NBPLACE": "84 places" }, "geometry": { "type": "Point", "coordinates": [ -0.59774898, 44.83287359, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "MÃ©rignac Centre", "NBPLACE": "84 places" }, "geometry": { "type": "Point", "coordinates": [ -0.64704882, 44.84218417, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Arlac", "NBPLACE": "400 places" }, "geometry": { "type": "Point", "coordinates": [ -0.62574436, 44.82710794, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Ravesies - Le Bouscat", "NBPLACE": "377 places" }, "geometry": { "type": "Point", "coordinates": [ -0.57655175, 44.86694449, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Brandenburg", "NBPLACE": "" }, "geometry": { "type": "Point", "coordinates": [ -0.54421762, 44.87562196, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Les Aubiers", "NBPLACE": "" }, "geometry": { "type": "Point", "coordinates": [ -0.57564737, 44.8734622, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Le Haillan Rostand", "NBPLACE": "" }, "geometry": { "type": "Point", "coordinates": [ -0.66825402, 44.858812, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Les pins", "NBPLACE": "" }, "geometry": { "type": "Point", "coordinates": [ -0.66050944, 44.85658219, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Gare de Begles", "NBPLACE": "" }, "geometry": { "type": "Point", "coordinates": [ -0.55190534, 44.79823278, 0.0 ] } },
{ "type": "Feature", "properties": { "NOM": "Gare de Pessac Alouette", "NBPLACE": "" }, "geometry": { "type": "Point", "coordinates": [ -0.65725388, 44.79383185, 0.0 ] } }
]
};
