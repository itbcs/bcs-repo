class Rent::AgenciesController < ApplicationController

  def show
    @rent_agency = Agency.find(params[:id].to_s)
  end

  def index
    @active_rent_agencies = Agency.active_renter
    @inactive_rent_agencies = Agency.inactive_renter
    @platform_agencies = Agency.platform_agency.not_renter
  end

  def activate
    puts params.inspect
    @rent_agency = Agency.find(params["agency"])
    if @rent_agency
      @rent_agency.active_renter = true
      @rent_agency.save(validate: false)
    else
      puts "Rent agency not found"
    end
    render json: {activate: true}, status: :ok
  end

  def deactivate
    puts params.inspect
    @rent_agency = Agency.find(params["agency"])
    if @rent_agency
      @rent_agency.active_renter = false
      @rent_agency.save(validate: false)
    else
      puts "Rent agency not found"
    end
    render json: {deactivate: true}, status: :ok
  end

  def new
    @rent_agency = Agency.new
  end

  def create
    @rent_agency = Agency.new(safe_params)
    if @rent_agency.save(validate: false)
      redirect_to rent_agency_path(@rent_agency)
    else
      puts @rent_agency.errors.messages
      render :new
    end
  end

  def edit
    @rent_agency = Agency.find(params[:id].to_s)
  end

  def update
    @rent_agency = Agency.find(params[:id].to_s)
    @rent_agency.update_attribute(:name, safe_params[:name])
    @rent_agency.update_attribute(:rent_typeform_contact_email, safe_params[:rent_typeform_contact_email])
    @rent_agency.update_attribute(:siret, safe_params[:siret])
    @rent_agency.update_attribute(:phone, safe_params[:phone])
    @rent_agency.update_attribute(:address, safe_params[:address])
    @rent_agency.update_attribute(:active_renter, safe_params[:active_renter])
    @rent_agency.update_attribute(:typeform_id, safe_params[:typeform_id])
    @rent_agency.update_attribute(:is_renter, safe_params[:is_renter])
    if @rent_agency.save(validate: false)
      redirect_to rent_agency_path(@rent_agency)
    else
      puts @rent_agency.errors.messages
      render :edit
    end
  end

  def destroy
    @rent_agency = Agency.find(params[:id])
    if @rent_agency.destroy
      redirect_to rent_agencies_path
    else
      redirect_to rent_agencies_path
    end
  end

  private

  def safe_params
    params.require(:agency).permit(:name, :rent_typeform_contact_email, :siret, :phone, :address, :active_renter, :typeform_id, :is_renter)
  end

end
