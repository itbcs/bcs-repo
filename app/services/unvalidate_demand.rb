class UnvalidateDemand

  def initialize demand
    @demand = demand
    @errors = []
    @matching = []
  end

  def call
    unvalidate_demand
    service_success
  end

  def unvalidate_demand
    @demand.status = "pending"
    @demand.activated = false
    if @demand.save
      @matching << "Demande #{@demand} - Désactivée"
    end
  end

  private

  def service_success
    ServiceSuccess.new(@matching)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
