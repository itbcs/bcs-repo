var firstInput = $('.first-input-for-validation');
var secondInput = $('.last-input-for-validation');

function handlePhoneValidation(selector) {
  var input = $(selector).find(".phone-input");

  var regex = /^((\+)33|0)[1-9](\d{2}){4}$/;

  $(input).keyup(function(){
    var input = $(this).val();
    console.log(input.match(regex));
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt( input, 10 ) : 0;
    $(this).val( function() {
      return ( input === 0 ) ? "" : input.toLocaleString( "fr-FR" );
    });
  });
};
handlePhoneValidation(firstInput);
handlePhoneValidation(secondInput);
