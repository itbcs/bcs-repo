class V2::AgenciesController < ApplicationController
  before_action :authenticate_agency!, only: [:home, :demands, :demand, :properties, :property_show]

  def props_waiting_for_choice
    @agency = current_agency
    @pending_offers = []
    @agency.properties.each do |prop|
      @choice = CustomerChoice.where(property: prop).last
      if !@choice
        @pending_offers << prop
      end
    end
    @pending_offers = @pending_offers.select{|prop| (prop.v2_demands.size > 0)}
  end



  def home
    @agency = current_agency
    @contact = current_agency
    @sold_properties = @agency.sold_properties

    puts "**** SOLD PROPERTIES COUNT ****"
    puts @sold_properties.count
    puts "*******************************"

    @demands = @agency.active_demands.to_a.first(4)
    @properties = @agency.properties.all.to_a.first(3)

    @properties_with_positive_choice = []
    @properties_with_positive_choice_temp = @agency.properties.to_a.select{|prop| (prop.customer_choices.size > 0)}
    @properties_with_positive_choice_temp.each do |prop|
      prop.customer_choices.each do |cc|
        if cc.choice == "J'aime" || cc.choice == "J'adore"
          @properties_with_positive_choice << prop
        end
      end
    end
    puts "---- PROPS WITH POSITIVE CHOICE ----"
    puts @properties_with_positive_choice.size
    puts "------------------------------------"
    @properties_with_positive_choice.uniq!

    @pending_offers = []
    @agency.active_demands.to_a.first(2).each do |demand|
      demand.customer_choices.each do |customer_choice|
        demand.properties.each do |prop|
          if customer_choice.choice == "J'aime"
            @pending_offers << prop
          end
        end
      end
    end
    @pending_offers


    @pending_offers_test = []
    @agency.properties.each do |prop|
      @choice = CustomerChoice.where(property: prop).last
      if !@choice
        @pending_offers_test << prop
      end
    end
    @pending_offers_test = @pending_offers_test.select{|prop| (prop.v2_demands.size > 0)}

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    @properties_zipcodes = @agency.properties.map do |prop|
      if prop.cp
        prop.cp.to_s.strip
      elsif prop.zipcode
        prop.zipcode.to_s.strip
      end
    end
    render :home
  end

  def demands
    @agency = current_agency
    @demands = @agency.active_demands

    @demand_view = @agency.demand_agency_views

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end
  end

  def search_with_ajax
    @agency = current_agency

    params_hash = {}
    extract_params = params.extract!("utf8", "authenticity_token", "controller", "action")
    params.each_pair{|key, value| params_hash[key] = value if value != ""}

    puts "---- PARAMS ----"
    puts params
    puts "----------------"

    puts "**** PARAMS HASH ****"
    puts params_hash
    puts "*********************"

    if params_hash.length == 1
      simple_query(params_hash)

    elsif params_hash.length == 2
      double_queries(params_hash)

    elsif params_hash.length == 3
      triple_queries(params_hash)

    elsif params_hash.length == 4
      quadruple_queries(params_hash)

    elsif params_hash.length == 5
      quintuple_queries(params_hash)

    elsif params_hash.length == 6
      sextuple_queries(params_hash)

    elsif params_hash.length == 7
      septuple_queries(params_hash)

    elsif params_hash.length == 8
      octuple_queries(params_hash)

    elsif params_hash.length == 9
      nonuple_queries(params_hash)

    elsif params_hash.length == 10
      full_queries(params_hash)
    end

    if !@demands
      @demands = []
    end
    # AgencyDashboardFilters.new(params_hash,current_agency).call
    render partial: 'v2/agencies/partials/demands_ajaxify'
  end

  def demand
    @agency = current_agency
    @demand = V2Demand.find(params[:demand_id])
    personnality_hash
    scoring_about_user
    scoring_about_project

    @view_by_agency = DemandAgencyView.where(agency: @agency).where(v2_demand: @demand).where(view: true).last

    if !@view_by_agency
      puts "Demand unviewed by current agency"
      @demand_agency_view = DemandAgencyView.new(agency: @agency, v2_demand: @demand, view: true)
      if @demand_agency_view.save
        puts "***************************"
        puts "#{@demand_agency_view}"
        puts "#{@demand.id}"
        puts "#{@agency.id}"
        puts "***************************"
      else
        puts "**** ERRORS ****"
        puts @demand_agency_view.errors.full_messages
      end
    else
      puts "Demand viewed by current agency"
    end

    @demand_properties = @demand.properties
    @already_proposed_properties = []
    @demand_properties.each do |prop|
      if @agency.properties.include?(prop)
        @already_proposed_properties << prop
      else
        puts "No prop inside agency"
      end
    end
    @customer = @demand.customer
    @properties = @agency.properties
    @filtered_properties = @properties.reject{|prop| @already_proposed_properties.include?(prop)}

    render :demand
  end

  def properties
    @agency = current_agency
    @properties = @agency.properties

    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    @properties.each do |prop|
      @imgs_attrs.each do |field|
        puts prop.send("#{field}")
        if !prop.send("#{field}").nil?
          @present_imgs << (field.to_s.remove("img_").to_i)
        end
      end
      @present_imgs
    end

    if params[:demands]
      puts params[:demands].inspect
      @matching_demands = params[:demands].map{|demand| V2Demand.find(demand)}
    elsif params[:empty]
      @empty_matching = Property.find(params[:empty])
    end

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end
    render :properties
  end

  def property_show
    @property = Property.find(params[:property_id])
    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]

    @imgs_attrs.each do |field|
      puts @property.send("#{field}")
      if !@property.send("#{field}").nil?
        @present_imgs << (field.to_s.remove("img_").to_i)
      end
    end
    @present_imgs

    gon.push({
      lat: @property.lat,
      lng: @property.lng,
      title: @property.title,
    })
    render :property
  end

  def pending_offers
    @agency = current_agency
    @pending_offers = @agency.v2_demands.all

    if unseen_messages_count != nil
      @count = unseen_messages_count
    end

    @pending_offers_test = []
    @agency.properties.each do |prop|
      @choice = CustomerChoice.where(property: prop).last
      if @choice
        @pending_offers_test << prop
      end
    end

    render :pending_offers
  end

  private

  def simple_query(params_hash)
    if params_hash["property_type"]
      property_type_filters(params_hash)
    elsif params_hash["search_formatted_area"]
      search_formatted_area_filter(params_hash)
    elsif params_hash["agency_view"]
      agency_view_filters(params_hash)
    elsif params_hash["demand_created_at"]
      created_at_filter(params_hash)
    end
  end

  def property_type_filters(params_hash)
    if params_hash["property_type"] == "Une maison"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21})
    elsif params_hash["property_type"] == "Un appartement"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22})
    elsif params_hash["property_type"] == "Un terrain"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23})
    end
  end

  def search_formatted_area_filter(params_hash)
    @demands = @agency.v2_demands.all.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
  end

  def agency_view_filters(params_hash)
    if params_hash["agency_view"] == "true"
      results = []
      @agency.demand_agency_views(view: true).each{|demand_view| results << demand_view.v2_demand}
      @demands = results.reject{|result| result == nil}
    elsif params_hash["agency_view"] = "false"
      results = []
      @agency.v2_demands.each{|v2_demand| results << v2_demand if v2_demand.demand_agency_views.where(agency_id: @agency).count == 0}
      @demands = results
    end
  end

  def created_at_filter(params_hash)
    if params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def double_queries(params_hash)
    if params_hash["budget_min"] && params_hash["budget_max"]
      budget_filters(params_hash)
    elsif params_hash["surface_min"] && params_hash["surface_max"]
      surface_filters(params_hash)
    elsif params_hash["rooms_min"] &&  params_hash["rooms_max"]
      rooms_filters(params_hash)
    elsif params_hash["property_type"] && params_hash["search_formatted_area"]
      property_type_and_area_filters(params_hash)
    elsif params_hash["property_type"] && params_hash["agency_view"]
      property_type_and_view_filters(params_hash)
    elsif params_hash["property_type"] && params_hash["demand_created_at"]
      property_type_and_created_at_filters(params_hash)
    elsif params_hash["search_formatted_area"] && params_hash["agency_view"]
      search_formatted_area_and_view_filters(params_hash)
    elsif params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      search_formatted_area_and_created_at_filters(params_hash)
    elsif params_hash["agency_view"] && params_hash["demand_created_at"]
      agency_view_and_created_at_filters(params_hash)
    end
  end

  def budget_filters(params_hash)
    @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a})
  end

  def surface_filters(params_hash)
    @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).page(params[:page]).per(11)
  end

  def rooms_filters(params_hash)
    @demands = @agency.v2_demands.all.where(rooms: {'$gte': params_hash["rooms_min"], '$lte': params_hash["rooms_max"]})
  end

  def property_type_and_area_filters(params_hash)
    if params_hash["property_type"] == "Une maison" && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif params_hash["property_type"] == "Un appartement" && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif params_hash["property_type"] == "Un terrain" && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def property_type_and_view_filters(params_hash)
    if params_hash["property_type"] == "Une maison" && params_hash["agency_view"] == "true"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: 21)
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["property_type"] == "Un appartement" && params_hash["agency_view"] == "true"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: 22)
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["property_type"] == "Un terrain" && params_hash["agency_view"] == "true"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: 23)
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["property_type"] == "Une maison" && params_hash["agency_view"] == "false"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: 21)
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["property_type"] == "Un appartement" && params_hash["agency_view"] == "false"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: 22)
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["property_type"] == "Un terrain" && params_hash["agency_view"] == "false"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: 23)
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def property_type_and_created_at_filters(params_hash)
    if params_hash["property_type"] == "Une maison" && params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un appartement" && params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un terrain" && params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Une maison" && params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un appartement" && params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un terrain" && params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Une maison" && params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un appartement" && params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un terrain" && params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Une maison" && params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un appartement" && params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif params_hash["property_type"] == "Un terrain" && params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def search_formatted_area_and_view_filters(params_hash)
    if params_hash["search_formatted_area"] && params_hash["agency_view"] == "true"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["search_formatted_area"] && params_hash["agency_view"] == "false"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def search_formatted_area_and_created_at_filters(params_hash)
    if params_hash["search_formatted_area"] && params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.all.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["search_formatted_area"] && params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.all.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["search_formatted_area"] && params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.all.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif params_hash["search_formatted_area"] && params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.all.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def agency_view_and_created_at_filters(params_hash)
    if params_hash["agency_view"] == "true" && params_hash["demand_created_at"] == "7 derniers jours"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["agency_view"] == "true" && params_hash["demand_created_at"] == "14 derniers jours"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["agency_view"] == "true" && params_hash["demand_created_at"] == "30 derniers jours"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["agency_view"] == "true" && params_hash["demand_created_at"] == "Depuis l'an dernier"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["agency_view"] == "false" && arams["demand_created_at"] == "7 derniers jours"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["agency_view"] == "false" && arams["demand_created_at"] == "14 derniers jours"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["agency_view"] == "false" && arams["demand_created_at"] == "30 derniers jours"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["agency_view"] == "false" && arams["demand_created_at"] == "Depuis l'an dernier"

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def triple_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"]
      budget_and_property_type(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["ganeyc_view"]
      budget_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["demand_created_at"]
      budget_and_created_at_filters(params_hash)

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"]
      surface_and_property_type(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["ganeyc_view"]
      surface_and_view_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["demand_created_at"]
      surface_and_created_at_filters(params_hash)

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"]
      rooms_and_property_type(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["ganeyc_view"]
      rooms_and_view_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["demand_created_at"]
      rooms_and_created_at_filters(params_hash)

    elsif params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"]
      property_type_and_area_and_view_filters(params_hash)
    elsif params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      property_type_and_area_and_created_at_filters(params_hash)
    elsif params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"]
      area_and_view_and_created_at_filters(params_hash)
    end
  end

  def budget_and_property_type(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
    end
  end

  def surface_and_property_type(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
    end
  end

  def rooms_and_property_type(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
    end
  end

  def budget_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_view_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_view_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def surface_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def rooms_and_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["demand_created_at"] == "7 derniers jours"
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["demand_created_at"] == "14 derniers jours"
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["demand_created_at"] == "30 derniers jours"
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["demand_created_at"] == "Depuis l'an dernier"
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def property_type_and_area_and_view_filters(params_hash, queries_results, results)
    if (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    end
  end

  def property_type_and_area_and_created_at_filters(params_hash)
    if (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 dernier jours")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def area_and_view_and_created_at_filters(params_hash)
    if params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def quadruple_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$gte': params_hash["rooms_min"], '$lte': params_hash["rooms_max"]})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$gte': params_hash["rooms_min"], '$lte': params_hash["rooms_max"]})

    elsif params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"] == "7 derniers jours"
      property_type_and_area_and_view_and_first_created_at_filters(params_hash)
    elsif params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"] == "14 derniers jours"
      property_type_and_area_and_view_and_second_created_at_filters(params_hash)
    elsif params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"] == "30 derniers jours"
      property_type_and_area_and_view_and_third_created_at_filters(params_hash)
    elsif params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"] == "Depuis l'an dernier"
      property_type_and_area_and_view_and_last_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"]
      budget_and_property_type_and_area_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["agency_view"]
      budget_and_property_type_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_property_type_and_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_property_type_and_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_property_type_and_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_property_type_and_last_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"]
      budget_and_area_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      budget_and_area_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_view_true_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_view_false_and_created_at_filters(params_hash)

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"]
      surface_and_property_type_and_area_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["agency_view"]
      surface_and_property_type_and_view_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "7 derniers jours")
      surface_and_property_type_and_first_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "14 derniers jours")
      surface_and_property_type_and_second_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "30 derniers jours")
      surface_and_property_type_and_third_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      surface_and_property_type_and_last_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"]
      surface_and_area_and_view_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      surface_and_area_and_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      surface_and_view_true_and_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      surface_and_view_false_and_created_at_filters(params_hash)

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"]
      rooms_and_property_type_and_area_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"]
      rooms_and_property_type_and_view_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "7 derniers jours")
      rooms_and_property_type_and_first_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "14 derniers jours")
      rooms_and_property_type_and_second_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "30 derniers jours")
      rooms_and_property_type_and_third_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      rooms_and_property_type_and_last_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"]
      rooms_and_area_and_view_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      rooms_and_area_and_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      rooms_and_view_true_and_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      rooms_and_view_false_and_created_at_filters(params_hash)
    end
  end

  def property_type_and_area_and_view_and_first_created_at_filters(params_hash)
    if (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    end
  end

  def property_type_and_area_and_view_and_second_created_at_filters(params_hash)
    if (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    end
  end

  def property_type_and_area_and_view_and_third_created_at_filters(params_hash)
    if (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    end
  end

  def property_type_and_area_and_view_and_last_created_at_filters(params_hash)
    if (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_property_type_and_area_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def budget_and_property_type_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_property_type_and_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def budget_and_property_type_and_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def budget_and_property_type_and_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def budget_and_property_type_and_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_area_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if iview
      end
    end
  end

  def budget_and_area_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_view_true_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_area_and_view_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if iview
      end
    end
  end

  def surface_and_area_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def surface_and_view_true_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def rooms_and_area_and_view_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if iview
      end
    end
  end

  def rooms_and_area_and_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def rooms_and_view_true_and_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_property_type_and_area_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def surface_and_property_type_and_view_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_property_type_and_first_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def surface_and_property_type_and_second_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def surface_and_property_type_and_third_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def surface_and_property_type_and_last_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def rooms_and_property_type_and_area_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def rooms_and_property_type_and_view_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_property_type_and_first_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def rooms_and_property_type_and_second_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def rooms_and_property_type_and_third_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def rooms_and_property_type_and_last_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def quintuple_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])  && params_hash["property_type"]
      budget_and_surface_and_property_type_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])  && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])  && params_hash["agency_view"]
      budget_and_surface_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])  && params_hash["demand_created_at"]
      budget_and_surface_and_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")
      budget_and_property_type_and_area_and_view_true_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")
      budget_and_property_type_and_area_and_view_false_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 dernier jours")
      budget_and_property_type_and_area_and_first_created_at(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 dernier jours")
      budget_and_property_type_and_area_and_second_created_at(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 dernier jours")
      budget_and_property_type_and_area_and_third_created_at(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_property_type_and_area_and_last_created_at(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_area_and_view_true_and_created_at(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_area_and_view_false_and_created_at(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["property_type"]
      budget_and_rooms_and_property_type_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["agency_view"]
      budget_and_rooms_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["demand_created_at"]
      budget_and_rooms_and_created_at_filters(params_hash)

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")
      surface_and_property_type_and_area_and_view_true_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")
      surface_and_property_type_and_area_and_view_false_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 dernier jours")
      surface_and_property_type_and_area_and_first_created_at(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 dernier jours")
      surface_and_property_type_and_area_and_second_created_at(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 dernier jours")
      surface_and_property_type_and_area_and_third_created_at(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      surface_and_property_type_and_area_and_last_created_at(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      surface_and_area_and_view_true_and_created_at(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      surface_and_area_and_view_false_and_created_at(params_hash)

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["property_type"]
      surface_and_rooms_and_property_type_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["agency_view"]
      surface_and_rooms_and_view_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["demand_created_at"]
      surface_and_rooms_and_created_at_filters(params_hash)

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")
      rooms_and_property_type_and_area_and_view_true_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")
      rooms_and_property_type_and_area_and_view_false_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 dernier jours")
      rooms_and_property_type_and_area_and_first_created_at(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 dernier jours")
      rooms_and_property_type_and_area_and_second_created_at(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 dernier jours")
      rooms_and_property_type_and_area_and_third_created_at(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      rooms_and_property_type_and_area_and_last_created_at(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      rooms_and_area_and_view_true_and_created_at(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      rooms_and_area_and_view_false_and_created_at(params_hash)
    end
  end

  def budget_and_surface_and_property_type_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])  && params_hash["property_type"] == "Une maison"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])  && params_hash["property_type"] == "Un appartement"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
    (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"])  && params_hash["property_type"] == "Un terrain"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
    end
  end

  def budget_and_surface_and_view_filters(params_hash)

    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["demand_created_at"] == "Depuis toujours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_property_type_and_area_and_view_true_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartemment") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_false_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartemment") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_property_type_and_area_and_first_created_at(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def budget_and_property_type_and_area_and_second_created_at(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def budget_and_property_type_and_area_and_third_created_at(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def budget_and_property_type_and_area_and_last_created_at(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_area_and_view_true_and_created_at(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_area_and_view_false_and_created_at(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_true_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartemment") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_false_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartemment") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_property_type_and_area_and_first_created_at(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def surface_and_property_type_and_area_and_second_created_at(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def surface_and_property_type_and_area_and_third_created_at(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def surface_and_property_type_and_area_and_last_created_at(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def surface_and_area_and_view_true_and_created_at(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_area_and_view_false_and_created_at(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_true_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartemment") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_false_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartemment") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_property_type_and_area_and_first_created_at(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def rooms_and_property_type_and_area_and_second_created_at(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def rooms_and_property_type_and_area_and_third_created_at(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def rooms_and_property_type_and_area_and_last_created_at(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formmatedf_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def rooms_and_area_and_view_true_and_created_at(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def rooms_and_area_and_view_false_and_created_at(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["property_type"] == "Une maison"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["property_type"] == "Un appartement"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
    (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])  && params_hash["property_type"] == "Un terrain"
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
    end
  end

  def budget_and_rooms_and_view_filters(params_hash)

    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "Depuis toujours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def surface_and_rooms_and_view_filters(params_hash)

    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @temp_demands.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["demand_created_at"] == "Depuis toujours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def sextuple_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"])
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"]
      budget_and_surface_and_property_type_and_area_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["agency_view"]
      budget_and_surface_and_property_type_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["demand_created_at"]
      budget_and_surface_and_property_type_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"]
      budget_and_surface_and_area_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      budget_and_surface_and_area_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["agency_view"] && params_hash["demand_created_at"]
      budget_and_surface_and_view_and_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"]
      budget_and_rooms_and_property_type_and_area_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"]
      budget_and_rooms_and_property_type_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["demand_created_at"]
      budget_and_rooms_and_property_type_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"]
      budget_and_rooms_and_area_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      budget_and_rooms_and_area_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["agency_view"] && params_hash["demand_created_at"]
      budget_and_rooms_and_view_and_created_at_filters(params_hash)

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"]
      surface_and_rooms_and_property_type_and_area_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"]
      surface_and_rooms_and_property_type_and_view_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["demand_created_at"]
      surface_and_rooms_and_property_type_and_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"]
      surface_and_rooms_and_area_and_view_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["demand_created_at"]
      surface_and_rooms_and_area_and_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["agency_view"] && params_hash["demand_created_at"]
      surface_and_rooms_and_view_and_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_property_type_and_area_and_view_true_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_property_type_and_area_and_view_false_and_created_at_filters(params_hash)

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      surface_and_property_type_and_area_and_view_true_and_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      surface_and_property_type_and_area_and_view_false_and_created_at_filters(params_hash)

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      rooms_and_property_type_and_area_and_view_true_and_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      rooms_and_property_type_and_area_and_view_false_and_created_at_filters(params_hash)
    end
  end

  def budget_and_surface_and_property_type_and_area_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def budget_and_surface_and_property_type_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_area_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_area_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_view_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def budget_and_rooms_and_property_type_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_rooms_and_area_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_area_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_rooms_and_view_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def surface_and_rooms_and_property_type_and_view_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def surface_and_rooms_and_area_and_view_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_area_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def surface_and_rooms_and_view_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_true_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_property_type_and_area_and_view_true_and_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_property_type_and_area_and_view_true_and_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_property_type_and_area_and_view_true_and_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_property_type_and_area_and_view_true_and_last_created_at_filters(params_hash)
    end
  end

  def budget_and_property_type_and_area_and_view_true_and_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_true_and_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_true_and_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_true_and_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_false_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_property_type_and_area_and_view_false_and_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_property_type_and_area_and_view_false_and_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_property_type_and_area_and_view_false_and_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_property_type_and_area_and_view_false_and_last_created_at_filters(params_hash)
    end
  end

  def budget_and_property_type_and_area_and_view_false_and_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_false_and_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_false_and_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_property_type_and_area_and_view_false_and_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_true_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")
      surface_and_property_type_and_area_and_view_true_and_first_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")
      surface_and_property_type_and_area_and_view_true_and_second_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")
      surface_and_property_type_and_area_and_view_true_and_third_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      surface_and_property_type_and_area_and_view_true_and_last_created_at_filters(params_hash)
    end
  end

  def surface_and_property_type_and_area_and_view_true_and_first_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_true_and_second_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_true_and_third_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_true_and_last_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_false_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")
      surface_and_property_type_and_area_and_view_false_and_first_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")
      surface_and_property_type_and_area_and_view_false_and_second_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")
      surface_and_property_type_and_area_and_view_false_and_third_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      surface_and_property_type_and_area_and_view_false_and_last_created_at_filters(params_hash)
    end
  end

  def surface_and_property_type_and_area_and_view_false_and_first_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_false_and_second_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_false_and_third_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_property_type_and_area_and_view_false_and_last_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_true_and_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")
      rooms_and_property_type_and_area_and_view_true_and_first_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")
      rooms_and_property_type_and_area_and_view_true_and_second_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")
      rooms_and_property_type_and_area_and_view_true_and_third_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      rooms_and_property_type_and_area_and_view_true_and_last_created_at_filters(params_hash)
    end
  end

  def rooms_and_property_type_and_area_and_view_true_and_first_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_true_and_second_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_true_and_third_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_true_and_last_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_false_and_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")
      rooms_and_property_type_and_area_and_view_false_and_first_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")
      rooms_and_property_type_and_area_and_view_false_and_second_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")
      rooms_and_property_type_and_area_and_view_false_and_third_created_at_filters(params_hash)
    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      rooms_and_property_type_and_area_and_view_false_and_last_created_at_filters(params_hash)
    end
  end

  def rooms_and_property_type_and_area_and_view_false_and_first_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_false_and_second_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_false_and_third_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def rooms_and_property_type_and_area_and_view_false_and_last_created_at_filters(params_hash)
    if (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def septuple_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["property_type"]
      budget_and_surface_and_rooms_and_property_type_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["agency_view"]
      budget_and_surface_and_rooms_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"]
      budget_and_surface_and_property_type_and_area_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_surface_and_property_type_and_view_and_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_surface_and_property_type_and_view_and_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_surface_and_property_type_and_view_and_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_surface_and_property_type_and_view_and_last_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"]
      budget_and_surface_and_area_and_view_and_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"]
      budget_and_rooms_and_property_type_and_area_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_rooms_and_property_type_and_view_and_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_rooms_and_property_type_and_view_and_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_rooms_and_property_type_and_view_and_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_rooms_and_property_type_and_view_and_last_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"]
      budget_and_rooms_and_area_and_view_and_created_at_filters(params_hash)

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && params_hash["agency_view"]
      surface_and_rooms_and_property_type_and_area_and_view_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "7 derniers jours")
      surface_and_rooms_and_property_type_and_view_and_first_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "14 derniers jours")
      surface_and_rooms_and_property_type_and_view_and_second_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "30 derniers jours")
      surface_and_rooms_and_property_type_and_view_and_third_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      surface_and_rooms_and_property_type_and_view_and_last_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && params_hash["agency_view"] && params_hash["demand_created_at"]
      surface_and_rooms_and_area_and_view_and_created_at_filters(params_hash)
    end
  end

  def budget_and_surface_and_rooms_and_property_type_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Une maison")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un appartement")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un terrain")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
    end
  end

  def budget_and_surface_and_rooms_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_rooms_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_property_type_and_area_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_view_and_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_view_and_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_view_and_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_view_and_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_area_and_view_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_view_and_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_view_and_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_view_and_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_view_and_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_area_and_view_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_and_view_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_view_and_first_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_view_and_second_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_view_and_third_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_view_and_last_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_area_and_view_and_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 dernier jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def octuple_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"]
      budget_and_surface_and_rooms_and_property_type_and_area_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["agency_view"]
      budget_and_surface_and_rooms_and_property_type_and_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_surface_and_rooms_and_property_type_and_last_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["serch_formatted_area"] && params_hash["agency_view"]
      budget_and_surface_and_rooms_and_area_view_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["serch_formatted_area"] && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_area_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_view_true_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_view_false_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] + params_hash["budget_max"]) && (params_hash["surface_min"] + params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_surface_and_property_type_and_area_view_true_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] + params_hash["budget_max"]) && (params_hash["surface_min"] + params_hash["surface_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_surface_and_property_type_and_area_view_false_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] + params_hash["budget_max"]) && (params_hash["rooms_min"] + params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_rooms_and_property_type_and_area_view_true_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] + params_hash["budget_max"]) && (params_hash["rooms_min"] + params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_rooms_and_property_type_and_area_view_false_created_at_filters(params_hash)

    elsif (params_hash["surface_min"] + params_hash["surface_max"]) && (params_hash["rooms_min"] + params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      surface_and_rooms_and_property_type_and_area_view_true_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] + params_hash["surface_max"]) && (params_hash["rooms_min"] + params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      surface_and_rooms_and_property_type_and_area_view_false_created_at_filters(params_hash)
    end
  end



  def budget_and_surface_and_rooms_and_property_type_and_area_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"]
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def budget_and_surface_and_rooms_and_property_type_and_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def budget_and_surface_and_rooms_and_property_type_and_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

   def budget_and_surface_and_rooms_and_property_type_and_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["rooms_min"] && (params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_rooms_and_area_view_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_rooms_and_area_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["search_formatted_area"] && (params_hash["demande_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["search_formatted_area"] && (params_hash["demande_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["search_formatted_area"] && (params_hash["demande_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && params_hash["search_formatted_area"] && (params_hash["demande_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_rooms_and_view_true_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_rooms_and_view_false_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["room_max"]) && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_area_view_true_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_surface_and_property_type_and_area_view_true_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_surface_and_property_type_and_area_view_true_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_surface_and_property_type_and_area_view_true_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_surface_and_property_type_and_area_view_true_last_created_at_filters(params_hash)
    end
  end

  def budget_and_surface_and_property_type_and_area_view_true_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_property_type_and_area_view_true_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_property_type_and_area_view_true_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_property_type_and_area_view_true_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

   def budget_and_surface_and_property_type_and_area_view_false_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_surface_and_property_type_and_area_view_false_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_surface_and_property_type_and_area_view_false_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_surface_and_property_type_and_area_view_false_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_surface_and_property_type_and_area_view_false_last_created_at_filters(params_hash)
    end
  end

  def budget_and_surface_and_property_type_and_area_view_false_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_area_view_false_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_area_view_false_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_property_type_and_area_view_false_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_true_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_rooms_and_property_type_and_area_view_true_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_rooms_and_property_type_and_area_view_true_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_rooms_and_property_type_and_area_view_true_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_rooms_and_property_type_and_area_view_true_last_created_at_filters(params_hash)
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_true_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_true_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_true_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_true_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

   def budget_and_rooms_and_property_type_and_area_view_false_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_rooms_and_property_type_and_area_view_false_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_rooms_and_property_type_and_area_view_false_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_rooms_and_property_type_and_area_view_false_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_rooms_and_property_type_and_area_view_false_last_created_at_filters(params_hash)
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_false_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_false_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_false_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_rooms_and_property_type_and_area_view_false_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_true_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")
      surface_and_rooms_and_property_type_and_area_view_true_first_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")
      surface_and_rooms_and_property_type_and_area_view_true_second_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")
      surface_and_rooms_and_property_type_and_area_view_true_third_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      surface_and_rooms_and_property_type_and_area_view_true_last_created_at_filters(params_hash)
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_true_first_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_true_second_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_true_third_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_true_last_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

   def surface_and_rooms_and_property_type_and_area_view_false_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")
      surface_and_rooms_and_property_type_and_area_view_false_first_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")
      surface_and_rooms_and_property_type_and_area_view_false_second_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")
      surface_and_rooms_and_property_type_and_area_view_false_third_created_at_filters(params_hash)
    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      surface_and_rooms_and_property_type_and_area_view_false_last_created_at_filters(params_hash)
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_false_first_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_false_second_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_false_third_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def surface_and_rooms_and_property_type_and_area_view_false_last_created_at_filters(params_hash)
    if (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def nonuple_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_first_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_second_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_third_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_surface_and_rooms_and_property_type_and_area_and_last_created_at_filters(params_hash)

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_area_and_view_true_and_created_at_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_area_and_view_false_and_created_at_filters(params_hash)
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_first_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "7 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_second_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "14 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_third_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "30 derniers jours")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_last_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      @demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
    end
  end

  def budget_and_surface_and_rooms_and_area_and_view_true_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_rooms_and_area_and_view_false_and_created_at_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def full_queries(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_created_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && params_hash["demand_created_at"]
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_created_filters(params_hash)
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_first_created_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_second_created_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_third_created_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_last_created_filters(params_hash)
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_first_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_second_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_third_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_true_and_last_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "true") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_first_created_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_second_created_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_third_created_filters(params_hash)
    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && params_hash["property_type"] && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")
      budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_last_created_filters(params_hash)
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_first_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "7 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 1.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_second_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "14 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 2.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_third_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "30 derniers jours")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': 4.week.ago.in_time_zone, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def budget_and_surface_and_rooms_and_property_type_and_area_and_view_false_and_last_created_filters(params_hash)
    if (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Une maison") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 21}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un appartement") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 22}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end

    elsif (params_hash["budget_min"] && params_hash["budget_max"]) && (params_hash["surface_min"] && params_hash["surface_max"]) && (params_hash["rooms_min"] && params_hash["rooms_max"]) && (params_hash["property_type"] == "Un terrain") && params_hash["search_formatted_area"] && (params_hash["agency_view"] == "false") && (params_hash["demand_created_at"] == "Depuis l'an dernier")

      @temp_demands = @agency.v2_demands
      @temp_demands = @agency.v2_demands.all.where(budget: {'$in' => (params_hash["budget_min"]..params_hash["budget_max"]).step(1000).to_a}).where(surface: {'$in' => (params_hash["surface_min"]..params_hash["surface_max"]).step(1000).to_a}).where(rooms: {'$in' => (params_hash["rooms_min"]..params_hash["rooms_max"]).step(1000).to_a}).where(property_type: {'$regex': 23}).where(search_formatted_area: {'$regex': params_hash["search_formatted_area"].to_s}).where(created_at: {'$gte': Time.zone.now.last_year, '$lte': Time.zone.now})
      @temp_demands.each do |demand|
        view = DemandAgencyView.where(v2_demand: demand, agency: @agency).last
        @demands << demand if !view
      end
    end
  end

  def unseen_messages_count
    @count = 0
    @agency = current_agency
    @agency_chatrooms = Chatroom.where(agency_id: @agency.id.to_s)

    @unseen_messages = {}
    @agency_chatrooms.each do |chatroom|
      unseen_messages = chatroom.chat_messages.where(seen_by_agency: false)
      if unseen_messages.to_a != []
        @unseen_messages[chatroom] = []
        @unseen_messages[chatroom] << unseen_messages.to_a
      end
    end

    @unseen_messages.each do |chatroom,chat_messages|
      chat_messages.each do |chatroom_message|
        chatroom_message.each do |msg|
          if msg.seen_by_agency == false
            @count += 1
          end
        end
      end
    end

    if @count == 0
      return nil
    else
      return @count
    end

  end

  def personnality_hash
    @svg_for_men = { 'aladdin' => 'Aladdin',
                    'cesar' => 'Jules César',
                    'churchill' => 'Winston Churchill',
                    'kurt-cobain' => 'Kurt Cobain',
                    'petit-prince' => 'Le petit Prince',
                    'van-gogh' => 'Vincent van Gogh'
    }.to_a.sample

    @svg_for_women = { 'amelie-poulain' => 'Amélie Poulain',
                      'cleopatre' => 'Cléopatre',
                      'coco-chanel' => 'Coco Chanel',
                      'leia' => 'Leia',
                      'raiponce' => 'Raiponce',
                      'sissi' => 'Princesse Sissi'
    }.to_a.sample
  end

  def scoring_hash
    @scoring_hash = { "actual_situation" => { "auto_identifier_11" => 0,
                                              "auto_identifier_12" => 5,
                                              "auto_identifier_13" => 5,
                    },
                      "age" => { "auto_identifier_44" => 1,
                                  "auto_identifier_45" => 3,
                                  "auto_identifier_46" => 4,
                                  "auto_identifier_47" => 5
                      },
                      "search_time" => { "auto_identifier_24" => 1,
                                          "auto_identifier_25" => 3,
                                          "auto_identifier_26" => 4,
                                          "auto_identifier_27" => 5

                      },
                      "project" => { "auto_identifier_7" => 1,
                                      "auto_identifier_8" => 3,
                                      "auto_identifier_9" => 4,
                                      "auto_identifier_10" => 5

                      },
                      "created_at" => { "moins de 7 jours" =>  1,
                                        "de 8 à 21 jours" => 2,
                                        "de 22 à 35 jours" => 3,
                                        "plus de 36 jours" => 5

                      },
                      "last_log_in" => { "moins de 7 jours" =>  1,
                                        "de 8 à 21 jours" => 2,
                                        "de 22 à 35 jours" => 3,
                                        "plus de 36 jours" => 5

                      },
                      "installation_date" => { "auto_identifier_16" => 1,
                                                "auto_identifier_17" => 3,
                                                "auto_identifier_18" => 4,
                                                "auto_identifier_19" => 5
                      },
                      "visited_property_count" => { "auto_identifier_28" => 1,
                                                    "auto_identifier_29" => 3,
                                                    "auto_identifier_30" => 4,
                                                    "auto_identifier_31" => 5

                      },
                      "banker_contact" => { "auto_identifier_58" => 0,
                                            "auto_identifier_59" => 2,
                                            "auto_identifier_60" => 4
                      },
                      "broker_contact" => { "auto_identifier_61" => 0,
                                            "auto_identifier_62" => 2,
                                            "auto_identifier_63" => 4

                      },
                      "destination" => { "auto_identifier_61" => 5,
                                          "auto_identifier_62" => 3,
                                          "auto_identifier_63" => 3

                      },
                      "properties" => { "moins de 2 offres" => 1,
                                        "de 3 à 5 offres" => 2,
                                        "de 6 à 8 offres" => 3,
                                        "plus de 8 offres" => 5
                      },
                      "customer_choices" => { "0" => 2,
                                              "de 1 à 3 réactions" => 3,
                                              "de 4 à 6 réactions" => 4,
                                              "plus de 6 réactions" => 5
                      },
                      "personality" => { "auto_identifier_136" => 1,
                                          "auto_identifier_137" => 2,
                                          "auto_identifier_138" => 1,
                                          "auto_identifier_139" => 2,
                                          "auto_identifier_140" => 1,
                                          "auto_identifier_141" => 2,
                                          "auto_identifier_142" => 1,
                                          "auto_identifier_143" => 2,
                                          "auto_identifier_144" => 1,
                                          "auto_identifier_145" => 2,
                                          "auto_identifier_146" => 1,
                                          "auto_identifier_147" => 2,
                                          "auto_identifier_148" => 1,
                                          "auto_identifier_149" => 2,
                                          "auto_identifier_150" => 1,
                                          "auto_identifier_151" => 2
                      }
  }
  # numeric_hash["actual_situation"].select{|k,v| puts numeric_hash["actual_situation"][k] if (k > 7 && k < 35)}
  end

  def scoring_about_user
    @demand = V2Demand.find(params[:demand_id])

    @user_progression = 0
    @counter_user = 0
    date_now = Date.today
    date_sent = Date.parse(@demand.created_at.strftime('%F'))
    date_sent_on_connexion = Date.parse(@demand.customer.last_sign_in_at.strftime('%F'))
    day_passed_after_create = (date_now - date_sent).to_i
    day_passed_since_last_connexion = (date_now - date_sent_on_connexion).to_i

    scoring_hash

    if @demand.actual_situation
      if V2Answer.find_by(auto_identifier: @demand["actual_situation"]).auto_identifier == 11
        @counter_user += scoring_hash["actual_situation"]["auto_identifier_11"]
      elsif V2Answer.find_by(auto_identifier: @demand["actual_situation"]).auto_identifier == 12
        @counter_user += scoring_hash["actual_situation"]["auto_identifier_12"]
      elsif V2Answer.find_by(auto_identifier: @demand["actual_situation"]).auto_identifier == 13
        @counter_user += scoring_hash["actual_situation"]["auto_identifier_13"]
      end
    end

    if @demand.age
      if V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 44
        @counter_user += scoring_hash["age"]["auto_identifier_44"]
      elsif V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 45
        @counter_user += scoring_hash["age"]["auto_identifier_45"]
      elsif V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 46
        @counter_user += scoring_hash["age"]["auto_identifier_46"]
      elsif V2Answer.find_by(auto_identifier: @demand["age"]).auto_identifier == 47
        @counter_user += scoring_hash["age"]["auto_identifier_47"]
      end
    end

    if @demand.search_time
      if V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 24
        @counter_user += scoring_hash["search_time"]["auto_identifier_24"]
      elsif V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 25
        @counter_user += scoring_hash["search_time"]["auto_identifier_25"]
      elsif V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 26
        @counter_user += scoring_hash["search_time"]["auto_identifier_26"]
      elsif V2Answer.find_by(auto_identifier: @demand["search_time"]).auto_identifier == 27
        @counter_user += scoring_hash["search_time"]["auto_identifier_27"]
      end
    end

    if @demand.project
      if V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 7
        @counter_user += scoring_hash["project"]["auto_identifier_7"]
      elsif V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 8
        @counter_user += scoring_hash["project"]["auto_identifier_8"]
      elsif V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 9
        @counter_user += scoring_hash["project"]["auto_identifier_9"]
      elsif V2Answer.find_by(auto_identifier: @demand["project"]).auto_identifier == 10
        @counter_user += scoring_hash["project"]["auto_identifier_10"]
      end
    end

    if @demand.created_at
      if day_passed_after_create <= 7
        @counter_user += scoring_hash["created_at"]["moins de 7 jours"]
      elsif (day_passed_after_create >= 8) && (day_passed_after_create <= 21)
        @counter_user += scoring_hash["created_at"]["de 8 à 21 jours"]
      elsif (day_passed_after_create >= 22) && (day_passed_after_create <= 35)
        @counter_user += scoring_hash["created_at"]["de 22 à 35 jours"]
      elsif day_passed_after_create >= 36
        @counter_user += scoring_hash["created_at"]["plus de 36 jours"]
      end
    end

    if @demand.customer.last_sign_in_at
      if day_passed_since_last_connexion <= 7
        @counter_user += scoring_hash["created_at"]["moins de 7 jours"]
      elsif (day_passed_since_last_connexion >= 8) && (day_passed_since_last_connexion <= 21)
        @counter_user += scoring_hash["created_at"]["de 8 à 21 jours"]
      elsif (day_passed_since_last_connexion >= 22) && (day_passed_since_last_connexion <= 35)
        @counter_user += scoring_hash["created_at"]["de 22 à 35 jours"]
      elsif day_passed_since_last_connexion >= 36
        @counter_user += scoring_hash["created_at"]["plus de 36 jours"]
      end
    end

    if @demand.personality
      @demand["personality"].each do |id|
        if V2Answer.find_by(auto_identifier: id).auto_identifier == 136
          @counter_user += scoring_hash["personality"]["auto_identifier_136"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 137
          @counter_user += scoring_hash["personality"]["auto_identifier_137"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 139
          @counter_user += scoring_hash["personality"]["auto_identifier_139"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 138
          @counter_user += scoring_hash["personality"]["auto_identifier_138"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 140
          @counter_user += scoring_hash["personality"]["auto_identifier_140"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 141
          @counter_user += scoring_hash["personality"]["auto_identifier_141"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 142
          @counter_user += scoring_hash["personality"]["auto_identifier_142"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 143
          @counter_user += scoring_hash["personality"]["auto_identifier_143"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 144
          @counter_user += scoring_hash["personality"]["auto_identifier_144"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 145
          @counter_user += scoring_hash["personality"]["auto_identifier_145"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 146
          @counter_user += scoring_hash["personality"]["auto_identifier_146"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 147
          @counter_user += scoring_hash["personality"]["auto_identifier_147"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 148
          @counter_user += scoring_hash["personality"]["auto_identifier_148"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 149
          @counter_user += scoring_hash["personality"]["auto_identifier_149"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 150
          @counter_user += scoring_hash["personality"]["auto_identifier_150"]
        elsif V2Answer.find_by(auto_identifier: id).auto_identifier == 151
          @counter_user += scoring_hash["personality"]["auto_identifier_151"]
        end
      end
    end

    if (@counter_user < 13) || (@counter_user == 13)
      @user_progression = -90
    elsif (@counter_user > 13) && (@counter_user <= 17)
      @user_progression = -70
    elsif (@counter_user > 17) && (@counter_user <= 21)
      @user_progression = -47
    elsif (@counter_user > 21) && (@counter_user <= 25)
      @user_progression = -20
    elsif (@counter_user > 25) && (@counter_user <= 30)
      @user_progression = 0
    elsif (@counter_user > 30) && (@counter_user <= 34)
      @user_progression = 25
    elsif (@counter_user > 34) && (@counter_user <= 38)
      @user_progression = 45
    elsif @counter_user > 38
      @user_progression = 75
    end
  end

  def scoring_about_project
    @demand = V2Demand.find(params[:demand_id])

    @project_progression = 0
    @counter_project = 0

    scoring_hash

    if @demand.installation_date
      if V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 16
        @counter_project += scoring_hash["installation_date"]["auto_identifier_16"]
      elsif V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 17
        @counter_project += scoring_hash["installation_date"]["auto_identifier_17"]
      elsif V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 18
        @counter_project += scoring_hash["installation_date"]["auto_identifier_18"]
      elsif V2Answer.find_by(auto_identifier: @demand["installation_date"]).auto_identifier == 19
        @counter_project += scoring_hash["installation_date"]["auto_identifier_19"]
      end
    end

    if @demand.visited_property_count
      if V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 28
        @counter_project += scoring_hash["visited_property_count"]["auto_identifier_28"]
      elsif V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 29
        @counter_project += scoring_hash["visited_property_count"]["auto_identifier_29"]
      elsif V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 30
        @counter_project += scoring_hash["visited_property_count"]["auto_identifier_30"]
      elsif V2Answer.find_by(auto_identifier: @demand["visited_property_count"]).auto_identifier == 31
        @counter_project += scoring_hash["visited_property_count"]["auto_identifier_31"]
      end
    end

    if @demand.banker_contact
      if V2Answer.find_by(auto_identifier: @demand["banker_contact"]).auto_identifier == 58
        @counter_project += scoring_hash["banker_contact"]["auto_identifier_58"]
      elsif V2Answer.find_by(auto_identifier: @demand["banker_contact"]).auto_identifier == 59
        @counter_project += scoring_hash["banker_contact"]["auto_identifier_59"]
      elsif V2Answer.find_by(auto_identifier: @demand["banker_contact"]).auto_identifier == 60
        @counter_project += scoring_hash["banker_contact"]["auto_identifier_60"]
      end
    end

    if @demand.broker_contact
      if V2Answer.find_by(auto_identifier: @demand["broker_contact"]).auto_identifier == 61
        @counter_project += scoring_hash["broker_contact"]["auto_identifier_61"]
      elsif V2Answer.find_by(auto_identifier: @demand["broker_contact"]).auto_identifier == 62
        @counter_project += scoring_hash["broker_contact"]["auto_identifier_62"]
      elsif V2Answer.find_by(auto_identifier: @demand["broker_contact"]).auto_identifier == 63
        @counter_project += scoring_hash["broker_contact"]["auto_identifier_63"]
      end
    end

    if @demand.destination
      if V2Answer.find_by(auto_identifier: @demand["destination"]).auto_identifier == 61
        @counter_project += scoring_hash["destination"]["auto_identifier_61"]
      elsif V2Answer.find_by(auto_identifier: @demand["destination"]).auto_identifier == 62
        @counter_project += scoring_hash["destination"]["auto_identifier_62"]
      elsif V2Answer.find_by(auto_identifier: @demand["destination"]).auto_identifier == 63
        @counter_project += scoring_hash["destination"]["auto_identifier_63"]
      end
    end

    if @demand.properties
      if @demand.properties.count <= 2
        @counter_project += scoring_hash["properties"]["moins de 2 offres"]
      elsif (@demand.properties.count >= 3) && (@demand.properties.count <= 5)
        @counter_project += scoring_hash["properties"]["de 3 à 5 offres"]
      elsif (@demand.properties.count >= 6) && (@demand.properties.count <= 8)
        @counter_project += scoring_hash["properties"]["de 6 à 8 offres"]
      elsif @demand.properties.count > 8
        @counter_project += scoring_hash["properties"]["plus de 8 offres"]
      end
    end

    if @demand.customer_choices
      if @demand.customer_choices == []
        @counter_project += scoring_hash["customer_choices"]["0"]
      elsif (@demand.customer_choices.size >= 1) && (@demand.customer_choices.size <= 3)
        @counter_project += scoring_hash["customer_choices"]["de 1 à 3 réactions"]
      elsif (@demand.customer_choices.size >= 4) && (@demand.customer_choices.size <= 6)
        @counter_project += scoring_hash["customer_choices"]["de 4 à 6 réactions"]
      elsif @demand.customer_choices.size > 6
        @counter_project += scoring_hash["customer_choices"]["plus de 6 réactions"]
      end
    end

    if (@counter_project < 8) || (@counter_project == 8)
      @project_progression = -90
    elsif (@counter_project > 8) && (@counter_project <= 11)
      @project_progression = -70
    elsif (@counter_project > 11) && (@counter_project <= 14)
      @project_progression = -47
    elsif (@counter_project > 14) && (@counter_project <= 17)
      @project_progression = -20
    elsif (@counter_project > 17) && (@counter_project <= 20)
      @project_progression = 0
    elsif (@counter_project > 20) && (@counter_project <= 23)
      @project_progression = 25
    elsif (@counter_project > 23) && (@counter_project <= 26)
      @project_progression = 45
    elsif @counter_project > 26
      @project_progression = 75
    end
  end
end
