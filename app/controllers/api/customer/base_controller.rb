class Api::Customer::BaseController < ApplicationController
  before_action :authenticate_customer_request
  attr_reader :current_customer

  private

  def authenticate_customer_request
    @current_customer = AuthorizeCustomerApiRequest.call(request.headers).result
    render json: { error: 'Not Authorized' }, status: 401 unless @current_customer
  end

end
