class Array

  def generate_estimation_button_icon
    if self == ["Une maison"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/estimation-house.svg", height: "25", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    elsif self == ["Un appartement"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/estimation-flat.svg", height: "22", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    end
    result
  end

  def generate_quality_items
    if self == ["Il y a des travaux de rénovation à prévoir"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    elsif self == ["Il est de qualité comparable aux autres biens du quartier"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    elsif self == ["Il est d’un standing supérieur aux autres bien du quartier"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    end
    result
  end

end
