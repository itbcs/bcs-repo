function gtmCustomEvent(position,label,value,theme) {
  window.dataLayer = window.dataLayer || [];
  dataLayer.push({
    'event': 'surveySubmit',
    'position': position,
    'eventLabel': label,
    'eventAction': theme + " - position " + position.toString(),
    'eventCategory': 'Category - tracking questionnaire',
    'surveyValue': value
  });
}

function addResumeRecap(innerHTML) {
  var fullSurveyContainer = $('.full-survey-container');
  var questionsContainer = $(".questions-container");
  // EMPTY PAGE WHEN SURVEY ENDED
  $(".overflow-container").css("height", "100%");
  $(questionsContainer).remove();
  $('.resume-container').remove();
  var recapContainer = $(innerHTML).find('#criterias_form');
  $(recapContainer).appendTo(fullSurveyContainer);
}

function dynamicAnswerScroll(){
  $(document).ready(function(){
    var fullSurveyContainer = $('.resume-container');
    $(fullSurveyContainer).animate({
      scrollTop: ($(fullSurveyContainer).get(0).scrollHeight) + 180}, 600);
  });
}

function svgWaitingLogo(){
  var logo = '<div class="loader loader--style1" title="0"> <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"> <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/> <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z"> <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatCount="indefinite"/> </path> </svg></div>';
  return logo;
}

function addLoader(position) {
  var demandContainer = $('.questions-container');
  $("#survey-form-" + position.toString()).fadeOut("slow");
  var waitingTime = $(svgWaitingLogo());
  $(waitingTime).appendTo(demandContainer);
}


function removeEmptyResume(surveyPosition) {
  if (surveyPosition == 1) {
    $(".empty-resume").remove();
  }
}

function removePastSurvey(position) {
  var demandContainer = $('.questions-container');
  $("#survey-form-" + position.toString()).hide().removeClass('survey-form');
  // $(demandContainer).empty();
}

function addNextSurvey(innerHTML,position) {
  $('.loader').remove();
  var demandContainer = $('.questions-container');
  var position2 = $(innerHTML).find('.current-position-data').text();
  var currentSurvey = $(innerHTML).find('.survey-form');
  $(currentSurvey).appendTo(demandContainer).hide().fadeIn("slow");
}

function addUserAnswer(innerHTML) {
  var userAnswer = $(innerHTML).find('.survey-recap-container');
  var resumeContainer = $('.resume-container');
  $(userAnswer).appendTo(resumeContainer).hide();
  $('.survey-recap-container').show('slide',{direction:'left'},600);
}

function handleNextSurveySuccess(innerHTML,surveyPosition,surveyTheme,surveyLabel,surveyValue,form,surveyCount,condition){
  if ((parseInt(surveyPosition)) < surveyCount){
    removePastSurvey(surveyPosition);
    gtmCustomEvent(surveyPosition,surveyLabel,surveyValue,surveyTheme);
    addNextSurvey(innerHTML,surveyPosition);
    addUserAnswer(innerHTML);
    dynamicButtonsLogic(innerHTML);
    formatNumericInput();
    surveyCourseStatus(innerHTML);
    removeEmptyResume(surveyPosition);
    progressionImage(innerHTML);
    progressionBubble(surveyTheme,innerHTML);
    dynamicAnswerScroll();
    answerEdition();
  } else {
    // positionHintRemove();
    removePastSurvey(surveyPosition);
    gtmCustomEvent(surveyPosition,surveyLabel,surveyValue,surveyTheme);
    addUserAnswer(innerHTML);
    lastSurveyCourseStatus(innerHTML);
    addResumeRecap(innerHTML);
    checkedInputsLogic();
    finalSurveyModalTrigger(innerHTML);
    // disableButtonOnClick();
  }
}

function bindSurveyForm(){
  $(".survey-form").submit(function(event){
      // var submitButton = $(this).find('.btn-button-sm');
      // console.log(submitButton);
      // $(submitButton).prop("disabled", true);
      event.preventDefault();
      var buttonSubmit = $(this).find(":button[type=submit]:focus");

      var $form = $(this),
            url = $form.attr('action'),
            method = $form.attr('method'),
            position = $form.find('input[name="position"]').val();
            demandId = $form.find('input[name="demand_id"]').val();
            surveyId = $form.find('input[name="survey_id"]').val();
            surveyCount = $form.find('input[name="survey_count"]').val();
            label = $form.find('input[name="label_demand"]').val();
            value = $form.find('input[name="value"]').val();
            theme = $form.find('input[name="theme"]').val();
            condition = $form.find('input[name="condition"]').val();

      if (buttonSubmit.data("passed")) {
        console.log("Vous avez cliqué sur je passe !!!!");
        var inputs = $form.find(".input-container input[name='answer_ids[]']");
        inputs.each(function(){
          inputValue = $(this).val();
          console.log(inputValue);
        });
      } else {
        console.log("Vous avez cliqué sur je continue !!!!");
      }


      if ((parseInt(position)) < (parseInt(surveyCount))){
        addLoader(position);
      }

      var request = $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      success: function(responseData, textStatus, jqXHR) {
        handleNextSurveySuccess(responseData,position,theme,label,value,$form,surveyCount,condition);
        bindSurveyForm();
      },
      error: function(responseData, textStatus, jqXHR) {
        alert("Fatal Error");
      }
    });
  });
}

bindSurveyForm();
