class SmsValidationsController < ApplicationController

  def new
    @test = "Test sms validation page"
  end

  def create
    @code = Devise.friendly_token.first(6)
    @message = "Voici votre code de validation : #{@code}"
    @phone = params[:phone]
    if Phonelib.valid?(@phone)
      phone = Phonelib.parse(@phone)
      @formatted_phone = phone.full_e164
      @sms = SmsValidation.create(to: @formatted_phone, text: @message, code: @code.to_s)
      deliver(@sms)
      datas = {}
      datas[:phone] = @formatted_phone
      datas[:code] = @code
      render :json => datas.to_json
    else
      render :json => {status: "numéro incorrect !"}.to_json
    end
  end

  def sms_return_validation
  end

  def deliver(sms)
    nexmo = Nexmo::Client.new
    response = nexmo.sms.send(from: "Bientotchezsoi", to: sms.to, text: sms.text)
  end

  private

  def sms_validation_params
    params.require(:sms_validation).permit(:phone, :to, :text, :code)
  end

end
