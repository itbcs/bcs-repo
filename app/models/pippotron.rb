class Pippotron

  def dicos
    words_1 = ["Dans le but de pallier à",
               "Compte tenu de",
               "Malgré",
               "Pour réagir face à",
               "Afin de circonvenir à",
               "En ce qui concerne"]
    words_2 = ["l'austérité",
               "la dégradation",
               "cette rigueur",
               "la dualité de la situation",
               "la baisse de confiance",
               "la morosité",
               "la situation",
               "l'ambiance",
               "la politique"]
    words_3 = ["actuelle",
               "générale",
               "induite",
               "conjoncturelle",
               "observée",
               "de ces derniers temps",
               "intrinsèque",
               "que nous constatons",
               "présente"]
    @result = "#{words_1.sample} #{words_2.sample} #{words_3.sample}"
  end

end
