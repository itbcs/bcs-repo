class FetchMapPhoto
  @queue = :low
  def self.perform(demand_id)
    MapScreenshot.new().map_generation("/je-cherche/map-edit/#{demand_id.to_s}", demand_id.to_s)
  end
end
