
# puts "Yeah !!! We're gonna erase all stuff"

# [Survey, Answer, Condition].each do |class_name|
#   class_name.destroy_all
# end

# puts "All Surveys-Answers-Conditions has been destroyed"





# puts "------ Some datas to play with -------"
# puts "---------- Surveys creation ----------"
# puts "********** START **********"

# s1 = Survey.create(position: 1,
#            answer_type: 'radio',
#            label_demand: 'L’achat de votre bien est un projet … ?',
#            demand_attribute: 'project',
#            property_type: 'all',
#            resume: 'Projet',
#            required: false,
#            active: true,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s2 = Survey.create(position: 2,
#            answer_type: 'radio',
#            label_demand: 'Acheter un bien, certes pas mon quotidien, mais j’ai déjà fait … ?',
#            demand_attribute: 'actual_situation',
#            property_type: 'all',
#            resume: 'Situation actuelle',
#            required: true,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s3 = Survey.create(position: 3,
#            answer_type: 'radio',
#            label_demand: 'Quel bien cherchez-vous ?',
#            demand_attribute: 'property_type',
#            property_type: 'all',
#            resume: 'Bien recherché',
#            required: true,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s4 = Survey.create(position: 4,
#            answer_type: 'radio',
#            label_demand: 'Cet achat est destiné à être ?',
#            demand_attribute: 'destination',
#            property_type: 'all',
#            resume: 'Destination',
#            required: true,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s5 = Survey.create(position: 5,
#            answer_type: 'radio',
#            label_demand: 'Vous aimeriez que votre maison soit plutôt ?',
#            demand_attribute: 'house_state',
#            property_type: 'maison',
#            resume: 'Etat de la maison',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s6 = Survey.create(position: 6,
#            answer_type: 'radio',
#            label_demand: 'Vous aimeriez que votre appartement soit plutôt ?',
#            demand_attribute: 'flat_state',
#            property_type: 'appartement',
#            resume: "Etat de l'appartement",
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s7 = Survey.create(position: 7,
#            answer_type: 'checkbox',
#            label_demand: 'Voulez-vous faire des travaux ?',
#            demand_attribute: 'travaux',
#            property_type: 'maison, appartement',
#            resume: 'Travaux',
#            required: true,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s8 = Survey.create(position: 8,
#            answer_type: 'text',
#            label_demand: 'Quel budget* souhaitez-vous y consacrer ?',
#            demand_attribute: 'budget',
#            property_type: 'maison, appartement',
#            resume: "Budget",
#            required: true,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: true,
#            comment: '*travaux éventuels compris, mais hors frais de notaire',
#            theme: 'Votre projet')

# s9 = Survey.create(position: 9,
#            answer_type: 'checkbox',
#            label_demand: 'Quel style recherchez-vous ?',
#            demand_attribute: 'style',
#            property_type: 'maison, appartement',
#            resume: 'Style',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s10 = Survey.create(position: 10,
#            answer_type: 'text',
#            label_demand: 'De quelle surface souhaiteriez-vous disposer ?',
#            demand_attribute: 'surface',
#            property_type: 'maison, appartement',
#            resume: 'Surface',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: true,
#            theme: 'Votre projet')

# s11 = Survey.create(position: 11,
#            answer_type: 'radio',
#            label_demand: 'Des chambres pour dormir, bien sûr, mais combien au minimum ?',
#            demand_attribute: 'rooms',
#            property_type: 'maison, appartement',
#            resume: 'Chambres',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: true,
#            theme: 'Votre projet')

# s12 = Survey.create(position: 12,
#            answer_type: 'text',
#            label_demand: 'Quel budget souhaitez-vous consacrer à votre achat de terrain ?',
#            demand_attribute: 'budget',
#            property_type: 'terrain',
#            resume: 'Budget pour le terrain',
#            required: true,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s13 = Survey.create(position: 13,
#            answer_type: 'text',
#            label_demand: 'Quel budget souhaitez-vous consacrer à la construction de la maison ?',
#            demand_attribute: 'house_budget',
#            property_type: 'terrain',
#            resume: 'Budget pour la maison',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s14 = Survey.create(position: 14,
#            answer_type: 'text',
#            label_demand: 'Quelle surface de terrain souhaiteriez-vous ?',
#            demand_attribute: 'surface',
#            property_type: 'terrain',
#            resume: 'Surface du terrain',
#            required: true,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s15 = Survey.create(position: 15,
#            answer_type: 'text',
#            label_demand: 'Pour votre maison à construire, quelle serait sa surface ?',
#            demand_attribute: 'house_construction_surface',
#            property_type: 'terrain',
#            resume: 'Surface de la maison (construction)',
#            required: true,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s16 = Survey.create(position: 16,
#            answer_type: 'radio',
#            label_demand: 'souhaiteriez-vous un étage pour votre maison ?',
#            demand_attribute: 'floor',
#            property_type: 'maison',
#            resume: 'Etage',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s17 = Survey.create(position: 17,
#            answer_type: 'radio',
#            label_demand: 'Avez-vous déjà eu des contacts avec un constructeur ?',
#            demand_attribute: 'contact_constructor',
#            property_type: 'terrain',
#            resume: 'Contact avec un constructeur',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre projet')

# s18 = Survey.create(position: 18,
#            answer_type: 'text',
#            label_demand: 'Quelle surface votre espace principal doit-il faire ?',
#            demand_attribute: 'principal_space',
#            property_type: 'maison, appartement',
#            resume: 'Espace principal',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: true,
#            theme: 'Votre bien idéal')

# s19 = Survey.create(position: 19,
#            answer_type: 'radio',
#            label_demand: 'Pour la cuisine, vous êtes indéniablement ?',
#            demand_attribute: 'kitchen',
#            property_type: 'maison, appartement',
#            resume: 'Cuisine',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s20 = Survey.create(position: 20,
#            answer_type: 'radio',
#            label_demand: 'Avoir plusieurs salles de bain est-il indispensable ? Si oui, combien ?',
#            demand_attribute: 'bathrooms',
#            property_type: 'maison, appartement',
#            resume: 'Salles de bain',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: true,
#            theme: 'Votre bien idéal')

# s21 = Survey.create(position: 21,
#            answer_type: 'checkbox',
#            label_demand: "Bien vivre chez soi, c'est aussi parfois avoir des petites pièces en plus qui sont importantes dans l'achat ! Cliquez sur les pièces qui comptent pour vous .",
#            demand_attribute: 'more_rooms',
#            property_type: 'maison, appartement',
#            resume: 'Pièces supplémentaires',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s22 = Survey.create(position: 22,
#            answer_type: 'checkbox',
#            label_demand: 'Tout le monde tient à son petit confort ... quels équipements vous feraient plaisir ?',
#            demand_attribute: 'equipements',
#            property_type: 'maison, appartement',
#            resume: 'Equipements',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s23 = Survey.create(position: 23,
#            answer_type: 'checkbox',
#            label_demand: 'Votre appartement se situe',
#            demand_attribute: 'accessibility',
#            property_type: 'appartement',
#            resume: 'Accessibilité',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s24 = Survey.create(position: 24,
#            answer_type: 'checkbox',
#            label_demand: "Il dispose d'un",
#            demand_attribute: 'flat_stairs',
#            property_type: 'appartement',
#            resume: 'Accès appartement',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s25 = Survey.create(position: 25,
#            answer_type: 'checkbox',
#            label_demand: "Votre maison peut-elle disposer d'un escalier ?",
#            demand_attribute: 'house_stairs',
#            property_type: 'maison',
#            resume: 'Escalier pour la maison',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s26 = Survey.create(position: 26,
#            answer_type: 'checkbox',
#            label_demand: "Les voisins et le soleil, c'est très important pour vous ? Votre bien devra bénéficier ...",
#            demand_attribute: 'exposition',
#            property_type: 'maison, appartement',
#            resume: 'Exposition',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s27 = Survey.create(position: 27,
#            answer_type: 'checkbox',
#            label_demand: 'Pour votre moyen de transport habituel, vous souhaitez disposer ...',
#            demand_attribute: 'parking',
#            property_type: 'appartement',
#            resume: 'Parking',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s28 = Survey.create(position: 28,
#            answer_type: 'checkbox',
#            label_demand: 'Dans votre immeuble, vous souhaitez idéalement',
#            demand_attribute: 'security',
#            property_type: 'appartement',
#            resume: 'Sécurité',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')


# s29 = Survey.create(position: 29,
#            answer_type: 'checkbox',
#            label_demand: "Les gros pulls c'est très bien dehors ! Quel(s) moyen(s) privilégiez-vous pour chauffer votre futur chez vous ?",
#            demand_attribute: 'heating',
#            property_type: 'maison, appartement',
#            resume: 'Chauffage',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')


# s30 = Survey.create(position: 30,
#           answer_type: 'checkbox',
#           label_demand: "Avec un chauffage ...",
#           demand_attribute: 'heating_type',
#           property_type: 'maison, appartement',
#           resume: 'Type de chauffage',
#           required: false,
#           active: false,
#           checkable_on_criteria: true,
#           client_side_condition: false,
#           theme: 'Votre bien idéal')


# s31 = Survey.create(position: 31,
#            answer_type: 'checkbox',
#            label_demand: 'Avez-vous une exigence en classification DPE ?',
#            demand_attribute: 'dpe',
#            property_type: 'maison, appartement',
#            resume: 'DPE',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s32 = Survey.create(position: 32,
#            answer_type: 'checkbox',
#            label_demand: "A l'extérieur de votre chez vous, vous aimeriez ?",
#            demand_attribute: 'flat_exterior',
#            property_type: 'appartement',
#            resume: "Extérieur de l'appartement",
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')

# s33 = Survey.create(position: 33,
#            answer_type: 'checkbox',
#            label_demand: "A l'extérieur de votre chez vous, vous aimeriez ?",
#            demand_attribute: 'house_exterior',
#            property_type: 'maison',
#            resume: 'Extérieur de votre maison',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Votre bien idéal')



# s34 = Survey.create(position: 34,
#            answer_type: 'radio',
#            label_demand: "Mon banquier est-il au courant de mon projet ?",
#            demand_attribute: 'banker_contact',
#            property_type: 'all',
#            resume: 'Mon banquier est-il au courant',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Vous en 4 mots')

# s35 = Survey.create(position: 35,
#            answer_type: 'radio',
#            label_demand: "Ai-je déjà rencontré un courtier ?",
#            demand_attribute: 'broker_contact',
#            property_type: 'all',
#            resume: 'Ai-je rencontré un courtier ?',
#            required: false,
#            active: false,
#            checkable_on_criteria: true,
#            client_side_condition: false,
#            theme: 'Vous en 4 mots')


# s36 = Survey.create(position: 36,
#            answer_type: 'radio',
#            label_demand: "La valeur n'attend pas le nombre des années ... mais nous aimerions quand même savoir quel âge vous avez, c'est important dans nos recommandations",
#            demand_attribute: 'age',
#            property_type: 'all',
#            resume: 'Age',
#            required: false,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Vous en 4 mots')

# s37 = Survey.create(position: 37,
#            answer_type: 'textarea',
#            label_demand: "Qu'aimez-vous particulièrement dans votre logement actuellement ?",
#            demand_attribute: 'actual_pros',
#            property_type: 'all',
#            resume: 'Les plus de votre bien actuel',
#            required: false,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Vous en 4 mots')

# s38 = Survey.create(position: 38,
#            answer_type: 'radio',
#            label_demand: 'Actuellement vous habitez ?',
#            demand_attribute: 'actual_housing',
#            property_type: 'all',
#            resume: 'Logement actuel',
#            required: false,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Vous en 4 mots')

# s39 = Survey.create(position: 39,
#            answer_type: 'text',
#            label_demand: 'Quelle est sa surface approximative ?',
#            demand_attribute: 'actual_housing_area',
#            property_type: 'all',
#            resume: 'Surface de votre logement actuel',
#            required: false,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Vous en 4 mots')

# s40 = Survey.create(position: 40,
#            answer_type: 'radio',
#            label_demand: 'Vous y avez emménagé ?',
#            demand_attribute: 'installation_date',
#            property_type: 'all',
#            resume: "Date d'installation",
#            required: false,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Vous en 4 mots')

# s41 = Survey.create(position: 41,
#            answer_type: 'textarea',
#            label_demand: 'Voulez-vous nous donner une dernière précision pour nous aider à trouver le bien de vos rêves ?',
#            demand_attribute: 'heart_stroke',
#            property_type: 'all',
#            resume: 'Précisions',
#            required: false,
#            active: false,
#            checkable_on_criteria: false,
#            client_side_condition: false,
#            theme: 'Pour finir')


# puts "********** END **********"
# puts "#{Survey.all.count} Surveys created"





# puts "---------- Answers creation ----------"
# puts "********** START **********"

# # S1 ANSWERS
# Answer.create(answer_type: 'radio', text: 'Solo, il devra me plaire à moi et rien qu’à moi', survey: s1)
# Answer.create(answer_type: 'radio', text: 'De couple, nous vivrons à 2 dans ce logement', survey: s1)
# Answer.create(answer_type: 'radio', text: 'Familial, un grand toit est nécessaire', survey: s1)
# Answer.create(answer_type: 'radio', text: "D'une Famille recomposée, il faut satisfaire toutes les tribus", survey: s1)
# # Answer.create(answer_type: 'radio', text: "D'investissement locatif, la pierre c'est du solide", survey: s1)

# # S2 ANSWERS
# Answer.create(answer_type: 'radio', text: 'Non, locataire, et une grande première', survey: s2)
# Answer.create(answer_type: 'radio', text: "Oui, je suis propriétaire actuellement(ou je l'ai été)", survey: s2)


# # S3 ANSWERS
# Answer.create(answer_type: 'radio', text: 'Une maison', survey: s3)
# Answer.create(answer_type: 'radio', text: 'Un appartement', survey: s3)
# Answer.create(answer_type: 'radio', text: 'Un terrain', survey: s3)


# # S4 ANSWERS
# Answer.create(answer_type: 'radio', text: 'Votre résidence principale', survey: s4)
# Answer.create(answer_type: 'radio', text: 'Votre résidence secondaire', survey: s4)
# Answer.create(answer_type: 'radio', text: 'Un investissement locatif', survey: s4)


# # S5 ANSWERS
# Answer.create(answer_type: 'radio', text: 'Ancienne', survey: s5)
# Answer.create(answer_type: 'radio', text: 'Récente*', survey: s5, comment: "* moins de 15 ans")
# Answer.create(answer_type: 'radio', text: 'Neuve**', survey: s5, comment: "** moins de 5 ans")


# # S6 ANSWERS
# Answer.create(answer_type: 'radio', text: 'Ancien', survey: s6)
# Answer.create(answer_type: 'radio', text: 'Récent*', survey: s6, comment: "* moins de 15 ans")
# Answer.create(answer_type: 'radio', text: 'Neuf**', survey: s6, comment: "** moins de 5 ans")


# # S7 ANSWERS
# Answer.create(answer_type: 'checkbox', text: 'Houlala, aucun travaux !!', survey: s7)
# Answer.create(answer_type: 'checkbox', text: "Oui j'aime mettre à mon goût la décoration", survey: s7)
# Answer.create(answer_type: 'checkbox', text: "Oui, je suis enthousiaste à l'idée de rénover, agrandir ...", survey: s7)


# # S8 ANSWERS
# Answer.create(answer_type: 'text', text: 'Budget idéal', unit: '€', limit: 10, survey: s8, input_width: "160px")
# Answer.create(answer_type: 'text', text: "Budget maximum", unit: '€', limit: 10, survey: s8, input_width: "160px")


# # S9 ANSWERS
# Answer.create(answer_type: 'checkbox', text: 'Classique', survey: s9)
# Answer.create(answer_type: 'checkbox', text: "Moderne", survey: s9)
# Answer.create(answer_type: 'checkbox', text: "Atypique", survey: s9)


# # S10 ANSWERS
# Answer.create(answer_type: 'text', text: 'Surface minimum', unit: 'm²', limit: 4, survey: s10, input_width: "160px")
# Answer.create(answer_type: 'text', text: "Surface idéale", unit: 'm²', limit: 4, survey: s10, input_width: "160px")


# # S11 ANSWERS
# Answer.create(answer_type: 'radio', text: '0', survey: s11)
# Answer.create(answer_type: 'radio', text: "1", survey: s11)
# Answer.create(answer_type: 'radio', text: '2', survey: s11)
# Answer.create(answer_type: 'radio', text: "3", survey: s11)
# Answer.create(answer_type: 'radio', text: '4', survey: s11)
# Answer.create(answer_type: 'radio', text: "5", survey: s11)
# Answer.create(answer_type: 'radio', text: '6 et +', survey: s11)



# # S12 ANSWERS
# Answer.create(answer_type: 'text', text: "Budget terrain", unit: '€', limit: 10, survey: s12, input_width: "130px")


# # S13 ANSWERS
# Answer.create(answer_type: 'text', text: "Budget maison", unit: '€', limit: 10, survey: s13, input_width: "130px")
# Answer.create(answer_type: 'radio', text: "Je ne sais pas", survey: s13)


# # S14 ANSWERS
# Answer.create(answer_type: 'text', text: "", unit: 'm²', limit: 6, survey: s14, input_width: "120px")


# # S15 ANSWERS
# Answer.create(answer_type: 'text', text: "Minimun", unit: 'm²', limit: 4, survey: s15, input_width: "120px")
# Answer.create(answer_type: 'text', text: "Maximum", unit: 'm²', limit: 4, survey: s15, input_width: "120px")


# # S16 ANSWERS
# Answer.create(answer_type: 'radio', text: "OUI", survey: s16)
# Answer.create(answer_type: 'radio', text: "NON", survey: s16)


# # S17 ANSWERS
# Answer.create(answer_type: 'radio', text: "OUI", survey: s17)
# Answer.create(answer_type: 'radio', text: "NON", survey: s17)


# # S18 ANSWERS
# Answer.create(answer_type: 'text', text: "Surface", unit: 'm²', limit: 3, survey: s18, input_width: "100px")


# # S19 ANSWERS
# Answer.create(answer_type: 'radio', text: "Cuisine ouverte", survey: s19)
# Answer.create(answer_type: 'radio', text: "Cuisine fermée", survey: s19)
# Answer.create(answer_type: 'radio', text: "L'une ou l'autre", survey: s19)


# # S20 ANSWERS
# Answer.create(answer_type: 'radio', text: "Non", survey: s20)
# Answer.create(answer_type: 'radio', text: "2", survey: s20)
# Answer.create(answer_type: 'radio', text: "3", survey: s20)
# Answer.create(answer_type: 'radio', text: "3 et +", survey: s20)


# # S21 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Une buanderie", survey: s21)
# Answer.create(answer_type: 'checkbox', text: "Un bureau", survey: s21)
# Answer.create(answer_type: 'checkbox', text: "Une cave", survey: s21)
# Answer.create(answer_type: 'checkbox', text: "Un cellier", survey: s21)
# Answer.create(answer_type: 'checkbox', text: "Une véranda", survey: s21)
# Answer.create(answer_type: 'checkbox', text: "Un atelier", survey: s21)


# # S22 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Un vrai dressing", survey: s22)
# Answer.create(answer_type: 'checkbox', text: "Des placards", survey: s22)
# Answer.create(answer_type: 'checkbox', text: "Une alarme", survey: s22)
# Answer.create(answer_type: 'checkbox', text: "La climatisation", survey: s22)


# # S23 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Au rez-de-chaussée", survey: s23)
# Answer.create(answer_type: 'checkbox', text: "A partir du 1er étage", survey: s23)
# Answer.create(answer_type: 'checkbox', text: "Au dernier étage uniquement", survey: s23)


# # S24 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Ascenseur", survey: s24)
# Answer.create(answer_type: 'checkbox', text: "Escalier pour faire mon sport quotidien", survey: s24)
# Answer.create(answer_type: 'checkbox', text: "Accès personne à mobilité réduite", survey: s24)


# # S25 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Non, de plain pied", survey: s25)
# Answer.create(answer_type: 'checkbox', text: "Oui, mais un seul étage", survey: s25)
# Answer.create(answer_type: 'checkbox', text: "Deux étages ne me font pas peur", survey: s25)


# # S26 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Une belle vue", survey: s26)
# Answer.create(answer_type: 'checkbox', text: "Pas de vis-à-vis", survey: s26)
# Answer.create(answer_type: 'checkbox', text: "Une exposition Sud", survey: s26)
# Answer.create(answer_type: 'checkbox', text: "Une exposition sans Nord", survey: s26)
# Answer.create(answer_type: 'checkbox', text: "Pas d'idée préconçue", survey: s26)


# # S27 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "D'un local vélo", survey: s27)
# Answer.create(answer_type: 'checkbox', text: "D'une place de parking", survey: s27)
# Answer.create(answer_type: 'checkbox', text: "D'un parking fermé", survey: s27)
# Answer.create(answer_type: 'checkbox', text: "D'un garage double impérativement", survey: s27)


# # S28 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Un gardien", survey: s28)
# Answer.create(answer_type: 'checkbox', text: "Un interphone/visiophone", survey: s28)
# Answer.create(answer_type: 'checkbox', text: "Un digicode", survey: s28)


# # S29 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Individuel", survey: s29)
# Answer.create(answer_type: 'checkbox', text: "Collectif", survey: s29)



# # S30 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Au fioul", survey: s30)
# Answer.create(answer_type: 'checkbox', text: "Au gaz ou électrique", survey: s30)
# Answer.create(answer_type: 'checkbox', text: "Pompe à chaleur, poêle à granulés, poêle à bois, cheminée", survey: s30)









# # S31 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "A ou B", survey: s31)
# Answer.create(answer_type: 'checkbox', text: "C", survey: s31)
# Answer.create(answer_type: 'checkbox', text: "D", survey: s31)
# Answer.create(answer_type: 'checkbox', text: "Même E et au-delà", survey: s31)


# # S32 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Balcon", survey: s32)
# Answer.create(answer_type: 'checkbox', text: "Terrasse", survey: s32)
# Answer.create(answer_type: 'checkbox', text: "Une piscine dans la résidence", survey: s32)


# # S33 ANSWERS
# Answer.create(answer_type: 'checkbox', text: "Un jardin", survey: s33)
# Answer.create(answer_type: 'checkbox', text: "Une piscine", survey: s33)
# Answer.create(answer_type: 'checkbox', text: "Une pergola", survey: s33)







# # NEW ANSWERS


# # BANQUIER
# Answer.create(answer_type: 'radio', text: "Je l'ai rencontré récemment et il en sait un peu", survey: s34)
# Answer.create(answer_type: 'radio', text: "Je pensais lui en parler à l'occasion", survey: s34)
# Answer.create(answer_type: 'radio', text: "Ouh là ! Cela fait très longtemps que je ne l'ai pas vu", survey: s34)

# # COURTIER
# Answer.create(answer_type: "radio", text: "Oui !", survey: s35)
# Answer.create(answer_type: "radio", text: "Bonne idée, à faire", survey: s35)
# Answer.create(answer_type: "radio", text: "Un court-quoi ?", survey: s35)







# # S34 ANSWERS
# Answer.create(answer_type: 'radio', text: "Moins de 35 ans", survey: s36)
# Answer.create(answer_type: 'radio', text: "Entre 35 et 50 ans", survey: s36)
# Answer.create(answer_type: 'radio', text: "Entre 50 et 65 ans", survey: s36)
# Answer.create(answer_type: 'radio', text: "Plus de 65 ans", survey: s36)


# # S35 ANSWERS
# Answer.create(answer_type: 'textarea', text: "", survey: s37)


# # S36 ANSWERS
# Answer.create(answer_type: 'radio', text: "Une maison", survey: s38)
# Answer.create(answer_type: 'radio', text: "Un appartement", survey: s38)


# # S37 ANSWERS
# Answer.create(answer_type: 'text', text: '', unit: 'm²', limit: 5, survey: s39, input_width: "100px")


# # S38 ANSWERS
# Answer.create(answer_type: 'radio', text: "Depuis moins de 5 ans", survey: s40)
# Answer.create(answer_type: 'radio', text: "Entre 5 et 10 ans", survey: s40)
# Answer.create(answer_type: 'radio', text: "Plus de 10 ans", survey: s40)
# Answer.create(answer_type: 'radio', text: "Une éternité", survey: s40)

# # S39 ANSWERS
# Answer.create(answer_type: 'textarea', text: "", survey: s41)

# puts "********** END **********"





# puts "---------- Conditions creation ----------"
# puts "************* START *************"

# c1 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s4)
# c2 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Une maison', children_operator: '', survey: s5)
# c3 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un appartement', children_operator: '', survey: s6)

# # Multi conditions
# c4_child_condition_child = Condition.create(demand_field: 'flat_state', operator: 'equal', value: 'Ancien')
# c4_child_condition = Condition.create(demand_field: 'house_state', operator: 'equal', value: 'Ancienne', children_operator: 'or', survey: s7, child_condition: c4_child_condition_child)
# c4 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: 'and', survey: s7, child_condition: c4_child_condition)
# # Multi conditions

# c5 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s8)
# c6 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s9)
# c7 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s10)
# c8 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s11)
# c9 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un terrain', children_operator: '', survey: s12)
# c10 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un terrain', children_operator: '', survey: s13)
# c11 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un terrain', children_operator: '', survey: s14)
# c12 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un terrain', children_operator: '', survey: s15)
# c13 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un terrain', children_operator: '', survey: s16)
# c14 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un terrain', children_operator: '', survey: s17)
# c15 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s18)
# c16 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s19)


# # Mutli conditions
# c17_child_condition_child = Condition.create(demand_field: 'surface', operator: 'superior', value: '100')
# c17_child_condition = Condition.create(demand_field: 'budget', operator: 'superior', value: '150000', children_operator: 'or', child_condition: c17_child_condition_child)
# c17 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: 'and', survey: s20, child_condition: c17_child_condition)
# # Multi conditions

# c18 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s21)

# # Multi conditions
# c19_child_condition_child = Condition.create(demand_field: 'surface', operator: 'superior', value: '100')
# c19_child_condition = Condition.create(demand_field: 'budget', operator: 'superior', value: '200000', children_operator: 'or', child_condition: c19_child_condition_child)
# c19 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: 'and', survey: s22, child_condition: c19_child_condition)
# # Multi conditions

# # Multi conditions : Type de chauffage
# c26_child_condition = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Une maison')
# c26 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: 'and', survey: s29, child_condition: c26_child_condition)
# # Multi conditions

# c20 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un appartement', children_operator: '', survey: s23)
# c21 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un appartement', children_operator: '', survey: s24)
# c22 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Une maison', children_operator: '', survey: s25)
# c23 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s26)
# c24 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un appartement', children_operator: '', survey: s27)
# c25 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un appartement', children_operator: '', survey: s28)

# c30 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s30)

# c27 = Condition.create(demand_field: 'property_type', operator: 'not_equal', value: 'Un terrain', children_operator: '', survey: s31)
# c28 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Un appartement', children_operator: '', survey: s32)
# c29 = Condition.create(demand_field: 'property_type', operator: 'equal', value: 'Une maison', children_operator: '', survey: s33)

# puts "********** END **********"




# p1 = Page.create(meta_title: "home title", meta_desc: "home description", name: "home", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.root_path}")
# p2 = Page.create(meta_title: "presse title", meta_desc: "presse description", name: "presse", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.presse_path}")
# p3 = Page.create(meta_title: "parrainage title", meta_desc: "parrainage description", name: "parrainage", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.parrainage_path}")
# p4 = Page.create(meta_title: "je vends title", meta_desc: "je vends description", name: "je-vends", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.je_vends_path}")
# p5 = Page.create(meta_title: "je construis title", meta_desc: "je construis description", name: "je-construis", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.i_construct_path}")
# p6 = Page.create(meta_title: "je m'informe title", meta_desc: "je m'informe description", name: "je-m-informe", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.i_inform_myself_path}")
# p7 = Page.create(meta_title: "blog title", meta_desc: "blog description", name: "blog", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.blog_path}")
# p8 = Page.create(meta_title: "honoraires title", meta_desc: "honoraires description", name: "honoraires", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.fees_path}")
# p9 = Page.create(meta_title: "equipe title", meta_desc: "equipe description", name: "equipe", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.team_path}")
# p10 = Page.create(meta_title: "contact title", meta_desc: "contact description", name: "contact", url: "https://bientotchezsoi.com#{Rails.application.routes.url_helpers.contact_path}")

# puts "---------- Blog categories creation ----------"
# puts "********** START **********"

# bc1 = BlogCategory.create(name: 'achat', lname: 'achat', h1_content: "Some H1 stuff")
# bc2 = BlogCategory.create(name: "billet d'humeur", lname: 'billet-d-humeur', h1_content: "Some H1 stuff")
# bc3 = BlogCategory.create(name: 'construction', lname: 'construction', h1_content: "Some H1 stuff")
# bc4 = BlogCategory.create(name: 'offre job', lname: 'offre-job', h1_content: "Some H1 stuff")
# bc5 = BlogCategory.create(name: 'vente', lname: 'vente', h1_content: "Some H1 stuff")

# puts "********** END **********"





# puts "---------- Posts creation ----------"
# puts "********** START **********"

# # BC1 POSTS
# p1 = Post.new(name: 'ACHETER À DEUX ET RESTER HEUREUX !', lname: 'acheter-a-deux', category_name: 'achat', blog_category: bc1, link_name: "VOUS QUI CHERCHEZ", title: "Un achat à deux représente une étape importante dans la vie d’un couple, le début d’une grande aventure. S’il s’agit d’un avantage indéniable pour obtenir un financement plus facilement, il est important de mesurer, aussi, les compromis à faire ensemble. Suivez les indications de Kenotte pour éviter toute tension inutile !")
# p1.tags_list = "Achat, Appartement, Coaching, Financement, Maison, Couple, Prix, Visite"
# p1.save

# p2 = Post.new(name: "L’IMPORTANCE D'ETABLIR UN PLAN DE FINANCEMENT", lname: 'plan-de-financement', category_name: 'achat', blog_category: bc1, link_name: "VOUS QUI CHERCHEZ", title: "Avant même de chercher le bien de vos rêves, et au risque de devoir rapidement redescendre sur terre, Kenotte vous conseille vivement de bien estimer vos capacités d’emprunt ! Pour réaliser un plan de financement solide, votre castor préféré préconise de passer par 2 étapes, en gardant en tête qu’il est préférable de toujours conserver un peu d’épargne de côté. Elle pourra en effet se révéler bien utile : frais d’installation, mobilier, travaux ou réparations non prévues. Et oui, la chaudière qui lâche quelques mois après l’installation, ça n’arrive pas qu’aux autres…")
# p2.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p2.save

# # BC2 POSTS
# p3 = Post.new(name: 'Kenotte découvre son nouveau quartier !', lname: 'kenotte-decouvre-son-nouveau-quartier', category_name: "billet d'humeur", blog_category: bc2)
# p3.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p3.save

# # BC3 POSTS
# p4 = Post.new(name: 'Que faire soi-même lors de la construction de sa maison ?', lname: 'faire-soi-meme-construction-de-maison', category_name: "construction", blog_category: bc3)
# p4.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p4.save

# p5 = Post.new(name: 'Choisir le bon constructeur pour votre maison', lname: 'choisir-constructeur-maison', category_name: "construction", blog_category: bc3)
# p5.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p5.save

# # BC4 POSTS
# p6 = Post.new(name: 'NÉGOCIATEUR IMMOBILIER', lname: 'negociateur-immobilier', category_name: "offre job", blog_category: bc4)
# p6.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p6.save

# p7 = Post.new(name: 'BUSINESS DEVELOPPEUR', lname: 'business-developpeur', category_name: "offre job", blog_category: bc4)
# p7.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p7.save

# # BC5 POSTS
# p8 = Post.new(name: 'Réussir votre vente : le coaching de Kenotte !', lname: 'reussir-vente-coaching-de-kenotte', category_name: "vente", blog_category: bc5, link_name: "VOUS QUI VENDEZ")
# p8.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p8.save

# p9 = Post.new(name: '« Home staging », effet de mode ou solution en or ?', lname: 'home-staging', category_name: "vente", blog_category: bc5, author: "kenotte", link_name: "VOUS QUI VENDEZ", title: "Mis en lumière par de nombreuses émissions de télévision, le home staging est depuis quelques années très plébiscité. Il apparaît même parfois comme une solution miracle pour valoriser son bien immobilier à faible coût. Kenotte vous délivre le véritable avantage de cette méthode et les clés pour l’appliquer avec succès !")
# p9.tags_list = "Coaching, Financement, Maison, Couple, Prix, Visite"
# p9.save

# p10 = Post.new(name: "Avant / après la visite d’un bien : le mémo !", lname: 'visite-d-un-bien', author: "julia", category_name: "achat", blog_category: bc1, link_name: "VOUS QUI CHERCHEZ", title: "A force d’éplucher les petites annonces, vous avez enfin repéré un bien immobilier qui vous intéresse ! Vous avez pris contact avec son propriétaire, ou l’agent immobilier qui s’occupe de la vente, pour le visiter ? Nous espérons que vous aurez déjà pensé à poser les quelques questions essentielles, en suivant les conseils avisés de Kenotte !")
# p10.tags_list = "Coaching, Maison, Couple, Prix, Visite, Achat"
# p10.save

# puts "********** END **********"


# puts "********** AGENCY CREATION START ***********"
# a1 = Agency.new(min_price: 80000.0, city: "Bordeaux", location_cp: "33200", name: "Agence test 001")
# a2 = Agency.new(min_price: 70000.0, city: "Bordeaux", location_cp: "33300", name: "Agence test 002")
# a3 = Agency.new(min_price: 20000.0, city: "Pessac", location_cp: "33600", name: "Agence test 003")
# a4 = Agency.new(min_price: 50000.0, city: "Merignac", location_cp: "33700", name: "Agence test 004")

# a1.save(validate: false)
# a2.save(validate: false)
# a3.save(validate: false)
# a4.save(validate: false)
# puts "********** AGENCY CREATION END ***********"


[Question, V2Answer, V2Condition, AnswerSequence].each do |model_name|
  puts "------- Delete exsisting instances -------"
  puts "We're gonna erase all #{model_name}"
  puts "#{model_name.count} #{model_name} in database"
  model_name.destroy_all
  puts "------------------------------------------"
end

puts "***** Destroy all models *****"
puts "----- Wait while processing -----"

q1 = Question.create(demand_attribute: "gender",
                    avatar: "kenotte-avatar",
                    label: "Etes vous un homme ou une femme ?",
                    required: true,
                    question_type: "radio_with_image",
                    advanced_radio: true,
                    field_type: "string",
                    label_resume: "Je suis",
                    agency_resume: "Genre",
                    criteria_resume: "Genre",
                    theme: "L'acheteur",
                    position: 1)

q2 = Question.create(demand_attribute: "firstname",
                    avatar: "kenotte-avatar",
                    label: "Et vous, quel est votre prénom ?",
                    required: true,
                    question_type: "text",
                    field_type: "string",
                    label_resume: "Mon petit nom",
                    agency_resume: "Prénom",
                    criteria_resume: "Prénom",
                    theme: "L'acheteur",
                    position: 2)


q3 = Question.create(demand_attribute: "destination",
                    avatar: "kenotte-avatar",
                    label: "Votre projet concerne...",
                    question_type: "radio_with_image",
                    field_type: "string",
                    advanced_radio: true,
                    required: true,
                    label_resume: "Ma demande concerne",
                    agency_resume: "La demande concerne",
                    criteria_resume: "Destination",
                    theme: "Le projet",
                    position: 3)

q4 = Question.create(demand_attribute: "project",
                    avatar: "kenotte-avatar",
                    label: "Qui vivra dans ce logement ?",
                    question_type: "radio_with_image",
                    field_type: "string",
                    advanced_radio: true,
                    required: true,
                    label_resume: "Mon projet",
                    agency_resume: "Le projet",
                    criteria_resume: "Le projet",
                    theme: "Le projet",
                    position: 4)

q5 = Question.create(demand_attribute: "actual_situation",
                    avatar: "kenotte-avatar",
                    label: "Pour bien comprendre, je me demandais, avez-vous dejà acheté ?",
                    image: "experience",
                    question_type: "radio",
                    field_type: "string",
                    required: true,
                    label_resume: "Ma situation actuelle",
                    agency_resume: "Situation actuelle",
                    criteria_resume: "Situation actuelle",
                    theme: "Le projet",
                    position: 5)

q6 = Question.create(demand_attribute: "actual_housing",
                    avatar: "kenotte-avatar",
                    label: "Actuellement vous habitez ?",
                    question_type: "radio_with_image",
                    field_type: "string",
                    advanced_radio: true,
                    required: true,
                    label_resume: "Mon logement actuel",
                    agency_resume: "Logement actuel",
                    criteria_resume: "Logement actuel",
                    theme: "Le projet",
                    position: 6)

q7 = Question.create(demand_attribute: "installation_date",
                    avatar: "kenotte-avatar",
                    label: "Vous y avez aménagé",
                    image: "date-ammenagement",
                    question_type: "radio",
                    field_type: "string",
                    required: true,
                    label_resume: "J'y ai emménagé",
                    agency_resume: "Date d'emménagement",
                    criteria_resume: "Date d'emménagement",
                    theme: "Le projet",
                    position: 7)

q8 = Question.create(demand_attribute: "email",
                    avatar: "kenotte-conseil",
                    label: "Ah! Besoin d'aide pour estimer votre bien ? C'est offert, profitez-en !",
                    question_type: "text",
                    field_type: "string",
                    required: false,
                    label_resume: "Mon email",
                    agency_resume: "Email",
                    criteria_resume: "Email",
                    theme: "L'acheteur",
                    advice: true,
                    position: 8)

q9 = Question.create(demand_attribute: "property_type",
                    avatar: "kenotte-avatar",
                    label: "Et aujourd'hui nous recherchons...",
                    question_type: "radio_with_image",
                    field_type: "string",
                    advanced_radio: true,
                    required: true,
                    label_resume: "Je recherche",
                    agency_resume: "Type de bien recherché",
                    criteria_resume: "Bien recherché",
                    theme: "Le projet",
                    position: 9)

q10 = Question.create(demand_attribute: "search_time",
                    avatar: "kenotte-avatar",
                    label: "Au fait, ouvrez votre agenda, depuis combien de temps recherchez-vous ?",
                    image: "temps-recherche",
                    question_type: "radio",
                    field_type: "string",
                    required: false,
                    label_resume: "Je recherche depuis",
                    agency_resume: "En recherche depuis",
                    criteria_resume: "Temps de recherche",
                    theme: "Le projet",
                    position: 10)

q11 = Question.create(demand_attribute: "visited_property_count",
                    avatar: "kenotte-avatar",
                    question_type: "radio",
                    image: "visites",
                    label: "Avant de nous rencontrer, combien de visites avez-vous effectué ?",
                    field_type: "string",
                    required: false,
                    label_resume: "J'ai visité",
                    agency_resume: "Les visites",
                    criteria_resume: "Les visites",
                    theme: "L'acheteur",
                    position: 11)

q12 = Question.create(demand_attribute: "state",
                    avatar: "kenotte-avatar",
                    question_type: "radio_with_image",
                    label: "Et je me demandais, vous voudriez plutôt que votre",
                    field_type: "string",
                    advanced_radio: true,
                    required: false,
                    variable_in_label: true,
                    variable_demand_field: "property_type",
                    second_part_label: "soit...",
                    label_resume: "Etat du logement",
                    agency_resume: "Etat du logement",
                    criteria_resume: "Etat de la maison",
                    theme: "Le projet",
                    position: 12)

q13 = Question.create(demand_attribute: "style",
                    avatar: "kenotte-avatar",
                    question_type: "radio_with_image",
                    label: "Son style sera plutôt..",
                    field_type: "string",
                    advanced_radio: true,
                    required: true,
                    label_resume: "Son style sera",
                    agency_resume: "Son style sera",
                    criteria_resume: "Style",
                    theme: "Le bien idéal",
                    position: 13)

q14 = Question.create(demand_attribute: "house_works",
                    avatar: "kenotte-avatar",
                    question_type: "radio_with_image",
                    label: "Et pour les travaux, c'est envisageable ?",
                    field_type: "string",
                    advanced_radio: true,
                    required: true,
                    label_resume: "Travaux",
                    agency_resume: "Travaux",
                    criteria_resume: "Travaux",
                    theme: "Le bien idéal",
                    position: 14)

q15 = Question.create(demand_attribute: "energy_renovation",
                    avatar: "kenotte-avatar",
                    image: "renovation-energetique",
                    question_type: "radio",
                    label: "La rénovation énergétique vous inspire ?",
                    field_type: "string",
                    required: true,
                    label_resume: "Rénovation énergétique",
                    agency_resume: "Rénovation énergétique",
                    criteria_resume: "Rénovation énergétique",
                    theme: "Le bien idéal",
                    position: 15)

q16 = Question.create(demand_attribute: "age",
                    avatar: "kenotte-avatar",
                    image: "man",
                    question_type: "radio",
                    label: "Quel âge avez-vous, cela peut être important dans nos recommandations",
                    field_type: "string",
                    required: true,
                    label_resume: "Mon âge",
                    agency_resume: "Age",
                    criteria_resume: "Age",
                    theme: "L'acheteur",
                    position: 16)

q17 = Question.create(demand_attribute: "school_type",
                    avatar: "kenotte-avatar",
                    image: "famille",
                    question_type: "checkbox",
                    label: "L'école c'est parfois important, vos enfants sont plutôt...",
                    field_type: "array",
                    required: false,
                    label_resume: "Mes enfants sont",
                    agency_resume: "Les enfants",
                    criteria_resume: "Les enfants",
                    theme: "L'acheteur",
                    position: 17)

q18 = Question.create(demand_attribute: "actual_housing_area",
                    avatar: "kenotte-avatar",
                    image: "surface-actu",
                    question_type: "text",
                    label: "Quel est la surface de votre",
                    field_type: "string",
                    variable_in_label: true,
                    variable_demand_field: "actual_housing",
                    required: false,
                    label_resume: "Surface de mon logement actuel",
                    agency_resume: "Surface du logement actuel",
                    criteria_resume: "Surface du logement actuel",
                    theme: "L'acheteur",
                    position: 18)


q19 = Question.create(demand_attribute: "surface",
                    avatar: "kenotte-avatar",
                    image: "surface",
                    question_type: "text",
                    label: "Et aujourd'hui quelle surface vous faut-il pour vous épanouir ?",
                    field_type: "array",
                    required: true,
                    label_resume: "Pour ce projet il me faudrait",
                    agency_resume: "Surface désirée",
                    criteria_resume: "Surface",
                    theme: "Le bien idéal",
                    position: 19)

q20 = Question.create(demand_attribute: "rooms",
                    avatar: "kenotte-avatar",
                    image: "chambre",
                    question_type: "incremental_numeric",
                    label: "Combien de chambres vous faut-il pour dormir ?",
                    field_type: "integer",
                    required: true,
                    label_resume: "Nombre de chambres",
                    agency_resume: "Nombre de chambres",
                    criteria_resume: "Chambres",
                    theme: "Le bien idéal",
                    position: 20)

q21 = Question.create(demand_attribute: "budget",
                      avatar: "kenotte-avatar",
                      question_type: "text",
                      label: "Quel est votre budget ? (hors frais de notaire, travaux inclus)",
                      field_type: "array",
                      image: "budget",
                      hint_image: "conseils-frais-notaire",
                      hint_label: "Frais de notaires",
                      required: true,
                      label_resume: "Mon budget",
                      agency_resume: "Le budget",
                      criteria_resume: "Budget",
                      theme: "Le projet",
                      position: 21)

q22 = Question.create(demand_attribute: "banker_contact",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Et votre banquier quand l'avez-vous vu (ou aperçu) pour la dernière fois ?",
                      field_type: "string",
                      image: "banquier",
                      required: true,
                      label_resume: "J'ai vu mon banquier",
                      agency_resume: "Contact avec un banquier",
                      criteria_resume: "Banquier",
                      theme: "Le projet",
                      position: 22)

q23 = Question.create(demand_attribute: "broker_contact",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Avez-vous parlé de votre projet avec un courtier en crédit ?",
                      field_type: "string",
                      image: "courtier",
                      required: true,
                      label_resume: "J'ai vu un courtier",
                      agency_resume: "Contact avec un courtier",
                      criteria_resume: "Courtier",
                      theme: "Le projet",
                      position: 23)

q24 = Question.create(demand_attribute: "ground_budget",
                      avatar: "kenotte-avatar",
                      question_type: "text",
                      label: "Quelle enveloppe prévoyez-vous pour l'achat du terrain ?",
                      field_type: "array",
                      image: "budget-terrain",
                      required: true,
                      label_resume: "Budget pour le terrain",
                      agency_resume: "Budget pour le terrain",
                      criteria_resume: "Budget terrain",
                      theme: "Le projet",
                      position: 24)

q25 = Question.create(demand_attribute: "house_budget",
                      avatar: "kenotte-avatar",
                      question_type: "text",
                      label: "Et quel budget pour votre maison ?",
                      field_type: "array",
                      image: "budget-terrain-maison",
                      required: true,
                      label_resume: "Budget pour la maison",
                      agency_resume: "Budget pour la maison",
                      criteria_resume: "Budget maison",
                      theme: "Le projet",
                      position: 25)

q26 = Question.create(demand_attribute: "floor",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Est ce qu'il y aura un étage dans cette maison neuve ?",
                      field_type: "string",
                      image: "etage-terrain-maison",
                      required: false,
                      label_resume: "Etages",
                      agency_resume: "Etages",
                      criteria_resume: "Etages",
                      theme: "Le projet",
                      position: 26)

q27 = Question.create(demand_attribute: "contact_constructor",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Et avez-vous déjà eu des contacts avec des constructeurs ?",
                      field_type: "string",
                      image: "constructeur",
                      required: true,
                      label_resume: "J'ai rencontré un constructeur",
                      agency_resume: "Contact avec un constructeur",
                      criteria_resume: "constructeur",
                      theme: "Le projet",
                      position: 27)

q28 = Question.create(demand_attribute: "house_stairs",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Si je vous dis maison avec étage, vous me répondez...",
                      field_type: "string",
                      image: "etage-terrain-maison",
                      required: false,
                      label_resume: "Etages",
                      agency_resume: "Etages",
                      criteria_resume: "Etages",
                      theme: "Le projet",
                      position: 28)

q29 = Question.create(demand_attribute: "flat_stairs",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Mon appartement je le préfère...",
                      field_type: "string",
                      image: "etage-terrain-maison",
                      required: false,
                      label_resume: "Etages",
                      agency_resume: "Etages",
                      criteria_resume: "Etages",
                      theme: "Le projet",
                      position: 29)

q30 = Question.create(demand_attribute: "accessibility",
                      avatar: "kenotte-avatar",
                      question_type: "radio_with_image",
                      label: "Et pour y accéder il vous faudrait...",
                      field_type: "string",
                      advanced_radio: true,
                      required: false,
                      label_resume: "Accessibilité",
                      agency_resume: "Accessibilité",
                      criteria_resume: "Accessibilité",
                      theme: "Le bien idéal",
                      position: 30)

q31 = Question.create(demand_attribute: "principal_space",
                      avatar: "kenotte-avatar",
                      question_type: "text",
                      label: "Quelle surface votre espace principal doit-il faire ?",
                      field_type: "string",
                      image: "piece-principale",
                      required: false,
                      label_resume: "Surface idéal de l'espace principal",
                      agency_resume: "Surface idéal de l'espace principal",
                      criteria_resume: "Espace principal",
                      theme: "Le bien idéal",
                      position: 31)

q32 = Question.create(demand_attribute: "kitchen",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Pour la cuisine vous êtes indéniablement",
                      field_type: "string",
                      image: "cuisine",
                      required: false,
                      label_resume: "Ma cuisine",
                      agency_resume: "La cuisine",
                      criteria_resume: "Cuisine",
                      theme: "Le bien idéal",
                      position: 32)

q33 = Question.create(demand_attribute: "bathrooms",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Pour partir du bon pied, combien de salles de bains vous faut-il ?",
                      field_type: "string",
                      image: "bathroom",
                      required: false,
                      label_resume: "Ma salle de bain",
                      agency_resume: "La salle de bain",
                      criteria_resume: "Salle de bain",
                      theme: "Le bien idéal",
                      position: 33)

q34 = Question.create(demand_attribute: "more_rooms",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox_with_image",
                      label: "Cliquez sur celles qui comptent pour vous.",
                      field_type: "array",
                      advanced_checkbox: true,
                      required: false,
                      label_resume: "Pièces importantes",
                      agency_resume: "Pièces importantes",
                      criteria_resume: "Pièces supplémentaires",
                      theme: "Le bien idéal",
                      position: 34)

q35 = Question.create(demand_attribute: "equipments",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox_with_image",
                      label: "Quels équipements vous feraient plaisir ?",
                      field_type: "array",
                      advanced_checkbox: true,
                      required: false,
                      label_resume: "Les équipements",
                      agency_resume: "Les équipements",
                      criteria_resume: "Equipements",
                      theme: "Le bien idéal",
                      position: 35)

q36 = Question.create(demand_attribute: "exposition",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "Depuis chez vous, vous aurez...",
                      field_type: "array",
                      image: "exposition",
                      required: false,
                      label_resume: "Exposition",
                      agency_resume: "Exposition",
                      criteria_resume: "Exposition",
                      theme: "Le bien idéal",
                      position: 36)

q37 = Question.create(demand_attribute: "parking",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "Pour votre moyen de transport habituel, vous souhaitez disposer...",
                      field_type: "array",
                      image: "transport",
                      required: false,
                      label_resume: "Mon moyen de transport",
                      agency_resume: "Moyen de transport",
                      criteria_resume: "Parking",
                      theme: "Le bien idéal",
                      position: 37)

q38 = Question.create(demand_attribute: "garage",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Un garage est-il nécessaire ?",
                      field_type: "string",
                      image: "garage",
                      required: false,
                      label_resume: "Mon garage",
                      agency_resume: "Le garage",
                      criteria_resume: "Garage",
                      theme: "Le bien idéal",
                      position: 38)

q39 = Question.create(demand_attribute: "security",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "Dans votre immeuble vous souhaitez idéalement...",
                      field_type: "array",
                      image: "securite",
                      required: false,
                      label_resume: "Sécurité",
                      agency_resume: "Sécurité",
                      criteria_resume: "Sécurité",
                      theme: "Le bien idéal",
                      position: 39)

q40 = Question.create(demand_attribute: "heating",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Quel moyen privilégiez-vous pour chauffer votre futur chez vous ?",
                      field_type: "string",
                      image: "chauffage",
                      required: false,
                      label_resume: "Mon type de chauffage",
                      agency_resume: "Type de chauffage",
                      criteria_resume: "Chauffage",
                      theme: "Le bien idéal",
                      position: 40)

q41 = Question.create(demand_attribute: "heating_type",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox_with_image",
                      label: "Quel(s) moyen(s) privilégiez-vous pour chauffer votre futur chez vous ?",
                      field_type: "array",
                      advanced_checkbox: true,
                      required: false,
                      label_resume: "Mon type de chauffage",
                      agency_resume: "Type de chauffage",
                      criteria_resume: "Chauffage",
                      theme: "Le bien idéal",
                      position: 41)

q42 = Question.create(demand_attribute: "dpe",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "Avez-vous une exigence en classification DPE ?",
                      field_type: "array",
                      image: "dpe",
                      required: false,
                      label_resume: "DPE",
                      agency_resume: "DPE",
                      criteria_resume: "DPE",
                      theme: "Le bien idéal",
                      position: 42)

q43 = Question.create(demand_attribute: "flat_exterior",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "A l'extérieur de votre chez vous, vous aimeriez ?",
                      field_type: "array",
                      image: "kenotte-jardin",
                      required: false,
                      label_resume: "L'extérieur de mon appartement",
                      agency_resume: "Extérieur de l'appartement",
                      criteria_resume: "Extérieur de votre appartement",
                      theme: "Le bien idéal",
                      position: 43)

q44 = Question.create(demand_attribute: "house_exterior",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "A l'extérieur de votre chez vous, vous aimeriez ?",
                      field_type: "array",
                      image: "kenotte-jardin",
                      required: false,
                      label_resume: "L'extèrieur de ma maison",
                      agency_resume: "Extèrieur de la maison",
                      criteria_resume: "Extérieur de ma maison",
                      theme: "Le bien idéal",
                      position: 44)

q45 = Question.create(demand_attribute: "man_persona",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "Et vous, vous rêveriez d'être",
                      field_type: "string",
                      advanced_radio: true,
                      required: true,
                      label_resume: "Je rêve d'être",
                      agency_resume: "Le membre s'identifie à",
                      criteria_resume: "Persona",
                      theme: "L'acheteur",
                      position: 45)

q46 = Question.create(demand_attribute: "woman_persona",
                      avatar: "kenotte-avatar",
                      question_type: "checkbox",
                      label: "Et vous, vous rêveriez d'être",
                      field_type: "string",
                      advanced_radio: true,
                      required: true,
                      label_resume: "Je rêve d'être",
                      agency_resume: "Le membre s'identifie à",
                      criteria_resume: "Persona",
                      theme: "L'acheteur",
                      position: 46)

q47 = Question.create(demand_attribute: "personality",
                      avatar: "kenotte-avatar",
                      question_type: "radio",
                      label: "Et votre première fois… en immobilier, les 3 ou 4 adjectifs qui résument le mieux seraient…",
                      field_type: "array",
                      radio_by_pair: true,
                      required: true,
                      label_resume: "Ma première fois en immobilier",
                      agency_resume: "La première fois en immobilier",
                      criteria_resume: "Personnalité",
                      theme: "L'acheteur",
                      position: 47)

# Sentences
Sentence.create(content: "Bonjour, je suis Kenotte, le castor de l'immoblier ! Nous allons faire connaissance pour vous conseiller et trouver la perle rare… Oups, il y a un reflet avec l'écran, je ne vous vois pas bien.", question: q1, icon: "fireworks")
Sentence.create(content: "Super !", question: q2, icon: "fireworks")
Sentence.create(demand_attribute: "firstname", content: "Enchanté de faire votre connaissance", content_after_variable: "! Passons aux choses sérieuses.", question: q3, icon: "fireworks")
Sentence.create(content: "Intéressant...", question: q4)
Sentence.create(content: "Beau projet ! Je suis impatient de connaître la suite !", question: q5)
Sentence.create(content: "Je vois.", question: q6)
Sentence.create(content: "Sympa !", question: q7)
Sentence.create(content: "Dites-m'en plus...", question: q10)
Sentence.create(content: "Chacun son passé...", question: q11)
Sentence.create(content: "Parfait ! A présent parlons un peu plus en détail de votre chez vous idéal", question: q13)
Sentence.create(demand_attribute: "firstname", content: "D'accord", content_after_variable: "!", question: q14)
Sentence.create(content: "C'est noté", question: q15)
Sentence.create(content: "Au fait, rien à voir, mais je me demandais... La valeur n'attend pas le nombre des années !", question: q16)
Sentence.create(content: "D'accord !", question: q17)
Sentence.create(demand_attribute: "firstname", content: "D'accord", content_after_variable: "!", question: q18)
Sentence.create(content: "D'accord !", question: q19)
Sentence.create(content: "Cela me semble cohérent.", question: q21)
Sentence.create(demand_attribute: "firstname", content: "J'ai oublié de vous demander", question: q26)
Sentence.create(content: "D'accord c'est noté.", question: q28)
Sentence.create(content: "Bon allez, zou, on bavarde mais il faut qu'on avance un peu... On va maintenant s'intéresser à ce qui fera de chez vous un endroit vraiment unique.", question: q31)
Sentence.create(content: "C'est rigolo j'aurai dit exactement comme vous !", question: q32)
Sentence.create(content: "Rien de plus désagréable que les bouchons à la porte de la douche le matin !", question: q33)
Sentence.create(content: "Bien vivre chez soi c'est aussi parfois avoir des petites pièces en plus qui sont importantes dans l'achat !", question: q34)
Sentence.create(content: "Tout le monde tient à son petit confort...", question: q35)
Sentence.create(content: "Les voisins et le soleil c'est très important pour vous ?", question: q36)
Sentence.create(demand_attribute: "firstname", content: "Très bien", content_after_variable: "!", question: q38)
Sentence.create(content: "D'accord !", question: q39)
Sentence.create(content: "Les gros pulls c'est très bien dehors !", question: q40)
Sentence.create(content: "Les gros pulls c'est très bien dehors !", question: q41)
Sentence.create(content: "Super poursuivons !", question: q42)
Sentence.create(demand_attribute: "firstname", content: "Très bien", content_after_variable: "!", question: q43)
Sentence.create(content: "Très bien !", question: q44)
Sentence.create(content: "Je vais vous confier un secret... toutes les nuits, je rêve que je suis Mick Jagger. Dingue non ?", question: q45)
Sentence.create(content: "Je vais vous confier un secret... toutes les nuits, je rêve que je suis Lady Gaga. Dingue non ?", question: q46)
Sentence.create(content: "Je l'aurais parié ! Nous sommes vraiment sur la même longueur d'onde !", question: q47)


# Q1 answer
V2Answer.create(answer_type: "radio", image: "woman", text: "Une femme", question: q1, order: 1, field_type: "string")
V2Answer.create(answer_type: "radio", image: "man", text: "Un homme", question: q1, order: 2, field_type: "string")

# Q2 answer
V2Answer.create(answer_type: "text", text: "Je tape mon prénom", placeholder: "Je tape mon prénom", question: q2, field_type: "string", order: 1)

# Q3 answers
V2Answer.create(answer_type: "radio", image: "residence-principale", text: "Ma résidence principale", question: q3, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", image: "residence-secondaire", text: "Ma résidence secondaire", question: q3, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", image: "investissement", text: "Un investissement locatif", question: q3, field_type: "string", order: 3)

# Q4 answers
V2Answer.create(answer_type: "radio", image: "solo", text: "Moi tout seul", question: q4, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", image: "couple", text: "Nous deux, en couple", question: q4, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", image: "famille", text: "Ma famille, sous un (plus) grand toit", question: q4, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", image: "famille-recomposee", text: "Ma famille recomposée, notre joyeuse tribu", question: q4, field_type: "string", order: 4)

# Q5 answers
V2Answer.create(answer_type: "radio", text: "Jamais, je suis locataire et c'est une grande première", question: q5, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Oui, je suis propriétaire", question: q5, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Oui, même si je suis locataire actuellement", question: q5, field_type: "string", order: 3)

# Q6 answers
V2Answer.create(answer_type: "radio", image: "appartement-actu", text: "Un appartement", question: q6, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", image: "maison-actu", text: "Une maison", question: q6, field_type: "string", order: 2)

# Q7 answers
V2Answer.create(answer_type: "radio", text: "Moins de 5 ans", question: q7, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Entre 5 et 10 ans", question: q7, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Entre 10 et 15 ans", question: q7, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "Une éternité", question: q7, field_type: "string", order: 4)

# Q8 answers
V2Answer.create(answer_type: "text", text: "Je saisis mon email", placeholder: "Je saisis mon email", question: q8, field_type: "string", order: 1)

# Q9 answers
V2Answer.create(answer_type: "radio", image: "maison", text: "Une maison", question: q9, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", image: "appartement", text: "Un appartement", question: q9, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", image: "terrain", text: "Un terrain", question: q9, field_type: "string", order: 3)

# Q10 answers
V2Answer.create(answer_type: "radio", text: "1 mois", question: q10, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "3 mois, plus ou moins", question: q10, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Plus de 4 mois, à l'affût", question: q10, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "Depuis longtemps", question: q10, field_type: "string", order: 4)

# Q11 answers
V2Answer.create(answer_type: "radio", text: "0 pointé !", order: 1, question: q11, field_type: "string")
V2Answer.create(answer_type: "radio", text: "1, 2 ou trois, c'est parti !", order: 2, question: q11, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Pas encore la dizaine, ouf !", order: 3, question: q11, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Je ne compte plus", order: 4, question: q11, field_type: "string")

# Q12 answers
V2Answer.create(answer_type: "radio", text: "Ancien (+ de 10 ans)", fem_text: "Ancienne (+ de 10 ans)", image: "pyramide", order: 1, question: q12, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Récent (- de 10 ans)", fem_text: "Récente (- de 10 ans)", image: "chateau", order: 2, question: q12, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Neuf", image: "fusee", fem_text: "Neuve", order: 3, question: q12, field_type: "string")

# Q13 answers
V2Answer.create(answer_type: "radio", text: "Classique", order: 1, image: "style-classique", question: q13, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Moderne", order: 2, image: "style-moderne", question: q13, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Atypique", order: 3, image: "style-atypique", question: q13, field_type: "string")

# Q14 answers
V2Answer.create(answer_type: "radio", text: "Houlala, aucun travaux !", order: 1, image: "no-travaux", question: q14, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Oui, j'aime mettre à mon goût la décoration", order: 2, image: "petits-travaux", question: q14, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Oui je suis enthousiaste à l'idée de rénover, agrandir", order: 3, image: "gros-travaux", question: q14, field_type: "string")

# Q15 answers
V2Answer.create(answer_type: "radio", text: "Oui, indispensable", order: 1, question: q15, field_type: "string")
V2Answer.create(answer_type: "radio", text: "A étudier", order: 2, question: q15, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Pas prioritaire", order: 3, question: q15, field_type: "string")

# Q16 answers
V2Answer.create(answer_type: "radio", text: "Moins de 35 ans", order: 1, question: q16, field_type: "string")
V2Answer.create(answer_type: "radio", text: "De 35 à 50 ans", order: 2, question: q16, field_type: "string")
V2Answer.create(answer_type: "radio", text: "De 50 à 65 ans", order: 3, question: q16, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Plus de 65 ans", order: 4, question: q16, field_type: "string")

# Q17 answers
V2Answer.create(answer_type: "checkbox", text: "Ecoliers(ou futur)", order: 1, question: q17, field_type: "array")
V2Answer.create(answer_type: "checkbox", text: "Collégiens", order: 2, question: q17, field_type: "array")
V2Answer.create(answer_type: "checkbox", text: "Lycéens", order: 3, question: q17, field_type: "array")
V2Answer.create(answer_type: "checkbox", text: "Etudiants", order: 4, question: q17, field_type: "array")

# Q18 answers
V2Answer.create(answer_type: "text", text: "Surface", placeholder: "Surface", order: 1, question: q18, field_type: "string", input_format: "surface", maxlength: 5, input_width: "150px", unit: "m²")



# Q19 answers
V2Answer.create(answer_type: "text", text: "Surface", placeholder: "Au minimum", order: 1, question: q19, field_type: "array", maxlength: 6, input_width: "150px", input_format: "surface", unit: "m²")
V2Answer.create(answer_type: "text", text: "Surface", placeholder: "Idéalement", order: 2, question: q19, field_type: "array", maxlength: 6, input_width: "150px", input_format: "surface", unit: "m²")

# Q20 answers
V2Answer.create(answer_type: "numeric", field_type: "integer", order: 1, min: 2, max: 8, step: 1, question: q20)

# Q21 answers
V2Answer.create(answer_type: "text", text: "Idéalement", placeholder: "Idéalement", question: q21, field_type: "array", order: 1, unit: "€", input_format: "currency", maxlength: 10, input_width: "150px", input_hint: "Budget minimum de 50 000 €")
V2Answer.create(answer_type: "text", text: "Au maximum", placeholder: "Au maximum", question: q21, field_type: "array", order: 2, unit: "€", input_format: "currency", maxlength: 10, input_width: "150px")

# Q22 answers
V2Answer.create(answer_type: "radio", text: "Très looongtemps", question: q22, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Récemment, il connaît tout mon projet", question: q22, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Il connaît tout mon projet et a donné son accord de principe", question: q22, field_type: "string", order: 3)

# Q23 answers
V2Answer.create(answer_type: "radio", text: "KÉ SA KO ? Non...", question: q23, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Bonne idée, à faire !", question: q23, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "À étudier", question: q23, field_type: "string", order: 3)

# Q24 answers
V2Answer.create(answer_type: "text", text: "Idéalement", placeholder: "Idéalement", question: q24, field_type: "array", order: 1, unit: "€", input_format: "currency", maxlength: 10, input_width: "150px")
V2Answer.create(answer_type: "text", text: "Au maximum", placeholder: "Au maximum", question: q24, field_type: "array", order: 2, unit: "€", input_format: "currency", maxlength: 10, input_width: "150px")

# Q25 answers
V2Answer.create(answer_type: "text", text: "Idéalement", placeholder: "Idéalement", question: q25, field_type: "array", order: 1, unit: "€", input_format: "currency", maxlength: 10, input_width: "150px")
V2Answer.create(answer_type: "text", text: "Au maximum", placeholder: "Au maximum", question: q25, field_type: "array", order: 2, unit: "€", input_format: "currency", maxlength: 10, input_width: "150px")

# Q26 answers
V2Answer.create(answer_type: "radio", text: "Absolument !", question: q26, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Non, pas du tout", question: q26, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Peu importe", question: q26, field_type: "string", order: 3)

# Q27 answers
V2Answer.create(answer_type: "radio", text: "Oui bien sur !", question: q27, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Non, pas du tout", question: q27, field_type: "string", order: 2)

# Q28 answers
V2Answer.create(answer_type: "radio", text: "Oui, 2 étages ne me font pas peur", question: q28, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Oui, mais 1 étage seulement", question: q28, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Non, du plain-pied uniquement", question: q28, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "Peu importe", question: q28, field_type: "string", order: 4)

# Q29 answers
V2Answer.create(answer_type: "radio", text: "Au rez-de-chaussée, sur le plancher des vaches", question: q29, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "À partir du 1er, je préfère l'altitude", question: q29, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Le dernier étage, plus proche du ciel", question: q29, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "Peu importe", question: q29, field_type: "string", order: 4)

# Q30 answers
V2Answer.create(answer_type: "radio", image: "ascenseur", text: "Un ascenseur obligatoirement", question: q30, order: 1, field_type: "string")
V2Answer.create(answer_type: "radio", image: "escalier", text: "Des escaliers pour faire mon sport quotidien", question: q30, order: 2, field_type: "string")

# Q31 answers
V2Answer.create(answer_type: "text", text: "Ma surface idéale", placeholder: "Ma surface idéale", question: q31, field_type: "string", order: 1, unit: "m²", input_format: "surface", maxlength: 5)

# Q32 answers
V2Answer.create(answer_type: "radio", text: "Cuisine ouverte", question: q32, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Cuisine fermée", question: q32, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "L'une comme l'autre", question: q32, field_type: "string", order: 3)

# Q33 answers
V2Answer.create(answer_type: "radio", text: "1, ça me suffit", question: q33, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Au moins 2 obligatoirement", question: q33, field_type: "string", order: 2)

# Q34 answers
V2Answer.create(answer_type: "checkbox", image: "Buanderie", text: "Une buanderie", question: q34, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", image: "cave", text: "Une cave", question: q34, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", image: "cellier", text: "Un cellier", question: q34, field_type: "array", order: 3)
V2Answer.create(answer_type: "checkbox", image: "veranda", text: "Une veranda", question: q34, field_type: "array", order: 4)

# Q35 answers
V2Answer.create(answer_type: "checkbox", image: "alarme", text: "Une alarme", question: q35, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", image: "clim", text: "La climatisation", question: q35, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", image: "dressing", text: "Un vrai dressing", question: q35, field_type: "array", order: 3)
V2Answer.create(answer_type: "checkbox", image: "placard", text: "Des placards", question: q35, field_type: "array", order: 4)

# Q36 answers
V2Answer.create(answer_type: "checkbox", text: "Une belle vue", question: q36, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", text: "Pas de vis-à-vis", question: q36, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", text: "Une exposition sud", question: q36, field_type: "array", order: 3)
V2Answer.create(answer_type: "checkbox", text: "Une exposition sans nord", question: q36, field_type: "array", order: 4)
V2Answer.create(answer_type: "checkbox", text: "Pas d'idée préconçue", question: q36, field_type: "array", order: 5)

# Q37 answers
V2Answer.create(answer_type: "checkbox", text: "D'un local vélo", question: q37, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", text: "D'une place de parking", question: q37, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", text: "D'un parking fermé", question: q37, field_type: "array", order: 3)

# Q38 answers
V2Answer.create(answer_type: "radio", text: "Oui", question: q38, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Non", question: q38, field_type: "string", order: 2)

# Q39 answers
V2Answer.create(answer_type: "checkbox", text: "Un gardien", question: q39, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", text: "Un interphone/visiophone", question: q39, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", text: "Un digicode", question: q39, field_type: "array", order: 3)

# Q40 answers
V2Answer.create(answer_type: "radio", text: "Individuel", question: q40, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Collectif", question: q40, field_type: "string", order: 2)

# Q41 answers
V2Answer.create(answer_type: "checkbox", image: "chauffage-fioul", text: "Au fioul", question: q41, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", image: "cheminee", text: "Pompe à chaleur, poêle à granulés, poêle à bois, cheminée", question: q41, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", image: "radiateur-electrique", text: "Au gaz ou électrique", question: q41, field_type: "array", order: 3)

# Q42 answers
V2Answer.create(answer_type: "checkbox", text: "Très performant: A ou B", question: q42, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", text: "Assez performant: C ou D", question: q42, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", text: "Peu performant: E et même au-delà !", question: q42, field_type: "array", order: 3)

# Q43 answers
V2Answer.create(answer_type: "checkbox", text: "Un balcon", question: q43, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", text: "Une terrasse", question: q43, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", text: "Soyons fou, une piscine dans la résidence", question: q43, field_type: "array", order: 3)

# Q44 answers
V2Answer.create(answer_type: "checkbox", text: "Un jardin", question: q44, field_type: "array", order: 1)
V2Answer.create(answer_type: "checkbox", text: "Une piscine", question: q44, field_type: "array", order: 2)
V2Answer.create(answer_type: "checkbox", text: "Une pergola, oui, oui", question: q44, field_type: "array", order: 3)

# Q45 answers
V2Answer.create(answer_type: "radio", text: "Cesar", image: "cesar", question: q45, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Churchill", image: "churchill", question: q45, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Kurt Cobain", image: "kurt-cobain", question: q45, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "Petit prince", image: "petit-prince", question: q45, field_type: "string", order: 4)
V2Answer.create(answer_type: "radio", text: "Aladdin", image: "aladdin", question: q45, field_type: "string", order: 5)
V2Answer.create(answer_type: "radio", text: "Van gogh", image: "van-gogh", question: q45, field_type: "string", order: 6)

# Q46 answers
V2Answer.create(answer_type: "radio", text: "Amelie Poulain", image: "amelie-poulain", question: q46, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Cleopatre", image: "cleopatre", question: q46, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Coco chanel", image: "coco-chanel", question: q46, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "Leia", image: "leia", question: q46, field_type: "string", order: 4)
V2Answer.create(answer_type: "radio", text: "Raiponce", image: "raiponce", question: q46, field_type: "string", order: 5)
V2Answer.create(answer_type: "radio", text: "Sissi", image: "sissi", question: q46, field_type: "string", order: 6)

# Q47 answers
# Pair 1
V2Answer.create(answer_type: "radio", text: "Lent", question: q47, field_type: "array", order: 2)
V2Answer.create(answer_type: "radio", text: "Rapide", question: q47, field_type: "array", order: 1)

# Pair 2
V2Answer.create(answer_type: "radio", text: "Difficile", question: q47, field_type: "array", order: 4)
V2Answer.create(answer_type: "radio", text: "Facile", question: q47, field_type: "array", order: 3)

# Pair 3
V2Answer.create(answer_type: "radio", text: "Ebouriffant", question: q47, field_type: "array", order: 5)
V2Answer.create(answer_type: "radio", text: "Ennuyeux", question: q47, field_type: "array", order: 6)

# Pair 4
V2Answer.create(answer_type: "radio", text: "Angoissant", question: q47, field_type: "array", order: 8)
V2Answer.create(answer_type: "radio", text: "Tranquille", question: q47, field_type: "array", order: 7)

# Pair 5
V2Answer.create(answer_type: "radio", text: "Audacieux", question: q47, field_type: "array", order: 10)
V2Answer.create(answer_type: "radio", text: "Prudent", question: q47, field_type: "array", order: 9)

# Pair 6
V2Answer.create(answer_type: "radio", text: "Impulsif", question: q47, field_type: "array", order: 12)
V2Answer.create(answer_type: "radio", text: "Réfléchi", question: q47, field_type: "array", order: 11)

# Pair 7
V2Answer.create(answer_type: "radio", text: "Serein", question: q47, field_type: "array", order: 13)
V2Answer.create(answer_type: "radio", text: "Conflictuel", question: q47, field_type: "array", order: 14)

# Pair 8
V2Answer.create(answer_type: "radio", text: "Limpide", question: q47, field_type: "array", order: 15)
V2Answer.create(answer_type: "radio", text: "Obscur", question: q47, field_type: "array", order: 16)


puts "***** New models created ******"


# CONDITIONS

# Q4 Conditions
V2Condition.create(demand_field: "destination", operator: "not_equal", value: "#{q3.v2_answers.where(auto_identifier: 6).last.auto_identifier}", question: q4, journey: "maison", parent_question_id: q3.id.to_s)

# Q8 Conditions
V2Condition.create(demand_field: "destination", operator: "not_equal", value: "#{q3.v2_answers.where(auto_identifier: 6).last.auto_identifier}", question: q4, journey: "maison", parent_question_id: q3.id.to_s)

# Q8 Conditions
V2Condition.create(demand_field: "actual_situation", operator: "equal", value: "#{q5.v2_answers.where(auto_identifier: 12).last.auto_identifier}", question: q8, journey: "maison", parent_question_id: q5.id.to_s)

# Q12 Conditions
V2Condition.create(demand_field: "property_type", operator: "not_equal", value: "#{q9.v2_answers.where(auto_identifier: 23).last.auto_identifier}", question: q12, journey: "appart, maison", parent_question_id: q9.id.to_s)

# Q13 Conditions
V2Condition.create(demand_field: "property_type", operator: "not_equal", value: "#{q9.v2_answers.where(auto_identifier: 23).last.auto_identifier}", question: q13, journey: "appart, maison", parent_question_id: q9.id.to_s)

# Q14 Conditions
V2Condition.create(demand_field: "state", operator: "equal", value: "#{q12.v2_answers.where(auto_identifier: 32).last.auto_identifier}", question: q14, journey: "appart, maison", parent_question_id: q12.id.to_s)

# Q15 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 21).last.auto_identifier}", question: q15, journey: "maison", parent_question_id: q9.id.to_s)

# Q16 Conditions
V2Condition.create(demand_field: "gender", operator: "equal", value: "#{q1.v2_answers.where(auto_identifier: 2).last.auto_identifier}", question: q16, journey: "maison", parent_question_id: q1.id.to_s)

# Q18 condition
V2Condition.create(demand_field: "actual_housing", operator: "equal", value: "#{q6.v2_answers.where(auto_identifier: 15).last.auto_identifier}", question: q18, journey: "maison", parent_question_id: q6.id.to_s)

# Q24 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 23).last.auto_identifier}", question: q24, journey: "terrain", parent_question_id: q9.id.to_s)

# Q25 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 23).last.auto_identifier}", question: q25, journey: "terrain", parent_question_id: q9.id.to_s)

# Q26 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 23).last.auto_identifier}", question: q26, journey: "terrain", parent_question_id: q9.id.to_s)

# Q27 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 23).last.auto_identifier}", question: q27, journey: "terrain", parent_question_id: q9.id.to_s)

# Q28 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 21).last.auto_identifier}", question: q28, journey: "maison", parent_question_id: q9.id.to_s)

# Q29 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 22).last.auto_identifier}", question: q29, journey: "appart", parent_question_id: q9.id.to_s)




# Q17 Conditions
c17_child_condition = V2Condition.create(demand_field: 'project', operator: 'equal', value: "#{q4.v2_answers.where(auto_identifier: 10).last.auto_identifier}")
V2Condition.create(demand_field: 'project', operator: 'equal', value: "#{q4.v2_answers.where(auto_identifier: 9).last.auto_identifier}", children_operator: 'or', question: q17, child_v2_condition: c17_child_condition, journey: "famille", parent_question_id: q4.id.to_s)


# Q30 Conditions
c30_child_condition = V2Condition.create(demand_field: 'flat_stairs', operator: 'not_equal', value: "#{q29.v2_answers.where(auto_identifier: 77).last.auto_identifier}")
V2Condition.create(demand_field: 'property_type', operator: 'equal', value: "#{q9.v2_answers.where(auto_identifier: 22).last.auto_identifier}", children_operator: 'and', question: q30, child_v2_condition: c30_child_condition, journey: "appart", parent_question_id: q9.id.to_s)




# Q37 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 22).last.auto_identifier}", question: q37, journey: "appart", parent_question_id: q9.id.to_s)

# Q38 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 21).last.auto_identifier}", question: q38, journey: "appart", parent_question_id: q9.id.to_s)

# Q39 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 22).last.auto_identifier}", question: q39, journey: "appart", parent_question_id: q9.id.to_s)

# Q40 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 21).last.auto_identifier}", question: q41, journey: "appart", parent_question_id: q9.id.to_s)

# Q41 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 22).last.auto_identifier}", question: q40, journey: "maison", parent_question_id: q9.id.to_s)




# Q43 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 22).last.auto_identifier}", question: q43, journey: "appartement", parent_question_id: q9.id.to_s)

# Q44 Conditions
V2Condition.create(demand_field: "property_type", operator: "equal", value: "#{q9.v2_answers.where(auto_identifier: 21).last.auto_identifier}", question: q44, journey: "maison", parent_question_id: q9.id.to_s)

# Q45 Conditions
V2Condition.create(demand_field: "gender", operator: "equal", value: "#{q1.v2_answers.where(auto_identifier: 2).last.auto_identifier}", question: q45, journey: "homme", parent_question_id: q1.id.to_s)

# Q46 Conditions
V2Condition.create(demand_field: "gender", operator: "equal", value: "#{q1.v2_answers.where(auto_identifier: 1).last.auto_identifier}", question: q46, journey: "femme", parent_question_id: q1.id.to_s)



# New question + condition + answer
# Q18bis condition + answers
q18bis = Question.create(demand_attribute: "actual_housing_area",
                    avatar: "kenotte-avatar",
                    image: "appartement-actu-surface",
                    question_type: "text",
                    label: "Quel est la surface de votre",
                    field_type: "string",
                    variable_in_label: true,
                    variable_demand_field: "actual_housing",
                    required: false,
                    label_resume: "Surface de mon logement actuel",
                    agency_resume: "Surface du logement actuel",
                    criteria_resume: "Surface du logement actuel",
                    theme: "L'acheteur",
                    position: 48)
Sentence.create(demand_attribute: "firstname", content: "D'accord !", question: q18bis)
V2Answer.create(answer_type: "text", text: "Surface", placeholder: "Surface", order: 1, question: q18bis, field_type: "string", input_format: "surface", maxlength: 5, input_width: "150px", unit: "m²")
V2Condition.create(demand_field: "actual_housing", operator: "equal", value: "#{q6.v2_answers.where(auto_identifier: 14).last.auto_identifier}", question: q18bis, journey: "appartement", parent_question_id: q6.id.to_s)


q4bis = Question.create(demand_attribute: "project",
                    avatar: "kenotte-avatar",
                    label: "Qui vivra dans ce logement ?",
                    question_type: "radio_with_image",
                    field_type: "string",
                    advanced_radio: true,
                    required: true,
                    label_resume: "Mon projet",
                    agency_resume: "Le projet",
                    criteria_resume: "Le projet",
                    theme: "Le projet",
                    position: 49)
Sentence.create(content: "Intéressant...", question: q4bis)
V2Answer.create(answer_type: "radio", image: "locataire", text: "Un locataire", question: q4bis, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", image: "famille", text: "De la famille", question: q4bis, field_type: "string", order: 2)
V2Condition.create(demand_field: "destination", operator: "equal", value: "#{q3.v2_answers.where(auto_identifier: 6).last.auto_identifier}", question: q4bis, journey: "appartement", parent_question_id: q3.id.to_s)


q16bis = Question.create(demand_attribute: "age",
                    avatar: "kenotte-avatar",
                    image: "woman",
                    question_type: "radio",
                    label: "Quel âge avez-vous, cela peut être important dans nos recommandations",
                    field_type: "string",
                    required: true,
                    label_resume: "Mon âge",
                    agency_resume: "Age",
                    criteria_resume: "Age",
                    theme: "L'acheteur",
                    position: 50)
Sentence.create(content: "Au fait, rien à voir, mais je me demandais... La valeur n'attend pas le nombre des années !", question: q16bis)
V2Answer.create(answer_type: "radio", text: "Moins de 35 ans", order: 1, question: q16bis, field_type: "string")
V2Answer.create(answer_type: "radio", text: "De 35 à 50 ans", order: 2, question: q16bis, field_type: "string")
V2Answer.create(answer_type: "radio", text: "De 50 à 65 ans", order: 3, question: q16bis, field_type: "string")
V2Answer.create(answer_type: "radio", text: "Plus de 65 ans", order: 4, question: q16bis, field_type: "string")
V2Condition.create(demand_field: "gender", operator: "equal", value: "#{q1.v2_answers.where(auto_identifier: 1).last.auto_identifier}", question: q16bis, journey: "maison", parent_question_id: q1.id.to_s)

q51 = Question.create(demand_attribute: "last_precision",
                      avatar: "kenotte-avatar",
                      label: "Voulez-vous nous donner une dernière précision pour nous aider à trouver votre",
                      question_type: "textarea",
                      field_type: "string",
                      required: false,
                      variable_in_label: true,
                      variable_demand_field: "property_type",
                      second_part_label: "?",
                      label_resume: "Dernières précisions",
                      criteria_resume: "Précisions",
                      agency_resume: "Précisions",
                      theme: "L'acheteur",
                      position: 51)
Sentence.create(content: "C'est à croire qu'on se ressemble !", question: q51)
V2Answer.create(answer_type: "textarea", text: "Je m'exprime", placeholder: "Je m'exprime", question: q51, field_type: "string", order: 1)


# Q52
q52 = Question.create(demand_attribute: "what_is_liked",
                      avatar: "kenotte-avatar",
                      label: "Qu'aimez-vous particulièrement dans votre",
                      question_type: "textarea",
                      field_type: "string",
                      required: false,
                      variable_in_label: true,
                      variable_demand_field: "property_type",
                      second_part_label: "actuellement ?",
                      label_resume: "Ce que vous aimez",
                      criteria_resume: "Les plus du logement actuel",
                      agency_resume: "Les plus du logement actuel",
                      theme: "L'acheteur",
                      position: 52)
Sentence.create(demand_attribute: "firstname", content: "Super", content_after_variable: "!", question: q52)
V2Answer.create(answer_type: "textarea", text: "Je m'exprime", placeholder: "Je m'exprime", question: q52, field_type: "string", order: 1)




# Q53
q53 = Question.create(demand_attribute: "income",
                      avatar: "kenotte-avatar",
                      label: "Devenir propriétaire, c'est super, mais il faut bien sécuriser son budget. Afin de vous aider sur ce point, pouvez-vous nous préciser votre niveau de revenu mensuel (c'est optionnel, mais ca peut vous aider !) ?",
                      question_type: "radio",
                      image: "budget",
                      field_type: "string",
                      required: false,
                      intermediate_exit: true,
                      label_resume: "Mes revenus",
                      agency_resume: "Les revenus",
                      criteria_resume: "Revenus",
                      theme: "Le projet",
                      position: 53)
Sentence.create(content: "Super", question: q53)
V2Answer.create(answer_type: "radio", text: "Moins de 2500 €", question: q53, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "De 2500 € à 3500 €", question: q53, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "De 3500 € à 6500 €", question: q53, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "Plus de 6500 €", question: q53, field_type: "string", order: 4)



# Q54
# q54 = Question.create(demand_attribute: "current_address",avatar: "kenotte-avatar",label: "J'aimerais vous trouver un quartier qui vous resssemble vraiment… Quelle est votre adresse actuelle ?",question_type: "text",field_type: "string",required: false,label_resume: "Mon adresse actuelle",agency_resume: "Adresse actuelle",google_autocomplete: true,criteria_resume: "Adresse actuelle",theme: "L'acheteur",position: 54)
# Sentence.create(content: "Super", question: q54)
# V2Answer.create(answer_type: "text", placeholder: "Mon adresse actuelle", question: q54, field_type: "string", order: 1)


q54 = Question.create(demand_attribute: "current_location",
                      avatar: "kenotte-avatar",
                      label: "Par rapport à votre périmètre de recherche, aujourd'hui vous vivez ...",
                      question_type: "radio",
                      image: "distance-lieux",
                      field_type: "string",
                      required: true,
                      label_resume: "Emplacement actuel",
                      agency_resume: "Emplacement actuel",
                      criteria_resume: "Emplacement actuel",
                      theme: "L'acheteur",
                      position: 54)
Sentence.create(content: "Super", question: q54)
V2Answer.create(answer_type: "radio", text: "A moins de 5km", question: q54, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "A plus de 5km", question: q54, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "Beaucoup plus, je change de région", question: q54, field_type: "string", order: 3)



q55 = Question.create(demand_attribute: "location_link",
                      avatar: "kenotte-avatar",
                      label: "Et l'idéal serait donc de vous rapprocher ...",
                      question_type: "radio",
                      image: "distance-quartier",
                      field_type: "string",
                      required: true,
                      label_resume: "Rapprochement",
                      agency_resume: "Rapprochement",
                      criteria_resume: "Rapprochement",
                      theme: "L'acheteur",
                      position: 55)
Sentence.create(content: "Super", question: q55)
V2Answer.create(answer_type: "radio", text: "De mon lieu de travail", question: q55, field_type: "string", order: 1)
V2Answer.create(answer_type: "radio", text: "Des écoles", question: q55, field_type: "string", order: 2)
V2Answer.create(answer_type: "radio", text: "De ma famille", question: q55, field_type: "string", order: 3)
V2Answer.create(answer_type: "radio", text: "De mes loisirs", question: q55, field_type: "string", order: 4)
V2Answer.create(answer_type: "radio", text: "Autres", question: q55, field_type: "string", order: 5)















# Multi conditions
# c19_child_condition_child = V2Condition.create(demand_field: 'surface', operator: 'superior', value: '100', hard_stock: true)
# c19_child_condition = V2Condition.create(demand_field: 'budget', operator: 'superior', value: '150000', children_operator: 'or', child_v2_condition: c19_child_condition_child, hard_stock: true)
# c19 = V2Condition.create(demand_field: 'property_type', operator: 'not_equal', value: "#{q18.v2_answers.where(auto_identifier: 36).last.auto_identifier}", children_operator: 'and', question: q19, child_v2_condition: c19_child_condition)


# test sur les ClientSideCondition multi
ClientSideCondition.create(question: q8)
ClientSideCondition.create(question: q20, step: 1, min: 2, max: 2, demand_field: "surface")
ClientSideCondition.create(question: q21)

