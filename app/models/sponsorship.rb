class Sponsorship
  include Mongoid::Document

  field :first_name, type: String
  field :last_name, type: String
  field :email, type: String
  field :phone, type: String
  field :property_city, type: String
  field :details, type: String
  field :property_type, type: String
  field :sponsored_first_name, type: String
  field :sponsored_last_name, type: String
  field :sponsored_email, type: String
  field :sponsored_phone, type: String

end
