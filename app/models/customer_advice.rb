# frozen_string_literal: true

# class CustomerAdvice
class CustomerAdvice
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :property
  belongs_to :demand

  field :like, type: String
  field :comment, type: String
  field :score, type: Integer
end
