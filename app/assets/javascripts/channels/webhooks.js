App.messages = App.cable.subscriptions.create('WebhooksChannel', {
  received: function(data) {
    console.log(data);
    return $('#webhooks-socket').prepend(this.renderMessage(data));
  },
  renderMessage: function(data) {
    if (data.service === "Mandrill") {
      var header = this.generateHeader(data);
      var triggerTime = this.generateTriggerTime(data);
      var dynamicContent = this.generateContent(data);
      var filterTarget = data.filter_target;
      return this.renderFullitem(header,triggerTime,dynamicContent,filterTarget);
    } else if (data.service === "Heroku") {
      var header = this.generateHeader(data);
      var triggerTime = this.generateTriggerTime(data);
      var dynamicContent = this.generateContent(data);
      var filterTarget = data.filter_target;
      return this.renderFullitem(header,triggerTime,dynamicContent,filterTarget);
    } else if (data.service === "Nexmo") {
      var header = this.generateHeader(data);
      var triggerTime = this.generateTriggerTime(data);
      var content = "<div class='test'></div>";
      var filterTarget = data.filter_target;
      return this.renderFullitem(header,triggerTime,content,filterTarget);
    } else if (data.service === "Codeship") {
      var header = this.generateHeader(data);
      var triggerTime = this.generateTriggerTime(data);
      var dynamicContent = this.generateContent(data);
      var filterTarget = data.filter_target;
      return this.renderFullitem(header,triggerTime,dynamicContent,filterTarget);
    } else if (data.service === "Typeform") {
      var header = this.generateHeader(data);
      var triggerTime = this.generateTriggerTime(data);
      var content = "<div class='test'></div>";
      var filterTarget = data.filter_target;
      return this.renderFullitem(header,triggerTime,content,filterTarget);
    } else if (data.service === "Bientotchezsoi") {
      var header = this.generateHeader(data);
      var triggerTime = this.generateTriggerTime(data);
      var dynamicContent = this.generateContent(data);
      var filterTarget = data.filter_target;
      return this.renderFullitem(header,triggerTime,dynamicContent,filterTarget);
    }
  },
  generateHeader: function(data) {
    var liveLogo = "<div class='websocket-live-item-logo'>New</div>";
    var html = "<div class='d-flex-start fullheader'>" + "<div class='inline-svg'>" + data.logo + "</div>" + "<div class='webhook-card-header'>" + data.service + "<div class='webhook-description'>" + data.description + "</div>" + liveLogo + "</div>" + "</div>";
    return html;
  },
  generateTriggerTime: function(data) {
    var html = "<div class='event-time-container'>" + "<div class='webhook-time-label'>" + "Evènement déclenché le" + "</div>" + "<div class='webhook-time'>" + data.event_trig_at + "</div>" + "<div class='point-delimiter'></div>" + "<div class='webhook-time-label'>Event</div>" + "<div class='webhook-time'>" + data.custom_action + "</div>" + "</div>";
    return html;
  },
  generateContent: function(data) {
    var cardContent = document.createElement("div");
    cardContent.classList.add("webhook-card-content");
    if (data.dynamic_content) {
      Array.from(data.dynamic_content).forEach(function(item){
        console.log(item);
        var contentItem = document.createElement("div");
        contentItem.classList.add("webhook-content-item");
        var contentLabel = document.createElement("div");
        contentLabel.classList.add("content-item-label");
        var contentValue = document.createElement("div");
        contentValue.classList.add("content-item-value");
        if (item.html_class) {
          contentValue.classList.add(item.html_class);
        }
        contentLabel.textContent = item.label;
        contentValue.textContent = item.value;
        contentItem.appendChild(contentLabel);
        contentItem.appendChild(contentValue);
        cardContent.appendChild(contentItem);
      });
    } else {
      console.log("There is no dynamic content");
    }
    return cardContent;
  },
  renderFullitem: function(header,triggerTime,content,filterTarget) {
    if (typeof(content) !== "undefined") {
      var html = "<div class='webhook-card' data-filtertarget='" + filterTarget + "'>" + header + triggerTime + content.outerHTML + "</div>";
    } else {
      var html = "<div class='webhook-card' data-filtertarget='" + filterTarget + "'>" + header + triggerTime + "</div>";
    }
    return html;
  }
});
