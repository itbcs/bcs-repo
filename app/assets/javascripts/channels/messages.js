App.messages = App.cable.subscriptions.create('MessagesChannel', {
  received: function(data) {
    console.log(data);
    $("#messages").removeClass('hidden');
    $("#chat_message_content").val('');
    $("#chatroom").animate({scrollTop: $('#chatroom')[0].scrollHeight}, "slow");
    return $('#messages').append(this.renderMessage(data));
  },

  renderMessage: function(data) {
    if (data.anonym === true) {
      console.log("Anonym chatroom");
    } else {
      console.log("Clear chatroom");
    }
    if (data.html_class === "agency-message") {
      return "<div class='agency-message-container'><div class='agency-avatar'><i class='fa fa-home'></i></div><div class='" + data.html_class + "'>" + "<div class='message-origin'>" + data.user + "</div>" + "<div class='message-content'>" + data.message + "</div></div></div>";
    }
    if (data.html_class === "customer-message") {
      return "<div class='customer-message-container'><div class='" + data.html_class + "'>" + "<div class='message-origin'>" + data.user + "</div>" + "<div class='message-content'>" + data.message + "</div></div><div class='customer-avatar'><i class='fa fa-user'></i></div></div>";
    }
  }
});
