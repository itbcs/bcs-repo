class Photo
  include Mongoid::Document
  include Mongoid::Timestamps
  mount_uploader :photo, ImageUploader
end
