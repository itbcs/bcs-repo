# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale
  include Response

  # if Rails.env.development?
  #   http_basic_authenticate_with :name => ENV['HTTP_AUTH_LOGIN'], :password => ENV['HTTP_AUTH_PWD']
  # end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  # def authenticate_customer!
  #   if cookies[:customer_id]
  #     @customer = Customer.find(cookies[:customer_id].to_s)
  #     redirect_to edit_customer_registration_path(@customer)
  #   else
  #     super
  #   end
  # end

  def not_found_redirect
    redirect_hash = {
      "/je-cherche/login?user=1" => "/customers/connexion",
      "/je-cherche/register?user=1" => "/customers/connexion",
      "/je-cherche/password/reset?user=1" => "/customers/password/new",
      "/conseils-immobilier" => "/blog",
      "/vente-appartements-neufs-t2-t3-2" => "/vente-appartement-neuf-t3",
      "/acheter-a-deux" => "/blog/achat/acheter-a-deux",
      "/category/achat" => "/blog/achat",
      "/reussir-vente-coaching-de-kenotte" => "/blog/vente/reussir-vente-coaching-de-kenotte",
      "/category/vente" => "/blog/vente",
      "/blog-special-achat" => "/blog/achat",
      "/je-cherche/login?user=2" => "/customers/connexion",
      "/home-staging" => "/blog/vente/home-staging",
      "/blog-special-vente" => "/blog/vente",
      "/category/billet-dhumeur" => "/blog/billet-d-humeur",
      "/category/construction" => "/blog/construction",
      "/category/non-classe" => "/blog",
      "/category/offre-job" => "/blog",
      "/category/tops" => "/blog",
      "/tag/achat" => "/blog",
      "/tag/appartement" => "/blog",
      "/tag/carte-scolaire" => "/blog",
      "/tag/coaching" => "/blog",
      "/tag/conseil" => "/blog",
      "/tag/construction" => "/blog",
      "/tag/couple" => "/blog",
      "/tag/famille" => "/blog",
      "/tag/financement" => "/blog",
      "/tag/maison" => "/blog",
      "/tag/prix" => "/blog",
      "/tag/terrain" => "/blog",
      "/tag/travaux" => "/blog",
      "/tag/vente" => "/blog",
      "/tag/visite" => "/blog",
      "/author/annegaelle" => "/blog",
      "/plan-de-financement" => "/blog/achat/plan-de-financement",
      "/avant-apres-la-visite-d-un-bien" => "/blog/achat/visite-d-un-bien",
      "/je-cherche/register?user=2" => "/agencies/connexion",
      "/je-cherche/password/reset?user=2" => "/agencies/password/new",
      "/negociateur-immobilier" => "/blog/offre-job/negociateur-immobilier",
      "/business-developpeur" => "/blog/offre-job/business-developpeur",
      "/faire-soi-meme-construction-de-maison" => "/blog/construction/faire-soi-meme-construction-de-maison",
      "/choisir-constructeur-maison" => "/blog/construction/choisir-constructeur-maison",
      "/type-de-maison-individuelle" => "/blog",
      "/author/kenotte/page/2" => "/blog",
      "/author/julia" => "/blog",
      "/kenotte-decouvre-son-nouveau-quartier" => "blog/billet-d-humeur/kenotte-decouvre-son-nouveau-quartier",
      "/inspiration-avant-apres-se-projeter-dans-un-nouveau-chez-soi" => "/blog",
      "/developpeur-ruby" => "/blog",
      "/top-5-les-pires-colocataires" => "/blog",
      "top-5-les-meilleures-annonces-immo-1" => "/blog",
      "top-5-vivre-a-la-campagne-avantages-1" => "/blog",
      "/category/tops/page/2" => "/blog",
      "/tag/achat/page/2" => "/blog",
      "/tag/appartement/page/2" => "/blog",
      "/tag/conseil/page/2" => "/blog",
      "/tag/maison/page/2" => "/blog",
      "/author/julia/page/2" => "/blog",
      "/top-5-films-animation-immobilier" => "/blog",
      "/top-5-emmenager-avec-des-enfants" => "/blog",
      "/tag/conseil/page/3" => "/blog",
      "/author/julia/page/3" => "/blog"
    }

    url_to_redirect = redirect_hash.has_key? params[:unmatched_route]

    if url_to_redirect
      redirect_to url_to_redirect, :status => 301
    else
      render_404
    end
  end

  def render_404
    redirect_to not_found_path
  end

end
