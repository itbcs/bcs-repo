# frozen_string_literal: true

# class Job
class Job
  include Mongoid::Document

  field :picto, type: String
  field :name, type: String
  field :lname, type: String
  field :mission, type: String
  field :profil, type: String
  field :more, type: String
  field :starting_date, type: Time

end
