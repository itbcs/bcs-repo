class SmsValidation
  include Mongoid::Document

  field :to, type: String
  field :text, type: String
  field :code, type: String
  field :phone, type: String

end
