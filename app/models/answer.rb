# frozen_string_literal: true

# class Answer
class Answer
  include Mongoid::Document
  embedded_in :survey

  field :answer_type, type: String
  field :demand_value, type: String
  field :resume, type: String
  field :text, type: String
  field :validation, type: String
  field :unit, type: String
  field :limit, type: Integer
  field :comment, type: String
  field :input_width, type: String

end
