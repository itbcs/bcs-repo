class EstimationValue
  include Mongoid::Document
  include Mongoid::Timestamps

  field :values, type: Hash
  field :type, type: String

  def self.load_file
    EstimationValue.destroy_all
    house_values = {}
    flat_values = {}
    CSV.foreach("estimations-values.csv", headers: false) do |row|
      city = row[0].split(";")[0].to_s
      flat_mini = row[0].split(";")[1].to_i
      flat_maxi = row[0].split(";")[2].to_i
      flat_average = row[0].split(";")[3].to_i
      house_mini = row[0].split(";")[4].to_i
      house_maxi = row[0].split(";")[5].to_i
      house_average = row[0].split(";")[6].to_i
      flat_values[city] = {min_price: flat_mini, max_price: flat_maxi, average_price: flat_average}
      house_values[city] = {min_price: house_mini, max_price: house_maxi, average_price: house_average}
    end
    EstimationValue.create(values: house_values.to_h, type: "Maison")
    EstimationValue.create(values: flat_values.to_h, type: "Appartement")
  end

  def self.city_names
    house_values = EstimationValue.where(type: "Maison").last
    flat_values = EstimationValue.where(type: "Appartement").last
    house_values.values.keys.map{|key| key.to_s}.reject!(&:empty?)
  end

end


class Array

  def generate_estimation_button_icon
    if self == ["Une maison"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/estimation-house.svg", height: "25", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    elsif self == ["Un appartement"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/estimation-flat.svg", height: "22", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    end
    result
  end

  def generate_quality_items
    if self == ["Il y a des travaux de rénovation à prévoir"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    elsif self == ["Il est de qualité comparable aux autres biens du quartier"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    elsif self == ["Il est d’un standing supérieur aux autres bien du quartier"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    end
    result
  end

end
