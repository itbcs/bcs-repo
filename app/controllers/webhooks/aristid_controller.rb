class Webhooks::AristidController < ActionController::API
  require "csv"

  def survey
    event = JSON.parse(request.body.read)
    object = event.to_h
    agency = object["agency"]
    customer = object["customer"]
    survey = object["survey"]
    @webhook = Webhook.new(service: "Aristid", action: "survey", content: object)
    puts "---- INTO ARISTID WEBHOOK ACTION ----"
    puts object.inspect
    if @webhook.save
      AristidMailer.customer_new_survey(@webhook.content["customer"],@webhook).deliver
      AristidMailer.agency_new_survey(@webhook,@webhook.content["agency"]["contact_email"]).deliver
      Rails.configuration.admin_emails.each do |email|
        AristidMailer.agency_new_survey(@webhook,email).deliver
      end
    end
    render json: {marketing_actions: "Pending"}, status: :ok
  end

  def test
    @webhook = Webhook.where(action: "initial_request").last
  end

  def initial_request
    if request.head?
      puts "Head request ok !!"
    end
    if params["mandrill_events"]
      values = params["mandrill_events"]
      parsed_object = JSON.parse(values)
      hashed_object = {"mandrill_events" => parsed_object.to_a}.to_h
      puts "---- Inside initial request ----"
      puts hashed_object
      @webhook = Webhook.new(service: "Aristid", action: "initial_request", content: hashed_object)
      if @webhook.save
        puts "Aristid initial_request webhook successfully saved !!"
        results = @webhook.handle_aristid_initial_request

        if results.has_key?(:reply_to)
          reply_to = results[:reply_to]
        else
          reply_to = results[:sender]
        end

        puts "---- Reply-to inspector ----"
        puts reply_to
        puts reply_to.inspect
        puts "----------------------------"

        url = "https://aristid.immo/api/agency/details"
        api_result = RestClient.get(url, {params: {email: results[:receiver]}})
        event = JSON.parse(api_result.body)
        aristid_url = event["agency"]["aristid_url"]
        agency_name = event["agency"]["name"]
        phone = event["agency"]["phone"]
        puts "---- API Call to aristid to check agency infos ----"
        puts event.inspect
        puts "---------------------------------------------------"
        AristidMailer.customer_initial_request(reply_to,aristid_url,agency_name,phone).deliver
      end
      puts "--------------------------------"
    end
    render :json => {status: 200}
  end

end
