class Typeform
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Attributes::Dynamic
  include GlobalID::Identification

  field :submitted_at, type: Time
  field :birthdate, type: Time
  field :roomate_birthdate, type: Time
  field :roomate_name, type: String
  field :pdf_filename, type: String
  field :entrepreneur, type: Boolean
  field :message, type: String
  field :employee_status, type: String
  field :roomate_occupation_status, type: String
  field :roomate_other_occupation, type: String
  field :guarantor, type: String
  field :roomate_housing_grant, type: String
  field :titulaire_bail, type: String
  field :has_roomate, type: Boolean

  mount_uploader :pdf, ImageUploader

  def tax
    if self[:tax]
      if self[:tax].to_s.downcase.to_sym == :oui
        value = true
      else
        value = false
      end
    else
      value = nil
    end
    return value
  end

  def submitted_at
    if self[:submitted_at]
      I18n.localize(self[:submitted_at], format: :medium)
    end
  end

  def birthdate
    if self[:birthdate]
      I18n.localize(self[:birthdate], format: :medium)
    else
      nil
    end
  end

  def roomate_birthdate
    if self[:roomate_birthdate]
      I18n.localize(self[:roomate_birthdate], format: :medium)
    else
      nil
    end
  end

end
