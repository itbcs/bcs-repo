var userPhoneNumber;

function showReturnCodeFormGenuine(data){
  console.log("toto !!!");
  var textBlock = $('.form-title-signup');
  if (data.status === "numéro incorrect !") {
    var textFailedNumber = $("<div class='code-failed'>" + "numéro incorrect !" + "</div>");
    if ($('.code-failed').length === 0) {
      textFailedNumber.appendTo(textBlock);
    }
  } else {
    userPhoneNumber = data.phone;
    $('#signin-block').hide('slide',{direction:'left'},1000).empty();
    $('#signup-block').css("border-left", "none");
    $('#signup-block').animate({width: "100%"}, 800);
    $("#sms-validation-form-genuine").hide();
    var textContent = $("<div class='code-success'>" + "Un code vous a été envoyé au " + "<strong>" + data.phone + "</strong>" + "</div>");
    var resendLink = $("<div class='resend-link'>" + "Je n'ai pas reçu mon code de validation ?" + "<span class='resend-code-link'>renvoyer le code</span>" + "</div>");
    $(textBlock).empty().text("La sécurité de vos données est fondamentale : l’authentification par SMS nous permet de vérifier que vous n’êtes pas un robot. Votre numéro de téléphone ne sera jamais cédé à des tiers")
    textContent.appendTo(textBlock);
    resendLink.appendTo(textBlock);

    $("span.resend-code-link").click(function(){
      console.log("Click on resend link");
      $('#sms-return-code').addClass('hidden');
      $("#sms-validation-form-genuine").show();
    })

    $('#sms-return-code').toggleClass('hidden');
    $('#sms-return-form-genuine').submit(function(event){
      event.preventDefault();
      var $form = $(this);
      var code = $form.find('input[name="code"]').val();
      if (code === data.code) {
        $('#new-customer-container').toggleClass('hidden');
        $(".code-success").hide();
        $(".code-failed").hide();
        $('#sms-return-code').hide();
        $('.resend-link').hide();
        putPhoneToForm(data.phone);
      } else {
        var textFailed = $("<div class='code-failed'>" + "code erroné !" + "</div>");
        textFailed.appendTo(textBlock);
      }
    });
  }
}



function putPhoneToForm(phoneNumber){
  $('#customer_phone').val(phoneNumber);
}



function SmsValidationFormFirst(){
  $("#sms-validation-form-genuine").submit(function(event){
    event.preventDefault();
    console.log("Submit sms code sending");
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    var position = $form.find('input[name="phone"]').val();
    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      success: function(data) {
        console.log("Success ajax request");
        console.log(data);
        showReturnCodeFormGenuine(data);
      },
      error: function(data) {
        alert("Error !!!!!");
      }
    });
  });
}


SmsValidationFormFirst();

$(".form-actions-cgu input").click(function() {
  var checked = $(this).prop("checked");
  console.log(checked);
});


















function handleDeviseAjaxFormGenuine() {
  $("#new-customer-container.without-errors form").submit(function(event) {
    event.preventDefault();
    var form = $(this);
    var url = $(form).attr('action');
    var method = $(form).attr('method');
    $.ajax({
      url: url,
      type: method,
      data: $(form).serialize(),
      success: function(data) {
        if (data.errors) {
          var submitButton = $(form).find("input[type='submit']");
          $(submitButton).attr("disabled", false);
          var count = 0;
          for (var [ key, value ] of Object.entries(data.errors)) {
            if (key === "cgu") {
              console.log("CGU error");
              var cguSentence = $("<div class='cgu-signup-unckecked'>Vous devez acceptez les CGU pour continuer</div>");
              if ($(".cgu-signup-unckecked").length === 0) {
                $(".form-actions-cgu").first().prepend(cguSentence);
              }
            } else {
              var sentence = $("<div class='custom-input-error'>Ce champ " + value[0] + "</div>");
              var idToCheck = "#customer_" + key;
              $(idToCheck).addClass("input-error-border");
              var currentInput = $(idToCheck);
              $(sentence).insertAfter($(currentInput));
              $(currentInput).focus(function() {
                $(this).removeClass("input-error-border");
              });
            }
          }
        }
        if (data.url) {
          document.location.href = data.url
        }
      },
      error: function(data) {
        console.log(data);
      }
    });
  });
}


handleDeviseAjaxFormGenuine();












