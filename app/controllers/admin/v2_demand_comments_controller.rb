class Admin::V2DemandCommentsController < ApplicationController
  before_action :find_demand

  def new
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    @demand_comment = @demand.v2_demand_comments.build()
    render "admin/demand_comments/new", layout: "admin"
  end

  def create
    @demand_comment = @demand.v2_demand_comments.build(safe_params)
    if @demand_comment.save
      redirect_to admin_demand_path(@demand.id.to_s), layout: "admin", notice: "Commentaire ajouté à la demande"
    else
      render "admin/demand_comments/new", layout: "admin", alert: "Un problème est survenu !!!"
    end
  end

  def edit
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    @demand_comment = @demand.v2_demand_comments.find(params[:id])
    render "admin/demand_comments/edit", layout: "admin"
  end

  def update
    @demand_comment = @demand.v2_demand_comments.find(params[:id])
    if @demand_comment.update(safe_params)
      redirect_to admin_demand_path(@demand.id.to_s), layout: "admin", notice: "Commentaire mis à jour"
    else
      render "admin/demand_comments/new", layout: "admin", alert: "Un problème est survenu !!!"
    end
  end

  def show
    @demand_comment = DemandComment.find(params[:id])
    render "admin/demand_comments/show", layout: "admin"
  end

  def index
    @demand_comments = @demand.demand_comments
    render "admin/demand_comments/index", layout: "admin"
  end

  def destroy
    @demand_comment = @demand.demand_comments.find(params[:id])
    @demand_comment.destroy
    redirect_to admin_demand_path(@demand.id.to_s), notice: "Commentaire effacé pour la demande #{@demand.id.to_s}"
  end

  private

  def safe_params
    params.require(:v2_demand_comment).permit(:body, :author, :important)
  end

  def find_demand
    @demand = V2Demand.find(params[:demand_id])
  end

end
