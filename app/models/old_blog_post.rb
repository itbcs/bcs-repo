class OldBlogPost
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :title, type: String
  field :old_date, type: Date
  field :content, type: String
  field :status, type: String

end

