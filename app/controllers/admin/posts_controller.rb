class Admin::PostsController < ApplicationController

  before_action :find_post, only: [:show, :edit, :update, :destroy]

  def show
    render :show, layout: "admin"
  end

  def index
    @posts = Post
    render :index, layout: "admin"
  end

  def new
    @post = Post.new
    render :new, layout: "admin"
  end

  def create
    @post = Post.new(safe_params)
  end

  def edit
    render :edit, layout: "admin"
  end

  def update
  end

  def destroy
  end

  private

  def find_post
    @post = Post.find(params[:id])
  end

  def safe_params
    params.require(:post).permit(:name, :lname, :category_name, :title, :content, :link_name, :photo_url, :author, :meta_desc, :meta_title, :targets, tags: [])
  end

end
