function checkedInputsLogic() {
  var limit = 5;
  var inputs = $("form#criterias_form").find("input:checkbox");
  var allCheckedInputs = $("#criterias_form input:checkbox:checked");
  if (allCheckedInputs.length == limit) {
    $('.checked-input-text').text("Vous avez choisi vos " + allCheckedInputs.length + " critères.");
    $("#modal-criterias-confirmation").removeClass("hidden");
    $("#negative-button").click(function(){
      $("#modal-criterias-confirmation").addClass("hidden");
    });
    $("#affirmative-button").click(function(){
      $("#criterias_form").submit();
    });
  }
  $(inputs).click(function(event) {
    var allCheckedInputs = $("#criterias_form input:checkbox:checked");
    var allUncheckedInputs = $("#criterias_form input:checkbox:not(:checked)");
    var container = $('.checked-input-count');
    if (allCheckedInputs.length > limit) {
      event.preventDefault();
    } else if (allCheckedInputs.length == limit) {
      $(container).find('.checked-input-text').text("Vous avez choisi vos " + allCheckedInputs.length + " critères.");
      // $('#validate-criterias').show();
      $("#modal-criterias-confirmation").removeClass("hidden");
      $("#modal-criterias-confirmation").find(".custom-modal-closer").click(function(){
        $("#modal-criterias-confirmation").addClass("hidden");
      });
      $("#negative-button").click(function(){
        $("#modal-criterias-confirmation").addClass("hidden");
      });
      $("#affirmative-button").click(function(){
        $("#criterias_form").submit();
      });
    } else {
      if (limit - allCheckedInputs.length == 1) {
        $(container).find('.checked-input-text').text("Encore " + (limit - allCheckedInputs.length) + " critère à choisir !" );
      } else {
        $(container).find('.checked-input-text').text("Encore " + (limit - allCheckedInputs.length) + " critères à choisir !" );
      }
      // $('#validate-criterias').hide();
    }
  });
}

checkedInputsLogic();
