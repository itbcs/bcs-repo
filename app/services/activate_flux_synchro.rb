class ActivateFluxSynchro

  def initialize(agency,responses)
    @responses = responses
    @agency = agency
    @errors = []
    @synchro = false
    @datas = {}
  end

  def call
    handle_synchro
    service_success
  end

  private

  def handle_synchro
    if @agency
      @props_count = 0
      puts "This agency is present on BCS"
      puts "This agency has #{@agency.properties.size} props"
      puts "Siret: #{@agency.siret}"
      @responses["properties"].each do |prop|
        @present_prop = @agency.properties.where(hektor_id: prop["hektor_id"]).last
        if @present_prop
          puts "Property already import from flux"
        end
        if !@present_prop
          puts prop.except("_id","agency_id").inspect
          photos = prop["images_array"].to_a.first(8)
          property_type_code = prop["property_type_code"]
          property_type = property_type_string(prop["property_type_code"].to_i)
          agency_id = @agency.id.to_s
          source = "hektor"
          fees = prop["fees"] ? prop["fees"] : "0"
          state = "Très bon"
          price = prop["price"]
          fees_target = (prop["fees_target_code"] == 1) ? "De l'acquéreur" : "Du vendeur"
          area = prop["area"]
          status = prop["status_code"]
          corps = prop["corps"]
          city = prop["city"]
          title = prop["title"]
          zipcode = prop["zipcode"]
          hektor_id = prop["hektor_id"]
          nb_room = prop["nb_room"] ? prop["nb_room"].to_i : nil
          build_year = prop["build_year"] ? prop["build_year"] : nil
          values = {title: title, state: state, price_int: price.to_i, price: price, fees_target: fees_target, fees: fees, status: status, source: source, hektor_id: hektor_id, corps: corps, build_year: build_year, nb_room: nb_room, geoloc_zipcode: zipcode,zipcode: zipcode, locality: city, localisation: city, city: city, property_type_code: property_type_code, property_type: property_type.downcase, agency_id: agency_id, area: area.to_i}
          puts values
          puts photos.size
          @new_prop = Property.new(values.to_h)
          photos.each_with_index do |photo,index|
            if index + 1 == 1
              @new_prop.remote_img_1_url = photo.to_s
            elsif index + 1 == 2
              @new_prop.remote_img_2_url = photo.to_s
            elsif index + 1 == 3
              @new_prop.remote_img_3_url = photo.to_s
            elsif index + 1 == 4
              @new_prop.remote_img_4_url = photo.to_s
            elsif index + 1 == 5
              @new_prop.remote_img_5_url = photo.to_s
            elsif index + 1 == 6
              @new_prop.remote_img_6_url = photo.to_s
            elsif index + 1 == 7
              @new_prop.remote_img_7_url = photo.to_s
            elsif index + 1 == 8
              @new_prop.remote_img_8_url = photo.to_s
            end
          end
          if @new_prop.save
            @props_count += 1
            @agency.properties.push(@new_prop)
            puts @new_prop
          else
            puts @new_prop.errors.messages
          end
        end
      end
      @agency.flux_origin = "hektor"
      @agency.flux_synchro = true
      if @agency.save(validate: false)
        @synchro = true
      end
    end
    @datas[:agency] = "#{@agency.name}"
    @datas[:props_count] = "#{@props_count}"
    if @agency.flux_synchro
      @datas[:agency_flux_status] = true
    end
  end

  def service_success
    ServiceSuccess.new(@datas)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
