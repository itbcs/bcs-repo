# frozen_string_literal: true

# class TrackingCode
class DemandValidation
  include Mongoid::Document

  field :city, type:  String
  field :prix_ancien, type: Integer
  field :prix_neuf, type: String
  field :prix_m2, type: String
end
