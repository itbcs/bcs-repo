class Conversation
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  has_many :messages, dependent: :destroy
  belongs_to :sender, class_name: "Customer"
  belongs_to :receiver, class_name: "Agency"
  belongs_to :property

end
