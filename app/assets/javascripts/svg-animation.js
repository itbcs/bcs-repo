
function svgAnimation() {
  var progressBar = $(".js-progress-bar");
  var dataValue = $(".svg-container-value").data("value");
  var percentageValue = parseFloat(dataValue) / 100;
  var strokeDashOffsetValue = 100 - (percentageValue * 100);
  progressBar.css("stroke-dashoffset", strokeDashOffsetValue);
}
setInterval(svgAnimation, 2000);
