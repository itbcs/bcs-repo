class WidgetsController < ApplicationController
  protect_from_forgery except: :show
  after_action :allow_iframe, only: :iframe_version

  def show
    @property = Property.find(params[:id])
    @agency = Agency.where(email: "laure@bientotchezsoi.com").last
    @property = @agency.properties.to_a.sample
    respond_to do |format|
      format.js {render js: js_constructor}
    end
  end

  def iframe_version
    @agency = Agency.where(email: "laure@bientotchezsoi.com").last
    @property = @agency.properties.to_a.sample
    respond_to do |format|
      format.html {render :iframe_version, layout: "widget"}
    end
  end

  private

  def js_constructor
    content = render_to_string(params[:template], layout: false)
    "document.write(#{content.to_json})"
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

end
