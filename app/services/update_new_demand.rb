class UpdateNewDemand
  def initialize question, demand, demand_attribute, answers_values
    @question = question
    @demand = demand
    @demand_attribute = demand_attribute
    @answers_values = answers_values
    @answers = []
    @errors = []
  end

  def call
    values_to_demand
    handle_estimation_request
    service_success
  end

  private

  def handle_estimation_request
    if @demand_attribute.to_sym == :email
      puts "********* UPDATE DEMAND SERVICE *********"
      puts "Client insert his email for estimation request"
      puts @answers_values.inspect
      puts "*****************************************"
      CustomerMailer.estimation_request(@demand).deliver
    end
  end

  def values_to_demand
    if @answers_values.is_a?(Array)
      @demand[@demand_attribute.to_sym] = @answers_values.to_a
    else
      if @answers_values.to_s != ""
        @demand[@demand_attribute.to_sym] = @answers_values.to_s
      end
    end
    @demand.save
  end


  def service_success
    ServiceSuccess.new(@demand)
  end

  def service_error
    ServiceError.new(@errors)
  end
end
