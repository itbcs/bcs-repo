class RentAgency
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  field :name, type: String
  field :email, type: String
  field :siret, type: String
  field :phone, type: String
  field :address, type: String
  field :typeform_id, type: String
  field :active, type: Boolean, default: false

end
