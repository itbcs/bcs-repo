class JobTestMailer < BaseMandrillMailer
  include Resque::Mailer

  def welcome(customer,demand)
    subject = "Bienvenue chez vous : votre recherche est active !"
    puts "************************"
    puts customer[:first_name]
    puts demand[:localisation]
    puts demand[:property_type]
    puts demand[:budget].first
    puts "************************"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "LOCALISATION" => demand[:localisation],
      "PROPERTY_TYPE" => demand[:property_type],
      "BUDGET_MINI" => demand[:budget].first,
      "PERSONAL_ACCOUNT_LINK" => "https://bientotchezsoi.com/customers/connexion",
    }
    body = mandrill_template("[TRC] welcome_email", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer[:email], subject, body, from, replyto)
  end

  def agency_welcome(agency)
    subject = "Bienvenue chez vous : votre agence va être activée."
    merge_vars = {
      "FIRST_NAME" => agency[:first],
      "SIGN_IN_URL" => "https://bientotchezsoi.com/agencies/connexion",
      "PERSONAL_ACCOUNT_LINK" => "https://bientotchezsoi.com/agencies/connexion",
    }
    body = mandrill_template("[TRA] welcome_email", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer[:email], subject, body, from, replyto)
  end

  def customer_demand_matching(customer)
    subject = "Un nouveau bien correspond à votre recherche"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "SIGN_IN_URL" => "https://bientotchezsoi.com/customers/connexion",
      "USER_URL" => "Property matching",
      "PROPERTY_LINK" => "https://bientotchezsoi.com/dashboard/customer/properties",
    }
    body = mandrill_template("[TRC] matching", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer[:email], subject, body, from, replyto)
  end

  def finished_demand_customer_notif(customer)
    subject = "Merci d'avoir finalisé votre demande"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "SIGN_IN_URL" => "https://bientotchezsoi.com/customers/connexion",
      "USER_URL" => "Demande finalisée",
    }
    body = mandrill_template("[TRC] matching", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer[:email], subject, body, from, replyto)
  end

  def agency_demand_matching_notif(agency, demand, demand_id)
    subject = "Une nouvelle recherche de bien est en cours dans votre secteur"
    merge_vars = generate_merge_vars(demand,agency,demand_id)
    puts "Mailer worker sending START"
    puts merge_vars
    body = mandrill_template("[TRA] new_search", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
    puts "Sending succeed !!!!"
  end


  def agency_reset_password_instructions(agency)
    subject = "Réinitialiser mon mot de passe"
    merge_vars = {
      "FIRST_NAME" => agency[:name],
      "RESET_PASSWORD_URL" => edit_agency_registration_path,
      "USER_URL" => "Property matching",
    }
    body = mandrill_template("[TRA] new_search", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end

  def estimation_notification(estimation)
    if estimation[:property_type] == "Une maison"
      prop_type = "maison"
    elsif estimation[:property_type] == "Un appartement"
      prop_type = "appartement"
    end

    minvalue = "#{ActionController::Base.helpers.number_with_delimiter(estimation[:results][:min_value], locale: :fr)} €"
    maxvalue = "#{ActionController::Base.helpers.number_with_delimiter(estimation[:results][:max_value], locale: :fr)} €"

    subject = "Votre estimation en ligne"
    merge_vars = {
      "FIRST_NAME" => estimation[:firstname],
      "LAST_NAME" => estimation[:lastname],
      "PROPERTY_TYPE" => prop_type,
      "MINVAL" => minvalue,
      "MAXVAL" => maxvalue,
    }
    body = mandrill_template("[TRC] estimation", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(estimation[:email], subject, body, from, replyto)
  end


  def demand_recap(demand,customer)
    subject = "Récapitulatif de votre demande"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "LAST_NAME" => customer[:last_name],
      "LOCALISATION" => demand[:localisation],
      "POSTAL_CODES" => demand[:postal_codes],
      "PERIMETER" => demand[:perimeter],
      "ACTUAL_SITUATION" => demand[:actual_situation],
      "PROPERTY_TYPE" => demand[:property_type],
      "DESTINATION" => demand[:destination],
      "BUDGET_MINI" => demand[:budget].first,
      "STYLE" => demand[:style],
      "SURFACE" => demand[:surface],
      "IMPORTANT_FIELDS" => demand[:important_fields],
      "EQUIPEMENTS" => demand[:equipements],
      "DPE" => demand[:dpe],
      "ROOMS" => demand[:floor],
      "PRINCIPAL_SPACE" => demand[:principal_space],
      "KITCHEN" => demand[:kitchen],
      "BATHROOMS" => demand[:bathrooms],
      "ACCESSIBILITY" => demand[:accessibility],
      "EXPOSITION" => demand[:exposition],
      "SECURITY" => demand[:security],
      "PARKING" => demand[:parking],
      "HEATING" => demand[:heating],
      "HEATING_TYPE" => demand[:heating_type],
      "ACTUAL_HOUSING" => demand[:actual_housing],
      "ACTUAL_HOUSING_AREA" => demand[:actual_housing_area],
      "INSTALLATION_DATE" => demand[:installation_date],
      "LAST_PRECISION" => demand[:last_precision],
      "FLAT_STATE" => demand[:flat_state],
      "HOUSE_STATE" => demand[:house_state],
      "FLAT_STAIRS" => demand[:flat_stairs],
      "HOUSE_STAIRS" => demand[:house_stairs],
      "TRAVAUX" => demand[:travaux],
      "FLAT_EXTERIOR" => demand[:flat_exterior],
      "HOUSE_EXTERIOR" => demand[:house_exterior],
      "AGE" => demand[:age],
      "FLOOR" => demand[:floor],
    }
    body = mandrill_template("[TRI] demand_recap", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer[:email], subject, body, from, replyto)
  end

  def generate_merge_vars(demand,agency,demand_id)
    vars = {}
    if demand[:budget] != nil
      real_budget = sanitize_budget(demand[:budget])
    else
      real_budget = "Donnée non renseignée"
    end
    vars["FIRST_NAME"] = agency[:name]
    vars["LOCALISATION"] = demand[:localisation]
    vars["PROPERTY_TYPE"] = demand[:property_type]
    vars["BUDGET"] = real_budget
    vars["AGENCY_LINK"] = "https://bientotchezsoi.com/agencies/connexion"
    vars["DEMAND_ID"] = "https://bientotchezsoi.com/dashboard/agency/demand/#{demand_id}"
    return vars
  end

  def sanitize_budget(budget)
    sanitize_array = budget.reject{|budget| budget.empty?}
    if sanitize_array.size == 1
      result = "#{ActionController::Base.helpers.number_with_delimiter(sanitize_array.first.gsub(/[[:space:]]/,'').to_i, locale: :fr)} €"
    elsif sanitize_array.size == 2
      result = "Entre #{ActionController::Base.helpers.number_with_delimiter(sanitize_array.first.gsub(/[[:space:]]/,'').to_i, locale: :fr)} € et #{ActionController::Base.helpers.number_with_delimiter(sanitize_array.last.gsub(/[[:space:]]/,'').to_i, locale: :fr)} €"
    end
    return result
  end


end
