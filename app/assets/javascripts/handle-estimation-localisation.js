function handleEstimationLocalisation() {
  var localisation = $(".localisation");
  if (localisation.length > 0) {
    var city = $(localisation).data("localisation");
    var receiver = $("input[type=text]#bdv_widget_input_address_id");
    $(receiver).val(city).trigger('change');
  } else {
  }
};


$(document).ready(function(){
  handleEstimationLocalisation();
});
