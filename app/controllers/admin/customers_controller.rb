class Admin::CustomersController < ApplicationController
  http_basic_authenticate_with name: ENV["HTTP_AUTH_LOGIN"], password: ENV["HTTP_AUTH_PWD"]



  def generate_xls_file
    if params[:collection]
      if params[:collection] == "activated"
        @customers = Customer.all.select{|customer| customer.valid?}
        @filename = "active-customers"
      elsif params[:collection] == "uncompleted"
        @customers = Customer.all.reject{|customer| customer.valid?}
        @filename = "uncompleted-customers"
      end
    end
    respond_to do |format|
      format.xlsx {
        response.headers['Content-Disposition'] = "attachment; filename='#{@filename}.xlsx'"
      }
    end
  end



  def index
    @valid_customers = Customer.where(email: {'$ne': ""})
    @unvalid_customers = Customer.where(email: {'$eq': ""})
    render "admin/customers/index", layout: "admin"
  end

  def valid
    @valid_customers = Customer.where(email: {'$ne': ""}).order_by(created_at: :desc).page params[:page]
    render "admin/customers/valid", layout: "admin"
  end

  def unvalid
    @unvalid_customers = Customer.where(email: {'$eq': ""}).order_by(created_at: :desc).page params[:page]
    render "admin/customers/unvalid", layout: "admin"
  end

  def show
    @customer = Customer.find(params[:id])
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all.limit(200)
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/customers/show", layout: "admin"
  end

  def edit
    @customer = Customer.find(params[:id])
    render "admin/customers/edit", layout: "admin"
  end

  def update
    @customer = Customer.find(params[:id])
  end

  def destroy
    @customer = Customer.find(params[:id].to_s)
    @customer.destroy
    redirect_to admin_valid_customers_path, notice: "Utilisateur effacé"
  end

end
