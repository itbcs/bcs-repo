class SponsorshipsController < ApplicationController

  def create
    @sponsorship = Sponsorship.new(safe_params)
    if @sponsorship.save
      redirect_to parrainage_path, notice: 'Votre parrainage à été pris en compte'
    else
      render "pages/parrainage", alert: "Nous avons rencontrer un problème"
    end
  end

  private

  def safe_params
    params.require(:sponsorship).permit(:first_name, :last_name, :email, :phone, :property_city, :details, :property_type, :sponsored_first_name, :sponsored_last_name, :sponsored_email, :sponsored_phone)
  end

end
