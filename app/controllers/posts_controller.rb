class PostsController < ApplicationController

  def show
    @post = Post.find_by(lname: params[:lname])
    @blog_categories = BlogCategory.all
    @last_five_posts = Post.all.limit(5)
    @tags = (Post.all.map {|po| po.tags.map{|t| t}}).flatten.uniq
  end

  def tags
    @blog_categories = BlogCategory.all
    @last_five_posts = Post.all.limit(5)
    @tags = (Post.all.map {|po| po.tags.map{|t| t}}).flatten.uniq
    @posts = (Post.all.map {|po| po if (po.tags.map {|t| t.downcase.parameterize == (params[:tag_lname]).parameterize })}).flatten.compact

    @filtered_posts = Post.all.select{|post| post.tags.map{|tag| tag.parameterize }.include?(params[:tag_lname])}

    @tag = params[:tag_lname]
    render :tags
  end

  def author
    @blog_categories = BlogCategory.all
    @last_five_posts = Post.all.limit(5)
    @tags = (Post.all.map {|po| po.tags.map{|t| t}}).flatten.uniq
    @posts = Post.where(author: params[:author_name].downcase)
    @posts = Post.all.select{|po| (po.author.downcase.parameterize == params[:author_name]) if po.author}
    @author = params[:author_name]
  end

end
