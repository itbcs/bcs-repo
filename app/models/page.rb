# frozen_string_literal: true

# class Page
class Page
  include Mongoid::Document
  include Mongoid::Timestamps

  field :meta_title, type: String
  field :meta_desc, type: String
  field :content, type: String
  field :status, type: String
  field :name, type: String
  field :lname, type: String
  field :url, type: String
  field :photo_url, type: String

end
