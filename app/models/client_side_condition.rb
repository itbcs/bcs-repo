class ClientSideCondition
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  embedded_in :question

  field :demand_field, type: String
  field :operator, type: String
  field :upper_range_limit_percent, type: Integer
  field :lower_range_limit_percent, type: Integer
  field :lower_fixed_limit, type: Integer
  field :upper_fixed_limit, type: Integer
  field :min, type: Integer
  field :max, type: Integer
  field :step, type: Integer

end
