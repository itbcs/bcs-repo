function apifluxesAgenciesIndexSearch(){
  $("form.apifluxes-agencies--search").submit(function(event){
    event.preventDefault();
    console.log(event);
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    $.ajax({
      url: url,
      processData: false,
      contentType: false,
      type: method,
      data: new FormData(this),
      beforeSend: function() {
        $("section.apifluxes-ajax-loading").removeClass("hidden");
        console.log("Before send action");
      },
      success: function(data) {
        console.log(data);
        $("section.apifluxes-ajax-loading").addClass("hidden");
        var rightWrapper = $(data).find("section.right-wrapper");
        $("section.right-wrapper").replaceWith(rightWrapper);
      },
      error: function(data) {
        alert("Ajax error call !!!");
      },
      complete: function(data) {
        console.log("Inside complete ajax callback");
        apifluxesAjaxAgencyPropsShow();
      }
    });
  });
}


function apifluxesAjaxAgencyPropsShow() {
  var showAjaxLinks = document.querySelectorAll(".custom-ajax-link");
  showAjaxLinks.forEach(function(item) {
    var url = $(item).data('url');
    var method = $(item).data('method');
    var text = $(item).data("title");
    item.addEventListener("click",function(e) {
      $.ajax({
        url: url,
        processData: false,
        contentType: false,
        type: method,
        beforeSend: function() {
          console.log(text);
          $("section.apifluxes-ajax-loading-pagination").removeClass("hidden");
          $(".pagination-loading-text").text("Loading " + text);
          console.log("Before send action");
        },
        success: function(data) {
          $("section.apifluxes-ajax-loading-pagination").addClass("hidden");
          $("section.apifluxes-properties--index").append(data);
        },
        error: function(data) {
          alert("Ajax error call !!!");
        },
        complete: function(data) {
          console.log("Inside complete ajax callback");
          handleCustomActionAjaxLink();
          handleShowSectionNestedCloser();
          apifluxesAjaxAgencyNestedPropsShow();
        }
      });
    })
  });
}



function apifluxesAjaxAgencyNestedPropsShow() {
  var nestedShowAjaxLinks = document.querySelectorAll(".nested-custom-ajax-link");
  nestedShowAjaxLinks.forEach(function(item) {
    var url = $(item).data('url');
    var method = $(item).data('method');
    var text = $(item).data("title");
    item.addEventListener("click",function(e) {
      $.ajax({
        url: url,
        processData: false,
        contentType: false,
        type: method,
        beforeSend: function() {
          $("section.apifluxes-ajax-loading-pagination").removeClass("hidden");
          $(".pagination-loading-text").text("Loading page " + text);
          console.log("Before send action");
        },
        success: function(data) {
          console.log(data);
          $("section.apifluxes-ajax-loading-pagination").addClass("hidden");
          $("section.apifluxes-properties--index").append(data);
        },
        error: function(data) {
          alert("Ajax error call !!!");
        },
        complete: function(data) {
          console.log("Inside complete ajax callback");
          handleShowSectionNestedCloser();
          handleAgencyPropShowClosedButton();
        }
      });
    })
  });
}

function handleCustomActionAjaxLink() {
  var customActionLink = document.querySelector(".custom-action-ajax-link");
  var url = customActionLink.getAttribute("href");
  var method = customActionLink.dataset.method;
  var modal = document.querySelector(".modern-modal-wrapper");
  var modalBody = document.querySelector(".modern-modal-wrapper .modern-modal .modern-modal-body");
  customActionLink.addEventListener("click", function(event) {
    var linkButton = this;
    event.preventDefault();
    $.ajax({
      url: url,
      processData: false,
      contentType: false,
      type: method,
      beforeSend: function() {
        console.log(linkButton);
      },
      success: function(data) {
      },
      error: function(data) {
        alert("Ajax error call !!!");
      },
      complete: function(data) {
        console.log(data);
        console.log(data.responseText);
        while (modalBody.firstChild) modalBody.removeChild(modalBody.firstChild);
        modal.classList.remove("hidden");
        modalBody.insertAdjacentHTML('beforeend', data.responseText);
        // clickEffect();
        handleModernModalCloser();
      }
    });
  });
}

function handleModernModalCloser() {
  var modal = document.querySelector(".modern-modal-wrapper");
  var modalCloser = document.querySelector(".modern-modal-closer");
  modalCloser.addEventListener("click", function(event) {
    // sleep(260).then(() => {
    //   modal.classList.add("hidden");
    // });
    modal.classList.add("hidden");
  });
}

function handleAgencyPropShowClosedButton() {
  var customSectionCloser = document.querySelector(".nested-show-custom-section-closer");
  customSectionCloser.addEventListener("click", function(e) {
    document.querySelector("section.show-wrapper.from-agency").remove();
  });
}

function handleShowSectionNestedCloser() {
  var customSectionCloser = document.querySelector(".custom-section-closer");
  customSectionCloser.addEventListener("click", function(e) {
    document.querySelector("section.show-wrapper").remove();
  });
}

function clickEffect() {
  var links = document.querySelectorAll('.ripplelink');
  for (var i = 0, len = links.length; i < len; i++) {
    links[i].addEventListener('click', function(e) {
      var targetEl = e.target;
      var inkEl = targetEl.querySelector('.ink');
      if (inkEl) {
        inkEl.classList.remove('animate');
      }
      else {
        inkEl = document.createElement('span');
        inkEl.classList.add('ink');
        inkEl.style.width = inkEl.style.height = Math.max(targetEl.offsetWidth, targetEl.offsetHeight) + 'px';
        targetEl.appendChild(inkEl);
      }
      inkEl.style.left = (e.offsetX - inkEl.offsetWidth / 2) + 'px';
      inkEl.style.top = (e.offsetY - inkEl.offsetHeight / 2) + 'px';
      inkEl.classList.add('animate');
    }, false);
  }
}

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

apifluxesAgenciesIndexSearch();
