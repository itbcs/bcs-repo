# frozen_string_literal: true

# class Condition
class Condition
  include Mongoid::Document
  embedded_in :survey
  recursively_embeds_one

  field :demand_field, type: String
  field :operator, type: String
  field :value, type: String
  field :children_operator, type: String
  field :logic_condition, type: String
  field :ruler_survey, type: Integer
end
