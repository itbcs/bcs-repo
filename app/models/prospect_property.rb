# frozen_string_literal: true

# class ProspectProperty
class ProspectProperty
  include Mongoid::Document
  include Mongoid::Timestamps
  #belongs_to :customer

  field :like, type: String
  field :comment, type: Integer
  field :score, type: Integer
  field :name, type:  String
  field :localisation, type: String
  field :area, type: Integer
  field :price, type: Integer
  field :desc, type: String
  field :status, type:  String
  field :ground, type:  Boolean
  field :constructor_contact, type: Boolean
  field :ground_area, type: Integer
  field :housing_area, type: Integer
  field :property_type, type: String
  field :nb_rooms, type: Integer
  field :options, type: Array
  field :since_when, type: Date
  field :firstname, type: String
  field :lastname, type: String
  field :email, type: String
  field :phone, type: String

end


class Array

  def generate_radio_button_icon
    if self == ["Une maison"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/flat-house.svg", width: "30", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    elsif self == ["Un appartement"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/flat-appartment.svg", width: "30", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    elsif self == ["Un terrain"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/flat-grass.svg", width: "30", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    end
    result
  end

  def generate_estimation_button_icon
    if self == ["Une maison"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/estimation-house.svg", height: "25", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    elsif self == ["Un appartement"]
      image = ActionController::Base.helpers.image_tag "https://cdn.bientotchezsoi-dev.com/images/estimation-flat.svg", height: "22", class: "radio-icon"
      result = ("<div class='custom-radio-button-icon'>#{image}#{self.last}<i class='fa fa-check custom-fa'></i></div>").html_safe
    end
    result
  end

end

