require "mandrill"
require "open-uri"
require "base64"

class BaseMandrillMailer < ActionMailer::Base

  private

  def send_mail(email, subject, body, from, replyto)
    mail(from: from, reply_to: replyto, to: email, subject: subject, body: body, content_type: "text/html")
  end

  def send_mail_with_attachments(email, subject, body, from, replyto, file, filename)
    puts "--- FILE PATH ---"
    puts file
    puts "-----------------"
    @tempo_file = open("#{file}").read
    attachments["#{filename}"] = {
      mime_type: 'application/pdf',
      content: @tempo_file,
    }
    @mail = mail(from: from, reply_to: replyto, to: email, subject: subject, body: "")

    html_part = Mail::Part.new do
      content_type 'text/html; charset=UTF-8'
      body "#{body}"
    end
    @mail.html_part = html_part
    puts "Email sending successfull !!!"
    File.delete(file) if File.exist?(file)
  end


  def mandrill_template(template_name, attributes)
    mandrill = Mandrill::API.new(ENV["SMTP_MANDRILL_PASSWORD"])
    merge_vars = attributes.map do |key, value|
      { name: key, content: value }
    end
    mandrill.templates.render(template_name, [], merge_vars)["html"]
  end

end
