function handleRedirectFromCriterias() {
  $("form.criterias-form-v2").submit(function(event) {
    event.preventDefault();
    var form = $(this);
    var url = $(form).attr('action');
    var method = $(form).attr('method');
    $.ajax({
      url: url,
      type: method,
      data: $(form).serialize(),
      success: function(data) {
        console.log(data.criterias);
        $("#survey-signin-modal").removeClass("hidden");
      },
      error: function(data) {
        alert("Fatal Error");
      }
    });
  });
}






function handleEmailValidation() {
  var regex = /^(\w|\d|\S)+@+(\w|\d|\S)+[.]+(\b(fr)|\b(com)|\b(org)|\b(net)|\b(io)|\b(tv)|\b(pro)|\b(biz)|\b(eu)|\b(info))$/i;
  $('input[name="email"]').keyup(function(){
    var parent = $(this).parent();
    if ($(this).val().match(regex)) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
      $(this).removeClass("bordered-error-input");
      $(".custom-error-message").remove();
    } else {
      var icon = "<i class='fa fa-envelope'></i>"
      var errorSentence = $("<div class='custom-error-message'>" + icon + "Email incorrect</div>");
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
      $(this).addClass("bordered-error-input");
      if ($(".custom-error-message").length > 0) {
      } else {
        $(".question-form").append(errorSentence);
      }
    }
  })
}



function handleRequiredQuestion() {
  var count = 0;
  var checkboxesSize = $("input[type=checkbox]");
  $("input[type=radio]").change(function(){
    if ($(this).prop("checked")) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
    } else {
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
    }
  });
  $("input[type=checkbox]").change(function(){
    if ($(this).prop("checked")) {
      count += 1
      if (count > 0) {
        $(".form-submit-button").attr("disabled", false);
        $(".form-submit-button").removeClass("disabled-button");
      }
    } else {
      count -= 1
      if (count === 0) {
        $(".form-submit-button").attr("disabled", true);
        $(".form-submit-button").addClass("disabled-button");
      }
    }
  });
  $("input[type=text]").keyup(function(){
    if ($(this).val().length !== 0) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
    } else {
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
    }
  });
  $("textarea").keyup(function(){
    if ($(this).val().length !== 0) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
    } else {
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
    }
  });
}


function currencyNumericInput() {
  $(".currency-input").keyup(function() {
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
      return;
    }
    var input = $(this).val();
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt( input, 10 ) : 0;
    $(this).val( function() {
      return ( input === 0 ) ? "" : input.toLocaleString( "fr-FR" );
    });
    // if (input < 50000) {
    //   console.log("Under 50 000");
    //   $(".form-submit-button").attr("disabled", true);
    //   $(".form-submit-button").addClass("disabled-button");
    // } else {
    //   console.log("Above 50 000");
    //   $(".form-submit-button").attr("disabled", false);
    //   $(".form-submit-button").removeClass("disabled-button");
    // }
  });
}


function currencyNumericInputWithCondition() {
  var inputValue;
  var lastInputValue;
  var currencyLastinputValue;
  var sentence;
  var startSentence;
  var propStatePercent;
  var fees;
  var delta;
  var maxValue;
  var hintTextToChange;
  var currencyMaxValue;
  var absoluteConditionContainer;
  var firstInput = $('.currency-input.condition-input-target[data-position="1"]');
  var lastInput = $('.currency-input.condition-input-target[data-position="2"]');
  var stateHiddenInput = $('input[name="prop_state"]');

  if ($(lastInput)) {
    if ($(lastInput).val()) {
      var presenceOfSecondInputValue = parseInt($(lastInput).val().replace(/\s/g,''));
    }
  }

  if ($(stateHiddenInput)) {
    if ($(stateHiddenInput).val() === "old") {
      propStatePercent = 0.08;
    } else if ($(stateHiddenInput).val() === "new") {
      propStatePercent = 0.03;
    } else if ($(stateHiddenInput).val() === "genuine") {
      propStatePercent = 0;
    } else {
      propStatePercent = 0;
    }
  } else {
    propStatePercent = 0;
  }

  $(firstInput).keyup(function() {
    inputValue = $(this).val().replace(/\s/g,'');
    startSentence = "Budget minimum de 50 000 €";
    sentence = "Budget maximum de ";
    delta = parseInt(inputValue) * 0.30;
    maxValue = Math.round(parseInt(inputValue) + delta);
    currencyMaxValue = maxValue.toLocaleString( "fr-FR" );
    currencyMaxValue = isNaN(maxValue) ? 0 : currencyMaxValue;
    absoluteConditionContainer = $(".absolute-max-budget-hint");

    if (parseInt(inputValue) > 50000 && lastInput.val() === "") {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
      hintTextToChange = $(".input-hint-v2 .input-hint-text");
      $(hintTextToChange).text(sentence + currencyMaxValue + " €");

      console.log("Fees: " + (maxValue * propStatePercent));
      var fullBudget = maxValue + (maxValue * propStatePercent);
      fullBudget = fullBudget.toLocaleString( "fr-FR" );
      fullBudget = "Pour un budget total (Frais de notaire inclus) estimé à " + fullBudget + "€";
      var feesHint = $("<div class='fees-hint'>" + fullBudget + "</div>");
      var feesHintTag = $(".fees-hint");
      if ($(feesHintTag).length > 0) {
        $(feesHintTag).text(fullBudget);
      } else {
        $("form.question-form").append($(feesHint));
      }

    } else if (parseInt(inputValue) > 50000 && presenceOfSecondInputValue <= maxValue) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
    } else {
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
      $(hintTextToChange).text(startSentence);
      $(".fees-hint").remove();
    }

  });
  $(lastInput).keyup(function() {
    lastInputValue = $(this).val().replace(/\s/g,'');
    currencyLastinputValue = parseInt(lastInputValue);
    currencyLastinputValue = currencyLastinputValue === undefined ? 0 : currencyLastinputValue
    if (lastInputValue === "") {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
    } else if (parseInt(inputValue) > 50000 && parseInt(currencyLastinputValue) <= maxValue && parseInt(currencyLastinputValue) > parseInt(inputValue)) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
      var fullBudget = maxValue + (maxValue * propStatePercent);
      fullBudget = fullBudget.toLocaleString( "fr-FR" );
      var devisingFirstInputValue = inputValue.toLocaleString( "fr-FR" );
      fullBudget = "Pour un budget total (Frais de notaire inclus) estimé entre " + devisingFirstInputValue + " et " + fullBudget + "€";
      var feesHint = $("<div class='fees-hint'>" + fullBudget + "</div>");
      var feesHintTag = $(".fees-hint");
      if ($(feesHintTag).length > 0) {
        $(feesHintTag).text(fullBudget);
      } else {
        $("form.question-form").append($(feesHint));
      }
    } else {
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
      $(".fees-hint").remove();
    }

  });
}


function handleMaxSurfaceLimitation() {
  console.log("Enter into surface limitation javascript");
  var minInput;
  var maxInput;
  var minInputValue;
  var maxInputValue;
  var sentence;
  var delta;
  var hintTextToChange;
  var inputs = $('input[name="surface[]"]');
  var hintTextToChange = $(".input-hint-v2 .input-hint-text");
  minInput = $(inputs).first();
  maxInput = $(inputs).last();

  $(minInput).keyup(function() {
    minInputValue = $(this).val().replace(/\s/g,'');
    minInputValueInteger = parseInt(minInputValue);
    delta = minInputValueInteger * 0.30;
    maxSurfaceValue = Math.round(minInputValueInteger + delta);
    surfaceMaxValue = maxSurfaceValue.toLocaleString( "fr-FR" );
    surfaceMaxValue = isNaN(maxSurfaceValue) ? 0 : surfaceMaxValue;
    sentence = "Surface maximum de ";
    if (parseInt(minInputValue) >= 10 && maxInput.val() === "") {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
      $(hintTextToChange).text(sentence + surfaceMaxValue + " m²");
    } else {
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
    }
  });

  $(maxInput).keyup(function() {
    maxInputValue = $(this).val().replace(/\s/g,'');
    var maxInputValueInteger = parseInt(maxInputValue);
    maxInputValueInteger = maxInputValueInteger === undefined ? 0 : maxInputValueInteger
    if (maxInputValue === "") {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
      if ($(".oversize-surface-hint").length !== 0) {
        $(".oversize-surface-hint").remove();
        $(".full-sentence .inline-svg").removeClass("pulse animated infinite");
      }
    } else if (maxInputValueInteger > minInputValueInteger && maxInputValueInteger <= maxSurfaceValue) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
    } else if (maxInputValueInteger > maxSurfaceValue) {
      $(".form-submit-button").attr("disabled", false);
      $(".form-submit-button").removeClass("disabled-button");
      var oversizeSurfaceHint = $("<div class='oversize-surface-hint'>Ouh la la ! C'est le grand écart ! Conseil de castor: réduisez l'ecart entre la surface minimale et la surface idéale pour recevoir dedss propositions plus ciblées.</div>");
      if ($(".oversize-surface-hint").length === 0) {
        $(".sentence-container").append(oversizeSurfaceHint);
      }
      $(".full-sentence .inline-svg").addClass("pulse animated infinite");
    } else {
      $(".form-submit-button").attr("disabled", true);
      $(".form-submit-button").addClass("disabled-button");
      if ($(".oversize-surface-hint").length !== 0) {
        $(".oversize-surface-hint").remove();
        $(".full-sentence .inline-svg").removeClass("pulse animated infinite");
      }
    }
  });
}


function surfaceNumericInput() {
  $(".surface-input").keyup(function() {
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
      return;
    }
    var input = $(this).val();
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt( input, 10 ) : 0;
    $(this).val( function() {
      return ( input === 0 ) ? "" : input.toLocaleString( "fr-FR" );
    });
  });
}


function handleClientSideCondition(data) {
  var refValue;
  var upperLimit;
  var lowerLimit;
  var lowerFixedLimit;
  var upperFixedLimit;
  var cscContainer = $(".client-side-condition-container");
  var operator = $(cscContainer).data("operator");
  if (operator === "range-limit") {
    var rangeLimitConditions = $(".range-limit-condition");
    refValue = $(rangeLimitConditions).data("reference");
    lowerLimit = $(rangeLimitConditions).data("lower-limit");
    upperLimit = $(rangeLimitConditions).data("upper-limit");
  } else if (operator === "strict-limit") {
    lowerLimitInfos = $(".strict-lower-limit-condition");
    upperLimitInfos = $(".strict-upper-limit-condition");
    if ($(lowerLimitInfos).length > 0) {
      refValue = $(lowerLimitInfos).data("reference");
      lowerLimit = $(lowerLimitInfos).data("lower-limit");
    } else if ($(upperLimitInfos).length > 0) {
      refValue = $(upperLimitInfos).data("reference");
      upperLimit = $(upperLimitInfos).data("upper-limit");
    }
  }
  var delta = parseInt(refValue) * (upperLimit / 100);
  var inputLowerLimit = parseInt(refValue) - delta;
  var inputUpperLimit = parseInt(refValue) + delta;
}


function handleIncrementalInput() {
  var $incrementInputs = $('.incremental-numeric-input');
  $incrementInputs.each(function() {
    var $this = $(this);
    var $input = $this.find("input");
    var $buttons = $this.find(".increment-button");
    $buttons.on("touch click", function(e) {
      var direction = $(this).data('direction');
      var interval = $input.attr("step");
      var currentVal = $input.val();
      var upperLimit = $input.attr("max");
      var lowerLimit = $input.attr("min");
      var newVal = parseInt(lowerLimit);
      if (isNaN(currentVal)) {
        $input.val(lowerLimit.toString());
      } else {
        if (direction === 'dec') {
          newVal = parseInt(currentVal) - parseInt(interval);
        } else if (direction === 'inc') {
          newVal = parseInt(currentVal) + parseInt(interval);
        }
        newVal = (newVal > lowerLimit) ? newVal : lowerLimit;
        newVal = (newVal < upperLimit) ? newVal : upperLimit;
        $input.val(newVal);
      }
    });
  });
}


function handleRadiosAnswerKeycode() {
  $(window).on('keyup', function(e) {
    var keycode = e.keyCode;
    $(".v2-radio").each(function() {
      var answerKeycode = $(this).data("keycode");
      if (keycode == answerKeycode) {
        $(this).find("input").prop("checked", true);
      } else {
      }
    });
    $(".radio-with-image").each(function() {
      var answerKeycode = $(this).data("keycode");
      if (keycode == answerKeycode) {
        $(this).find("input").prop("checked", true);
      } else {
        console.log("Unmatched input");
      }
    });
  });
}



function handleCheckboxesAnswerKeycode() {
  $(window).on('keyup', function(e) {
    var keycode = e.keyCode;
    $(".v2-checkbox").each(function() {
      var answerKeycode = $(this).data("keycode");
      if (keycode == answerKeycode) {
        var currentInput = $(this).find("input");
        if ($(currentInput).is(":checked")) {
          console.log("Checked input");
          $(this).find("input").prop("checked", false);
        } else {
          console.log("Unchecked input");
          $(this).find("input").prop("checked", true);
        }
      } else {
        console.log("Unmatched input");
      }
    });
    $(".checkbox-with-image").each(function() {
      var answerKeycode = $(this).data("keycode");
      if (keycode == answerKeycode) {
        console.log("Matching keycode");
        var currentInput = $(this).find("input");
        if ($(currentInput).is(":checked")) {
          console.log("Checked input");
          $(this).find("input").prop("checked", false);
        } else {
          console.log("Unchecked input");
          $(this).find("input").prop("checked", true);
        }
      } else {
        console.log("Unmatched input");
      }
    });
  });
}


function handleProgressionBar(html){
  var v2StepperContainer = $('.v2-stepper-container');
  $(v2StepperContainer).replaceWith(html);
}

function handleHintStepper(position, questions) {
  var v2FirstStep = $(".item-step-25");
  var v2SecondStep = $(".item-step-50");
  var v2ThirdStep = $(".item-step-75");
  var v2FirstHint = $(".item-step-25-hint");
  var v2SecondHint = $(".item-step-50-hint");
  var v2ThirdHint = $(".item-step-75-hint");
  var result = Math.round(((position / questions) * 100));

  console.log(result);

  if (result > 25 && result <= 27){
    v2FirstStep.removeClass('hidden');
    v2FirstHint.removeClass('hidden');
    setTimeout(function(){
      v2FirstHint.addClass('hidden');
    }, 4000);
  }
  else if (result > 50 && result <= 52){
    v2FirstStep.removeClass('hidden').addClass('opacity-background-5');
    v2FirstHint.addClass('hidden');
    v2SecondHint.removeClass('hidden');
    v2SecondStep.removeClass('hidden');
    setTimeout(function(){
      v2SecondHint.addClass('hidden');
    }, 4000);
  }
  else if (result > 75 && result <= 77) {
    v2FirstStep.removeClass('hidden').addClass('opacity-background-5');
    v2FirstHint.addClass('hidden');
    v2ThirdHint.removeClass("hidden");
    v2SecondStep.removeClass('hidden').addClass('opacity-background-5');
    v2SecondHint.addClass('hidden');
    v2ThirdStep.removeClass('hidden');
    setTimeout(function(){
      v2ThirdHint.addClass('hidden');
    }, 4000);
  }
}

function handleCustomerResponse(label,response) {
  console.log("***** Customer response function ****");
  console.log(response);
  var answer = $("<div class='response-customer-item'><div class='response-label'>" + label + "</div>" + "<div class='response-content'>" + response + "</div>" + "</div>");
  $(".customer-answers-container").append(answer);
}

function handleMultipleCustomerResponse(label,responses) {
  console.log("***** Customer multiple response function ****");
  var answer = $("<div class='response-customer-item'><div class='response-label'>" + label + "</div>" + "</div>");
  $(responses).each(function() {
    var responseToAdd = $("<div class='response-content'>" + this + "</div>");
    $(answer).append(responseToAdd);
  });
  $(".customer-answers-container").append(answer);
}

function handleCustomerSkippingResponse(label) {
  var response = "Vous avez passé cette question";
  var answer = $("<div class='response-customer-item'><div class='response-label'>" + label + "</div>" + "<div class='response-content'>" + response + "</div>" + "</div>");
  $(".customer-answers-container").append(answer);
}


function handleAjaxFormSkipRequest() {
  $(".question-skip-btn").click(function(event) {
    event.preventDefault();
    var url = $(this).data("url");
    var position = $(this).data("position");
    var demand = $(this).data("demand");
    var form = $(this).data("form");
    var label = $(this).data("label");
    var customParams = {position: position, demand_id: demand};

    var submitButton = $(form).find(".form-buttons-block");
    $(submitButton).replaceWith("<div class='d-flex-center form-buttons-block'><i class='fa fa-spinner fa-spin'></i></div>");

    $.ajax({
      url: url,
      type: "POST",
      data: customParams,
      success: function(data) {
        console.log("Success");
        $("#fullform-container-" + position).fadeOut("slow").remove();
        $("#full-sentence-" + position).fadeOut("slow").remove();
        // handleCustomerSkippingResponse(label);
        $(".question-container").append(data).hide().fadeIn("slow");
        handleEmailValidation();
        var clientSideConditonContainer = $(".client-side-condition-container");
        var currentStepper = $(".v2-stepper-container");
        if ($(clientSideConditonContainer).length > 0) {
          currencyNumericInputWithCondition();
        } else {
          handleRequiredQuestion();
        }
        var currentStepper = $(data).find(".v2-stepper-container");
        handleIncrementalInput();
        currencyNumericInput();
        surfaceNumericInput();
        handleProgressionBar(currentStepper);
        SmsValidationForm();
        handleEmptyPhoneInput();
        handleContinueSurvey();
      },
      error: function(data) {
        alert("Fatal Error");
      }
    });
  });
}

function handleAjaxFormRequest() {
  var customerResponse;
  var multipleResponses = [];
  $(".question-form").submit(function(event) {
    event.preventDefault();
    var form = $(this);

    var submitButton = $(form).find(".form-buttons-block");
    $(submitButton).replaceWith("<div class='d-flex-center form-buttons-block'><i class='fa fa-refresh fa-spin'></i></div>");

    var url = $(form).attr('action');
    var method = $(form).attr('method');
    var label = $(form).find('input[name="label"]').val();
    var position = $(form).find('input[name="position"]').val();
    var demandField = $(form).find('input[name="demand_field"]').val();
    var questionType = $(form).find('input[name="question_type"]').val();
    var fieldType = $(form).find('input[name="field_type"]').val();
    var questions = $(form).find('input[name="questions"]').val();


    if (questionType === "checkbox") {
      var checkedValues = $(form).find('input[name="' + demandField + '[]"]:checked');
      $(checkedValues).each(function() {
        var response = $(this).data("text");
        multipleResponses.push(response);
      });
    } else if (questionType === "checkbox_with_image") {
      var checkedValues = $(form).find('input[name="' + demandField + '[]"]:checked');
      $(checkedValues).each(function() {
        var response = $(this).data("text");
        multipleResponses.push(response);
      });
    } else if (questionType === "radio_with_image") {
      customerResponse = $(form).find('input[name="' + demandField + '"]:checked').data("text");
    } else if (questionType === "radio") {
      customerResponse = $(form).find('input[name="' + demandField + '"]:checked').data("text");
    } else if (questionType === "textarea") {
      customerResponse = $(form).find('textarea[name="' + demandField + '"]').val();
    } else if (questionType === "incremental_numeric") {
      customerResponse = $(form).find('input[name="' + demandField + '"]').val();
    } else if (questionType === "text") {
      if (fieldType !== "array") {
        customerResponse = $(form).find('input[name="' + demandField + '"]').val();
      } else {
        var textValues = $(form).find('input[name="' + demandField + '[]"]');
        $(textValues).each(function() {
          var response = $(this).val();
          if (response !== "") {
            multipleResponses.push(response);
          }
        });
      }
    }
    $.ajax({
      url: url,
      type: method,
      data: $(form).serialize(),
      success: function(data) {
        $(".question-container").append(data).hide().fadeIn("slow");
        var currentStepper = $(data).find(".v2-stepper-container");
        handleIncrementalInput();
        currencyNumericInput();
        surfaceNumericInput();
        handleProgressionBar(currentStepper);
        handleHintStepper(position, questions);
        handleEmailValidation();
        var clientSideConditonContainer = $(".client-side-condition-container");

        if ($(clientSideConditonContainer).length > 0) {
          currencyNumericInputWithCondition();
          handleMaxSurfaceLimitation();
          handleEmailValidation();
        } else {
          handleRequiredQuestion();
        }

        var exit = $(data).find('input[name="intermediate_exit"]');

        if (exit.length !== 0) {
          var modal = $("#survey-signin-modal");
          $(modal).removeClass('hidden');
          handleDeviseAjaxForm();
        }


        var geoLoc = $(data).find('input[name="geoloc"]');
        if ($(geoLoc).length > 0) {
          console.log("Geoloc field present");
          initializeAutocomplete('6-1-google-place');
        }

        SmsValidationForm();
        handleEmptyPhoneInput();
        handleContinueSurvey();

        // if (questionType === "checkbox") {
        //   handleMultipleCustomerResponse(label,multipleResponses);
        // } else if (questionType === "radio") {
        //   handleCustomerResponse(label,customerResponse);
        // } else if (questionType === "radio_with_image") {
        //   handleCustomerResponse(label,customerResponse);
        // } else if (questionType === "checkbox_with_image") {
        //   handleMultipleCustomerResponse(label,multipleResponses);
        // } else if (questionType === "textarea") {
        //   handleCustomerResponse(label,customerResponse);
        // } else if (questionType === "incremental_numeric") {
        //   handleCustomerResponse(label,customerResponse);
        // } else if (questionType === "text") {
        //   if (fieldType !== "array") {
        //     handleCustomerResponse(label,customerResponse);
        //   } else {
        //     handleCustomerResponse(label,multipleResponses);
        //   }
        // }
        $("#fullform-container-" + position).fadeOut("slow").remove();
        $("#full-sentence-" + position).fadeOut("slow").remove();
        absoluteConditionContainer = $(".absolute-max-budget-hint");
        if ($(absoluteConditionContainer).length > 0) {
          $(".absolute-max-budget-hint").fadeOut("slow").remove();
        }
      },
      error: function(data) {
        alert("Fatal Error");
      }
    });
  });
}

handleAjaxFormRequest();
handleAjaxFormSkipRequest();
handleRequiredQuestion();

