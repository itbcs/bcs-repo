class OldPlatformUser
  include Mongoid::Document
  include Mongoid::Timestamps

  field :old_platform_id, type: Integer
  field :lastname, type: String
  field :firstname, type: String
  field :email, type: String
  field :address, type: String
  field :city, type: String
  field :country, type: String
  field :phone, type: String

end
