class EmployeeRole
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String
  field :content, type: String

  def self.list
    results = EmployeeRole.all.map{|er| er.title.to_s.downcase}
    return results
  end

  def self.richer_list
    results = [{role: "Superadmin", content: "Le superadmin peux modifier toutes les collections."},
               {role: "Admin", content: "L'Admin peux modifier un nombre restreint de collections."},
               {role: "Agent", content: "L'Agent a accès aux collections properties et agencies"},
               {role: "Editor", content: "L'Editor peux seulement créer des articles de blog"}
              ]
  end

end
