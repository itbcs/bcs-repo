function displayMoreDetails() {
  $('.customer-infos-container-top').find('.fa.fa-plus-circle').click(function(){
    var bottomElement = $('.customer-infos-container-bottom'),
        topElement = $('.customer-infos-container-top'),
        plusCircle = $(this),
        moinsCircle = $('.customer-infos-container-top').find('.fa.fa-minus-circle');

    if ($(bottomElement).css('height') == "0px") {
      $(bottomElement).css('height', '160px');
      $(topElement).css('margin-bottom', '20px');
      $(plusCircle).fadeOut(300);
      $(moinsCircle).delay(600).fadeIn(300);
    }
  });
};

displayMoreDetails();

function hideMoreDetails() {
  $('.customer-infos-container-top').find('.fa.fa-minus-circle').click(function(){
    var bottomElement = $('.customer-infos-container-bottom'),
        topElement = $('.customer-infos-container-top'),
        moinsCircle = $(this),
        plusCircle = $('.customer-infos-container-top').find('.fa.fa-plus-circle');

    if ($(bottomElement).css('height') == "160px") {
      $(bottomElement).css('height', '0px');
      $(topElement).css('margin-bottom', '0px');
      $(moinsCircle).fadeOut(300);
      $(plusCircle).delay(600).fadeIn(300);
    }
  });
};

hideMoreDetails();
