class V2::V2ConditionsController < ApplicationController

  def new
    @question = Question.find(params[:question_id])
    if @question.v2_condition
      @present_condition = @question.v2_condition.id.to_s
    end
    @values = []
    Question.all.each do |question|
      question.v2_answers.each do |answer|
        @values << [answer.text, answer.auto_identifier]
      end
    end
    @condition = V2Condition.new()
  end

  def create
    if params["v2_condition"]["parent_condition"]
      @question = Question.find(params["v2_condition"]["question"])
      @condition = @question.v2_condition.child_v2_condition = V2Condition.new(safe_params)
    else
      @question = Question.find(params["v2_condition"]["question"])
      @condition = @question.v2_condition = V2Condition.new(safe_params)
    end
    if @condition.save
      redirect_to v2_demand_view_path, notice: "Condition créee avec succès !!"
    else
      render :new, alert: "Une erreur est survenue !!"
    end
  end

  def edit
    @question = Question.find(params[:question_id])
    @condition = @question.v2_condition
    render :edit
  end

  def update
    @question = Question.find(params["v2_condition"]["parent_question_id"])
    @condition = @question.v2_condition
    if @condition.update_attributes(safe_params)
      puts "Condition successfully update"
      redirect_to v2_demand_view_path, notice: "Condition modifiée avec succès !!"
    else
      render :update, alert: "Une erreur est survenue !!"
    end
  end

  def destroy
    @question = Question.find(params[:question])
    if params[:level].to_i == 1
      @condition = @question.v2_condition
    elsif params[:level].to_i == 2
      @condition = @question.v2_condition.child_v2_condition
    end
    if @condition.destroy
      puts "Condition successfully delete"
      redirect_to v2_demand_view_path, notice: "Condition supprimée avec succès !!"
    else
      redirect_to v2_demand_view_path, alert: "Une erreur est survenue !!"
    end
  end

  private

  def safe_params
    params.require(:v2_condition).permit(:demand_field, :demand_field_identifier, :operator, :value, :children_operator, :logic_condition, :journey, :hard_stock, :parent_question_id)
  end

end
