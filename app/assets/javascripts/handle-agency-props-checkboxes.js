function appendCheckedProp(fixedContainer,itemInfos,uniqueId) {
  var itemTitle = $(itemInfos).find(".property-input-title").first().text();
  var itemAddress = $(itemInfos).find(".checkbox-demand-full-address").text();
  var itemPrice = $(itemInfos).find(".property-label-price").text();
  var itemType = $(itemInfos).find(".property-input-title-small").text();
  var itemPhoto = $(itemInfos).parent().find(".block-image").attr('style');
  var itemCloseButton = "<div class='closed-temporary-item-button'>" + "<i class='fa fa-times-circle'></div>" + "</div>";
  var htmlTitle = "<div class='tempo-item title'>" + itemTitle.substring(0,24) + "</div>";
  var htmlAddress = "<div class='tempo-item address'>" + itemAddress + "</div>";
  var htmlPrice = "<div class='tempo-item price'>" + itemPrice + "</div>";
  var htmlType = "<div class='tempo-item type'>" + itemType + "</div>";
  var leftBlock = '<div class="tempo-photo-left-block" style="' + itemPhoto + '">' + '</div>';
  var rightBlock = "<div class='temporary-append-item'>" + htmlTitle + htmlType + htmlAddress + htmlPrice + itemCloseButton + "</div>";
  var fullItemToAppend = $("<div id='" + uniqueId + "' class='full-temporary-item'>" + leftBlock + rightBlock + "</div>");
  $(fixedContainer).append(fullItemToAppend).find(".closed-temporary-item-button").click(function(){handleCloseItemButton(uniqueId)});
}


function handleCloseItemButton(uniqueId) {
  var itemToDelete = $("#" + uniqueId + ".full-temporary-item");
  $(itemToDelete).remove();
  var appendItemsCount = $(".demand-proposal-left-block .full-temporary-item").length;
  if (appendItemsCount === 0) {
    var emptyFixedContainer = $('.demand-proposal-fixed-container');
    $(emptyFixedContainer).addClass("hidden");
  }
  var checkboxDiv = $(".checkbox-property-input[data-unique-id='" + uniqueId + "']");
  var input = $(checkboxDiv).find('input');
  $(input).prop( "checked", false );
  if ($(input).prop("checked") === true) {
    $(checkboxDiv).find(".bottom-block-btn").removeClass("green-background");
    $(checkboxDiv).find(".bottom-block-btn").addClass("red-background");
    $(checkboxDiv).find(".bottom-block-btn").text("ANNULER");
    var content = $(checkboxDiv).find(".block-content");
    var icon = "<i class='fa fa-check-circle-o'></i>";
    var message = "Bien selectionné ";
    if ($(checkboxDiv).find(".checked-message").length === 0) {
      var html = $("<div class='checked-message'>" + message + icon + "</div>");
      $(content).append(html);
    }
  } else {
    $(checkboxDiv).find(".checked-message").remove();
    $(checkboxDiv).find(".bottom-block-btn").removeClass("red-background");
    $(checkboxDiv).find(".bottom-block-btn").addClass("green-background");
    $(checkboxDiv).find(".bottom-block-btn").text("SELECTIONNER");
  }
}


function deleteCheckedProp(fixedContainer,itemInfos,uniqueId) {
  var itemToDelete = $("#" + uniqueId + ".full-temporary-item");
  console.log(itemToDelete);
  $(itemToDelete).remove();
}



function handleCheckedItemDisplay() {
  $('.unproposed-properties-wrapper .checkbox-property-input').click(function() {
    var block = $(this);
    var input = $(this).find('input');
    $(input).click(function() {
      if (this.checked) {
        $(block).find(".bottom-block-btn").removeClass("green-background");
        $(block).find(".bottom-block-btn").addClass("red-background");
        $(block).find(".bottom-block-btn").text("ANNULER");
        var content = $(block).find(".block-content");
        var icon = "<i class='fa fa-check-circle-o'></i>";
        var message = "Bien selectionné ";
        if ($(block).find(".checked-message").length === 0) {
          var html = $("<div class='checked-message'>" + message + icon + "</div>");
          $(content).append(html);
        }
      } else {
        $(block).find(".checked-message").remove();
        $(block).find(".bottom-block-btn").removeClass("red-background");
        $(block).find(".bottom-block-btn").addClass("green-background");
        $(block).find(".bottom-block-btn").text("SELECTIONNER");
      }
    });
  });
}

handleCheckedItemDisplay();


function handleBottomFixedItems() {
  $('.unproposed-properties-wrapper .checkbox-property-input').click(function() {
    var block = $(this);
    var input = $(this).find('input');
    var uniqueId = $(this).data("unique-id");
    if ($(input).prop("checked") === true) {
      console.log("Input checked !!!!");
      var fixedContainer = $('.demand-proposal-fixed-container');
      var subFixedContainer = $(fixedContainer).find(".demand-proposal-left-block");
      $(fixedContainer).removeClass("hidden");
      var itemInfos = $(this).find(".block-content");
      appendCheckedProp(subFixedContainer,itemInfos,uniqueId);
    } else {
      console.log("Input not checked");
      var fixedContainer = $('.demand-proposal-fixed-container');
      var subFixedContainer = $(fixedContainer).find(".demand-proposal-left-block");
      var itemInfos = $(this).find(".block-content");
      deleteCheckedProp(subFixedContainer,itemInfos,uniqueId)
    }
    var appendItemsCount = $(".demand-proposal-left-block .temporary-append-item").length;
    if (appendItemsCount === 0) {
      var emptyFixedContainer = $('.demand-proposal-fixed-container');
      $(emptyFixedContainer).addClass("hidden");
    }
  });
}




handleBottomFixedItems();



