function answerEdition(){
  $(".edit-survey-answer").click(function() {
    var questionsContainer = $(".questions-container");
    var forms = $(questionsContainer).find(".survey-form");
    var target = ($(this).data("target")).toString();
    var position = $(this).data("position");

    // Questions collection
    var questions = $(".questions-container");
    var positions = $(questions).find("[data-position]");

    // Answers collection
    var answers = $('.resume-container');
    var answersPositions = $(answers).find("[data-answerposition]");


    // Answers collection iteration
    $(answersPositions).each(function(){
      var currentAnswerPosition = $(this).data("answerposition");
      if (currentAnswerPosition >= position) {
        $(this).remove();
      }
    });

    // Questions collection iteration
    $(positions).each(function(){
      var currentPosition = $(this).data("position");
      if (currentPosition == position) {
        $(this).addClass("survey-form").show();
      } else if (currentPosition > position) {
        $(this).remove();
      }
    });


  });
}
