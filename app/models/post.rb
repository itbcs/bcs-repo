# frozen_string_literal: true

# class Post
class Post
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :blog_category

  field :name, type: String
  field :lname, type: String
  field :category_name, type: String
  field :title, type: String
  field :content, type: String
  field :link_name, type: String
  field :photo_url, type: String
  field :author, type: String
  field :tags, type: Array, default: []
  field :meta_desc, type: String
  field :meta_title, type: String
  field :targets, type: String

  def tags_list=(arg)
    self.tags = arg.split(',').map { |v| v.strip }
  end

  def tags_list
    self.tags.join(', ')
  end

end
