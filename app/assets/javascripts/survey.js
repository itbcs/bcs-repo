//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap-sprockets
//= require shared
//= require survey/survey-progression
//= require survey/next-survey
//= require survey/survey-modals
//= require survey/formating-number-input
//= require survey/survey-button-logic
//= require survey/limited_checked_input
//= require survey/edit-user-answer
//= require survey/handle-clientside-conditions
