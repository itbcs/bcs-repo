class GeocodingWithoutZipcode

  def initialize(property)
    @property = property
    @errors = []
    @results = []
  end

  def call
    handle_geocoding
    service_success
  end

  private

  def handle_geocoding
    geoloc_coord = [@property.lat, @property.lng]
    zipcode = Geocoder.search(geoloc_coord, params: {countrycodes: "fr"})

    if zipcode.first
      zipcode.first.address_components.each do |ac|
        puts "**** AC ****"
        puts ac
        puts "************"
        if ac["types"] == ["postal_code"]
          real_zipcode = ac["long_name"].to_s.strip
          @property.geoloc_zipcode = real_zipcode
          @property.zipcode = real_zipcode
          if @property.save
            puts "Property successfully saved !"
            @results << real_zipcode
          else
            puts "Failed property saved !"
            puts @property.errors.full_messages
          end
        end
      end
    end

  end


  def service_success
    ServiceSuccess.new(@results)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
