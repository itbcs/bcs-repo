# frozen_string_literal: true

source 'https://rubygems.org'

# Used for Heroku to select Ruby version
ruby '2.5.1'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end




# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
gem 'puma'
gem 'puma-heroku'
gem 'puma_worker_killer'

gem 'kaminari-mongoid'
gem 'kaminari-actionview'

gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'figaro'

# Manage BDD
gem 'bson_ext'
gem 'mongo'
gem 'mongoid'
gem 'mongoid-history'
gem 'mongoid_orderable'
gem 'mongoid_auto_increment'

# Version configuration
gem 'config'
gem 'devise'
gem 'devise-i18n'
gem 'devise_ssl_session_verifiable'

# Internationalization
gem 'i18n'
gem 'rails-i18n', '~> 5.0'

# Manage variables between Ruby/JS
gem 'gon'
gem 'simple_form'
gem 'country_select'

# Manage pdf generation
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'

# Api requests
gem 'rest-client'
gem 'mechanize'
gem 'nokogiri'

gem "select2-rails"
gem 'popper_js', '~> 1.14.5'

# Dezipper
gem 'rubyzip', '~> 1.1.0'
gem 'axlsx', '2.1.0.pre'
gem 'axlsx_rails'

#map tools
# gem 'leaflet-rails', '~> 0.7.7'
gem 'leaflet-rails'
gem 'geocoder'

#sms tools
gem 'nexmo'
gem 'phonelib'
gem 'mandrill-api'

#recaptcha tool
gem "recaptcha"

# AWS S3
gem 'aws-sdk-s3', '~> 1'
gem 'fog-aws'
gem 'carrierwave-mongoid', :require => 'carrierwave/mongoid'
gem 'mini_magick'
gem 'cloudinary'

# Manage logs
gem 'ruby_cowsay'

# Tracker manager
gem 'google-tag-manager-rails'

# Bootstrap static css
gem 'bootstrap-sass'
gem 'sass-rails'
gem 'bootstrap-datepicker-rails'

gem 'font-awesome-rails'
gem "animate-rails"
gem 'modernizr-rails'
gem 'autoprefixer-rails'

gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'jquery-fileupload-rails'
gem 'lightbox2-rails'

# Social Share
gem 'social-share-button'

# Background jobs
gem 'resque'
gem 'resque_mailer'

# Client side validations
gem 'client_side_validations'
gem 'client_side_validations-mongoid'
gem 'client_side_validations-simple_form'

# Utilisaion de haml
gem "haml-rails"
gem 'sitemap_generator'

# JSON authentication token method
gem 'simple_command'
gem 'bcrypt'
gem 'jwt'

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen'
  gem 'binding_of_caller'
  gem 'web-console'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
