class PhoneMessagesController < ApplicationController

  def sending_sms
    sms = PhoneMessage.create(from: "Kenotte", to: "+33610374185", body: "De nouvelles offres vous attendent, rdv dans votre espace Bientotchezsoi: https://bientotchezsoi.com/customers/connexion. A bientôt (chez soi)", template: "offers_without_choices")
    deliver(sms)
    redirect_to root_path
  end

  private

  def deliver sms
    nexmo = Nexmo::Client.new
    response = nexmo.sms.send(from: sms.from, to: sms.to, text: sms.body)
  end

  def safe_params
    params.require(:phone_message).permit(:from, :to, :body, :template)
  end

end
