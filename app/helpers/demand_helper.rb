module DemandHelper

  def french_formatted_number number
    number.to_i.to_s.reverse.gsub(/...(?=.)/,'\& ').reverse
  end

  def fullname first_name, last_name
    if first_name && last_name
      "#{last_name.upcase} #{first_name.capitalize}"
    end
  end

  def surface_format surface_array
    if surface_array && surface_array.is_a?(Array)
      if surface_array.size == 1
        "#{surface_array.last} m²"
      elsif surface_array.size == 2
        "entre #{surface_array.first} et #{surface_array.last} m²"
      end
    else
      "#{surface_array} m²"
    end
  end

  def budget_format budget_array
    if budget_array
      sanitize_array = budget_array.reject{|budget| budget.empty?}
      if sanitize_array && sanitize_array.size > 0
        if sanitize_array.size == 1
          result = "#{sanitize_array.first} €"
        elsif sanitize_array.size == 2
          result = "Entre #{sanitize_array.first} € et #{sanitize_array.last} €"
        end
      end
      return result
    end
  end


  def important_fields_format fields_array
    if fields_array && fields_array != [""]
      if fields_array.size == 1
        "#{fields_array.last.capitalize}"
      elsif fields_array.size == 2
        "#{fields_array.first.capitalize} et #{fields_array.last}"
      elsif fields_array.size > 2
        "#{fields_array[0..-2].join(', ')} et #{fields_array.last}"
      end
    end
  end

  def demand_type_and_localisation property_type, localisation
    if property_type && localisation
      "#{property_type} à #{localisation}"
    else
      "Vous n'avez pas renseigné ces champs !"
    end
  end

  def perimeter_format value
    if value.size == 2
      formatted_value = "#{value.first} et #{value.last}"
    elsif value.size > 2
      formatted_value = "#{value[0..-2].join(', ')} et #{value.last}"
    elsif value.size == 1
      formatted_value = value.last
    end
    return formatted_value
  end

  def important_fields_format important_fields
    if important_fields
      if important_fields.size > 2
        formatted_fields = "#{important_fields[0..-2].join(', ')} et #{important_fields.last}"
      elsif important_fields.size == 2
        formatted_fields = "#{important_fields.first} et #{important_fields.last}"
      elsif important_fields.size == 1
        formatted_fields = important_fields.last
      elsif important_fields.size == 0
        formatted_fields = "Aucun critères choisis"
      end
      return formatted_fields
    end
  end


  def customer_situation(demand)
    if demand.actual_situation
      result = demand.actual_situation.first(3).upcase
      if result == "NON"
        return false
      elsif result == "OUI"
        return true
      end
    else
      return false
    end
  end

  def format_budget_spread_data(budget_array)
    sanitize_array = budget_array.reject{|budget| budget.empty?}
    if sanitize_array && sanitize_array.size > 0
      if sanitize_array.size == 1
        result = sanitize_array.first
      elsif sanitize_array.size == 2
        result = sanitize_array.last
      end
    end
    return result.remove("\u00A0").to_i
  end

  def format_postal_codes_spread_data(postal_codes)
    if postal_codes
      if postal_codes.size == 1
        return "#{postal_codes.first}"
      elsif postal_codes.size == 2
        return "#{postal_codes.first} et #{postal_codes.last}"
      elsif postal_codes.size > 2
        return "#{postal_codes[0..-2].join(",")} et #{postal_codes.last}"
      end
    end
  end

  def format_equipements_spread_data(equipements)
    if equipements
      if equipements.size == 1
        return "#{equipements.first}"
      elsif equipements.size == 2
        return "#{equipements.first} et #{equipements.last}"
      elsif equipements.size > 2
        return "#{equipements[0..-2].join(",")} et #{equipements.last}"
      end
    end
  end

end
