function handleWidgetDynamicElements() {
  var closeBtn = document.querySelector(".bcs-advert-close-btn");
  var openBtn = document.querySelector(".bcs-advert-open-btn");
  var fullContainer = document.querySelector(".bcs-advert-fullcontainer");
  closeBtn.addEventListener("click", function(){
    fullContainer.classList.add("hidden");
    closeBtn.classList.add("hidden");
    openBtn.classList.remove("hidden");
  });
  openBtn.addEventListener("click", function(){
    fullContainer.classList.remove("hidden");
    closeBtn.classList.remove("hidden");
    openBtn.classList.add("hidden");
  });
}

handleWidgetDynamicElements();
