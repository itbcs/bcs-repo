class Webhooks::MandrillController < ActionController::API
  include ApplicationHelper
  def sent
    if params["mandrill_events"]
      values = params["mandrill_events"]
      parsed_object = JSON.parse(values)
      hashed_object = {"mandrill_events" => parsed_object.to_a}.to_h
      @webhook = Webhook.new(service: "#{controller_name}", description: "Mailing service", logo: "mandrill", action: "#{action_name}", content: hashed_object)
      if @webhook.save
        puts "Webhook saved !!"
        ActionCable.server.broadcast 'webhooks',
          content: @webhook.content.as_json,
          service: "#{@webhook.service.downcase.capitalize}",
          description: "#{@webhook.description}",
          event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
          logo: svg(@webhook.logo),
          dynamic_content: [{label: "Status",value: "#{@webhook.content['mandrill_events'].first['event']}"}, {label: "Subject",value: "#{@webhook.content['mandrill_events'].first['msg']['subject']}"}, {label: "Email",value: "#{@webhook.content['mandrill_events'].first['msg']['email']}"}],
          filter_target: "#{controller_name.downcase}",
          custom_action: "#{@webhook.action.capitalize}"
      end
    end
    if request.head?
      puts "Head request ok !!"
    end
    render :json => {status: 200}
  end

  def open
    if params["mandrill_events"]
      values = params["mandrill_events"]
      parsed_object = JSON.parse(values)
      hashed_object = {"mandrill_events" => parsed_object.to_a}.to_h
      @webhook = Webhook.new(service: "#{controller_name}", description: "Mailing service", logo: "mandrill", action: "#{action_name}", content: hashed_object)
      if @webhook.save
        puts "Webhook saved !!"
        city = @webhook.content["mandrill_events"].first["location"]["city"]
        region = @webhook.content["mandrill_events"].first["location"]["region"]
        country = @webhook.content["mandrill_events"].first["location"]["country"]
        full_localisation = "#{city} (#{region}), #{country}"
        ActionCable.server.broadcast 'webhooks',
          content: @webhook.content.as_json,
          service: "#{@webhook.service.downcase.capitalize}",
          description: "#{@webhook.description}",
          event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
          logo: svg(@webhook.logo),
          dynamic_content: [{label: "Status",value: "#{@webhook.content['mandrill_events'].first['event']}"}, {label: "Subject",value: "#{@webhook.content['mandrill_events'].first['msg']['subject']}"}, {label: "Email",value: "#{@webhook.content['mandrill_events'].first['msg']['email']}"}, {label: "Localisation",value: "#{full_localisation}"}],
          filter_target: "#{controller_name.downcase}",
          custom_action: "#{@webhook.action.capitalize}"
      end
    end
    if request.head?
      puts "Head request ok !!"
    end
    render :json => {status: 200}
  end

  def click
    if params["mandrill_events"]
      values = params["mandrill_events"]
      parsed_object = JSON.parse(values)
      hashed_object = {"mandrill_events" => parsed_object.to_a}.to_h
      @webhook = Webhook.new(service: "#{controller_name}", description: "Mailing service", logo: "mandrill", action: "#{action_name}", content: hashed_object)
      if @webhook.save
        puts "Webhook saved !!"
        ActionCable.server.broadcast 'webhooks',
          content: @webhook.content.as_json,
          service: "#{@webhook.service.downcase.capitalize}",
          description: "#{@webhook.description}",
          event_trig_at: "#{I18n.localize(@webhook.created_at, format: :medium_shortest)} à #{I18n.localize(@webhook.created_at, format: :time_without_seconds)}",
          logo: svg(@webhook.logo),
          custom_action: "#{@webhook.action.capitalize}"
      end
    end
    if request.head?
      puts "Head request ok !!"
    end
    render :json => {status: 200}
  end


end
