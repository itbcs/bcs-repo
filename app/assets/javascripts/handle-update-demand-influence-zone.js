function handleMultipleFieldsZipcodes(){
  initializeAutocompleteWithClass('.dynamic-geoloc');
  $(".add-zipcode-trigger").click(function(){
    var inputsContainer = $(".all-inputs-item");
    if (inputsContainer.length > 0) {
      var clonedObject = cloneLastInput();


      var radiusRadios = $(clonedObject).find(".custom-radiobuttons-with-svg-container");
      var allRadiosInputs = $(radiusRadios).find(".radio-input-with-icon");

      var firstItem = allRadiosInputs[0];
      var middleItem = allRadiosInputs[1];
      var lastItem = allRadiosInputs[2];

      console.log(firstItem);
      console.log(middleItem);
      console.log(lastItem);

      var randomGeneratedName = Math.random().toString(36).substr(2,8);

      changeRadioInputIdAndForAttr(firstItem,randomGeneratedName);
      changeRadioInputIdAndForAttr(middleItem,randomGeneratedName);
      changeRadioInputIdAndForAttr(lastItem,randomGeneratedName);

      handleAjaxZipcodesAreaQuery(clonedObject);

      var clonedObjectInput = $(clonedObject).find("input[type=text]");
      $(clonedObjectInput).val("");
      $(clonedObjectInput).removeAttr("value");
      $(clonedObjectInput).removeClass();
      $(clonedObjectInput).addClass("dynamic-geoloc");
      var clonedObjectDestroyer = $(clonedObject).find(".absolute-destroy-icon");
      if ($(clonedObjectDestroyer).hasClass("hidden")) {
        $(clonedObjectDestroyer).removeClass("hidden");
      }
      handleDestroyButton(clonedObjectInput,clonedObject);
      $(".all-inputs-container").append(clonedObject);
      initializeAutocompleteWithClass('.dynamic-geoloc');
    } else {
      console.log("No input to clone");
    }
  });
}

function handleDestroyButton(clonedObjectInput,clonedObject) {
  var destroyButton = $(clonedObject).find(".absolute-destroy-icon");
  $(destroyButton).click(function(){
    $(clonedObject).remove();
  });
}

function cloneLastInput(){
  var lastInput = $(".all-inputs-item").last();
  var clonedInput = $(lastInput).clone();
  return clonedInput;
}

function changeRadioInputIdAndForAttr(item,randomThing) {
  var randomString = Math.random().toString(36).substr(2,8);
  console.log(randomString);
  var input = $(item).find("input[type=radio]");
  var label = $(item).find("label");
  $(input).removeAttr("id");
  $(input).removeAttr("checked");
  $(label).removeAttr("for");
  $(input).attr("id",randomString);
  var inputName = $(input).attr("name");
  console.log(inputName);
  var sequentialInputName = inputName + "[:" + randomThing + "]";
  console.log(sequentialInputName);
  $(input).removeAttr("name");
  $(input).attr("name",sequentialInputName);
  $(label).attr("for",randomString);
}


function handleAjaxZipcodesAreaQuery(clonedObject) {
  var ajaxButton = $(clonedObject).find(".ajax-influence-zone-trigger");
  $(ajaxButton).click(function(){
    console.log("click on button");
  });
}

function startHandleAjaxZipcodesAreaQuery() {
  var fullObject = $(".all-inputs-item");
  var objectAjaxButton = $(fullObject).find(".ajax-influence-zone-trigger");
  var waitIcon = $("<i class='fa fa-circle-o-notch fa-spin waiting-fa-icon'></i>");
  $(objectAjaxButton).click(function() {
    $(this).addClass("hidden");
    $(".all-inputs-item").append(waitIcon);
    console.log("Start click on button");
    ajaxQuery(this,fullObject);
  });
}


function ajaxQuery(item,fullobject) {
  console.log(item);
  console.log(fullobject);
  var geolocDynamic = $(fullobject).find("input.dynamic-geoloc").val();
  var geolocZipcode = $(fullobject).find("input.geoloc_zipcode").val();
  var geolocAddress = $(fullobject).find("input.geoloc_address").val();
  var geolocLocality = $(fullobject).find("input.geoloc_locality").val();
  var geolocLng = $(fullobject).find("input.lng").val();
  var geolocLat = $(fullobject).find("input.lat").val();
  var demandId = $(fullobject).find("input.demand").val();
  var influenceRadius = $(fullobject).find("input[name='v2_demand[:geoloc_items][][:coordinates][:radius]']:checked").val();
  var url = $(fullobject).find("input.url").val();
  var fullData = {dynamic: geolocDynamic, zipcode: geolocZipcode, address: geolocAddress, locality: geolocLocality, lng: geolocLng, lat: geolocLat, radius: influenceRadius, demand: demandId};
  console.log(fullData);
  $.ajax({
    url: url,
    type: "get",
    data: fullData,
    success: function(data) {
      if (data.length === 0) {
        handleWaitAndButtonDisplay();
        appendEmptyResult();
      } else {
        handleWaitAndButtonDisplay();
        appendResult(data);
      }
    },
    error: function(data) {
      alert("Fatal Error");
    }
  });
}

function handleWaitAndButtonDisplay() {
  var hiddenButton = $(".all-inputs-item").last().find(".ajax-influence-zone-trigger.hidden");
  var waitAppendedIcon = $(".all-inputs-item").last().find(".waiting-fa-icon");
  if ($(hiddenButton).length > 0) {
    $(hiddenButton).removeClass("hidden");
  }
  if ($(waitAppendedIcon).length > 0) {
    $(waitAppendedIcon).remove();
  }
}


function appendEmptyResult() {
  var itemContainer = $("<div class='ajax-influence-result-container'></div>");
  $(".ajax-influence-result-container").empty();
  if ($(".ajax-influence-result-container").length > 0) {
    console.log("container present");
  } else {
    $(".all-inputs-container").last().append(itemContainer);
  }
  var itemToAppend = $("<div class='ajax-influence-result'>" + "Aucun résultat" + "</div>");
  $(".ajax-influence-result-container").append(itemToAppend);
}


function appendResult(data) {
  var itemContainer = $("<div class='ajax-influence-result-container'></div>");
  $(".ajax-influence-result-container").empty();
  for (var [key,value] of Object.entries(data)) {
    console.log(key);
    console.log(value);
    if (value.district) {
      var locality = ("<div class='locality'>" + value.locality + "</div>");
      var distance = ("<div class='distance'>" + parseFloat(value.distance) + " Km" + "</div>");
      var district = ("<div class='district'>" + "( Quartier: " + value.district + " )</div>");
      var zipcode = ("<div class='zipcode'>" + value.zipcode + "</div>");
      var itemToAppend = $("<div class='ajax-influence-result'>" + distance + locality + zipcode + district + "</div>");
    } else {
      var locality = ("<div class='locality'>" + value.locality + "</div>");
      var distance = ("<div class='distance'>" + value.distance + " Km" + "</div>");
      var zipcode = ("<div class='zipcode'>" + value.zipcode + "</div>");
      var itemToAppend = $("<div class='ajax-influence-result'>" + distance + locality + zipcode + "</div>");
    }
    if ($(".ajax-influence-result-container").length > 0) {
      console.log("container present");
    } else {
      $(".all-inputs-container").last().append(itemContainer);
    }
    $(".ajax-influence-result-container").append(itemToAppend);
  }
}



$(document).ready(function(){
  handleMultipleFieldsZipcodes();
  startHandleAjaxZipcodesAreaQuery();
});

