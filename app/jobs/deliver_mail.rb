class DeliverMail
  @queue = :low

  def self.perform
    # CustomerMailer.welcome.deliver_later
    @test_customer = Customer.all.where(email: 'aurelien@bientotchezsoi.com').last
    TestMailer.welcome(@test_customer).deliver_later
  end

end
