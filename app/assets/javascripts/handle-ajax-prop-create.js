function handleAjaxPropCreate(){
  $("form#new_property").submit(function(event){
    event.preventDefault();
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    $(".custom-loader-container").removeClass("hidden");
    $.ajax({
      url: url,
      processData: false,
      contentType: false,
      type: method,
      data: new FormData(this),
      success: function(data) {
        var submitButton = $form.find("input[type='submit']");
        $(submitButton).attr("disabled", false);
        console.log(data);
        if (data.errors) {
          $(".custom-loader-container").addClass("hidden");
          handleFormErrorsSentence(data.errors);
        }
        if (data.url) {
          $(".custom-loader-container").addClass("hidden");
          document.location.href = data.url;
        }
      },
      error: function(datas) {
        alert("Ajax error call !!!");
      }
    });
  });
}

function handleFormErrorsSentence(errors){
  for (var [ key, value ] of Object.entries(errors)) {
    if (key === "email") {
      var sentence = $("<div class='custom-input-error " + key + "'>Cet email " + value[0] + "</div>");
    } else if (key === "first_name") {
      var sentence = $("<div class='custom-input-error " + key + "'>Votre prénom est obligatoire</div>");
    } else if (key === "last_name") {
      var sentence = $("<div class='custom-input-error " + key + "'>Votre nom est obligatoire</div>");
    } else if (key === "password") {
      var sentence = $("<div class='custom-input-error " + key + "'>Mot de passe obligatoire</div>");
    } else {
      var sentence = $("<div class='custom-input-error " + key + "'>"+ value[0] + "</div>");
    }
    var idToCheck = "#property_" + key;
    $(idToCheck).addClass("input-error-border");
    var currentInput = $(idToCheck);
    $(sentence).insertAfter($(currentInput));
    $(currentInput).focus(function() {
      $(this).removeClass("input-error-border");
      $(this).next().remove();
    });
  }
}


handleAjaxPropCreate();
