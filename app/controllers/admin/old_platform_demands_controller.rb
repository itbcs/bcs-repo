class Admin::OldPlatformDemandsController < ApplicationController
  http_basic_authenticate_with name: ENV["HTTP_AUTH_LOGIN"], password: ENV["HTTP_AUTH_PWD"]

  def index
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all.limit(200)
    @sponsorships = Sponsorship.all
    @old_platform_demand = OldPlatformDemand.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/old_platform_demands/index", layout: "admin"
  end

  def show
    @customer = Customer.find(params[:id])
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all.limit(200)
    @sponsorships = Sponsorship.all
    @old_platform_demand = OldPlatformDemand.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/old_platform_demand/show", layout: "admin"
  end

  def destroy
    @customer = Customer.find(params[:id].to_s)
    @customer.destroy
    redirect_to admin_customers_path, notice: "Utilisateur effacé"
  end

end
