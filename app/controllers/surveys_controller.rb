class SurveysController < ApplicationController
  include PdfHelper
  before_action :find_demand, only: [:next_question, :next_question_edit]
  before_action :find_current_survey, only: [:next_question, :next_question_edit]
  before_action :current_answer_values, only: [:next_question, :next_question_edit]
  before_action :current_answer_ids, only: [:next_question, :next_question_edit]

  def demand_edit
    @demand = Demand.find(params[:demand_id].to_s)
    @surveys = Survey.all.sort_by{|s| s.position}
    @survey = Survey.where(position: 1).last
    render :demand_edit, layout: 'survey'
  end

  def intital_survey
    @demand = Demand.find(params[:demand_id])
    @customer = @demand.customer
    @survey = Survey.where(position: 1).last
    @survey_course_status = 0
    render :initial_survey, layout: 'survey'
  end

  def user_results_recap
    @demand = Demand.find(params[:demand_id])
    @survey_count = Survey.all.size.to_i
    populate_answers(@demand)
    render 'user_results_recap'
  end

  def criterias_submit
    @demand = Demand.find(params[:demand_id])
    @criterias = params[:criterias].to_a
    @demand.important_fields = @criterias
    @demand.completed = true
    @customer = @demand.customer
    # FetchMapPhoto.perform_later(@demand.id.to_s)

    if params[:edit]
      @demand_props_agencies = @demand.properties.map{|prop| prop.agency}.compact.uniq!
      if @demand_props_agencies
        @demand_props_agencies.each do |agency|
          AgencyMailer.search_update(agency,@demand).deliver
        end
      end
      ["annegaelle@bientotchezsoi.com","laure@bientotchezsoi.com","philippe@bientotchezsoi.com","aurelien@bientotchezsoi.com"].each do |email|
        AdminMailer.search_update(@demand,@customer,email, l(@demand.created_at, format: :medium), l(@demand.created_at, format: :count)).deliver
      end
    end

    @demand.save
    redirect_to customer_zone_path
  end

  def next_question
    if params[:position].to_i == Survey.all.size.to_i
      @last_survey = find_current_survey
      UpdateDemand.new(@current_survey, @demand, @answer_ids, @answer_values).call
      @survey = Survey.where(position: params[:position]).last
      find_demand
      @survey_count = Survey.all.size.to_i
      populate_answers(@demand)
      @survey_course_status = survey_course_status(params[:position])
      render partial: 'final_survey'
    else
      @position = params[:position]
      UpdateDemand.new(@current_survey, @demand, @answer_ids, @answer_values).call
      result = GetNextSurvey.new(@current_survey,@demand,@position).call
      @survey = result.data if result.success?
      @last_survey = find_current_survey
      find_demand
      @survey_course_status = survey_course_status(@position)
      render partial: 'full_survey'
    end
  end


  def next_question_edit
    if params[:position].to_i == Survey.all.size.to_i
      @last_survey = find_current_survey
      UpdateDemand.new(@current_survey, @demand, @answer_ids, @answer_values).call
      @survey = Survey.where(position: params[:position]).last
      find_demand
      @survey_count = Survey.all.size.to_i
      populate_answers(@demand)
      @survey_course_status = survey_course_status(params[:position])
      render :demand_edit_final_step, layout: 'survey'
    else
      @position = params[:position]
      UpdateDemand.new(@current_survey, @demand, @answer_ids, @answer_values).call
      result = GetNextSurvey.new(@current_survey,@demand,@position).call
      @survey = result.data if result.success?
      @last_survey = find_current_survey
      find_demand
      @survey_course_status = survey_course_status(@position)
      render :demand_edit, layout: 'survey'
    end
  end

  private

  def find_demand
    @demand = Demand.find(params[:demand_id])
  end

  def find_current_survey
    @current_survey = Survey.where(position: params[:position]).last
  end

  def current_answer_values
    @answer_values = params[:answer_values] if params[:answer_values]
  end

  def current_answer_ids
    @answer_ids = params[:answer_ids] if params[:answer_ids]
  end

  def survey_course_status(position)
    survey_count = Survey.all.size.to_f
    ((position.to_f / survey_count) * 100).round(0)
  end

  def populate_answers(demand)
    @attributes_by_theme = {}
    themes = []
    if demand.property_type == "Une maison"
      values = [6,12,13,14,15,17,23,24,27,28,31]
      @surveys = Survey.all.sort_by{|s| s.position}.reject{|s| values.include?(s.position.to_i)}
    elsif demand.property_type == "Un appartement"
      values = [5,12,13,14,15,16,17,25,32]
      @surveys = Survey.all.sort_by{|s| s.position}
    elsif demand.property_type == "Un terrain"
      values = [5,6,7,8,9,10,11,13,16,18,19,20,21,22,23,24,26,27,28,29,30,31,32]
      @surveys = Survey.all.sort_by{|s| s.position}
    end

    @surveys.each do |survey|
      (themes << survey.theme) unless themes.include?(survey.theme)
    end
    themes.each do |theme|
      label_plus_attribute = []
      if demand.property_type == "Une maison"
        values = [6,12,13,14,15,17,23,24,27,28,31]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      elsif demand.property_type == "Un appartement"
        values = [5,12,13,14,15,16,17,25,32]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      elsif demand.property_type == "Un terrain"
        values = [5,6,7,8,9,10,11,13,16,18,19,20,21,22,23,24,26,27,28,29,30,31,32]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      end
      @theme_surveys.each do |survey|


        if survey.demand_attribute == ("budget") || survey.demand_attribute == ("terrain_budget") || survey.demand_attribute == ("house_budget")
          (attribute = demand[survey.demand_attribute.to_sym].map {|attr| format_number(attr)}) if demand[survey.demand_attribute.to_sym] != nil
        elsif survey.demand_attribute == "surface" || survey.demand_attribute == "terrain_surface" || survey.demand_attribute == "principal_space" || survey.demand_attribute == "actual_housing_area"
          (attribute = demand[survey.demand_attribute.to_sym].map {|attr| format_area(attr)}) if demand[survey.demand_attribute.to_sym] != nil
        else
          if demand[survey.demand_attribute.to_sym].is_a?(Array)
            if demand[survey.demand_attribute.to_sym].size > 0
              if demand[survey.demand_attribute.to_sym].first != ""
                attribute = demand[survey.demand_attribute.to_sym]
                puts "******************"
                puts demand[survey.demand_attribute.to_sym]
                puts "******************"
              end
            end
          else
            attribute = demand[survey.demand_attribute.to_sym]
          end
        end
        (label_plus_attribute << [survey.resume,attribute,survey.demand_attribute,survey.checkable_on_criteria]) if attribute != nil
      end
      @attributes_by_theme[theme] = label_plus_attribute
    end
    @attributes_by_theme["Votre projet"].insert(0, ["Localisation", demand.localisation])
    @attributes_by_theme["Votre projet"].insert(1, ["Zone de recherche", demand.postal_codes])
    @attributes_by_theme
  end


  def find_survey_criteria(criterias)
    survey_labels = []
    criterias.each do |crit|
      @survey = Survey.where(demand_attribute: crit.to_s).last
      survey_labels << @survey.resume
    end
    survey_labels
  end

  def to_params
    params.require(:survey).permit(:position, :demand_id, :answer_values, :answer_ids, :criterias)
  end
end
