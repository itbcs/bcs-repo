class CustomerMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/customer_mailer/welcome
  def welcome
    CustomerMailer.welcome
  end

end
