class V2Answer
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :question
  before_create :demand_auto_field
  before_save :handle_input_hint
  before_save :handle_fem_text
  before_save :handle_input_format
  before_save :handle_unit
  before_save :handle_score


  field :answer_type, type: String
  field :text, type: String
  field :unit, type: String
  field :hint, type: String
  field :min, type: Integer
  field :max, type: Integer
  field :step, type: Integer
  field :comment, type: String
  field :input_width, type: String
  field :keycode, type: Integer
  field :identifier, type: String
  field :order, type: Integer
  field :placeholder, type: String
  field :image, type: String
  field :field_type, type: String
  field :attr, type: String
  field :input_format, type: String
  field :maxlength, type: Integer
  field :demand_attribute, type: String
  field :fem_text, type: String
  field :input_hint, type: String
  field :slug, type: String
  field :score, type: Integer
  field :project_score, type: Integer
  field :member_score, type: Integer
  field :profile_score, type: Integer

  auto_increment :auto_identifier, collection: :answer_sequences

  def demand_auto_field
    @question = self.question
    @field = @question.demand_attribute
    self.demand_attribute = @field
  end

  def handle_input_hint
    if input_hint == ""
      self.input_hint = nil
    elsif input_hint != ""
      self.input_hint = input_hint
    end
  end

  def handle_fem_text
    if fem_text == ""
      self.fem_text = nil
    elsif fem_text != ""
      self.fem_text = fem_text
    end
  end

  def handle_input_format
    puts "********************************************"
    puts "Enter into input_format before_save method"
    puts input_format
    puts input_format.inspect
    puts input_format.class
    puts input_format == ""
    puts "********************************************"
    if input_format == ""
      self.input_format = nil
    elsif input_format != ""
      self.input_format = input_format
    end
  end

  def handle_unit
    if unit == ""
      self.unit = nil
    elsif unit != ""
      self.unit = unit
    end
  end

  def handle_score
    if score == ""
      self.score = nil
    elsif score != ""
      self.score = score.to_i
    end
  end

  def self.slugs
    ["female","male","firstname","primary-residence","secondary-residence","rental-investment","alone","couple","family","blended-family","tenant","relative","less-5-km","more-5-km","far-far-away","workplace","schools","family","leisures","others","first-time-owner","owner","been-owner","o-flat","o-house","less-than-5y","from-5-to-7y","from-10-to-15y","long-ago","input-email-estimation","s-house","s-flat","s-plot","s-1-mo","s-3-mo","s-4-mo","s-long","visit-none","visit-1-3","visit-10","visit-over","existing","recent","new","classic","modern","atypical","no-restoration","decoration","restoration","green-renov-ok","green-renov-whynot","green-renov-nok","less-35-yo","35-50-yo","50-65-yo","up-65-yo","primary-school","high-school","college","university","actual-surface-co","actual-surface-ho","req-surface-min"," req-surface-max","bedrooms","budget-min","budget-max","income-less-2500","income-2500-3500","income-3500-6500","income-over-6500","banker-long","banker-recent","banker-ok","unkown-broker","ideal-plot-surface","maximum-plot-surface","ideal-new-house-surface","maximum-new-house-surface","yes-stairs-new-house","no-stairs-new-house","whatever-stairs-house","yes-contact-constructor","no-contact-constructor","house-2-stairs","house-1-stair","house-no-stair","house-whatever","flat-ground-floor","flat-1-floor","flat-top-floor","flat-whatever-floor","living-ideal-surface","open-kitchen","closed-kitchen","either-kitchen","1-bathroom","2-bathroom","laundry","basement","cellar","veranda","alarm","cooling","dressing","storage-closet","great-view","opposite","south-orientation","wt-north-orientation","either-orientation","bicycle-room","parking","box-parking","yes-garage","no-garage","caretaker","intercom","digicode","individual","collective","fuel","wood-geothermy","gas-electric","high-perf-energy","medium-perf-energy","low-perf-energy","balcony","deck","swimming-pool-condo","garden","swimming-pool-individual","pergola","cesar","churchill","kurt-cobain","little-prince","aladdin","van-gogh","amelie-poulain","cleopatre","coco-chanel","leia","rainponce","sissi","con-difficult","pro-easy","pro-mindblowing","con-boring","con-stressful","pro-quiet","pro-bold","pro-prudent","con-impulsive","pro-considered","pro-smooth","con-conflictual","pro-limpid","con-obscur","con-slow","pro-rapid","pro-actual-home","pro-search-home"]
  end

end




