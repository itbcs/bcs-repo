class CustomerMailer < BaseMandrillMailer
  include Resque::Mailer

  def customer_rent_request(customer_email,customer_first_name,agency_phone,agency_email,agency_address,agency_name)
    puts "Enter into customer rent request"
    puts "---- Inspect variables ----"
    puts "#{customer_email} #{customer_first_name} #{agency_phone} #{agency_email} #{agency_address} #{agency_name}"
    puts "---------------------------"
    subject = "Aloha #{customer_first_name}, Complétez votre dossier de location "
    merge_vars = {
      "FIRST_NAME" => customer_first_name,
      "AGENCY" => agency_name,
      "AGENCY_PHONE" => agency_phone,
      "AGENCY_EMAIL" => "location@bientotchezsoi.com",
      "AGENCY_ADDRESS" => agency_address,
    }
    body = mandrill_template("trc-dossier-locataire", merge_vars)
    from = "Bientôt Chez Soi <hello@bientotmaloc.com>"
    replyto = "hello@bientotmaloc.com"
    send_mail(customer_email, subject, body, from, replyto)
  end


  def welcome(customer,demand)
    file = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/pdfs/legals/conditions-generales-utilisation.pdf"
    filename = "Conditions générales d'utilisation"
    puts "**** ENTER INTO WELCOME CUSTOMER EMAIL ****"
    puts demand[:property_type]
    puts demand[:budget]
    puts "*******************************************"
    subject = "Aloha #{customer[:first_name]}, on s'appelle bientôt (chez soi) ?"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "LOCALISATION" => demand[:search_formatted_area],
      "PROPERTY_TYPE" => demand[:property_type],
      "BUDGET" => demand[:budget],
      "PERSONAL_ACCOUNT_LINK" => "https://bientotchezsoi.com/customers/connexion",
    }
    body = mandrill_template("trc-welcome-email", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail_with_attachments(customer[:email], subject, body, from, replyto, file, filename)
  end

  def unviewed_offers(customer_email,variables)
    subject = "Evaluez la dernière proposition de notre agence partenaire."
    merge_vars = variables
    body = mandrill_template("trc-attente-evaluation-offres", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer_email, subject, body, from, replyto)
  end

  def customer_demand_matching(customer)
    subject = "Aloha #{customer[:first_name]}, une nouvelle offre vous attend dans votre espace "
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "SIGN_IN_URL" => "https://bientotchezsoi.com/customers/connexion",
      "USER_URL" => "Property matching",
    }
    body = mandrill_template("trc-notif-nouvelle-offre", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer[:email], subject, body, from, replyto)
  end

  def estimation_request(demand)
    puts "Enter into estimation request mailer"
    puts demand.inspect
    actual_housing = demand[:actual_housing].split().last.downcase
    subject = "Estimez gratuitement votre #{actual_housing}"
    merge_vars = {
      "FIRST_NAME" => demand[:firstname],
      "PROPERTY_TYPE" => actual_housing,
    }
    body = mandrill_template("trc-demande-estimation", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(demand[:email], subject, body, from, replyto)
  end

  def one_day_old_positive_choices(customer_email,agency_email,agency_name,customer_firstname,customer_choice,property_title)
    puts "Goes into one day old positive choices"
    subject = "Ajoutez votre visite à votre parcours"
    merge_vars = {
      "CUSTOMER_EMAIL" => customer_email,
      "AGENCY_EMAIL" => agency_email,
      "AGENCY_NAME" => agency_name,
      "FIRST_NAME" => customer_firstname,
      "CUSTOMER_CHOICE" => customer_choice,
      "PROPERTY_TITLE" => property_title,
    }
    body = mandrill_template("trc-visit-schedule", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(customer_email, subject, body, from, replyto)
  end

  def offers_without_choice(email,demand_id,vars,firstname)
    puts "Goes into offer without choice mailer method"
    subject = "Evaluez vite nos dernières propositions #{firstname}"
    merge_vars = vars
    body = mandrill_template("trc-offers-without-choices-v2", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(email, subject, body, from, replyto)
  end


end
