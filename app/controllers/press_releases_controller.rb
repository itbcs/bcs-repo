class PressReleasesController < ApplicationController

  def show
    @press_release = PressRelease.find(params[:id])
  end

end
