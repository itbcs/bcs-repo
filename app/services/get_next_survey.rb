class GetNextSurvey
  def initialize survey, demand, position
    @current_survey = survey
    @demand = demand
    @position = position
  end

  def call
    @next_survey = Survey.find_by(position: @position.to_i + 1)
    if @next_survey.condition
      check_condition(@next_survey)
    else
      @next_survey
    end
    service_success
  end

  private

  def check_condition survey
    if survey&.condition&.child_condition
      if handle_child_condition(survey.condition)
        @next_survey = survey
      else
        next_survey = Survey.find_by(position: survey.position.to_i + 1)
        check_condition(next_survey)
      end
    elsif survey.condition
      if handle_conditions(survey.condition)
        @next_survey = survey
      else
        next_survey = Survey.find_by(position: survey.position.to_i + 1)
        check_condition(next_survey)
      end
    else
      @next_survey = survey
    end
  end

  def handle_child_condition child_condition
    if child_condition.child_condition
      child_result = handle_child_condition child_condition.child_condition
      current_result = handle_conditions child_condition
      puts "#{child_condition.operator} #{child_condition.value} #{current_result}"
      b = handle_children_operator current_result,child_result,child_condition.children_operator
      puts "pere et fils #{child_condition.children_operator} #{b}"
      b
    else
      a = handle_conditions child_condition
      puts "#{child_condition.operator} #{child_condition.value} : #{a}"
      a
    end
  end

  def handle_children_operator value, child_value, children_operator
    case children_operator
    when :and.to_s
      and_operator value, child_value
    when :or.to_s
      or_operator value, child_value
    end
  end

  def handle_conditions condition
    case condition.operator.to_s
      when :equal.to_s
        equal @demand[condition.demand_field.to_s], condition.value.to_s
      when :not_equal.to_s
        not_equal @demand[condition.demand_field.to_s], condition.value.to_s
      when :superior.to_s
        if @demand[condition.demand_field.to_sym].last == ""
          number = @demand[condition.demand_field.to_sym].first.gsub(",", "").to_i
        else
          number = @demand[condition.demand_field.to_sym].last.gsub(",", "").to_i
        end
        superior number, condition.value.to_i
      when :inferior.to_s
        number = @demand[condition.demand_field.to_sym].last.gsub(",", "").to_i
        inferior number, condition.value.to_i
    end
  end


  def not_equal demand_value, condition_value
    demand_value != condition_value
  end

  def equal demand_value, condition_value
    demand_value == condition_value
  end

  def and_operator demand_value, condition_value
    demand_value && condition_value
  end

  def or_operator demand_value, condition_value
    demand_value || condition_value
  end

  def superior demand_value, condition_value
    demand_value > condition_value
  end

  def inferior demand_value, condition_value
    demand_value < condition_value
  end



  def service_success
    ServiceSuccess.new(@next_survey)
  end

  def service_error
    ServiceError.new(@errors)
  end
end
