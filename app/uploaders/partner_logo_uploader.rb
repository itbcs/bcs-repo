class PartnerLogoUploader < CarrierWave::Uploader::Base
  storage :fog

  def store_dir
    "carrierwave-uploads/partner-logos/partner-#{model.id.to_s}"
  end

  def filename
    "#{secure_token(10)}.#{file.extension}" if original_filename.present?
  end
  protected

  def secure_token(length=16)
    var = "@#{mounted_as}_secure_token"
    model.instance_variable_get(var.to_sym) or model.instance_variable_set(var.to_sym, SecureRandom.hex(length/2))
  end

end
