class Apifluxes::AgenciesController < ApplicationController
  include ApplicationHelper

  def show
    @api_response = RestClient.get("https://bcs-api.herokuapp.com/agencies/#{params[:id].to_s}")
    @responses = JSON.parse(@api_response.body.to_s)
    render :show, layout: false
  end

  def index
    @start_page = true
    filtered_params = handle_params
    query_url = generate_api_url_query(filtered_params)
    @api_response = RestClient.get(query_url)
    @responses = JSON.parse(@api_response.body.to_s)
  end

  def create
    filtered_params = handle_params
    query_url = generate_api_url_query(filtered_params)
    @query_resume = handle_query_resume(filtered_params)
    @query_resume_field_count = @query_resume.size
    @api_response = RestClient.get(query_url)
    @responses = JSON.parse(@api_response.body.to_s)
    @result_count = @responses.size
    render :index
  end

  def activate_agency_flux_recovery
    @api_response = RestClient.get("https://bcs-api.herokuapp.com/agencies/#{params[:id].to_s}")
    @responses = JSON.parse(@api_response.body.to_s)
    @bcs_agency = Agency.where(email: params[:email]).last
    result = ActivateFluxSynchro.new(@bcs_agency,@responses).call
    if result.success?
      @datas = result.data
    else
      @data = {error: true}
    end
    render partial: "apifluxes/partials/flux-synchro-modal-return-result", format: "html"
  end

  private

  def handle_query_resume(filtered_params)
    puts "Handle resume params"
    @query_resume = []
    filtered_params.each do |fp|
      if fp[:field].to_sym == :city
        @query_resume << {field: "Ville", value: "#{fp[:value]}"}
      elsif fp[:field].to_sym == :zipcode
        @query_resume << {field: "Code postal", value: "#{fp[:value]}"}
      elsif fp[:field].to_sym == :name
        @query_resume << {field: "Nom", value: "#{fp[:value]}"}
      end
    end
    puts @query_resume.inspect
    @query_resume
  end

  def generate_api_url_query(filtered_params)
    start_url = "https://bcs-api.herokuapp.com/agencies"
    filtered_params.each_with_index do |item,index|
      if index == 0
        query = "/?#{item[:field]}=#{item[:value].parameterize}"
        start_url += query
      else
        query = "&#{item[:field]}=#{item[:value].parameterize}"
        start_url += query
      end
    end
    puts start_url
    start_url
  end

  def handle_params
    @params_with_query_options = []
    sanitize_params = params.except("utf8","button","controller","action").delete_if { |k, v| v.empty? }
    puts sanitize_params
    sanitize_params.each do |k,v|
      @params_with_query_options << {field: "#{k}", value: "#{v}"}
    end
    puts @params_with_query_options
    @params_with_query_options
  end

end
