class EstimationsController < ApplicationController

  # def create
  #   @estimation = Estimation.new(safe_params)
  #   estimation_result = handle_estimation(@estimation)
  #   min_value = estimation_result - (estimation_result / 20)
  #   max_value = estimation_result + (estimation_result / 20)
  #   @estimation.results = {min_value: min_value.to_i, max_value: max_value.to_i, value: estimation_result.to_i}
  #   if @estimation.save
  #     JobTestMailer.estimation_notification(@estimation).deliver
  #     redirect_to estimation_result_path(estimation_id: @estimation.id.to_s), notice: "Votre demande à été prise en compte"
  #   else
  #     render new_estimation_path, alert: "Un problème est survenu"
  #   end
  #   # render json: ({min_value: min_value.to_i, max_value: max_value.to_i, value: estimation_result.to_i}).to_json
  # end

  def create
    @estimation = Estimation.new(safe_params)
    if @estimation.save
      redirect_to root_path, notice: "Votre demande à été prise en compte"
    else
      render :new_estimation, alert: "Une erreur est survenue"
    end
  end

  def update
    @estimation = Estimation.find(params[:estimation_id])
    @estimation.update(safe_params)
    if @estimation.save
      redirect_to new_estimation_path, notice: "Votre demande à été prise en compte"
    else
      render estimation_result_path(estimation_id: @estimation.id.to_s), alert: "Un problème est survenu"
    end
  end

  def new_estimation
    @estimations = EstimationValue.all
    if params[:localisation]
      @localisation = params[:localisation]
    end
    render :new_estimation
  end

  def result
    @estimation = Estimation.find(params[:estimation_id])
    render :result
  end

  private


  def handle_estimation(estimation)
    house_values = EstimationValue.where(type: "Maison").last
    flat_values = EstimationValue.where(type: "Appartement").last
    if estimation.property_type == "Une maison"
      if estimation.quality == "Il y a des travaux de rénovation à prévoir"
        values = house_values.values[estimation.localisation]
        min_price = values[:min_price].to_f
        surface = estimation.living_space.to_f
        score = min_price * surface
        formatted_score = score.to_i
      elsif estimation.quality == "Il est de qualité comparable aux autres biens du quartier"
        values = house_values.values[estimation.localisation]
        average_price = values[:average_price].to_f
        surface = estimation.living_space.to_f
        score = average_price * surface
        formatted_score = score.to_i
      elsif estimation.quality == "Il est d’un standing supérieur aux autres bien du quartier"
        values = house_values.values[estimation.localisation]
        max_price = values[:max_price].to_f
        surface = estimation.living_space.to_f
        score = max_price * surface
        formatted_score = score.to_i
      end
      return formatted_score
    elsif estimation.property_type == "Un appartement"
      if estimation.quality == "Il y a des travaux de rénovation à prévoir"
        values = flat_values.values[estimation.localisation]
        min_price = values[:min_price].to_f
        surface = estimation.living_space.to_f
        score = min_price * surface
        formatted_score = score.to_i
      elsif estimation.quality == "Il est de qualité comparable aux autres biens du quartier"
        values = flat_values.values[estimation.localisation]
        min_price = values[:average_price].to_f
        surface = estimation.living_space.to_f
        score = min_price * surface
        formatted_score = score.to_i
      elsif estimation.quality == "Il est d’un standing supérieur aux autres bien du quartier"
        values = flat_values.values[estimation.localisation]
        min_price = values[:max_price].to_f
        surface = estimation.living_space.to_f
        score = min_price * surface
        formatted_score = score.to_i
      end
      return formatted_score
    end
  end

  def safe_params
    params.require(:estimation).permit(:localisation, :property_type, :living_space, :quality, :email, :firstname, :lastname, :phone, :comment, :results, :estimation_value, :contact_require)
  end

end
