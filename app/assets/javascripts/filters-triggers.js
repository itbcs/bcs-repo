$('.ajax-modal-content').click(function(){
  var demandId = $(this).data("demand");
  var url = $(this).data("url");
  console.log(url);
  $.ajax({
    method: "get",
    url: url,
    success: function(html) {
      $("#deactivated-modal .modal-body").html(html);
      $("#deactivated-modal").modal("show");
    },
  })
});
