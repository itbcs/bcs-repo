function formatArrayNumericInput(){
  $(".agency-form-edit").find(".custom-form-multi-inputs").find("input").keyup(function() {
    console.log("Keyup on input");
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
      return;
    }
    var input = $(this).val();
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt( input, 10 ) : 0;
    $(this).val( function() {
      return ( input === 0 ) ? "" : input;
    });
  });
}

formatArrayNumericInput();






$("#menu-trigger").click(function() {
  $('#menu-container').toggle();
});


$(".validated-demands-trig").click(function() {
  $(this).toggleClass('active');
  $("table.validated-demand").toggle();
  $(".validated-demands-title").toggle();
});

$(".pending-demands-trig").click(function() {
  $(this).toggleClass('active');
  $("table.pending-demand").toggle();
  $(".pending-demands-title").toggle();
});


$('#phone-popover').popover();
$('.logo-plus-text').dropdown();




var photoCollection = [];
var image;
var fileType;
var fileSize;
var fileName;

function previewFile() {
  var imageContainer = $('#image-upload-container');
  var preview = document.querySelector('#image-upload-container .bcs-button-sm-special img.preview-img');
  var files = document.querySelector('#image-upload-container input[type=file]').files;

  Array.from(files).forEach(function(file) {
    var reader  = new FileReader();
    $('.custom-multi-photo-wrapper').remove();
    reader.addEventListener("load", function () {
      image = $("<div class='custom-multi-photo-wrapper'><img class='preview-img' src=" + reader.result + " id=" + file.name + ">" + "<div class='preview-pdf-file-name'>" + file.name + "</div>" + "<div class='preview-file-size'>" + "( " + (file.size/1000000) + " Mo" + " )" + "</div></div>");
      $(image).appendTo($(imageContainer));
    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  });
}












function previewSteppedFile() {
  var imageContainer = $('#stepped-upload-container');
  var files = $('.upload-photos-container-stepped .upload-top');

  Array.from(files).forEach(function(container){
    var fileInput = $(container).find("label span input[type=file]");
    var file = fileInput[0].files[0];
    console.log(file);
    var reader  = new FileReader();
    $('.custom-multi-photo-wrapper').remove();
    reader.addEventListener("load", function () {
      image = $("<div class='custom-multi-photo-wrapper'><img class='preview-img' src=" + reader.result + " id=" + file.name + ">" + "<div class='preview-pdf-file-name'>" + file.name + "</div>" + "<div class='preview-file-size'>" + "( " + (file.size/1000000).toFixed(2) + " Mo" + " )" + "</div></div>");
      $(image).appendTo($(imageContainer));
    }, false);
    if (file) {
      reader.readAsDataURL(file);
      console.log(container.id);
      var position = $(container).data("position");
      var nextPosition = position += 1;
      if (nextPosition <= 8) {
        var nextInput = $("#photo-input-" + nextPosition);
        $(nextInput).removeClass("hidden");
        $(container).addClass("hidden");
      } else {
        alert("Limite de photo atteinte");
      }
    }
  })
}





$(".ajax-upload-container form").submit(function(e){
  console.log("toto");
  e.preventDefault();
  var form = $(this);
  var url = form.context.action;
  $.ajax({
    data: form.serialize(),
    method: "GET",
    url: url,
    success: function(data) {
      console.log("Ajax success");
    },
  })
});










function previewPdfFile() {
  var imageContainer = $('#pdf-upload-container');
  var preview = document.querySelector('#pdf-upload-container .bcs-button-sm-special img.preview-img');
  var file    = document.querySelector('#pdf-upload-container input[type=file]').files[0];
  console.log(file);
  console.log(file.length);
  fileType = file.type;
  fileSize = file.size;
  fileName = file.name;
  var reader  = new FileReader();
  reader.addEventListener("load", function () {
    if (fileType === "application/pdf") {
      image = $("<div class='preview-img'>" + "</div>" + "<div class='preview-pdf-file-name'>" + fileName + "</div>" + "<div class='preview-file-size'>" + "( " + (fileSize/1000000).toFixed(2) + " Mo" + " )" + "</div>");
    } else {
      image = $("<img class='preview-img' src=" + reader.result + ">");
    }
    $(image).appendTo($(imageContainer));
  }, false);
  if (file) {
    reader.readAsDataURL(file);
  }
}



function handleArrayInputs() {
  var inputs = $('input#agency_');
  var removeButton = "<div class='erase-input'><i class='fa fa-times'></i></div>";
  var inputContainer = $('.custom-form-multi-inputs').find(".form-group");

  $(inputs).each(function() {
    $(this).after(removeButton);
  });


  var input = $('input#agency_').last();
  $(document).ready(function() {
    $(".add-zipcode-input").click(function() {
      if (input.length !== 0) {
        var newInput = $(input).clone();
      } else {
        var newInput = $("<input class='text optional' type='text' name='agency[catchment_area][]' id='agency_'>")
      }
      $(newInput).val("");
      $(newInput).appendTo(inputContainer);
      $(newInput).after(removeButton);
      handleRemoveInputs();
      formatArrayNumericInput();
    });
  });

}

function handleRemoveInputs() {
  $(".erase-input").click(function(){
    console.log(this);
    $(this).prev().remove();
    $(this).remove();
  });
}


handleArrayInputs();
handleRemoveInputs();






$(document).ready(function() {
  setTimeout(function() {
    $('.alert').fadeOut('fast');
  }, 4000);
});















var elem = document.createElement('script');
elem.src = 'https://quantcast.mgr.consensu.org/cmp.js';
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
(function() {
  var gdprAppliesGlobally = false;
  function addFrame() {
    if (!window.frames['__cmpLocator']) {
      if (document.body) {
        var body = document.body,
        iframe = document.createElement('iframe');
        iframe.style = 'display:none';
        iframe.name = '__cmpLocator';
        body.appendChild(iframe);
      } else {
        setTimeout(addFrame, 5);
      }
    }
  }
    addFrame();
    function cmpMsgHandler(event) {
        var msgIsString = typeof event.data === "string";
        var json;
        if(msgIsString) {
        json = event.data.indexOf("__cmpCall") != -1 ? JSON.parse(event.data) : {};
        } else {
        json = event.data;
        }
        if (json.__cmpCall) {
        var i = json.__cmpCall;
        window.__cmp(i.command, i.parameter, function(retValue, success) {
            var returnMsg = {"__cmpReturn": {
            "returnValue": retValue,
            "success": success,
            "callId": i.callId
            }};
            event.source.postMessage(msgIsString ?
            JSON.stringify(returnMsg) : returnMsg, '*');
        });
        }
    }
    window.__cmp = function (c) {
        var b = arguments;
        if (!b.length) {
        return __cmp.a;
        }
        else if (b[0] === 'ping') {
        b[2]({"gdprAppliesGlobally": gdprAppliesGlobally,
            "cmpLoaded": false}, true);
        } else if (c == '__cmp')
        return false;
        else {
        if (typeof __cmp.a === 'undefined') {
            __cmp.a = [];
        }
        __cmp.a.push([].slice.apply(b));
        }
    }
    window.__cmp.gdprAppliesGlobally = gdprAppliesGlobally;
    window.__cmp.msgHandler = cmpMsgHandler;
    if (window.addEventListener) {
        window.addEventListener('message', cmpMsgHandler, false);
    }
    else {
        window.attachEvent('onmessage', cmpMsgHandler);
    }
    })();
    window.__cmp('init', {
        'Language': 'fr',
    'Initial Screen Title Text': 'Le respect de votre vie privée est notre priorité',
    'Initial Screen Reject Button Text': 'Je refuse',
    'Initial Screen Accept Button Text': 'J&#039;accepte',
    'Initial Screen Purpose Link Text': 'Afficher toutes les utilisations prévues',
    'Purpose Screen Title Text': 'Le respect de votre vie privée est notre priorité',
    'Purpose Screen Body Text': 'Vous pouvez configurer vos réglages et choisir comment vous souhaitez que vos données personnelles soient utilisée en fonction des objectifs ci-dessous. Vous pouvez configurer les réglages de manière indépendante pour chaque partenaire. Vous trouverez une description de chacun des objectifs sur la façon dont nos partenaires et nous-mêmes utilisons vos données personnelles.',
    'Purpose Screen Enable All Button Text': 'Consentement à toutes les utilisations prévues',
    'Purpose Screen Vendor Link Text': 'Afficher la liste complète des partenaires',
    'Purpose Screen Cancel Button Text': 'Annuler',
    'Purpose Screen Save and Exit Button Text': 'Enregistrer et quitter',
    'Vendor Screen Title Text': 'Le respect de votre vie privée est notre priorité',
    'Vendor Screen Body Text': 'Vous pouvez configurer vos réglages indépendamment pour chaque partenaire listé ci-dessous. Afin de faciliter votre décision, vous pouvez développer la liste de chaque entreprise pour voir à quelles fins il utilise les données. Dans certains cas, les entreprises peuvent révéler qu&#039;elles utilisent vos données sans votre consentement, en fonction de leurs intérêts légitimes. Vous pouvez cliquer sur leurs politiques de confidentialité pour obtenir plus d&#039;informations et pour vous désinscrire.',
    'Vendor Screen Accept All Button Text': 'Tout Accepter',
    'Vendor Screen Reject All Button Text': 'Tout Refuser',
    'Vendor Screen Purposes Link Text': 'Revenir aux Objectifs',
    'Vendor Screen Cancel Button Text': 'Annuler',
    'Vendor Screen Save and Exit Button Text': 'Enregistrer et quitter',
    'Initial Screen Body Text': 'Nos partenaires et nous-mêmes utilisent différentes technologies, telles que les cookies, pour personnaliser les contenus et les publicités, proposer des fonctionnalités sur les réseaux sociaux et analyser le trafic. Merci de cliquer sur le bouton ci-dessous pour donner votre accord. Vous pouvez changer d’avis et modifier vos choix à tout moment',
    'Initial Screen Body Text Option': 1,
    'Publisher Name': 'Bientôt Chez Soi',
    'Consent Scope': 'service',
    'Publisher Purpose IDs': [1,2,3,4,5],
    'Post Consent Page': 'https://bientotchezsoi.com/charte/',
    'Publisher Logo': 'https://bientotchezsoi.com/wp-content/uploads/2018/01/Logo-Simplificateur.png',
    'UI Layout': 'banner',
    'No Option': false,
    });




function handleSmoothScroll() {
  $(".smooth-scroll-link").click(function(e) {
    e.preventDefault();
    $("html").animate({scrollTop : 0}, 600);
  });
}

handleSmoothScroll();



function formatNumericInput() {
  $(".formatted_numeric_input").find(".form-group").find("input").keyup(function() {
    console.log("Tototototo");
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
      return;
    }
    var input = $(this).val();
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? parseInt( input, 10 ) : 0;
    $(this).val( function() {
      return ( input === 0 ) ? "" : input.toLocaleString( "fr-FR" );
    });
  });
}


formatNumericInput();









function handleCustomerChoicesModal() {
  var links = $(".custom-ajax-link");
  $(links).click(function(){
    var dynamicContentContainer = $(".custom-modal-container-v2 .modal-content-v2");
    dynamicContentContainer.empty();
    var demandId = $(this).data("demand");
    var propertyId = $(this).data("property");
    var url = $(this).data("url");
    var userAction = $(this).data("action");
    $(".custom-modal-container-v2").removeClass("hidden");

    $.ajax({
      data: {demand_id: demandId, prop_id: propertyId, user_action: userAction},
      method: "GET",
      url: url,
      success: function(data) {
        console.log(data);
        dynamicContentContainer.append(data);
      },
    })

  });
}

function handleCustomerChoicesModalClosing() {
  var triggers = $(".modal-closer-v2");
  $(triggers).click(function(){
    $(".custom-modal-container-v2").addClass("hidden");
  });
}

handleCustomerChoicesModal();
handleCustomerChoicesModalClosing();








