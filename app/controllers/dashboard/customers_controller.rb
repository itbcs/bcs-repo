require 'rest-client'

class Dashboard::CustomersController < ApplicationController
  before_action :authenticate_customer!


  def messages
    @customer = current_customer
    @customer_chatrooms = Chatroom.where(customer_id: @customer.id.to_s)

    @unseen_messages = {}
    @customer_chatrooms.each do |chatroom|
      unseen_messages = chatroom.chat_messages.where(seen_by_customer: false)
      if unseen_messages.to_a != []
        @unseen_messages[chatroom] = []
        @unseen_messages[chatroom] << unseen_messages.to_a
      else
        puts "Empty results"
      end
    end

    puts @unseen_messages
    render :messages
  end



  def partnerships
    @customer = current_customer
    @demand = current_customer.demands.last
    if @customer.status == "expert"
      @partners = Partner.where(targets: "EXPERT")
    elsif @customer.status == "primo"
      @partners = Partner.where(targets: "PRIMO")
    elsif @customer.vip
      @partners = Partner.where(targets: "VIP")
    end
    render :partnerships
  end


  def home
    if session[:bcs_customer_id]
      handle_temporary_customer
    else
      @customer = current_customer
      @demand = @customer.v2_demands.last
    end
    if @demand
      handle_vip_customer(@customer,@demand)
      handle_member_status(@customer,@demand)
    end
    if !@customer.has_status?
      if @demand
        handle_member_enum_status(@customer,@demand)
      end
    end
    @target_posts = @demand ? handle_customer_blog_posts(@customer) : Post.all
    puts "CUSTOMER HOME BLOG ARTICLES"
    puts @target_posts.size
    puts "***************************"
    @customer_question = CustomerQuestion.new()
    @proverb = Proverb.all.sample

    if session[:modal_seen_by_customer]
      @modal_seen_by_customer = true
    else
      @modal_seen_by_customer = false
    end

    render :home
  end


  def handle_customer_blog_posts(customer)
    if customer.status
      puts "**** CUSTOMER STATUS ****"
      puts customer.status
      puts "*************************"
      @posts = Post.where(targets: { '$regex': customer.status })
    else
      @posts = Post.all
    end
    @targets = @posts.to_a.map{|post| post.targets}
    puts "**** TARGETS ****"
    puts @targets.inspect
    return @posts
  end

  def handle_member_status(customer,demand)
    if demand[:destination] == "6"
      customer.status = "investisseur"
      customer.save
    elsif demand[:actual_situation] == "11"
      customer.status = "primo"
      customer.save
    elsif demand[:actual_situation] == "12"
      customer.status = "expert"
      customer.save
    elsif demand[:actual_situation] == "13"
      customer.status = "expert"
      customer.save
    end
  end

  def handle_member_enum_status(customer,demand)
    puts "Enter into Member enum status"
    puts customer.inspect
    puts "*****************************"
    if demand[:destination] == "6"
      customer.enum_status = 2
      customer.save
    elsif demand[:actual_situation] == "11"
      customer.enum_status = 1
      customer.save
    elsif demand[:actual_situation] == "12"
      customer.enum_status = 3
      customer.save
    elsif demand[:actual_situation] == "13"
      customer.enum_status = 3
      customer.save
    end
    puts customer.inspect
  end

  def handle_vip_customer(customer,demand)
    if customer.vip == false
      if demand[:budget].first.to_i >= 500000
        customer.vip = true
        customer.save
      end
    elsif customer.vip == true
      if demand[:budget].first.to_i >= 500000
        customer.vip = true
        customer.save
      else
        customer.vip = false
        customer.save
      end
    end
  end

  def handle_temporary_customer
    @temporary_customer = Customer.find(session[:bcs_customer_id])
    if @temporary_customer
      if @temporary_customer.v2_demands.any?
        @demand = @temporary_customer.v2_demands.last.clone
        @demand.remote_browser_screenshot_url = @temporary_customer.v2_demands.last.map_photo_url
        @customer = current_customer
        @customer.v2_demands.push(@demand)
        @demand.status = "pending"
        @demand.save

        @score = Score.create(count: 50, customer: @customer)
        @customer.save
        puts @customer.score.inspect

        puts "***** demand inspect *****"
        puts @demand.inspect
        puts "**************************"

        CustomerMailer.welcome(@customer,@demand).deliver
        Rails.configuration.admin_emails.each do |email|
          AdminMailer.new_demand_recap(@demand,@customer,email,l(@demand.created_at, format: :medium),l(@demand.created_at, format: :count)).deliver
        end

        @temporary_customer.v2_demands.last.destroy
        @temporary_customer.destroy
        session.delete(:bcs_customer_id)
      else
        @customer = current_customer
        @demand = @customer.v2_demands.last
      end
    else
      @customer = current_customer
      @demand = current_customer.v2_demands.last
    end
  end


  def properties
    @customer = current_customer
    @demand = current_customer.v2_demands.last
    render :properties
  end

  def property
    @customer = current_customer
    @demand = current_customer.v2_demands.last
    @property = Property.find(params[:property_id])

    @present_imgs = []
    @imgs_attrs = ["img_1_url", "img_2_url", "img_3_url", "img_4_url", "img_5_url", "img_6_url", "img_7_url", "img_8_url"]
    @imgs_attrs.each do |field|
      puts @property.send("#{field}")
      if !@property.send("#{field}").nil?
        @present_imgs << (field.to_s.remove("img_").to_i)
      end
    end
    @present_imgs

    @chatroom = Chatroom.where(property_id: @property.id.to_s).where(customer_id: @customer.id.to_s).last
    @customer_choice = @property.customer_choices.where(v2_demand: @demand).last
    render :property
  end

  def property_partial
    @property = Property.find(params[:property_id])
    render layout: false
  end

  def selections
    @customer = current_customer
    render :selections
  end

  def advices
    @customer = current_customer
    @target_posts = handle_customer_blog_posts(@customer)
    @demand = current_customer.v2_demands.last
    render :advices
  end

  def vip
    @customer = current_customer
    render :vip
  end

  def profil
    render :profil
  end

  private

  def handle_customer_status(status, customer)
    if status == "investisseur"
      results = Post.all.select{|post| post.targets.split(",").include?(status) if post.targets}
    elsif status == "primo"
      results = Post.all.select{|post| post.targets.split(",").include?(status) if post.targets}
    elsif status == "expert"
      results = Post.all.select{|post| post.targets.split(",").include?(status) if post.targets}
    else
      results = Post.all
    end

    if customer.vip
      vip_posts = Post.all.select{|post| post.targets.split(",").include?(status) if post.targets}
      vip_posts.each do |post|
        results << post
      end
    end
    return results
  end

  def first_demand_budget_value budget_array
    if budget_array
      sanitize_array = budget_array.reject{|budget| budget.empty?}
      if sanitize_array && sanitize_array.size > 0
        if sanitize_array.size == 1
          result = sanitize_array.first.gsub(/[[:space:]]/,'').to_i
        elsif sanitize_array.size == 2
          result = sanitize_array.first.gsub(/[[:space:]]/,'').to_i
        end
      end
      return result
    end
  end

  def finished_demand_notification(demand, customer)
    if !demand.sending_customer_notification
      JobTestMailer.finished_demand_customer_notif(customer).deliver
      demand.sending_customer_notification = true
      demand.save
    end
  end

  def find_formatted_survey_label(demand)
    labels = []
    demand.attributes.each do |attr|
      survey = Survey.where(demand_attribute: attr.first.to_s).last
      (labels << survey.resume) if survey
    end
    labels
  end

  def test_on_label(demand)
    labels = []
    @demand_filter = demand.attributes.map{|attr| attr if (attr != nil || attr != "")}
    @demand_filter.each do |attribute|
      labels << attribute
    end
    labels
  end



end
