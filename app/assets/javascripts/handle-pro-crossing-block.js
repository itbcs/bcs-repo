function handleProCrossingCloser() {
  $("#pro-crossing-closer-trigger").click(function(){
    $("section.pro-crossing").addClass("hidden");
  });
}

$(document).ready(function() {
  handleProCrossingCloser();
});
