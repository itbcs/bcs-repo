class XmlParser

  def get_file(ftp)
    count = 0
    ftp.chdir("./")
    ftp.chdir("xml")
    puts ftp.list
    xmls = ftp.nlst('*.xml')
    puts "€€€€€€€€€€€€€€€€€€€€"
    puts xmls.size
    puts "€€€€€€€€€€€€€€€€€€€€"
    xmls.first(3).each do |filename|
      binary_file = ftp.getbinaryfile(filename,nil)
      response_count = format_xml(binary_file,filename)
    end
  end

  def format_xml(binary_file,filename)
    datas = []
    count = 0
    find_agencies = []
    formatted_agency_name = filename.to_s.strip.remove(".xml")
    puts "**************"
    puts "Agency XML File"
    puts "**************"
    puts "#{formatted_agency_name}"
    xml_string_file = Nokogiri::XML(binary_file)
    properties = xml_string_file.xpath("//ad")
    puts "Nombre de biens : #{properties.size}"

    properties.first(2).each do |property|
      empty_property_hash = {}
      empty_property_hash[:attributes] = {}
      empty_property_hash[:images_array] = []

      @test = property.xpath("./*")
      puts "**************"
      puts "Agency XML File - property"
      puts "**************"



      property.xpath("./*").each do |tag|



        if tag.name == "agenceVille"
          @agency_city = tag.content
        elsif tag.name == "email"
          @agency_email = tag.content
        elsif tag.name == "agence"
          @agency_name = tag.content
          @agency_siret = tag.attributes["siret"].value
          @agency_url = tag.attributes["url"].value
        elsif tag.name == "agenceLocation"
          @agency_cp = tag.attributes["cp"].value
          @agency_lat = tag.attributes["latitude"].value
          @agency_lng = tag.attributes["longitude"].value
        elsif tag.name == "agenceLogo"
          @agency_logo = tag.content
        elsif tag.name == "telephone"
          @agency_phone = tag.content
        elsif tag.name == "id"
          puts "#{tag.attributes['dateEnr']} et #{tag.attributes['dateMaj']}"
        elsif tag.name == "images"
          tag.xpath("./*").map{|xtag| empty_property_hash[:images_array].push(xtag.content)}
        end
        empty_property_hash[tag.name.to_sym] = tag.content.to_s.strip
        if tag.attributes != {}
          empty_property_hash[:attributes][tag.name.to_sym] = tag.attributes.map{|key, value| [value.name, value.value]}
        end


      end
      puts "Full hash return"
      empty_property_hash.each do |key2, value2|
        puts "#{key2} - #{value2}"
      end
      puts "----------------------------"
      datas << empty_property_hash
      count += 1
      # puts "*******************"
      # puts empty_property_hash
      # puts "*******************"
    end

    datas.each do |property_hash|
      property_hash.each do |key,value|
        if key == "titre"
          @property_title = value
        elsif key == "reference_client"
          @property_agency = value
        elsif key == "type_offre"
          @property_offer_type = value
        elsif key == "corps"
          @property_description = value
        elsif key == "prix"
          @property_price = value
        elsif key == "type_bien"
          @property_type = value
        end
      end
      # @property_attributes = {title: @property_title, offer_type: @property_offer_type, property_type: @property_type}
      # @temporary_property = TestProperty.new(@property_attributes)
    end

    #agency_matching = TestAgency.where(hektor_reference: formatted_agency_name)
    #if agency_matching == []
    #  @attributes = {hektor_reference: formatted_agency_name, city: @agency_city, name: @agency_name, email: @agency_email, location_cp: @agency_cp, location_lng: @agency_lng, location_lat: @agency_lat, siret: @agency_siret, website: @agency_url, logo: @agency_logo, source: "Hektor", phone: @agency_phone}
    #  @agency = TestAgency.new(@attributes).save
    #end

    return count
  end

end
