class Admin::AgenciesController < ApplicationController
  http_basic_authenticate_with name: ENV["HTTP_AUTH_LOGIN"], password: ENV["HTTP_AUTH_PWD"]


  def generate_xls_file
    if params[:collection]
      if params[:collection] == "activated"
        @agencies = Agency.all.where(activated: true)
        @filename = "activated-agencies"
      elsif params[:collection] == "partner"
        @agencies = Agency.all.where(partner: true)
        @filename = "partner-agencies"
      end
    end
    respond_to do |format|
      format.xlsx {
        response.headers['Content-Disposition'] = "attachment; filename='#{@filename}.xlsx'"
      }
    end
  end


  def index
    @agencies = Agency.all
    @partner_agencies = Agency.where(activated: true)
    @true_partner_agencies = Agency.where(partner: true)
    @platform_agencies = Agency.where(source: {'$ne': "hektor"})
    @agencies_with_demands = Agency.where(activated: true).where(demand_ids: {'$ne': []})


    @uptodate_partner_agencies = Agency.where(partner: true)
    @uptodate_activated_agencies = Agency.where(activated: true)
    @uptodate_hektor_agencies = Agency.where(source: "hektor")
    @uptodate_pending_agencies = Agency.where(activated: false)

    render "admin/agencies/index", layout: "admin"
  end


  def active
    @uptodate_activated_agencies = Agency.where(activated: true).order_by(created_at: :desc).page params[:page]
    render "admin/agencies/active", layout: "admin"
  end

  def partner
    @uptodate_partner_agencies = Agency.where(partner: true).order_by(created_at: :desc).page params[:page]
    render "admin/agencies/partner", layout: "admin"
  end

  def flux
    @uptodate_hektor_agencies = Agency.where(source: "hektor").order_by(created_at: :desc).page params[:page]
    render "admin/agencies/flux", layout: "admin"
  end

  def waiting
    @uptodate_pending_agencies = Agency.where(activated: false).where(source: {'$ne': "hektor"}).order_by(created_at: :desc).page params[:page]
    render "admin/agencies/waiting", layout: "admin"
  end



  def add_value_for_default_field
    @empty_values = Agency.where(partner: true).where(activated: nil)
    @empty_values.each do |agency|
      agency.activated = true
      agency.save
    end
  end

  def show
    @agency = Agency.find(params[:id].to_s)
    @properties = @agency.properties.where(offer_type: "Vente")
    @paginate_properties = @agency.properties.page params[:page]
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    smallest_price_property = (@properties.sort_by{|prop| prop.price }).first
    if smallest_price_property
      (@agency.min_price = smallest_price_property.price.to_i) if smallest_price_property.price
      @agency.save
    end
    render "admin/agencies/show", layout: "admin"
  end

  def destroy
    @agency = Agency.find(params[:id])
    @agency.destroy
    redirect_to admin_active_agencies_path
  end

  # def agency_activation
  #   @agency = Agency.find(params[:id])
  #   @agency.partner = true
  #   if agency.save
  #     redirect_to admin_agencies_path, notice: "Agence activé"
  #   else
  #     redirect_to admin_agencies_path, alert: "Problème pour activer l'agence"
  #   end
  # end

  def activate_agency
    @agency = Agency.find(params[:agency_id])

    result = AgencyActivation.new(@agency).call
    demands = result.data if result.success?

    puts "----------"
    puts "Controller service return demands"
    puts demands
    puts "----------"

    status = handle_results(@agency, demands.uniq)

    # puts status
    # puts status.class

    AgencyMailer.validated_agency_welcome(@agency).deliver
    if status.nil?
      redirect_to "/admin/agencies/active", layout: "admin", alert: "Agence activée, néanmoins aucune demande n'a été trouvée !!"
    else
      redirect_to "/admin/agencies/active", layout: "admin", notice: "Agence activée, nous avons trouvés #{demands.uniq.size} demandes !!"
    end
  end

  private

  def handle_results(agency, demands)

    puts "--------------------"
    puts "Handle results demands"
    puts demands.inspect
    puts "--------------------"

    if !demands.compact.empty?
      demands.each do |demand|
        if !agency.v2_demands.include?(demand)
          agency.v2_demands.push(demand)
        end
      end
      agency.save
      return true
    else
      return nil
    end
  end

end
