$(document).ready(function(){
  var currentLocation = location.pathname;
  if (currentLocation === "/dashboard/customer/partnerships") {
    console.log("You are on the partnerships page - smoothscroll desactivated !!");
  }
  if (currentLocation !== "/dico-de-kenotte") {
    $('a[href*="#"]')
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
      var dataToggle = $(this).data("toggle");
      if (
        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
        &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (!dataToggle) {
          console.log("Click on link");
          if (target.length) {
            event.preventDefault();
            $('html, body').animate({
              scrollTop: (target.offset().top)
            }, 300);
          }
        }
      }
    });
  }
})
