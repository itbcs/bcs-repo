class ChangePropertyStatus

  def initialize property
    @property = property
  end

  def call
    change_status
    service_success
  end

  private

  def change_status
    puts "**** Enter into ChangePropertyStatus service ****"
    puts @property.inspect
    @property.status = "5"
    @property.sold_at = Time.now
    if @property.save
      puts "!!!! Property successfully saved with status => #{@property.status}"
    end
    puts "*************************************************"
  end

  def service_success
    ServiceSuccess.new(@property)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
