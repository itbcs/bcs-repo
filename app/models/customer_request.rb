class CustomerRequest
  include Mongoid::Document
  include Mongoid::Timestamps

  field :email, type: String
  field :customer_action, type: String

end
