function handleChatroomAutoscroll() {
  var htmlElement = $("#chatroom");
  var messageContainerAttributes = $("#messages");
  if ($(messageContainerAttributes).length > 0) {
    var rightToScroll = $(messageContainerAttributes).data("scroll");
  }
  if ($(htmlElement).length > 0) {
    if (rightToScroll) {
      console.log("Chatroom dom element present and messages count > 0");
      $("#chatroom").animate({scrollTop: $('#chatroom')[0].scrollHeight}, "fast");
    }
  }
}


$("#websocket-link").click(function(){
  setTimeout(handleChatroomAutoscroll, 500);
});
