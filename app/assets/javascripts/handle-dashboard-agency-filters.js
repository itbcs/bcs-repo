function HandleDashboardAgencyFilters() {
  $('.filter').find('.content').each(function(index, element){
    $(element).click(function(){

      var radioInputContainerLarge = $(element).siblings('.radio-input-form-container-large'),
          radioInputContainerNormal = $(element).siblings('.radio-input-form-container-normal'),
          selectInputContainer = $(element).siblings('.select-input-form-container'),
          inputContainer = $(element).siblings('.text-input-form-container');

      if (($(inputContainer).css('height') == "0px") ||
          ($(radioInputContainerLarge).css('height') == "0px") ||
          ($(radioInputContainerNormal).css('height') == "0px") ||
          ($(selectInputContainer).css('height') == "0px")) {

        $(inputContainer).css('height', '100px');
        $(radioInputContainerLarge).css('height', '140px');
        $(radioInputContainerNormal).css('height', '100px');
        $(selectInputContainer).css('height', "100px");
      }
      else {
        $(inputContainer).css('height', '0px');
        $(radioInputContainerLarge).css('height', '0px');
        $(radioInputContainerNormal).css('height', '0px');
        $(selectInputContainer).css('height', "0px");
      }
    });
  });
};

HandleDashboardAgencyFilters();


function AddActiveClassOnFilters() {
  var allHeadersFilters = $('.list-block').find('thead').find('.sort');

  $(allHeadersFilters).each(function(index, element){
      $(element).click(function(){
        $(allHeadersFilters).not(element).removeClass('active-filter');
        $(this).addClass('active-filter');
      });
  });
};
AddActiveClassOnFilters();


function formatNumericFiltersInput() {
  $(".formatted_numeric_filter_input").find("input").keyup(function() {
    var selection = window.getSelection().toString();
    if ( selection !== '' ) {
      return;
    }
    var input = $(this).val();
    var input = input.replace(/[\D\s\._\-]+/g, "");
    input = input ? input : "";
    $(this).val( function() {
      return (input) ? input : "";
    });
  });
};
formatNumericFiltersInput();

function RestartFilters() {
  $('.bcs-filters-button-restart-filters').click(function(){
    window.location.reload(true);
  });
};

RestartFilters();

$(document).ready(function(){
  $(document).ajaxSend(function() {
    $(".demands-loader-container").fadeIn(300);
    $("#demands").fadeOut(300);
  });
});

function handleFiltersWithAjax() {
  $('#filters-form').submit(function(event) {
  $(".demands-loader-container").removeClass("hidden");
  var button = $(form).find("input[type='submit']");
  var form = $(this),
      budgetMin = $(form).find('#min-budget-input'),
      budgetMax = $(form).find('#max-budget-input'),
      surfaceMin = $(form).find('#min-surface-input'),
      surfaceMax = $(form).find('#max-surface-input'),
      roomMin = $(form).find('#min-rooms-input'),
      roomMax = $(form).find('#max-rooms-input'),
      house = $(form).find('#house-input'),
      appartment = $(form).find('#appartment-input'),
      ground = $(form).find('#ground-input'),
      postalCodes = $(form).find('#postal-codes-input'),
      viewed = $(form).find('#demands-viewed-input'),
      unviewed = $(form).find('#demands-unviewed-input'),
      createdAt = $(form).find('#created-at-input'),
      url = $(form).attr("action"),
      type = $(form).attr("method");

  event.preventDefault();
  // var customParams = { budgetMin: $(budgetMin).val(),
  //                     budgetMax: $(budgetMax).val(),
  //                     surfaceMin: $(surfaceMin).val(),
  //                     surfaceMax: $(surfaceMax).val(),
  //                     roomMin: $(roomMin).val(),
  //                     roomMax: $(roomMax).val(),
  //                     house: $(house).val(),
  //                     appartment: $(appartment).val(),
  //                     ground: $(ground).val(),
  //                     postalCodes: $(postalCodes).val(),
  //                     viewed: $(viewed).val(),
  //                     unviewed: $(unviewed).val(),
  //                     createdAt: $(createdAt).val() };
    $.ajax({
      url: url,
      type: type,
      data: $(form).serialize(),
      success: function(data) {
        console.log(data);
        $(".ajaxified-return-table").replaceWith(data);
        $("#demands").fadeIn(300);
        $(".demands-loader-container").addClass("hidden");
        handleFiltersWithAjax();
        $(button).attr("disabled", false);
      },
      error: function(data) {
        alert("Fatal Error");
      },
    });
  });
}
handleFiltersWithAjax();
