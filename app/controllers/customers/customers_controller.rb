class Customers::CustomersController < ApplicationController
  before_action :authenticate_customer!, only: [:dashboard]

  def dashboard
    if session[:bcs_customer_id]
      @temporary_customer = Customer.find(session[:bcs_customer_id])
      @demand = @temporary_customer.demands.last.clone
      @customer = current_customer
      @customer.demands.push(@demand)
      @reward = Reward.new(badge: "Chaton timide", score: 50)
      @customer.rewards.push(@reward)
      @demand.completed = true
      @demand.save
      @customer.save
      @label_test = test_on_label(@demand)
      (@labels = find_formatted_survey_label(@demand)) if @demand
      @temporary_customer.destroy if @temporary_customer
      session.delete(:bcs_customer_id)
    else
      @customer = current_customer
      if !@customer.rewards.any?
        @reward = Reward.new(badge: "Chaton timide", score: 50)
        @customer.rewards.push(@reward)
      end
      @customer.save
      @demand = @customer.demands.last
      (@label_test = test_on_label(@demand)) if @demand
      (@labels = find_formatted_survey_label(@demand)) if @demand
    end
    if @customer.sign_in_count < 2
      # CustomerMailer.welcome.deliver_now
    end
    @customer_adapted_blog_post = Post.all.where(name: "Avant / après la visite d’un bien : le mémo !").last
    render "dashboard/connected/dashboard"
  end

  private

  def find_formatted_survey_label(demand)
    labels = []
    demand.attributes.each do |attr|
      survey = Survey.where(demand_attribute: attr.first.to_s).last
      (labels << survey.resume) if survey
    end
    labels
  end

  def test_on_label(demand)
    labels = []
    @demand_filter = demand.attributes.map{|attr| attr if (attr != nil || attr != "")}
    @demand_filter.each do |attribute|
      labels << attribute
    end
    labels
  end

end
