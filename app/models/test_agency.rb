# frozen_string_literal: true

# class Agency
class TestAgency
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :email, type: String
  field :city, type: String
  field :source, type: String
  field :location_cp, type: String
  field :location_lat, type: String
  field :location_lng, type: String
  field :siret, type: String
  field :website, type: String
  field :phone, type: String
  field :logo, type: String
  field :hektor_reference, type: String
  field :source, type: String
  field :postal_codes, type: Array

  # TODO: Gerer par denormalisation la liste des ids des demandes associes a ses offres
end
