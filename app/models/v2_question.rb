class V2Question
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  has_many :v3_answers

  field :demand_attribute, type: String
  field :label, type: String
  field :required, type: Boolean
  field :checkable_on_criteria, type: Boolean
  field :client_side_condition, type: Boolean
  field :position, type: Integer
  field :kenotte, type: String
  field :kenotte_avatar, type: String
  field :question_image, type: String
  field :hint_label, type: String
  field :hint_image, type: String
  field :kenotte_advice, type: String

end
