class Apifluxes::PropertiesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_employee!

  def show
    if params[:from_agency]
      @from_agency = true
    end
    @prop_id = params[:id]
    @query_url = "https://bcs-api.herokuapp.com/properties/#{@prop_id}/?api_key=ea5f04afe43b502a653a9a27b3aa9688"
    @api_response = RestClient.get(@query_url)
    @api_status_code = @api_response.code
    @response = JSON.parse(@api_response.body.to_s)
    @french_response = generate_french_fields_names(@response)

    render :show, layout: false
  end

  def index
    @start_page = true
  end

  def create
    filtered_params = handle_params
    query_url = generate_api_url_query(filtered_params)
    @query_resume = handle_query_resume(filtered_params)
    @query_resume_field_count = @query_resume.size
    @api_response = RestClient.get(query_url)
    @api_status_code = @api_response.code
    @responses = JSON.parse(@api_response.body.to_s)

    if params["page"]
      @all_items = session[:apifluxes_results_count]
      puts "Retrieve result count from session hash"
    else
      puts "Retrieve result count from api json"
      @all_items = JSON.parse(RestClient.get(generate_api_url_query_fullresponse(handle_fullresponse_params)).body.to_s).size.to_f
      session[:apifluxes_results_count] = @all_items
    end

    @number_of_pages = (@all_items / 25.0).ceil
    @result_count = @all_items
    @request = request.parameters.except("controller","action","_method","utf8","authenticity_token")
    @query = request.parameters.except("authenticity_token","_method","utf8","action","controller","page").delete_if { |k, v| v.empty? }

    if @query.has_key?("query")
      @query = @query["query"]
    end

    if @responses.is_a?(Hash)
      if @responses["data"]
        @too_many_objects = true
        @result_count = 0
      end
      if @responses["empty"]
        @empty_collection = true
        @result_count = 0
      end
    end

    if ("0".."9").to_a.include?(query_url.chars.last)
      @current_page = query_url.chars.last.to_i
    end

    if params["page"]
      @current_page = params["page"]
    end

    if @number_of_pages == 1
      @hide_pagination = true
    end

    if @current_page && @number_of_pages
      @pagination_range = (1..@number_of_pages.to_i)
    end
    render :index, layout: false
  end

  private

  def generate_french_fields_names(response)
    @french_object = []
    @filtered_fields = response.except("corps","_id","agency_id","title","images_array","hidden_attributes","price","price_int","created_at","area","zipcode","property_type_code","warrant_id","human_city","city","country","county")
    @filtered_fields.each do |key,value|
      if value != nil
        if key.to_sym == :build_year
          @french_object << {label: "Année de construction", content: "#{value}"}
        elsif key.to_sym == :fees
          content = "#{ActionController::Base.helpers.number_with_delimiter(value.to_i, locale: :fr)} €"
          @french_object << {label: "Honoraires", content: content}
        elsif key.to_sym == :heating_energy
          @french_object << {label: "Type de chauffage", content: "#{value}"}
        elsif key.to_sym == :heating_format
          @french_object << {label: "Type de chauffage", content: "#{value}"}
        elsif key.to_sym == :source
          @french_object << {label: "Source", content: "#{value}"}
        elsif key.to_sym == :nb_room
          @french_object << {label: "Nombre de pièces", content: "#{value}"}
        elsif key.to_sym == :status_code
          content = status_code_humanized(value.to_i)
          @french_object << {label: "Statut", content: content}
        elsif key.to_sym == :kitchen_type
          @french_object << {label: "Type de cuisine", content: "#{value}"}
        end
      end
    end
    puts @french_object.inspect
    @french_object
  end

  def handle_query_resume(filtered_params)
    puts "Handle resume params"
    @query_resume = []
    filtered_params.each do |fp|
      if fp[:field].to_sym == :city
        @query_resume << {field: "Ville", value: "#{fp[:value]}"}
      elsif fp[:field].to_sym == :zipcode
        @query_resume << {field: "Code postal", value: "#{fp[:value]}"}
      elsif fp[:field].to_sym == :min_price
        @query_resume << {field: "Prix min", value: "#{ActionController::Base.helpers.number_with_delimiter(fp[:value], locale: :fr)}", unit: "€"}
      elsif fp[:field].to_sym == :max_price
        @query_resume << {field: "Prix max", value: "#{ActionController::Base.helpers.number_with_delimiter(fp[:value], locale: :fr)}", unit: "€"}
      elsif fp[:field].to_sym == :min_area
        @query_resume << {field: "Surface min", value: "#{ActionController::Base.helpers.number_with_delimiter(fp[:value], locale: :fr)}", unit: "m²"}
      elsif fp[:field].to_sym == :max_area
        @query_resume << {field: "Surface max", value: "#{ActionController::Base.helpers.number_with_delimiter(fp[:value], locale: :fr)}", unit: "m²"}
      end
    end
    @query_resume
  end

  def generate_api_url_query(filtered_params)
    start_url = "https://bcs-api.herokuapp.com/properties"
    if filtered_params.size > 0
      filtered_params.each_with_index do |item,index|
        if index == 0
          query = "/?#{item[:field]}=#{item[:value].parameterize}"
          start_url += query
        else
          query = "&#{item[:field]}=#{item[:value].parameterize}"
          start_url += query
        end
      end
      if !params["page"]
        start_url += "&api_key=ea5f04afe43b502a653a9a27b3aa9688&page=1"
      else
        start_url += "&api_key=ea5f04afe43b502a653a9a27b3aa9688"
      end
    else
      start_url += "/?api_key=ea5f04afe43b502a653a9a27b3aa9688"
    end
    start_url
  end

  def generate_api_url_query_fullresponse(filtered_params)
    start_url = "https://bcs-api.herokuapp.com/properties"
    if filtered_params.size > 0
      filtered_params.each_with_index do |item,index|
        if index == 0
          query = "/?#{item[:field]}=#{item[:value].parameterize}"
          start_url += query
        else
          query = "&#{item[:field]}=#{item[:value].parameterize}"
          start_url += query
        end
      end
      start_url += "&api_key=ea5f04afe43b502a653a9a27b3aa9688"
    else
      start_url += "/?api_key=ea5f04afe43b502a653a9a27b3aa9688"
    end
    start_url
  end


  def handle_fullresponse_params
    if params[:query] || params["query"]
      @query = params[:query]
    end
    @params_with_query_options = []
    sanitize_params = params.except("utf8","button","controller","action","page","query","authenticity_token").delete_if { |k, v| v.empty? }
    sanitize_params.each do |k,v|
      @params_with_query_options << {field: "#{k}", value: "#{v}"}
    end

    if @query
      @query.each do |k,v|
        @params_with_query_options << {field: "#{k}", value: "#{v}"}
      end
    end

    @params_with_query_options
  end

  def handle_params
    if params[:query] || params["query"]
      @query = params[:query]
    end
    @params_with_query_options = []
    sanitize_params = params.except("utf8","button","controller","action","query","authenticity_token").delete_if { |k, v| v.empty? }
    sanitize_params.each do |k,v|
      @params_with_query_options << {field: "#{k}", value: "#{v}"}
    end

    if @query
      @query.each do |k,v|
        @params_with_query_options << {field: "#{k}", value: "#{v}"}
      end
    end

    @params_with_query_options
  end

end
