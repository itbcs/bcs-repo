class AuthorizeAgencyApiRequest
  prepend SimpleCommand

  def initialize(headers = {})
    @headers = headers
  end

  def call
    agency
  end

  private

  attr_reader :headers

  def agency
    @agency ||= Agency.find(decoded_auth_token[:agency_id]) if decoded_auth_token
    @agency || errors.add(:token, 'Invalid token') && nil
  end

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  def http_auth_header
    if headers['Authorization'].present?
      return headers['Authorization'].split(' ').last
    else
      errors.add(:token, 'Missing token from headers')
    end
    nil
  end
end
