class TestProperty
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String
  field :offer_type, type: String
  field :description, type: String
  field :price, type: String
  field :property_type, type: String
end
