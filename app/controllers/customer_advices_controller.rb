class CustomerAdvicesController < ApplicationController

  def create
    @demand = Demand.find(safe_params[:demand])
    @property = Property.find(safe_params[:property])
    @customer_advice = CustomerAdvice.new()
    @customer_advice.demand = @demand
    @customer_advice.property = @property
    @customer_advice.like = safe_params[:like]
    @customer_advice.comment = safe_params[:comment]
    @customer_advice.score = safe_params[:score]
    if @customer_advice.save
      redirect_to dashboard_customer_properties_path, notice: "Votre avis à été pris en compte"
    else
      render dashboard_customer_new_advice_path, alert: "Problèmes dans le questionnaire"
    end
  end

  def new
    @demand_id = params[:demand_id]
    @property_id = params[:property_id]
    @customer_advice = CustomerAdvice.new()
    render "dashboard/customer_advices/new"
  end

  private

  def safe_params
    params.require(:customer_advice).permit(:like, :comment, :score, :property, :demand)
  end

end
