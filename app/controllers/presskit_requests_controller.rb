class PresskitRequestsController < ApplicationController

  def create
    @presskit_request = PresskitRequest.new(safe_params)
    if @presskit_request.save
      redirect_to presse_path, notice: "Votre demande à été prise en compte"
    else
      render presse_path, alert: "Nous avons rencontré un problème"
    end
  end

  def safe_params
    params.require(:presskit_request).permit(:firstname, :lastname, :email, :phone, :object, :enterprise, :message)
  end

end
