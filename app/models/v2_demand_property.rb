class V2DemandProperty
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :property, optional: true
  belongs_to :v2_demand, optional: true

end
