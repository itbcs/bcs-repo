class PdfsController < ApplicationController
  include PdfHelper

  def rent_agency
    number = (1..1000000).sample
    respond_to do |format|
     format.html
     format.pdf do
       render pdf: "location-#{number}",
       template: "pdfs/rent_agency.html.haml",
       layout: 'pdf.html.haml',
       margin:  {top: 0,bottom: 0,left: 0,right: 0}
     end
    end
  end

  def aristid_survey
    @webhook = Webhook.find("5da99f7d82badb0007a534f9")
    puts @webhook.inspect
    number = rand(1..1000000)
    puts number
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "aristid-#{number}",
        template: "pdfs/aristid_survey.html.haml",
        layout: 'pdf.html.haml',
        margin:  {top: 10,bottom: 0,left: 0,right: 0}
      end
    end
  end

  def aristid_survey_agency
    @webhook = Webhook.find("5dc02e5648e9380007dcd361")
    number = rand(1..1000000)
    puts number
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "aristid-#{number}",
        template: "pdfs/aristid_survey_agency.html.haml",
        layout: 'pdf.html.haml',
        margin:  {top: 10,bottom: 0,left: 0,right: 0}
      end
    end
  end

  def show
    @demand = V2Demand.find(params[:demand_id])
    if @demand.v2_demand_comments?
      @comments = @demand.v2_demand_comments.where(important: true)
      if @comments.size > 0
        @important_comments = @comments
      end
    end
    # pdf = render_to_string pdf: "votre-demande-#{@demand.id.to_s.last(5).downcase}", template: "pdfs/show.html.haml", encoding: "UTF-8"
    # save_path = Rails.root.join('pdfs','filename.pdf')
    # File.open(save_path, 'wb') do |file|
    #   file << pdf
    # end

    user_progression = UserProgression.new(@demand).call
    if user_progression.success?
      @user_progression = user_progression.data
    end

    project_progression = ProjectProgression.new(@demand).call
    if project_progression.success?
      @project_progression = project_progression.data
    end

    respond_to do |format|
     format.html
     format.pdf do
       render pdf: "votre-demande-#{@demand.id.to_s.last(5).downcase}",
       template: "pdfs/show-v2.html.haml",
       layout: 'pdf.html.haml',
       margin:  {top: 0,bottom: 0,left: 0,right: 0}
     end
    end
  end

  def admin_show
    if params[:model]
      @demand = OldPlatformDemand.find(params[:demand_id])
    else
      @demand = V2Demand.find(params[:demand_id])
      if @demand.v2_demand_comments.any?
        @demand_comments = @demand.v2_demand_comments
      end
    end

    user_progression = UserProgression.new(@demand).call
    if user_progression.success?
      @user_progression = user_progression.data
    end

    project_progression = ProjectProgression.new(@demand).call
    if project_progression.success?
      @project_progression = project_progression.data
    end

    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "votre-demande-#{@demand.id.to_s.last(5).downcase}",
        template: "pdfs/admin-show-v2.html.haml",
        layout: 'pdf.html.haml',
        margin:  {top: 0,bottom: 0,left: 0,right: 0}
      end
    end
  end



  def v2_demand_recap(demand)
    @responses = {}
    @sanitize_keys = demand.fields.except("_id","created_at","updated_at","version","modifier_id","customer_id","property_ids","map_photo","browser_screenshot","flat_state","house_state","travaux","flat_budget","terrain_budget","terrain_surface","house_construction_surface","actual_pros","heart_stroke","criterias","estimation_request","search_formatted_area","search_zipcodes_area","completed","activated","deactivated","refused","status","sending_customer_recap_email","from_oldplatform","activation_date","old_platform_created_at","deactivation_comment","deactivation_reason","email","age","firstname","gender").keys
    @sanitize_keys.each do |key|
      @demand_value = demand.send("#{key}")
      @question_label_resume = Question.where(demand_attribute: key.to_sym).last.agency_resume
      @theme = Question.where(demand_attribute: key).last.theme

      if @responses.has_key?(@theme)
        if @demand_value
          @responses[@theme].push([@question_label_resume,@demand_value])
        end
      else
        @responses[@theme] = []
        if @demand_value
          @responses[@theme].push([@question_label_resume,@demand_value])
        end
      end

    end
    @responses["Le projet"].push(["Zone de recherche", @demand.search_zipcodes_area])
    @responses["Le projet"].push(["Critères importants", @demand.criterias])
    return @responses
  end



  def populate_answers(demand)
    @attributes_by_theme = {}
    themes = []
    if demand.property_type == "Une maison"
      values = [6,12,13,14,15,17,23,24,27,28,31]
      @surveys = Survey.all.sort_by{|s| s.position}.reject{|s| values.include?(s.position.to_i)}
    elsif demand.property_type == "Un appartement"
      values = [5,12,13,14,15,16,17,25,32]
      @surveys = Survey.all.sort_by{|s| s.position}.reject{|s| values.include?(s.position.to_i)}
    elsif demand.property_type == "Un terrain"
      values = [5,6,7,8,9,10,11,13,16,18,19,20,21,22,23,24,26,27,28,29,30,31,32]
      @surveys = Survey.all.sort_by{|s| s.position}.reject{|s| values.include?(s.position.to_i)}
    else
      @surveys = Survey.all
    end


    @surveys.each do |survey|
      (themes << survey.theme) unless themes.include?(survey.theme)
    end
    themes.each do |theme|
      label_plus_attribute = []

      if demand.property_type == "Une maison"
        values = [6,12,13,14,15,17,23,24,27,28,31]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      elsif demand.property_type == "Un appartement"
        values = [5,12,13,14,15,16,17,25,32]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      elsif demand.property_type == "Un terrain"
        values = [5,6,7,8,9,10,11,13,16,18,19,20,21,22,23,24,26,27,28,29,30,31,32]
        @theme_surveys = Survey.all.where(theme: theme).reject{|s| values.include?(s.position)}
      else
        @theme_surveys = Survey.all
      end

      @theme_surveys.each do |survey|
        if survey.demand_attribute == ("budget") || survey.demand_attribute == ("terrain_budget") || survey.demand_attribute == ("house_budget")
          (attribute = (demand[survey.demand_attribute.to_sym].map {|attr| format_number(attr)}).compact) if demand[survey.demand_attribute.to_sym] != nil
        elsif survey.demand_attribute == "surface" || survey.demand_attribute == "terrain_surface" || survey.demand_attribute == "principal_space" || survey.demand_attribute == "actual_housing_area"
          (attribute = (demand[survey.demand_attribute.to_sym].map {|attr| format_area(attr)}).compact) if demand[survey.demand_attribute.to_sym] != nil
        else
          if demand[survey.demand_attribute.to_sym].is_a?(Array)
            if demand[survey.demand_attribute.to_sym].size > 0
              if demand[survey.demand_attribute.to_sym].first != ""
                attribute = demand[survey.demand_attribute.to_sym]
              end
            end
          else
            attribute = demand[survey.demand_attribute.to_sym]
          end
        end
        (label_plus_attribute << [survey.resume,attribute,survey.demand_attribute]) if attribute != nil
      end
      @attributes_by_theme[theme] = label_plus_attribute
    end
    @attributes_by_theme["Votre projet"].insert(0, ["Localisation", demand.localisation])
    @attributes_by_theme["Votre projet"].insert(1, ["Zone de recherche", demand.postal_codes])
    @attributes_by_theme
  end


end
