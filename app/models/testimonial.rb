# frozen_string_literal: true

class Testimonial
  include Mongoid::Document
  include Mongoid::Timestamps

  field :first_name, type: String
  field :last_name, type: String
  field :rating, type: Integer
  field :content, type: String
  field :image, type: String

  validates :rating, inclusion: { in: [1, 2, 3, 4, 5] }
  validates :content, length: { minimum: 40, maximum: 120 }

end
