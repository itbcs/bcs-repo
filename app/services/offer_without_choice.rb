class OfferWithoutChoice

  def initialize(demand)
    @errors = []
    @demand = demand
    @customer = @demand.customer
    @properties = []
  end

  def call
    customer_props_without_choice
    service_success
  end

  private

  def customer_props_without_choice
    if @demand.properties?
      @demand.properties.each do |prop|
        choice = prop.customer_choices.where(v2_demand: @demand).last
        puts "**** Inside OfferWithoutChoice Service iteration"
        puts choice.inspect
        puts "************************************************"
        if !choice
          @properties << prop
        end
      end
    end
  end

  def service_success
    ServiceSuccess.new(@properties)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
