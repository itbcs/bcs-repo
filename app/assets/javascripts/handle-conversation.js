function autoScroll() {
  $(document).ready(function(){
    var fullSurveyContainer = $('.conversation-wrapper');
    if (fullSurveyContainer.length > 0) {
      $(fullSurveyContainer).animate({
        scrollTop: ($(fullSurveyContainer).get(0).scrollHeight) + 180}, 600);
    }
  });
}



function bindConversationForm(){
  $(".customer_message").submit(function(event){
    event.preventDefault();
    var $form = $(this);
    var url = $form.attr('action');
    var method = $form.attr('method');
    var label = $form.find('input[name="label_demand"]').val();
    var value = $form.find('input[name="value"]').val();
    var theme = $form.find('input[name="theme"]').val();
    var messageContainer = $('.conversation-wrapper');

    $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      success: function(datas) {
        console.log("Ajax success call !!!");
        var messageBody = datas[0].body;
        var creationDate = datas[1];
        var origin = datas[2];
        if (origin === "agency") {
          var html = "<div class='full-message-container-agency'><div class='message-item'>" + messageBody + "<div class='abs-message-creation-time'>" + creationDate + "</div>" + "</div><div class='customer-avatar'><i class='fa fa-user'></i></div></div>";
          $(messageContainer).append(html);
          $(messageContainer).find(".full-message-container-agency:last").hide().slideDown("fast");
          $('#customer_message_body').val('');
        } else {
          var html = "<div class='full-message-container'><div class='message-item'>" + messageBody + "<div class='abs-message-creation-time'>" + creationDate + "</div>" + "</div><div class='customer-avatar'><i class='fa fa-user'></i></div></div>";
          $(messageContainer).append(html);
          $(messageContainer).find(".full-message-container:last").hide().slideDown("fast");
          $('#customer_message_body').val('');
        }
        autoScroll();
      },
      error: function(datas) {
        alert("Ajax error call !!!");
      }
    });

  });
}

bindConversationForm();

$('#javascript-scroll-on-load').ready(function(){
  autoScroll();
});
