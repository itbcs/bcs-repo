class AgencyMailer < BaseMandrillMailer
  include Resque::Mailer

  def sixty_month_old_properties(agency)
    subject = "Bonjour #{}, vos biens sont-ils toujours en vente"
    merge_vars = {
      "LOCALISATION" => demand[:localisation],
      "PROPERTY_TYPE" => demand[:property_type],
      "FIRST_NAME" => agency[:name],
      "SIGN_IN_URL" => "https://bientotchezsoi.com/agencies/connexion",
    }
    body = mandrill_template("tra-nouvelle-recherche", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end

  def send_rent_agency_mail_with_pdf(typeform,vars)

    # pdf = render_to_string pdf: "votre-demande-#{@demand.id.to_s.last(5).downcase}", template: "pdfs/show.html.haml", encoding: "UTF-8"
    # save_path = Rails.root.join('pdfs','filename.pdf')
    # File.open(save_path, 'wb') do |file|
    #   file << pdf
    # end

    pdf_html = ActionController::Base.new.render_to_string(template: 'pdfs/rent_agency', layout: 'pdf')
    pdf = WickedPdf.new.pdf_from_string(pdf_html)
  end


  def typeform_rent_test(typeform,vars,name,s3_url,email)
    puts "Inspect typeform rent test"
    puts "Typeform: #{typeform} - Vars: #{vars} - Name: #{name} - S3_url: #{s3_url} - Email: #{email}"
    puts "--- File uploaded to S3 url ---"
    puts s3_url
    puts "-------------------------------"
    file = s3_url
    filename = name
    subject = "Nouvelle candidature pour votre offre de location"
    merge_vars = vars
    body = mandrill_template("tra-contact-locataire", merge_vars)
    from = "Bientôt Chez Soi <hello@bientotmaloc.com>"
    replyto = "hello@bientotmaloc.com"
    send_mail_with_attachments(email, subject, body, from, replyto, file, filename)
  end

  def agency_demand_matching_notif(agency)
    subject = "Nouvelle recherche : #{demand[:property_type]} à #{demand[:localisation]}"
    merge_vars = {
      "LOCALISATION" => demand[:localisation],
      "PROPERTY_TYPE" => demand[:property_type],
      "FIRST_NAME" => agency[:name],
      "SIGN_IN_URL" => "https://bientotchezsoi.com/agencies/connexion",
    }
    body = mandrill_template("tra-nouvelle-recherche", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end

  def agency_welcome(agency)
    subject = "Votre compte est en attente de validation"
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
    }
    body = mandrill_template("tra-attente-validation", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail_with_attachments(agency[:email], subject, body, from, replyto)
  end

  def agency_week_stats(agency,values)
    if values[:choices]
      choices_string = values[:choices].join(",")
    else
      choices_string = "Aucun retour client cette semaine"
    end
    subject = "Les statistiques hebdo de votre agence"
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
      "DEMANDS_COUNT" => values[:demands],
      "CHOICES" => choices_string,
      "UNVIEWED_DEMANDS" => values[:unviewed_demands],
      "PROPOSED_PROPS" => values[:proposed_props],
      "AGENCY_DASHBOARD" => "https://bientotchezsoi.com/agencies/connexion",
    }
    body = mandrill_template("tra-stat", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end


  def validated_agency_welcome(agency)
    subject = "Votre compte agence est activé"
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
      "AGENCY_LINK" => "https://bientotchezsoi.com/agencies/connexion",
    }
    body = mandrill_template("tra-bienvenue", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end


  def customer_like(agency,customer,comment,demand,property)
    puts "Enter into customer like mailer"
    puts demand.inspect
    puts property.inspect
    puts "O_id version: #{demand[:_id]["$oid"].to_s}"
    puts "O_id prop version: #{property[:_id]["$oid"].to_s}"
    puts "Demand_id: #{demand[:id].to_s}"
    puts "Property_id: #{property[:id].to_s}"
    puts "*******************************"
    reference = "REF#{demand[:_id]["$oid"].to_s.upcase.last(4)}"
    if comment
      checked_comment = comment
    else
      checked_comment = "Aucun commentaire"
    end
    subject = "Un client a eu un coup de cœur pour votre bien"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "LAST_NAME" => customer[:last_name],
      "CUSTOMER_PHONE" => customer[:phone],
      "COMMENT" => checked_comment,
      "DEMAND_ID" => "https://bientotchezsoi.com/dashboard/agency/demand/#{demand[:_id]["$oid"].to_s}",
      "PROPERTY_ID" => "https://bientotchezsoi.com/dashboard/agency/property/#{property[:_id]["$oid"].to_s}",
      "DEMAND_REF" => reference,
    }
    body = mandrill_template("tra-like-offre", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end

  def customer_hate(agency,customer,prop,choice,comment,demand)
    if comment
      checked_comment = comment
    else
      checked_comment = "Aucun commentaire"
    end
    subject = "Un membre de Bientôt chez soi a réagi à votre offre"
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
      "PROPERTY_TYPE" => prop[:property_type],
      "ROOMS" => prop[:nb_room],
      "PROPERTY_NAME" => prop[:title],
      "COMMENT" => checked_comment,
      "PROPERTY_PRICE" => "#{prop[:price]} €",
      "LOCALISATION" => demand[:search_formatted_area],
      "BUDGET" => demand[:budget],
      "STATUS_LIKE" => "#{choice}",
    }
    body = mandrill_template("tra-dislike-offre", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end

  def search_update(agency,demand)
    subject = "Un membre a mis sa recherche à jour"
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
      "LOCALISATION" => demand[:localisation],
      "PROPERTY_TYPE" => demand[:property_type],
      "BUDGET_MINI" => demand[:budget],
      "BUDGET_MAXI" => demand[:budget],
      "DASHBOARD_LINK" => "https://bientotchezsoi.com/agencies/connexion",
    }
    body = mandrill_template("tra-maj-recherche", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end


  def chatroom_message_notif(agency,message,customer,property,sending_time)
    customer_fullname = "#{customer[:first_name]} #{customer[:last_name]}"
    subject = "Vous venez de recevoir un message privé suite à votre offre #{property[:title]}"
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
      "CUSTOMER_NAME" => customer_fullname,
      "MESSAGE_CONTENT" => message,
      "SENDING_TIME" => sending_time,
      "MESSAGE_LINK" => "https://bientotchezsoi.com/dashboard/agency/messages",
    }
    body = mandrill_template("tra-notification-message", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(agency[:email], subject, body, from, replyto)
  end


end






