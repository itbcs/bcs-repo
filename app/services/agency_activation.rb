class AgencyActivation

  def initialize agency
    @agency = agency
    @errors = []
    @matching = []
  end

  def call
    activate_agency
    new_agency_matching
    service_success
  end

  private

  def activate_agency
    @agency.activated = true
    @agency.save
  end

  # def new_agency_matching
  #   @validated_demands = Demand.where(status: "success")
  #   if @agency.catchment_area != []
  #     @agency.catchment_area.each do |agency_zipcode|
  #       @validated_demands.select{|d| d.postal_codes.include?(agency_zipcode)}.each do |demand|
  #         formatted_budget = demand.budget.delete_if(&:empty?).compact
  #         if formatted_budget != []
  #           if formatted_budget.size == 1
  #             if (demand.budget.delete_if(&:empty?).first.to_s.strip.gsub(/[[:space:]]/,'').gsub(",","").to_i) >= @agency.min_price.to_i
  #               @matching << demand
  #             end
  #           elsif formatted_budget.size == 2
  #             demand_min_price = formatted_budget.first.to_s.strip.gsub(/[[:space:]]/,'').gsub(",","").to_i
  #             demand_max_price = formatted_budget.last.to_s.strip.gsub(/[[:space:]]/,'').gsub(",","").to_i

  #             if demand_min_price >= @agency.min_price.to_i
  #               if demand.budget.first.gsub(/[[:space:]]/,'').to_i >= @agency.min_price.to_i
  #                 @matching << demand
  #               end
  #             elsif (demand_max_price >= @agency.min_price.to_i)
  #               if demand.budget.last.gsub(/[[:space:]]/,'').to_i >= @agency.min_price.to_i
  #                 @matching << demand
  #               end
  #             end

  #           end
  #         end
  #       end
  #     end
  #     @matching
  #   else
  #   end
  # end


  def new_agency_matching
    @validated_demands = V2Demand.where(activated: true)
    @validated_demands_with_matching_zipcode = @validated_demands.where(:search_zipcodes_area.in => @agency.catchment_area)
    if @agency.catchment_area != []

      puts "**** VALIDATED DEMANDS WITH MATCHING ZIPCODE ****"
      puts @validated_demands_with_matching_zipcode.count
      puts "************************"

      @validated_demands_with_matching_zipcode.each do |demand|
        formatted_budget = demand[:budget]

        if formatted_budget != []
          if formatted_budget.size == 1
            if demand[:budget].first.to_i >= @agency.min_price.to_i
              @matching << demand
            end
          elsif formatted_budget.size == 2
            demand_min_price = demand[:budget].first.to_i
            demand_max_price = demand[:budget].last.to_i
            if demand_min_price >= @agency.min_price.to_i
              if demand[:budget].first.to_i >= @agency.min_price.to_i
                @matching << demand
              end
            elsif (demand_max_price >= @agency.min_price.to_i)
              if demand[:budget].last.to_i >= @agency.min_price.to_i
                @matching << demand
              end
            end
          end
        end
      end
      @matching
    else
    end
  end


  def service_success
    ServiceSuccess.new(@matching)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
