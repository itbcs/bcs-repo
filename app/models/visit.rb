class Visit
  include Mongoid::Document
  include Mongoid::Timestamps

  field :customer_id, type: String
  field :customer_ip, type: String
  field :customer_email, type: String

end
