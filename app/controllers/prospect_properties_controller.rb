class ProspectPropertiesController < ApplicationController

  def bien_a_vendre
    @prospect_property = ProspectProperty.new
    render :bien_a_vendre
  end

  def create
    @prospect_property = ProspectProperty.new(safe_params)
    if @prospect_property.save
      redirect_to root_path, notice: "Votre demande à été prise en compte"
    else
      render :bien_a_vendre, alert: "Erreurs dans le formulaire"
    end
  end

  private

  def safe_params
    params.require(:prospect_property).permit(:like, :comment, :score, :name, :localisation, :area, :price, :desc, :status, :ground, :constructor_contact, :ground_area, :housing_area, :property_type, :nb_rooms, :options, :since_when, :firstname, :lastname, :email, :phone)
  end

end
