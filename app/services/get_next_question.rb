class GetNextQuestion
  def initialize question, demand, position
    @current_survey = question
    @demand = demand
    @position = position
  end

  def call
    @next_question = Question.find_by(position: @position.to_i + 1)
    if @next_question.v2_condition
      check_condition(@next_question)
    else
      @next_question
    end
    service_success
  end

  private

  def check_condition question
    if question&.v2_condition&.child_v2_condition
      if handle_child_condition(question.v2_condition)
        @next_question = question
      else
        next_question = Question.find_by(position: question.position.to_i + 1)
        check_condition(next_question)
      end
    elsif question.v2_condition
      if handle_conditions(question.v2_condition)
        @next_question = question
      else
        next_question = Question.find_by(position: question.position.to_i + 1)
        check_condition(next_question)
      end
    else
      @next_question = question
    end
  end

  def handle_child_condition child_condition
    if child_condition.child_v2_condition
      child_result = handle_child_condition child_condition.child_v2_condition
      current_result = handle_conditions child_condition
      puts "#{child_condition.operator} #{child_condition.value} #{current_result}"
      b = handle_children_operator current_result,child_result,child_condition.children_operator
      puts "pere et fils #{child_condition.children_operator} #{b}"
      b
    else
      a = handle_conditions child_condition
      puts "#{child_condition.operator} #{child_condition.value} : #{a}"
      a
    end
  end

  def handle_children_operator value, child_value, children_operator
    case children_operator
    when :and.to_s
      and_operator value, child_value
    when :or.to_s
      or_operator value, child_value
    end
  end

  def handle_conditions condition
    puts "**** condition value ****"
    puts condition.value
    puts "*************************"
    if !condition.hard_stock
      case condition.operator.to_s
        when :equal.to_s
          equal @demand[condition.demand_field.to_s], condition.value.to_s
        when :not_equal.to_s
          not_equal @demand[condition.demand_field.to_s], condition.value.to_s
        when :superior.to_s

          puts "**** condition demand_field ****"
          puts condition.demand_field.to_sym
          puts "********************************"

          if @demand[condition.demand_field.to_sym].last == ""
            number = @demand[condition.demand_field.to_sym].first.gsub(",", "").to_i
          else
            number = @demand[condition.demand_field.to_sym].last.gsub(",", "").to_i
          end
          superior number, condition.value.to_i
        when :inferior.to_s
          number = @demand[condition.demand_field.to_sym].last.gsub(",", "").to_i
          inferior number, condition.value.to_i
      end
    else
      case condition.operator.to_s
        when :equal.to_s
          equal(@demand[condition.demand_field.to_sym], condition.value.to_s)
        when :not_equal.to_s
          not_equal(@demand[condition.demand_field.to_sym], condition.value.to_s)
        when :superior.to_s
          demand_field = @demand[condition.demand_field.to_sym]
          if demand_field.is_a?(Array)
            number = @demand[condition.demand_field.to_sym].first.to_i
          else
            number = @demand[condition.demand_field.to_sym].to_i
          end
          superior(number, condition.value.to_i)
        when :inferior.to_s
          demand_field = @demand[condition.demand_field.to_sym]
          if demand_field.is_a?(Array)
            number = @demand[condition.demand_field.to_sym].first.to_i
          else
            number = @demand[condition.demand_field.to_sym].to_i
          end
          inferior(number, condition.value.to_i)
      end
    end
  end


  def not_equal demand_value, condition_value
    demand_value != condition_value
  end

  def equal demand_value, condition_value
    demand_value == condition_value
  end

  def and_operator demand_value, condition_value
    demand_value && condition_value
  end

  def or_operator demand_value, condition_value
    demand_value || condition_value
  end

  def superior demand_value, condition_value
    demand_value > condition_value
  end

  def inferior demand_value, condition_value
    demand_value < condition_value
  end



  def service_success
    ServiceSuccess.new(@next_question)
  end

  def service_error
    ServiceError.new(@errors)
  end
end
