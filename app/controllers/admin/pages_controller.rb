class Admin::PagesController < ApplicationController
  http_basic_authenticate_with name: ENV["HTTP_AUTH_LOGIN"], password: ENV["HTTP_AUTH_PWD"]

  def new
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    @page = Page.new()
    render "admin/pages/new", layout: "admin"
  end

  def create
    @page = Page.new(safe_params)
    @lname = safe_params[:name].parameterize
    @page.lname = @lname
    if @page.save
      redirect_to admin_page_path(@page.id.to_s), notice: "Page créer avec succès !!"
    else
      render edit_admin_page_path(@page.id.to_s), alert: "Un problème est survenu !!"
    end
  end

  def index
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/pages/index", layout: "admin"
  end

  def show
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    @page = Page.find(params[:id])
    render "admin/pages/show", layout: "admin"
  end

  def edit
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    @page = Page.find(params[:id])
    render "admin/pages/edit", layout: "admin"
  end

  def update
    @page = Page.find(params[:id])
    @lname = safe_params[:name].parameterize
    @page.lname = @lname
    if @page.update(safe_params)
      redirect_to admin_page_path(@page.id.to_s), notice: "Page modifiée avec succès !!"
    else
      render edit_admin_page_path(@page.id.to_s), alert: "Un problème est survenu !!"
    end
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy
    redirect_to admin_pages_path, notice: "Page effacée"
  end

  def home
    render "admin/pages/home", layout: "admin"
  end

  def activate_old_demand_proper_field
    @demands = Demand.where(status: "success")
    @demands.each do |demand|
      demand.activated = true
      demand.save
    end
  end


  def reformat_all_demands_postal_codes_to_string
    Demand.where(status: "success").each do |demand|
      demand.postal_codes = demand.postal_codes.map{|zipcode| zipcode.to_s}
      if demand.save
        puts "Demande sauvegardée - #{demand.postal_codes}"
      end
    end
  end


  def migrate_old_demand_to_brand_new

    # Demand.where(from_oldplatform: true).each do |demand|
    #   customer = demand.customer
    #   if customer.demands.size > 1
    #     customer.demands[0].destroy
    #   end
    # end

    # @old_demands = OldPlatformDemand.all.select{|old_demand| old_demand.customer.present?}

    # @old_demands.each do |demand|
    #   customer = demand.customer
    #   last_customer_demand = customer.old_platform_demands.last
    #   new_demand = Demand.new(last_customer_demand.attributes)
    #   new_demand.from_oldplatform = true
    #   if new_demand.save
    #     puts "Demande valide: #{new_demand.valid?}"
    #     puts "Demande sauvegardée"
    #   end
    # end

  end

  private

  def safe_params
    params.require(:page).permit(:meta_title, :meta_desc, :content, :status, :name, :lname, :url, :photo_url)
  end

end
