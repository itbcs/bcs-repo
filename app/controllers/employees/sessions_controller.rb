class Employees::SessionsController < Devise::SessionsController
  before_action :configure_sign_in_params, only: [:create]

  def new
    super
  end

  def create
    super
  end

  def destroy
    super
  end

  protected

  def configure_sign_in_params
    devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute, :firstname, :lastname, :role])
  end

  def after_sign_in_path_for(resource)
    apifluxes_properties_path
  end

end
