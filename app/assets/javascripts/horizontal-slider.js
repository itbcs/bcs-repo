var scrollDuration = 1000;
var leftPaddle = document.getElementsByClassName('left-paddle');
var rightPaddle = document.getElementsByClassName('right-paddle');
var itemsLength = $('.item').length;
var itemSize = $('.item').outerWidth(true);
var paddleMargin = 20;

var getMenuWrapperSize = function() {
  return $('.menu-wrapper').outerWidth();
}
var menuWrapperSize = getMenuWrapperSize();
$(window).on('resize', function() {
  menuWrapperSize = getMenuWrapperSize();
});

var menuVisibleSize = menuWrapperSize;
var getMenuSize = function() {
  return itemsLength * itemSize;
};
var menuSize = getMenuSize();
var menuInvisibleSize = menuSize - menuWrapperSize;

var getMenuPosition = function() {
  return $('.menu').scrollLeft();
};


var sliderElement = document.getElementById('slider-menu');
if (sliderElement) {
  var sliderWidth = sliderElement.scrollWidth;
  $('ul.menu').animate( { scrollLeft: (sliderWidth - 1020) / 2}, scrollDuration);
}


$('.menu').on('scroll', function() {
  menuInvisibleSize = menuSize - menuWrapperSize;
  var menuPosition = getMenuPosition();
  var menuEndOffset = menuInvisibleSize - paddleMargin;
  $('#print-wrapper-size span').text(menuWrapperSize);
  $('#print-menu-size span').text(menuSize);
  $('#print-menu-invisible-size span').text(menuInvisibleSize);
  $('#print-menu-position span').text(menuPosition);
});


$(rightPaddle).on('click', function() {
  var sliderElement = document.getElementById('slider-menu');
  var sliderWidth = sliderElement.scrollWidth;
  $('ul.menu').animate( { scrollLeft: sliderWidth}, scrollDuration);
});

$(leftPaddle).on('click', function() {
  $('ul.menu').animate( { scrollLeft: '0' }, scrollDuration);
});
