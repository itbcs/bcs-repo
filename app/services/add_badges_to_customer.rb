class AddBadgesToCustomer

  def initialize customer, badges
    @customer = customer
    @new_badges = []
    @errors = []
    @badges = badges
  end

  def call
    save_badges
    service_success
  end

  private

  def save_badges
    @new_active_badges = []
    puts "**** ADD BADGES TO DEMAND ****"
    puts @badges.inspect

    @badges.each do |badge|
      if badge[:active]
        @new_active_badges << {"name" => badge[:name], "svg" => badge[:svg], "content" => badge[:content], "points" => badge[:points]}
      end
    end

    if @customer[:badges]
      @diff_badges = @new_active_badges.sort_by{|badge| badge[:svg]} - @customer[:badges].sort_by{|badge| badge[:svg]}

      puts "**** BADGES DIFF ****"
      puts @customer[:badges].sort_by{|badge| badge[:svg]}.inspect
      puts @new_active_badges.sort_by{|badge| badge[:svg]}.inspect
      puts @diff_badges.inspect
      puts "*********************"

      if @diff_badges != []
        @new_badges = @diff_badges
        @diff_badges.each do |new_badge|
          @customer.push(badges: new_badge)
        end
        @customer.save
      else
        @new_badges = []
      end
    else
      @new_badges = []
      @customer.badges = @new_active_badges.sort_by{|badge| badge[:svg]}
      @customer.save
    end

    puts "*** DEMAND BADGES ***"
    puts @customer.badges.inspect

    puts "******************************"
  end

  def service_success
    ServiceSuccess.new(@new_badges)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
