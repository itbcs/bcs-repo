class RentAgenciesController < ApplicationController

  def show
    @rent_agency = RentAgency.find(params[:id].to_s)
  end

  def index
    @active_rent_agencies = RentAgency.where(active: true)
    @inactive_rent_agencies = RentAgency.where(active: false)
  end

  def activate
    puts params.inspect
    @rent_agency = RentAgency.find(params["agency"])
    if @rent_agency
      @rent_agency.active = true
      @rent_agency.save
    else
      puts "Rent agency not found"
    end
    render json: {activate: true}, status: :ok
  end

  def deactivate
    puts params.inspect
    @rent_agency = RentAgency.find(params["agency"])
    if @rent_agency
      @rent_agency.active = false
      @rent_agency.save
    else
      puts "Rent agency not found"
    end
    render json: {deactivate: true}, status: :ok
  end

  def new
    @rent_agency = Agency.new
  end

  def create
    @rent_agency = RentAgency.new(safe_params)
    if @rent_agency.save
      redirect_to rent_agency_path(@rent_agency)
    else
      render :new
    end
  end

  def edit
    @rent_agency = RentAgency.find(params[:id].to_s)
  end

  def update
    @rent_agency = RentAgency.find(params[:id].to_s)
    if @rent_agency.update(safe_params)
      redirect_to rent_agency_path(@rent_agency)
    else
      render :edit
    end
  end

  def destroy
  end

  private

  def safe_params
    params.require(:agency).permit(:name, :rent_typeform_contact_email, :siret, :phone, :address, :active, :typeform_id, :is_renter)
  end

end
