class Admin::EstimationsController < ApplicationController

  def show
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @estimation = Estimation.find(params[:id])
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/estimations/show", layout: "admin"
  end

  def index
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/estimations/index", layout: "admin"
  end

  def destroy
    @estimation = Estimation.find(params[:id])
    if @estimation.destroy
      redirect_to admin_estimations_path, notice: "Estimation effacée"
    else
      render admin_estimations_path, alert: "Un problème est survenu !!"
    end
  end

  private

  def safe_params
    params.require(:estimation).permit(:localisation, :property_type, :living_space, :quality, :email, :firstname, :lastname, :phone, :comment, :results, :estimation_value, :contact_require)
  end

end
