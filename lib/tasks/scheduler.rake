desc "This task is called by the Heroku scheduler add-on"


task :sixty_month_old_properties => :environment do
  @agencies = []
  @properties = Property.from_bcs.where(created_at: {'$lte': 2.month.ago})
  @properties.each do |prop|
    @agencies << prop.agency
  end
  @agencies = @agencies.uniq
  @agencies.each do |ag|
    AgencyMailer.sixty_month_old_properties(ag).deliver
  end
end

task :handle_rents_new_typeform => :environment do
  @agencies = Agency.active_renter
  @agencies.each do |agency|
    if agency.typeform_id
      HandleNewRentsTypeform.new("#{agency.typeform_id}").call
    end
  end
  # Les clefs de bordeaux
  # 5c9a2bf4c745bf00078152ad
  # HandleNewRentsTypeform.new("kJhY0n").call

  # Guy hocket nansouty
  # 5cd1c43c86e60800078990fc
  # HandleNewRentsTypeform.new("a5JvuI").call

  # Bientot chez soi
  # 5c408bf150899800072e0655
  # HandleNewRentsTypeform.new("XtVP8E").call
end

task :sending_agency_stats_mail => :environment do
  if Date.today.monday?
    @agencies = Agency.where(activated: true)
    @agencies.each do |ag|
      values = ag.agency_stats
      AgencyMailer.agency_week_stats(ag,values).deliver
    end
  end
end


task :sending_customer_unviewed_offers => :environment do
  @results = Demand.unviewed_offers
  if @results != []
    @results.each do |key,value|
      email = "#{key}"
      variables = generate_merge_vars(key,value)
      CustomerMailer.unviewed_offers(email,variables).deliver
    end
  end
end


task :sending_customer_fresh_choices => :environment do
  @choices = CustomerChoice.one_day_old_positive_choices
  if !Date.today.friday? || !Date.today.saturday? || !Date.today.sunday?
    puts "**** We are not friday, saturday or sunday ****"
    if @choices != []
      puts "---- There is #{@choices.size} positive choices since yesterday ----"
      @choices.each do |choice|
        customer_email = choice.v2_demand.customer.email
        agency_email = choice.property.agency.email
        agency_name = choice.property.agency.name
        customer_firstname = choice.v2_demand.customer.first_name
        customer_choice = choice.choice
        property_title = choice.property.title
        CustomerMailer.one_day_old_positive_choices(customer_email,agency_email,agency_name,customer_firstname,customer_choice,property_title).deliver
      end
    end
  end
end

task :offers_without_choices => :environment do
  @demands = V2Demand.where(activated: true)
  @demands.each do |demand|
    @email = demand.customer.email
    @firstname = demand.customer.first_name
    if demand.properties?
      result = OfferWithoutChoice.new(demand).call
      if result.success?
        if result.data != []
          @results = result.data
          @vars = handle_properties(@results, demand.id.to_s, @firstname)
          CustomerMailer.offers_without_choice(@email,demand.id.to_s,@vars,@firstname).deliver
        end
      end
    end
  end
end


def handle_properties(properties,demand_id,firstname)
  vars = {}
  if properties != []
    properties.to_a.last(10).each_with_index do |prop,index|
      vars["PROP_#{index+1}"] = prop.id.to_s
      if prop.img_1_url
        vars["PROP_#{index+1}_IMG"] = prop.img_1_url
      else
        vars["PROP_#{index+1}_IMG"] = "https://s3.eu-west-3.amazonaws.com/bientotchezsoi/pictos/kenotte/Castor-Inspecteur.svg"
      end
      vars["PROP_#{index+1}_LINK"] = "https://bientotchezsoi.com/access/properties/#{prop.id.to_s}?demand_id=#{demand_id}"
      vars["PROP_#{index+1}_ROOMS"] = prop.nb_room
      vars["PROP_#{index+1}_LOCALISATION"] = prop.localisation
      vars["PROP_#{index+1}_PRICE"] = prop.price
      vars["PROP_#{index+1}_TITLE"] = prop.title
      vars["FIRST_NAME"] = firstname
    end
  end
  return vars
end


def generate_merge_vars(key,value)
  email = key
  @merge_vars = {"CUSTOMER_EMAIL" => "#{key}", "FIRST_NAME" => "#{value.first[-1]}", "LINK" => "#{value.first[-2]}", "RESULT" => "Type de bien: #{value.first[2]}, Prix: #{value.first[0]}, Proposé depuis: #{value.first[3]}"}
  return @merge_vars
end
