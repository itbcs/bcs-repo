class MailTester
  @queue = :low

  def self.perform(agency)
    AgencyMailer.agency_demand_matching_notif(agency).deliver
  end

end
