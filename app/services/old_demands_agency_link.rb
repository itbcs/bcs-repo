class OldDemandsAgencyLink

  def initialize(demand)
    @demand = demand
  end

  def call
    agency_link
    service_success
  end

  private

  def agency_link
    @properties = @demand.properties.any? ? @demand.properties : nil
    if @properties
      @properties.each do |property|
        agency = property.agency
        puts "**** AGENCY ****"
        puts agency.inspect
        puts "****************"
        agency.v2_demands.push(@demand)
        if agency.save(validate: false)
          puts "**** AGENCY SAVING ****"
          puts "Agency successfully saved !!"
          puts "***********************"
          @success_link = true
        else
          puts "**** AGENCY SAVING ****"
          puts "Agency failed saved !!"
          puts "***********************"
          @success_link = false
        end
      end
    end
  end

  def service_success
    ServiceSuccess.new(@demand)
  end

end
