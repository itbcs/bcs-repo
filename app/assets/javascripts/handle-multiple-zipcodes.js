var position;

$("#admin-add-postal-codes-trigger").click(function() {
  var inputContainer = $(".admin-postal-codes-container");
  var lastInput = $("input.admin-zipcode-input").last();
  console.log(lastInput);
  var inputPosition = $(lastInput).data("position");
  position = inputPosition;
  var clonedInput = $(lastInput).clone();
  $(clonedInput).val('');
  var newPosition = position += 1;
  $(clonedInput).attr("id", "demand_postal_codes_" + newPosition.toString());
  $(clonedInput).attr("data-position", newPosition);
  var fullInputWithCol = $("<div class='col-xs-4'></div>");
  $(fullInputWithCol).append(clonedInput);
  $(inputContainer).append(fullInputWithCol);
});
