class Question
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  include Mongoid::Orderable

  before_save :format_required_field

  has_many :v2_answers
  has_one :sentence
  embeds_one :v2_condition, cascade_callbacks: true
  embeds_one :client_side_condition, cascade_callbacks: true

  accepts_nested_attributes_for :v2_answers

  orderable

  field :demand_attribute, type: String
  field :label, type: String
  field :required, type: Boolean
  field :checkable_on_criteria, type: Boolean
  field :position, type: Integer
  field :question_type, type: String
  field :field_type, type: String
  field :image, type: String
  field :advanced_checkbox, type: Boolean
  field :advanced_radio, type: Boolean
  field :has_sentence, type: Boolean
  field :client_request, type: Boolean
  field :avatar, type: String
  field :advice, type: Boolean, default: false
  field :kenotte_sentence, type: String
  field :variable_in_label, type: Boolean
  field :variable_demand_field, type: String
  field :second_part_label, type: String
  field :hint_label, type: String
  field :hint_image, type: String
  field :radio_by_pair, type: Boolean
  field :label_resume, type: String
  field :agency_resume, type: String
  field :criteria_resume, type: String
  field :intermediate_exit, type: Boolean
  field :google_autocomplete, type: Boolean
  field :theme, type: String
  field :investor_exit, type: Boolean
  field :fem_label, type: String
  field :score, type: Integer

  def format_required_field
    if required
      if required.to_s == "true"
        required = true
      end
    else
      required = false
    end
  end


end
