class DynamicAssetUploader < CarrierWave::Uploader::Base
  include Cloudinary::CarrierWave

  def public_id
    return "carrierwave-uploads/#{model.class.to_s.parameterize}/dynamic-asset-#{model.id.to_s}"
  end

end
