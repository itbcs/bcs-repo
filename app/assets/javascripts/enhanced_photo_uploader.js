function enhancedFileUpload() {
  var files = $(".photo-upload-placeholder");
  Array.from(files).forEach(function(container){
    var remover = $(container).find(".absolute-photo-remover");
    var fileInput = $(container).find("label span input[type=file]");
    var file = fileInput[0].files[0];
    console.log(file);
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      var fileName = file.name;
      var fileSize = (file.size/1000000).toFixed(2) + " Mo";
      var fileDesc = $("<div class='file-description'>" + fileName + " " + fileSize + "</div>");
      image = $("<div class='custom-multi-photo-wrapper'><img class='preview-img' src=" + reader.result + " id=" + file.name + ">" + "<div class='preview-pdf-file-name'>" + file.name + "</div>" + "<div class='preview-file-size'>" + "( " + (file.size/1000000).toFixed(2) + " Mo" + " )" + "</div></div>");
      var containerText = $(container).find(".context");
      $(containerText).text("Modifier").addClass('full');
      $(container).attr("style", "background-image: url('"+ reader.result +"')");
      $(container).append(fileDesc);
    }, false);
    if (file) {
      $(remover).removeClass("hidden");
      reader.readAsDataURL(file);
    }
  })
}

function removeUploadedFile(input,img_div) {
  console.log("Enter into removeUploadedFile function !!!!");
  console.log(input);
  var originalButton = $("<div class='context'>Ajouter une photo +</div>");
  $("#" + "property_" + input).val(null);
  $("#" + img_div).removeAttr('style');
  $("#" + img_div).find('.context').remove();
  $("#" + img_div).find('.absolute-photo-remover').addClass("hidden");
  $("#" + img_div).find('label').append(originalButton);
  $("#" + img_div).find('.file-description').remove();
}
