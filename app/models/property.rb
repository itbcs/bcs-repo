# frozen_string_literal: true

# class Property
class Property
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification
  belongs_to :agency
  has_and_belongs_to_many :demands
  has_and_belongs_to_many :v2_demands
  has_and_belongs_to_many :old_platform_demands
  has_many :customer_advices
  has_many :customer_choices
  has_many :conversations
  has_many :chatrooms, dependent: :destroy
  has_many :v2_demand_properties

  before_save :remove_empty_string
  before_save :remove_empty_status
  before_save :format_price
  before_update :format_price
  before_save :handle_integer_price
  before_update :handle_integer_price
  before_save :handle_area_format

  validates_presence_of :localisation, message: "Veuillez renseigner la ville"
  validates_presence_of :property_type, message: "Veuillez renseigner le type de bien"
  validates_presence_of :area, message: "Veuillez renseigner la surface"
  validates_presence_of :title, message: "Veuillez renseigner le titre"
  validates_presence_of :price, message: "Veuillez renseigner le prix"
  validates_presence_of :fees, message: "Veuillez renseigner les honoraires"
  validates_presence_of :fees_target, message: "Veuillez renseigner la cible des honoraires"
  validates_presence_of :state, message: "Veuillez renseigner l'etat du bien"
  validates_presence_of :nb_room, message: "Veuillez renseigner le nombre de pièces"
  validates_presence_of :status, message: "Veuillez renseignez le statut du bien"

  scope :sold, -> {where(status: "5")}
  scope :unsold, -> {where(status: {"$ne" => "5"})}
  scope :onsale, -> {where(status: {"$eq" => "7"})}
  scope :empty_status, -> {where(status: {"$eq" => ""})}
  scope :nil_status, -> {where(status: {"$eq" => nil})}
  scope :houses, -> {where(property_type: {"$eq" => "Maison"})}
  scope :sold_for_less_than_dynamic_month, -> (month) {where(sold_at: {"$gte" => DateTime.now.prev_month(month)})}
  scope :from_bcs, -> {where(hektor_id: nil)}
  scope :from_hektor, -> {where(hektor_id: {"$ne" => nil})}
  scope :bcs_properties, -> {where(agency_id: BSON::ObjectId('5c408bf150899800072e0655'))}
  scope :agency_sold, -> (agency_id) {where(agency_id: agency_id).where(status: "5")}

  embeds_many :pictures, cascade_callbacks: true
  accepts_nested_attributes_for :pictures
  mount_uploader :pdf, ImageUploader

  mount_uploader :img_1, ImageUploader
  mount_uploader :img_2, ImageUploader
  mount_uploader :img_3, ImageUploader
  mount_uploader :img_4, ImageUploader
  mount_uploader :img_5, ImageUploader
  mount_uploader :img_6, ImageUploader
  mount_uploader :img_7, ImageUploader
  mount_uploader :img_8, ImageUploader

  mount_uploader :image_1, PropertyCloudinaryUploader
  mount_uploader :image_2, PropertyCloudinaryUploader
  mount_uploader :image_3, PropertyCloudinaryUploader
  mount_uploader :image_4, PropertyCloudinaryUploader
  mount_uploader :image_5, PropertyCloudinaryUploader
  mount_uploader :image_6, PropertyCloudinaryUploader
  mount_uploader :image_7, PropertyCloudinaryUploader
  mount_uploader :image_8, PropertyCloudinaryUploader

  field :name, type: String
  field :localisation, type: String
  field :area, type: Integer
  field :price, type: String
  field :price_seller_net, type: Float
  field :prix, type: Integer
  field :desc, type: String
  field :status, type: String
  field :last_update_at, type: Time
  field :source, type:  String
  field :title, type: String
  field :reference, type: String
  field :offer_type, type: String
  field :offer_type_code, type: Integer
  field :negociator, type: String
  field :negociator_tel, type: String
  field :negociator_photo, type: String
  field :inventory, type:  String
  field :expenses, type: Float
  field :expenses_desc, type:  String
  field :expenses_type, type:  Integer
  field :fees, type: String
  field :fees_from, type: Float
  field :fees_buyer, type: Float
  field :fees_seller, type: Float
  field :fees_buyer_percentage, type: Float
  field :price_seller_net, type: Float
  field :foncier, type: Integer
  field :corps, type: String
  field :print_corps, type: String
  field :url_barem_fees, type: String
  field :url, type: String
  field :mandat_nb, type: Integer
  field :mandat_type, type: String
  field :mandat_start_date, type: Time
  field :mandat_end_date, type: Time
  field :available_date, type: Time
  field :property_type, type: String
  field :property_type_code, type: Integer
  field :living_space, type:  Integer
  field :ground_space, type:  Integer
  field :nb_room, type: Integer
  field :nb_sleeping_room, type: Integer
  field :nb_sleeping, type: Integer
  field :nb_floor, type: Integer
  field :nb_bathrooms, type: Integer
  field :Nb_waterrooms, type: Integer
  field :nb_wc, type: Integer
  field :nb_kitchen, type: Integer
  field :chauffage, type: String
  field :energie_chauffage, type: String
  field :format_chauffage, type: String
  field :parking, type: String
  field :pool, type: String
  field :garden, type: String
  field :Garden_area, type: Integer
  field :balcony, type:  String
  field :terrace, type:  String
  field :exposition, type: String
  field :environnement, type: String
  field :view, type: String
  field :prestige, type: String
  field :build_year, type: Integer
  field :elevator, type: Boolean
  field :plain_pied, type: Boolean
  field :dpe_non_concerne, type: Boolean
  field :dpe_vierge, type: Boolean
  field :country, type: String
  field :city, type: String
  field :city_name, type: String
  field :zipcode, type: String
  field :departement, type: Integer
  field :sectors, type: String
  field :heart_stroke, type: String
  field :translations_language, type: String
  field :translations_title, type: String
  field :traductions_corps, type: String
  field :traductions_print_corps, type: String
  field :copropriete_lot, type: String
  field :copropriete_nb_lot, type: Integer
  field :copropriete_quote_part, type: Integer
  field :copropriete_save_plan, type: Integer
  field :copropriete_statut_syndicat, type: Integer
  field :furnitures, type: Boolean
  field :buildable, type: Boolean
  field :images, type: Array
  field :property_house_type, type: String
  field :property_flat_type, type: String
  field :fees_target, type: String
  field :terrain_area, type: String

  field :images_array, type: Array
  field :secteurs, type: String
  field :type_bien, type: String
  field :ville, type: String
  field :titre, type: String
  field :cp, type: String
  field :state, type: String
  field :dpe, type: String
  field :ges, type: String
  field :latitude, type: Float
  field :longitude, type: Float

  field :price_int, type: Integer
  field :garden_area, type: Integer
  field :field_area, type: Integer
  field :lounge_area, type: Integer
  field :build_year, type: String
  field :human_city, type: String
  field :swimming_pool, type: Boolean
  field :parking, type: Boolean
  field :balcony, type: Boolean
  field :elevator, type: Boolean
  field :washroom, type: Boolean
  field :terrace, type: Boolean
  field :garden, type: Boolean
  field :kitchen_type, type: Integer
  field :kitchen_equipment, type: Integer
  field :heating_energy, type: Integer
  field :heating_format, type: Integer
  field :heating_type, type: Integer

  field :geoloc_zipcode, type: String
  field :geoloc_address, type: String
  field :locality, type: String
  field :lng, type: Float
  field :lat, type: Float
  field :sold_at, type: Time
  field :hektor_id, type: String, default: nil

  def self.handle_empty_status
    @properties = Property.from_bcs.empty_status
    if @properties.size > 0
      @properties.each do |prop|
        puts prop.status
        prop.status = "7"
        if prop.save
          puts "Prop successfully saved with status: #{prop.status}"
        end
      end
    else
      puts "Empty collection for query: Property.from_bcs.empty_status"
    end
  end

  def self.handle_nil_status
    @properties = Property.from_bcs.nil_status
    if @properties.size > 0
      @properties.each do |prop|
        if prop.destroy
          puts "Prop successfully destroy with status: #{prop.status}"
        end
      end
    else
      puts "Empty collection for query: Property.from_bcs.nil_status"
    end
  end

  def format_price
    if price
      self.price = price.tr('^0-9', '')
    else
      self.price = nil
    end
  end

  def handle_integer_price
    if price
      self.price_int = price.tr('^0-9', '').to_i
    else
      self.price_int = nil
    end
  end

  def handle_area_format
    puts "Enter into before_action area format"
    puts self[:area].inspect
    if self[:area]
      self[:area] = self[:area].to_s.gsub(/[[:space:]]/,'').to_i
    end
  end

  def remove_empty_string
    puts "-------- sanitize params --------"
    puts ges.inspect
    puts dpe.inspect
    if ges.to_s.empty?
      ges = nil
      puts "**** NEW GES ****"
      puts ges.inspect
    end
    if dpe.to_s.empty?
      dpe = nil
      puts "**** NEW DPE ****"
      puts dpe.inspect
    end
  end

  def remove_empty_status
    if status.to_s.empty?
      puts "**** PARAMS STATU ****"
      puts status.inspect
      puts "**********************"
      status = nil
      puts "**** NEW STATUS ****"
      puts status.inspect
      puts "********************"
    else
      puts status.inspect
    end
  end

  def self.house_types
    return ["Maison","Maison de village","Ferme","Mas","Villa","Bastide","Rez-de-villa","Echoppe","Propriété","Château"]
  end

  def self.flat_types
    return ["Appartement","Duplex","Immeuble","Loft","Rez-de-jardin","Studio","Triplex"]
  end

end



class Array

  def generate_property_fees
    if self == ["Du vendeur"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    elsif self == ["De l'acquéreur"]
      result = ("<div class='custom-radio-fullwidth'><i class='fa fa-check custom-fa'></i>#{self.last}</div>").html_safe
    end
    result
  end

end
