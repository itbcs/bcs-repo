var userPhoneNumber;


function handleEmptyPhoneInput() {
  var input = $("#phone-input");
  $(input).keyup(function() {
    var inputValue = $(this).val();
    var inputValue = inputValue.replace(/[\D\s\._\-]+/g, "");
    inputValue = inputValue ? inputValue : "";
    $(this).val( function() {
      return (inputValue) ? inputValue : "";
    });
    if ((inputValue).toString().length >= 10) {
      $("#sms-val-button").removeClass("disabled");
      $("#sms-val-button").attr("disabled", false);
    } else {
      $("#sms-val-button").addClass("disabled");
      $("#sms-val-button").attr("disabled", true);
    }
  });
}

function showReturnCodeForm(data){
  var textBlock = $('.form-title-signup');
  if (data.status === "numéro incorrect !") {
    var textFailedNumber = $("<div class='code-failed'>" + "numéro incorrect !" + "</div>");
    if ($('.code-failed').length === 0) {
      textFailedNumber.appendTo(textBlock);
    }
  } else {
    userPhoneNumber = data.phone;
    $(".custom-form-container.sms-validation").addClass("hidden");
    var returnText = $(".signup-sms-hint");
    var returnContainer = $('.custom-form-container.sms-return-code');
    $(returnText).text("Un code vous a été envoyé au " + data.phone);
    var resendLink = $("<div class='resend-link'>" + "Je n'ai pas reçu mon code ?" + "<span class='resend-code-link'>Le renvoyer</span>" + "</div>");

    $(returnText).replaceWith(resendLink);

    if ($('.custom-form-container.sms-return-code .resend-link').length === 0) {
      resendLink.appendTo(returnContainer);
    }


    $("span.resend-code-link").click(function(){
      console.log("Click on resend link");
      $('.custom-form-container.sms-return-code').addClass('hidden');
      $(".custom-form-container.sms-validation").removeClass("hidden");
    })

    $('.custom-form-container.sms-return-code').removeClass('hidden');
    $('#sms-return-form-v2').submit(function(event){
      event.preventDefault();
      var $form = $(this);
      var code = $form.find('input[name="code"]').val();
      if (code === data.code) {
        $('#survey-signup-form').removeClass('hidden');
        $('#survey-signup-sms').addClass('hidden');
        $('#survey-continue-block').addClass('hidden');
        $(".custom-modal-header h2").text("Je m'inscris");
        $(".custom-modal-header h5").text("Je crée mon compte personnel pour finaliser ma demande");
        putPhoneToForm(data.phone);
      } else {
        var textFailed = $("<div class='code-failed'>" + "code erroné !" + "</div>");
        textFailed.appendTo(textBlock);
      }
    });
  }
}



function putPhoneToForm(phoneNumber){
  $('#customer_phone').val(phoneNumber);
}



function SmsValidationForm(){
  $("#sms-validation-form-v2").submit(function(event){
      event.preventDefault();
      var $form = $(this),
            url = $form.attr('action'),
            method = $form.attr('method'),
            position = $form.find('input[name="phone"]').val();
      $(".absolute-waiting-container").removeClass("hidden");

  $.ajax({
      url: url,
      type: method,
      data: $form.serialize(),
      success: function(data) {
        $(".absolute-waiting-container").addClass("hidden");
        showReturnCodeForm(data);
      },
      error: function(data) {
        alert("Error !!!!!");
      }
    });
  });
}



function handleContinueSurvey(){
  var continueButton = $("#survey-continue-block #continue-button");
  $(continueButton).click(function(){
    var modal = $("#survey-signin-modal");
    $(modal).addClass("hidden");
    $(modal).remove();
  });
}


SmsValidationForm();
handleEmptyPhoneInput();
handleContinueSurvey();


