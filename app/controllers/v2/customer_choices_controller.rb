class V2::CustomerChoicesController < ApplicationController

  def customer_choice_ajax
    @demand = V2Demand.find(params[:demand_id])
    @property = Property.find(params[:prop_id])

    if params[:user_action]
      @action = params[:user_action]
      if @action.to_sym == :love
        render partial: "v2/customer_choices/partials/love"
      elsif @action.to_sym == :hate
        render partial: "v2/customer_choices/partials/hate"
      end
    end
  end

end
