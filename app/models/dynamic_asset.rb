class DynamicAsset
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title, type: String

  mount_uploader :image, DynamicAssetUploader

end
