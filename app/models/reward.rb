# frozen_string_literal: true

# class Gamification
class Reward
  include Mongoid::Document
  belongs_to :customer

  field :badge, type: String
  field :badge_image, type: String
  field :badge_desc, type: String
  field :score, type: Integer
  field :tags, type: String
end
