class KeyFactsController < ApplicationController

  def create
    @keyfact = KeyFact.new(safe_params)
    if @keyfact.save
      redirect_to root_path, notice: "Keyfact crée avec succès"
    else
      render root_path, alert: "Un problème est survenu !!"
    end
  end

  def new
    @keyfact = KeyFact.new()
    render :new
  end

  private

  def safe_params
    params.require(:key_fact).permit(:number, :text, :number_type, :photo_cache, :photo)
  end

end
