class AdminAgencyActivation

  def initialize demand
    @agency = agency
    @matching = []
  end

  def call
    agency_activation
    service_success
  end

  private

  def agency_activation
    @agency.activated = true
    if @agency.save
      @matching << "Agence: #{@agency.id} - activée"
    end
  end


  def service_success
    ServiceSuccess.new(@matching)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
