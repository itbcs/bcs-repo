class AddCommentToDemand

  def initialize demand
    @demand = demand
    @errors = []
  end

  def call
  end

  private

  def service_success
    ServiceSuccess.new(@demand)
  end

  def service_error
    ServiceError.new(@errors)
  end

end
