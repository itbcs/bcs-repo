module SurveyHelper

  def handle_keycode(keycode)
    if keycode == 49
      letter = "1"
    elsif keycode == 50
      letter = "2"
    elsif keycode == 51
      letter = "3"
    elsif keycode == 52
      letter = "4"
    elsif keycode == 53
      letter = "5"
    elsif keycode == 54
      letter = "6"
    elsif keycode == 55
      letter = "7"
    elsif keycode == 56
      letter = "8"
    elsif keycode == 57
      letter = "9"
    end
    return letter
  end

  def get_survey_input_css_class answer_type
    case answer_type
      when :radio.to_s
        input_class = 'styled-radio'
      when :checkbox.to_s
        input_class = 'styled-checkbox'
      when :text.to_s
        input_class = 'formatted-numeric-input'
      when :textarea.to_s
        input_class = 'styled-textarea'
    end
    input_class
  end

  def get_survey_input_value answer
    value = ''
    unless answer.answer_type == :textarea.to_s || answer.answer_type == :text.to_s
      value = answer.id
    end
    value
  end

  def handle_array_results(result_array)
    if result_array || result_array != []
      sanitize_array = result_array.reject{|result| (result == "") || (result == nil)}
      if sanitize_array.size == 1
        string_result = sanitize_array.first.to_s
      elsif sanitize_array.size == 2
        string_result = "#{sanitize_array.first} et #{sanitize_array.last.to_s.downcase}"
      elsif sanitize_array.size > 2
        string_result = "#{sanitize_array[0..-2].join(", ")} et #{sanitize_array.last.to_s.downcase}"
      else
        string_result = "Vous avez passez cette question"
      end
      return string_result
    else
      return "Vous avez passez cette question"
    end
  end

end
