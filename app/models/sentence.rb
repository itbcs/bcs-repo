class Sentence
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :question

  field :content, type: String
  field :content_after_variable, type: String
  field :demand_attribute, type: String
  field :icon, type: String

end
