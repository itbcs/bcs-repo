class Chatroom
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  has_many :chat_messages
  belongs_to :customer
  belongs_to :property
  belongs_to :demand, optional: true
  belongs_to :v2_demand
  belongs_to :agency

  field :topic, type: String
  field :slug, type: String
  field :seen_by_agency, type: Boolean, default: false
  field :seen_by_customer, type: Boolean, default: false


  def self.with_unseen_messages
    ids = Chatroom.all.select{|cr| cr.chat_messages.where(seen_by_agency: false)}.pluck(:id)
    Chatroom.where(:id.in => ids)
  end

end
