function handleUnlikeModal() {
  $('.unlike-ajax').click(function(event) {
  var link = $(this),
      url = $(link).data("url"),
      demandId = $(link).data('demand'),
      propertyId = $(link).data('property');

  console.log(`demand id = ${demandId}`);
  console.log(`property id = ${propertyId}`);

    $.ajax({
      url: url,
      type: 'POST',
      data: { demand_id: demandId, property_id: propertyId },
      success: function(data) {
        $('#modal-container-unlike .modal').empty();
        $(data).appendTo('.modal');
        handleUnlikeModal();
      },
      error: function(data) {
        alert("Fatal Error");
      },
    });
  });
}
handleUnlikeModal();

function handleLikeModal() {
  $('.like-ajax').click(function(event) {
  var link = $(this),
      url = $(link).data("url"),
      demandId = $(link).data('demand'),
      propertyId = $(link).data('property');

  console.log(`demand id = ${demandId}`);
  console.log(`property id = ${propertyId}`);

    $.ajax({
      url: url,
      type: 'POST',
      data: { demand_id: demandId, property_id: propertyId },
      success: function(data) {
        $('#modal-container-like .modal').empty();
        $(data).appendTo('.modal');
        handleLikeModal();
      },
      error: function(data) {
        alert("Fatal Error");
      },
    });
  });
}
handleLikeModal();
