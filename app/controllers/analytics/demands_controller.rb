class Analytics::DemandsController < ApplicationController

  def index
    @demand = V2Demand.find("5d1e6f2cba5b8b00074aadbb")
    puts @demand.created_at.to_date
    puts Date.parse("2019-07-04")
    @demands = V2Demand.all.select{|demand| demand.created_at.to_date == Date.parse("2019-07-04")}
    render :index
  end

  def show
    render :show
  end

end
