class PartnersController < ApplicationController

  def show
    @partner = Partner.find(params[:id].to_s)
    if params["origin"]
      @from_dashboard = true
      render :show, layout: false
    else
      render :show
    end
  end

  def index
    @partners = Partner.all
  end

  def new
    @partner = Partner.new
  end


  def create
    @partner = Partner.new(safe_params)
    if @partner.save
      redirect_to partner_path(@partner), notice: "Partenaire créée avec succès !!"
    else
      render :new, alert: "Oupss, une erreur est survenue !!"
    end
  end

  def edit
    @partner = Partner.find(params[:id])
  end

  def update
    @partner = Partner.find(params[:id])
    @partner.update_attributes(safe_params)
    if @partner.save
      redirect_to partner_path(@partner), notice: "Partenaire modifié avec succès !!"
    else
      render :edit, alert: "Oupss, une erreur est survenue !!"
    end
  end


  private

  def safe_params
    params.require(:partner).permit(:name, :content, :widget_url, :api_url, :service_url, :service_identifier, :service_password, :technology, :widget_html_target, :iframe_url, :javascript_code, :logo_cache, :logo, targets:[])
  end

end
