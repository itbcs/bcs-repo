# frozen_string_literal: true
require 'resque/server'

ENV['REDISTOGO_URL'] ||= 'redis://username:password@host:1234/'

uri = URI.parse(ENV['REDISTOGO_URL'])
Resque.redis = Redis.new(host: uri.host, port: uri.port, password: uri.password, thread_safe: true)

Dir['/app/app/jobs/*.rb'].each { |file| require file }

if Rails.env.production?
  Resque::Server.use(Rack::Auth::Basic) do |user, password|
   user = ENV['HTTP_AUTH_LOGIN']
   password == ENV['HTTP_AUTH_PWD']
  end
end
