function progressionBubble(surveyTheme,innerHTML) {
  var newTheme = $(innerHTML).find('.quizz-bubble');
  var newThemeText = $(innerHTML).find('.bubble-title').text();
  var themeTest = $(innerHTML).find('.bubble-title');
  var stepperContainer = $('.stepper-container');
  var formatedNewText = newThemeText.replace(/[^a-zA-Z0-9]/g, '');
  var formatedSurveyTheme = surveyTheme.replace(/[^a-zA-Z0-9]/g, '');
  if (formatedSurveyTheme === formatedNewText) {
    $(newTheme).appendTo(stepperContainer);
    $(newTheme).hide();
  } else {
    $(newTheme).appendTo(stepperContainer).hide().fadeIn("fast");
  }
}

function surveyCourseStatus(innerHTML){
  var currentCourseStatus = $(innerHTML).find('.stepper');
  var stepperContainer = $('.stepper-container');
  stepperContainer.empty();
  $(currentCourseStatus).appendTo(stepperContainer);
}

function lastSurveyCourseStatus(innerHTML){
  var lastCourseStatus = $(innerHTML).find('.finished-survey-status');
  var stepperContainer = $('.stepper-container');
  stepperContainer.remove();
}

function progressionImage(innerHTML){
  var castorImage = $(innerHTML).find('.quizz-castor');
  var stepperContainer = $('.stepper-container');
  $(castorImage).appendTo(stepperContainer);
}
