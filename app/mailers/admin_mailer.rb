class AdminMailer < BaseMandrillMailer
  include Resque::Mailer

  def admin_agency_demand_suggestion(agency,customer,demand,email,values)
    puts "Go into admin mailer => une agence vient de proposer un bien à un membre"
    subject = "#{agency[:name]} vient de proposer un bien à #{customer[:first_name]} #{customer[:last_name]}"
    customer_fullname = "#{customer[:first_name]} #{customer[:last_name]}"
    demand_url = "https://bientotchezsoi.com/admin/demands/#{demand[:_id]["$oid"].to_s}"
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
      "AGENCY_EMAIL" => agency[:email],
      "CUSTOMER_EMAIL" => customer[:email],
      "CUSTOMER_FULLNAME" => customer_fullname,
      "CUSTOMER_PHONE" => customer[:phone],
      "DEMAND_URL" => demand_url,
    }
    body = mandrill_template("admin-offre-agence", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(email, subject, body, from, replyto)
  end


  def admin_customer_property_view(agency,customer,property,email)
    puts "Go into admin mailer => un membre vient de consulter un bien"
    customer_fullname = "#{customer[:first_name]} #{customer[:last_name]}"
    demand_url = "https://bientotchezsoi.com/admin/properties/#{property[:_id]["$oid"].to_s}"
    subject = "#{customer_fullname} vient de consulter un bien de l'agence #{agency[:name]}"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "CUSTOMER_EMAIL" => customer[:email],
      "CUSTOMER_PHONE" => customer[:phone],
      "PROPERTY_TITLE" => property[:title],
      "PROPERTY_LOCALISATION" => property[:localisation],
      "LAST_NAME" => customer[:last_name],
      "AGENCY_NAME" => agency[:name],
      "PROPERTY_TYPE" => property[:property_type],
      "ROOMS" => property[:nb_room],
      "PROPERTY_PRICE" => property[:price_int],
      "OFFER_LINK_ADMIN" => demand_url,
    }
    body = mandrill_template("admin-view-customer", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(email, subject, body, from, replyto)
  end


  def pending_agency_validation(admin_email,agency)
    subject = "A valider : l'agence #{agency[:name]} est en attente d'activation"
    catchment_area = agency[:catchment_area].join(",")
    merge_vars = {
      "AGENCY_NAME" => agency[:name],
      "PHONE_NUMBER" => agency[:phone],
      "POSTCODE_AGENCY" => catchment_area,
      "BUDGET_MINI" => agency[:min_price],
      "VALIDATION_LINK" => "https://bientotchezsoi.com/admin/agencies/waiting",
    }
    body = mandrill_template("admin-agence-attente-validation", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(admin_email, subject, body, from, replyto)
  end

  def new_demand_recap(demand,customer,email,demand_date_creation, demand_time_creation)
    puts "***** ENTER INTO ADMIN DEMAND RECAP *****"
    puts demand
    puts customer
    puts email
    puts demand_date_creation
    puts demand_time_creation
    puts "*****************************************"
    subject = "A valider #{customer[:first_name]} #{customer[:last_name]} recherche #{demand[:property_type]} à #{demand[:search_formatted_area]}"
    customer_fullname = "#{customer[:first_name]} #{customer[:last_name]}"
    if demand[:actual_situation].first(3).upcase == "JAM"
      customer_is_owner = "Le client n'est pas propriétaire"
    elsif demand[:actual_situation].first(3).upcase == "OUI"
      customer_is_owner = "Le client est déjà propriétaire"
    else
      customer_is_owner = "Donnée inconnue"
    end

    if demand[:budget] != nil
      real_budget = demand[:budget]
    else
      real_budget = "Donnée non renseignée"
    end

    demand_ref = "BCS_#{demand[:_id]["$oid"].to_s.upcase.last(5)}"
    demand_url = "https://bientotchezsoi.com/admin/demands/#{demand[:_id]["$oid"].to_s}"

    puts "*******************"
    puts "PARAMS_INSPECT = #{demand[:budget].inspect}"
    puts "REAL_BUDGET_VARIABLE = #{real_budget}"
    puts "#{demand.inspect}"
    puts "#{customer[:phone]}"
    puts "#{customer_is_owner}"
    puts "#{demand_ref}"
    puts "#{demand_date_creation}"
    puts "#{demand_time_creation}"
    puts "*******************"
    merge_vars = {
      "DEMAND_URL" => demand_url,
      "DEMAND_DATE" => demand_date_creation,
      "DEMAND_TIME" => demand_time_creation,
      "CUSTOMER_FULLNAME" => customer_fullname,
      "CUSTOMER_PHONE" => customer[:phone],
      "CUSTOMER_SITUATION" => customer_is_owner,
      "DEMAND_REF" => demand_ref,
      "LOCALISATION" => demand[:search_formatted_area],
      "POSTAL_CODES" => demand[:search_zipcodes_area],
      "ACTUAL_SITUATION" => demand[:actual_situation],
      "PROPERTY_TYPE" => demand[:property_type],
      "DESTINATION" => demand[:destination],
      "BUDGET" => demand[:budget],
      "STYLE" => demand[:style],
      "SURFACE" => demand[:surface],
      "IMPORTANT_FIELDS" => demand[:criterias],
    }
    body = mandrill_template("admin-attente-valid-client", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(email, subject, body, from, replyto)
  end

  def customer_question(message,customer,admin_email)
    subject = "#{customer[:first_name]} #{customer[:last_name]} a envoyé un message depuis son espace membre"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "LAST_NAME" => customer[:last_name],
      "EMAIL" => customer[:email],
      "PHONE" => customer[:phone],
      "MESSAGE" => message,
    }
    body = mandrill_template("none-message-client", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "audrey@bientotchezsoi.com"
    send_mail(admin_email, subject, body, from, replyto)
  end

  def agency_like(admin,agency,customer,property,choice,comment)
    checked_comment = comment ? comment : "Aucun commentaire"
    subject = "#{customer[:first_name]} #{customer[:last_name]} a aimé l'offre de #{agency[:name]}"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "LAST_NAME" => customer[:last_name],
      "STATUS_LIKE" => choice,
      "PROPERTY_TYPE" => property[:property_type],
      "ROOMS" => property[:nb_room],
      "PROPERTY_PRICE" => "#{property[:price]} €",
      "AGENCY" => agency[:name],
      "COMMENT" => checked_comment,
    }
    body = mandrill_template("admin-like-offre", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "audrey@bientotchezsoi.com"
    send_mail(admin, subject, body, from, replyto)
  end


  def agency_dislike(admin,agency,customer,property,choice,comment)
    puts "Enter into admin - customer dislike"
    puts "Property type: #{property[:property_type]}"
    if comment
      checked_comment = comment
    else
      checked_comment = "Aucun commentaire"
    end
    subject = "#{customer[:first_name]} #{customer[:last_name]} n'a pas aimé l'offre de #{agency[:name]}"
    merge_vars = {
      "FIRST_NAME" => customer[:first_name],
      "LAST_NAME" => customer[:last_name],
      "STATUS_LIKE" => choice,
      "PROPERTY_TYPE" => property[:property_type],
      "ROOMS" => property[:nb_room],
      "PROPERTY_PRICE" => "#{property[:price]} €",
      "AGENCY" => agency[:name],
      "COMMENT" => checked_comment,
    }
    body = mandrill_template("admin-dislike-proposition", merge_vars)
    from = "[Admin] Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "audrey@bientotchezsoi.com"
    send_mail(admin, subject, body, from, replyto)
  end


  def search_update(demand,customer,email,date,time)
    subject = "#{customer[:first_name]} #{customer[:last_name]} a mis sa recherche à jour"
    customer_fullname = "#{customer[:first_name]} #{customer[:last_name]}"

    if demand[:actual_situation].first(3).upcase == "NON"
      customer_is_owner = "Le client n'est pas propriétaire"
    elsif demand[:actual_situation].first(3).upcase == "OUI"
      customer_is_owner = "Le client est déjà propriétaire"
    end


    demand_ref = "BCS_#{demand[:_id]["$oid"].to_s.upcase.last(5)}"
    demand_url = "https://bientotchezsoi.com/admin/demands/#{demand[:_id]["$oid"].to_s}"

    puts "-------- Mailer infos -------"
    puts subject
    puts customer_fullname
    puts customer_is_owner
    puts demand[:property_type]
    puts demand[:localisation]
    puts customer[:phone]
    puts demand[:budget]
    puts demand_ref
    puts demand_url
    puts email
    puts date
    puts time
    puts "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"

    merge_vars = {
      "DEMAND_URL" => demand_url,
      "DEMAND_DATE" => date,
      "DEMAND_TIME" => time,
      "DEMAND_REF" => demand_ref,
      "LOCALISATION" => demand[:localisation],
      "PROPERTY_TYPE" => demand[:property_type],
      "BUDGET_MINI" => demand[:budget].first,
      "CUSTOMER_FULLNAME" => customer_fullname,
      "CUSTOMER_PHONE" => customer[:phone],
      "CUSTOMER_SITUATION" => customer_is_owner,
    }

    body = mandrill_template("admin-maj-recherche", merge_vars)
    from = "Bientôt Chez Soi <jerecherche@bientotchezsoi.com>"
    replyto = "jerecherche@bientotchezsoi.com"
    send_mail(email, subject, body, from, replyto)
  end


  def sanitize_budget(budget)
    sanitize_array = budget.reject{|budget| budget.empty?}
    if sanitize_array.size == 1
      result = "#{ActionController::Base.helpers.number_with_delimiter(sanitize_array.first.gsub(/[[:space:]]/,'').to_i, locale: :fr)} €"
    elsif sanitize_array.size == 2
      result = "Entre #{ActionController::Base.helpers.number_with_delimiter(sanitize_array.first.gsub(/[[:space:]]/,'').to_i, locale: :fr)} € et #{ActionController::Base.helpers.number_with_delimiter(sanitize_array.last.gsub(/[[:space:]]/,'').to_i, locale: :fr)} €"
    end
    return result
  end

end
