class Admin::PressReleasesController < ApplicationController

  def new
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @presskit_requests = PresskitRequest.all
    @press_releases = PressRelease.all
    @press_release = PressRelease.new()
    @pages = Page.all
    render "admin/press_releases/new", layout: "admin"
  end

  def show
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @press_release = PressRelease.find(params[:id])
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/press_releases/show", layout: "admin"
  end

  def index
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @press_releases = PressRelease.all
    @presskit_requests = PresskitRequest.all
    @pages = Page.all
    render "admin/press_releases/index", layout: "admin"
  end

  def create
    @press_release = PressRelease.new(safe_params)
    if @press_release.save
      redirect_to admin_press_releases_path, notice: "Communiqué de presse en ligne !!"
    else
      render admin_press_releases_path, alert: "Un problème est survenu !!"
    end
  end

  def destroy
    @press_release = PressRelease.find(params[:id])
    if @press_release.destroy
      redirect_to admin_press_releases_path, notice: "Communiqué effacé !!"
    else
      render admin_press_releases_path, alert: "Un problème est survenu !!"
    end
  end

  def edit
    @customers = Customer.all
    @demands = Demand.all
    @agencies = Agency.all
    @properties = Property.all
    @sponsorships = Sponsorship.all
    @estimations = Estimation.all.where(contact_require: true)
    @presskit_requests = PresskitRequest.all
    @press_releases = PressRelease.all
    @press_release = PressRelease.find(params[:id])
    render "admin/press_releases/edit", layout: "admin"
  end

  def update
    @press_release = PressRelease.find(params[:id])
    @press_release.update(safe_params)
    if @press_release.save
      redirect_to admin_press_releases_path, notice: "Communiqué de presse modifié"
    else
      render admin_press_releases_path, alert: "Un problème est survenu !!"
    end
  end

  private

  def safe_params
    params.require(:press_release).permit(:title, :content, :pdf, :pdf_cache, :photo, :photo_cache)
  end

end
