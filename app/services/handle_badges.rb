class HandleBadges

  def initialize demand
    @demand = demand
    @customer = @demand.customer
    @badges = []
    @errors = []
    @points = 0
  end

  def call
    generate_badges
    handle_active_badges
    service_success
  end

  private

  def handle_active_badges
    @active_badges = []
    @badges.each do |badge|
      @active_badges << badge[:svg]
    end
    @inactive_badges = all_badges - @active_badges

    @inactive_badges.each do |badge|
      if badge.split("-").size == 2
        full_name = "#{badge.split("-")[1].capitalize}"
      elsif badge.split("-").size == 3
        full_name = "#{badge.split("-")[1].capitalize} #{badge.split("-")[2]}"
      end
      puts "**** SPLIT NAME ****"
      puts full_name
      puts "********************"
      @badges << {name: full_name, svg: badge, active: false}
    end

  end

  def generate_badges
    @customer = @demand.customer
    @signup_date = @customer.created_at
    @budget = @demand[:budget].first.to_i
    @banker = @demand[:banker_contact]
    @broker = @demand[:broker_contact]
    @postal_codes = @demand[:search_zipcodes_area]
    @properties_count = @demand.properties.size
    @choices = @demand.customer_choices.where(comment: {"$ne" => nil})
    @all_customer_choices = @demand.customer_choices.sort_by{|choice| choice.created_at}
    @destination = @demand[:destination]
    @actual_situation = @demand[:actual_situation]
    @house_works = @demand[:house_works]
    @property_type = @demand[:property_type]
    @gender = @demand[:gender]
    @platform_customer_count = Customer.count
    @same_area_demands_count = V2Demand.where(search_zipcodes_area: {"$in" => @postal_codes}).size
    @last_15_days_visits = Visit.where(customer_id: @customer.id).where(created_at: {"$gte" => Date.today - 15.days, "$lte" => Date.today}).size
    @signup = (Date.today - @signup_date.to_date).to_i


    puts "**** ALL CUSTOMER CHOICES ****"
    puts @all_customer_choices.inspect
    if @all_customer_choices != []
      if @all_customer_choices.first.choice == "J'adore" || @all_customer_choices.first.choice == "J'aime"
        @badges << {name: "First time lover", svg: "24-fisrt-lover", active: true, content: "Vous avez aimé votre première offre", points: 200}
      elsif @all_customer_choices.first.choice == "Je n'aime pas"
        @badges << {name: "Judge skred", svg: "23-juge-skred", active: true, content: "Votre sanction est sans appel, vous n’avez pas aimé votre première offre", points: 340}
      end
    end
    puts "******************************"

    if @destination == "6"
      @badges << {name: "Investisseur", svg: "20-investisseur", active: true, content: "Bravo ! Nous sommes ravis de vous accompagner dans votre investissement immobilier", points: 200}
    end

    if @signup >= 30
      puts "**** SIGNUP DIFF ****"
      puts @signup
      puts "*********************"
      @badges << {name: "Moiniversaire", svg: "25-moiniversaire", active: true, content: "Déjà un mois que vous êtes membre de la communauté", points: 200}
    end

    if @last_15_days_visits >= 3
      puts "**** LAST 15 DAYS PLATFORM VISITS ****"
      puts @last_15_days_visits
      puts "**************************************"
      @badges << {name: "Habitué", svg: "16-habitue", active: true, content: "Vous êtes venu au moins 3 fois nous voir lors des 15 derniers jours", points: 100}
    end

    if @same_area_demands_count >= 50
      puts "**** SAME AREA DEMANDS ****"
      puts @same_area_demands_count
      puts "***************************"
      @badges << {name: "Super voisin", svg: "07-super-voisin", active: true, content: "Vous avez plus de 200 membres qui cherchent autour de vous", points: 100}
    end

    if @platform_customer_count <= 30000
      @badges << {name: "Castor fondateur", svg: "06-fondateur", active: true, content: "Vous faites partie des 30 000 premiers membres de la communauté", points: 150}
    end

    if @demand.status == "success"
      @badges << {name: "Débutant", svg: "02-debutant", point_key: "debutant", active: true, content: "Vous avez débuté votre recherche", points: 50}
    end

    if @demand.activated
      @badges << {name: "Rocket castor", svg: "21-rocket", active: true, content: "Your recherche is on the launchpad ! Bientôt des propositions pour vous :-)", points: 100}
    end

    if @actual_situation
      if @actual_situation == "12" || @actual_situation == "13"
        @badges << {name: "Serial Castor", svg: "05-serial-castor", active: true, content: "Vous avez déjà été propriétaire", points: 100}
      end
    end

    if @house_works
      if @house_works == "40"
        @badges << {name: "Bob le bricoleur", svg: "15-bricoleur", active: true, content: "Les travaux ne vous font pas peur", points: 100}
      end
    end

    if @property_type
      if @property_type == "23"
        @badges << {name: "Constructeur", svg: "19-constructeur", active: true, content: "Vous vous lancez dans l’aventure de la construction", points: 200}
      end
      if @property_type == "21"
        percent = handle_house_expert
        if percent == 100
          @badges << {name: "Expert", svg: "04-expert", active: true, content: "Vous avez répondu à 100% des réponses expert", points: 100}
        elsif percent >= 50
          @badges << {name: "Intermediaire", svg: "03-intermediare", active: true, content: "Vous avez répondu à 50% des réponses expert", points: 75}
        end
      elsif @property_type == "22"
        percent = handle_flat_expert
        if percent == 100
          @badges << {name: "Expert", svg: "04-expert", active: true, content: "Vous avez répondu à 100% des réponses expert", points: 100}
        elsif percent >= 50
          @badges << {name: "Intermediaire", svg: "03-intermediare", active: true, content: "Vous avez répondu à 50% des réponses expert", points: 75}
        end
      elsif @property_type == "23"
        percent = handle_terrain_expert
        if percent == 100
          @badges << {name: "Expert", svg: "04-expert", active: true, content: "Vous avez répondu à 100% des réponses expert", points: 100}
        elsif percent >= 50
          @badges << {name: "Intermediaire", svg: "03-intermediare", active: true, content: "Vous avez répondu à 50% des réponses expert", points: 75}
        end
      end
    end

    if @properties_count != 0
      @badges << {name: "Guest", svg: "22-first-offer", active: true, content: "Vous avez reçu votre première offre de bien", points: 350}
      @comments_ratio = ((@choices.size.to_f / @properties_count.to_f) * 100).to_i
      if @properties_count >= 5
        @badges << {name: "Rob le racoleur", svg: "14-racoleur", active: true, content: "Votre recherche intéresse nos partenaires qui vous ont fait au moins 5 offres", points: 300}
      end
    end

    if @comments_ratio
      if @comments_ratio == 100
        @badges << {name: "Hellpeur", svg: "13-hellpeur", active: true, content: "Nos agences craignent votre verdict ! 100% de commentaires sur les offres : votre avis est à 100% suivi", points: 200}
      elsif @comments_ratio >= 75
        @badges << {name: "Super helpeur", svg: "12-super-helpeur", active: true, content: "Vous avez commenté au moins 75% des offres proposées", points: 150}
      end
    end

    if @budget >= 400000
      @badges << {name: "VIP", svg: "01-VIP", point_key: "vip", active: true, content: "Votre budget est supérieur à 400 000 €", points: 100}
    end

    if @banker
      if @banker == "59" || @banker == "60"
        @badges << {name: "Budget", svg: "17-budget", active: true, content: "Vous avez consulté votre banquier", points: 50}
      end
    end

    if @broker
      if @broker == "63"
        @badges << {name: "Budget verrouillé", svg: "18-budget-verrouille", active: true, content: "Vous avez rencontré un courtier", points: 300}
      end
    end

    if @choices != []
      @badges << {name: "Helpeur", svg: "11-helpeur", active: true, content: "Vous avez commenté votre 1ère proposition", points: 200}
    end

  end

  def service_success
    ServiceSuccess.new(@badges)
  end

  def service_error
    ServiceError.new(@errors)
  end

  def handle_house_expert
    @count = 0

    if @gender == "1"
      @question_count = house_expert_questions.reject!{|item| item == :man_persona}.size
      house_expert_questions.reject!{|item| item == :man_persona}.each do |field|
        (@count += 1) if @demand[field]
      end
    elsif @gender == "2"
      @question_count = house_expert_questions.reject!{|item| item == :woman_persona}.size
      house_expert_questions.reject!{|item| item == :woman_persona}.each do |field|
        (@count += 1) if @demand[field]
      end
    end

    @percentage = ((@count.to_f / @question_count.to_f) * 100).to_i
    puts "**** Ratio - House Expert questions ***"
    puts @count
    puts @question_count
    puts @percentage
    puts "***************************************"
    return @percentage
  end

  def handle_flat_expert
    @count = 0

    if @gender == "1"
      @question_count = flat_expert_questions.reject!{|item| item == :man_persona}.size
      flat_expert_questions.reject!{|item| item == :man_persona}.each do |field|
        (@count += 1) if @demand[field]
      end
    elsif @gender == "2"
      @question_count = flat_expert_questions.reject!{|item| item == :woman_persona}.size
      flat_expert_questions.reject!{|item| item == :woman_persona}.each do |field|
        (@count += 1) if @demand[field]
      end
    end

    @percentage = ((@count.to_f / @question_count.to_f) * 100).to_i
    puts "**** Ratio - House Expert questions ***"
    puts @count
    puts @question_count
    puts @percentage
    puts "***************************************"
  end

  def handle_terrain_expert
    @count = 0

    if @gender == "1"
      @question_count = terrain_expert_questions.reject!{|item| item == :man_persona}.size
      terrain_expert_questions.reject!{|item| item == :man_persona}.each do |field|
        (@count += 1) if @demand[field]
      end
    elsif @gender == "2"
      @question_count = terrain_expert_questions.reject!{|item| item == :woman_persona}.size
      terrain_expert_questions.reject!{|item| item == :woman_persona}.each do |field|
        (@count += 1) if @demand[field]
      end
    end

    @percentage = ((@count.to_f / @question_count.to_f) * 100).to_i
    puts "**** Ratio - House Expert questions ***"
    puts @count
    puts @question_count
    puts @percentage
    puts "***************************************"
  end

  def all_badges
    ["01-VIP","02-debutant","03-intermediare","04-expert","05-serial-castor","06-fondateur","07-super-voisin","08-explorateur","09-super-explorateur","10-globe-trotteur","11-helpeur","12-super-helpeur","13-hellpeur","14-racoleur","15-bricoleur","16-habitue","17-budget","18-budget-verrouille","19-constructeur","20-investisseur","21-rocket","22-first-offer","23-juge-skred","24-fisrt-lover","25-moiniversaire","26-estimation"]
  end

  def badges_points
    {vip: 100, debutant: 50, intermediaire: 75, expert: 100, serial_castor: 100, castor_fondateur: 150, super_voisin: 100, explorateur: 300, super_explorateur: 500, globe_trotteur: 300, helpeur: 200, super_helper: 150, hells_peur: 200, rob: 300, bob: 100, habitue: 100, budget: 50, budget_verrouille: 300, constructeur: 200, investisseur: 200, rocket_castor: 100, guest: 350, judge_skred: 340, first_time_lover: 200, moiniversaire: 200, estimator: 400}
  end

  def house_expert_questions
    [:banker_contact,:broker_contact,:floor,:house_stairs,:accessibility,:principal_space,:kitchen,:bathrooms,:more_rooms,:equipments,:exposition,:parking,:garage,:security,:heating,:heating_type,:dpe,:house_exterior,:man_persona,:woman_persona,:personality]
  end

  def flat_expert_questions
    [:banker_contact,:broker_contact,:floor,:flat_stairs,:accessibility,:principal_space,:kitchen,:bathrooms,:more_rooms,:equipments,:exposition,:parking,:garage,:security,:heating,:heating_type,:dpe,:flat_exterior,:man_persona,:woman_persona,:personality]
  end

  def terrain_expert_questions
    [:banker_contact,:broker_contact,:ground_budget,:house_budget,:floor,:contact_constructor,:accessibility,:principal_space,:kitchen,:bathrooms,:more_rooms,:equipments,:exposition,:parking,:garage,:security,:man_persona,:woman_persona,:personality]
  end

end
