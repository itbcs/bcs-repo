module CustomerHelper

  def reward_picture(score)
    if score == 0
      @photo_url = "https://cdn.bientotchezsoi-dev.com/images/rank-fox.svg"
    elsif score == 50
      @photo_url = "https://cdn.bientotchezsoi-dev.com/images/rank-cat.svg"
    elsif score == 100
      @photo_url = "https://cdn.bientotchezsoi-dev.com/images/rank-kenotte.svg"
    elsif score == 150
      @photo_url = "https://cdn.bientotchezsoi-dev.com/images/rank-rabbit.svg"
    end
    return @photo_url
  end

end
