class DemandsController < ApplicationController

  def destroy
    @customer = current_customer
    @demand = V2Demand.find(params[:id])
    @demand.destroy
    @customer.vip = false
    @customer.save
    redirect_to customer_zone_path
  end

  private

  def safe_params
    params.require(:demand).permit()
  end

end
