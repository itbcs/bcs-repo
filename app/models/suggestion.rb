class Suggestion
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :phone, type: String
  field :name, type: String
  field :email, type: String
  field :last_name, type: String
  field :message, type: String
  field :origin, type: String

  validates_presence_of :name, :last_name, :message
  validates_format_of :email, with: /\A[^@\s]+@[^@\s]+\z/

end
