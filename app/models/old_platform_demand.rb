class OldPlatformDemand
  include Mongoid::Document
  include Mongoid::Timestamps
  belongs_to :customer
  has_and_belongs_to_many :properties

  field :completed, type: Boolean, default: false
  field :old_platform_user_id, type: Integer
  field :localisation, type:  String
  field :postal_codes, type:  Array
  field :perimeter, type: String
  field :project, type: String
  field :actual_situation, type: String
  field :property_type, type: String
  field :destination, type: String
  field :flat_state, type: String
  field :house_state, type: String
  field :travaux, type: Array
  field :budget, type: Array
  field :flat_budget, type: Array
  field :terrain_budget, type: Array
  field :house_budget, type: Array
  field :style, type: Array
  field :surface, type: Array
  field :terrain_surface, type: Array
  field :house_construction_surface, type: Array
  field :rooms, type: String
  field :floor, type: String
  field :contact_constructor, type: String
  field :principal_space, type: Array
  field :kitchen, type: String
  field :bathrooms, type: String
  field :more_rooms, type: Array
  field :equipements, type: Array
  field :accessibility, type: Array
  field :flat_stairs, type: Array
  field :house_stairs, type: Array
  field :exposition, type: Array
  field :parking, type: Array
  field :security, type: Array
  field :heating, type: Array
  field :heating_type, type: Array
  field :dpe, type: Array
  field :flat_exterior, type: Array
  field :house_exterior, type: Array
  field :age, type: String
  field :actual_pros, type: Array
  field :actual_housing, type: String
  field :actual_housing_area, type: Array
  field :installation_date, type: String
  field :last_precision, type: Array
  field :heart_stroke, type: Array
  field :status, type: String, default: "pending"
  field :comments, type: String
  field :question_position, type: Integer
  field :important_fields, type: Array
  field :localisation_lat, type: Float
  field :localisation_lng, type: Float
  field :sending_customer_notification, type: Boolean, default: false
  field :fetch_map_photo, type: Boolean, default: false
  field :old_platform_created_at, type: Date


  def validate?
    self.status == "success"
  end

  def failed?
    self.status == "fail"
  end

  def pending?
    self.status == "pending"
  end

end
