class OldPlatformQuestion
  include Mongoid::Document

  field :old_id, type: Integer
  field :question, type: String
  field :resume, type: String

end
