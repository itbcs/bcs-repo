class AmbassadorsController < ApplicationController

  def create
    @ambassador = Ambassador.new(safe_params)
    if @ambassador.save
      redirect_to ambassador_path, notice: 'Votre parrainage à été pris en compte'
    else
      render "pages/ambassador", alert: "Nous avons rencontrer un problème"
    end
  end

  private

  def safe_params
    params.require(:ambassador).permit(:first_name, :last_name, :email, :phone, :property_city, :property_type, :sponsored_first_name, :sponsored_last_name, :sponsored_email, :sponsored_phone)
  end

end
