class Contact
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::Validations

  field :firstname, type: String
  field :lastname, type: String
  field :email, type: String
  field :phone, type: String
  field :choice, type: String
  field :comment, type: String
  field :hunt, type: String

  validates_presence_of :firstname, :lastname, :email, :choice, :comment
  validates_numericality_of :phone, allow_blank: true, message: "Numéro de téléphone incorrect"

end
