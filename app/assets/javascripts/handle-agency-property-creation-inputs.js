var value;

$('select#property_property_type').change(function(){
  value = $(this).val();
  if (value === "Maison") {
    $('.house-conditionnal-input').removeClass("hidden");
    $('.flat-conditionnal-input').addClass("hidden");
  } else if (value === "Appartement") {
    $('.flat-conditionnal-input').removeClass("hidden");
    $('.house-conditionnal-input').addClass("hidden");
  } else if (value === "Terrain") {
    $('.flat-conditionnal-input').addClass("hidden");
    $('.house-conditionnal-input').addClass("hidden");
  }
});
