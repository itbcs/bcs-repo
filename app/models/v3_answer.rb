class V3Answer
  include Mongoid::Document
  include Mongoid::Timestamps
  include GlobalID::Identification

  belongs_to :v2_question

  field :answer_type, type: String
  field :text, type: String
  field :unit, type: String
  field :hint, type: String
  field :min, type: Integer
  field :max, type: Integer
  field :step, type: Integer
  field :comment, type: String
  field :input_width, type: String
  field :keycode, type: Integer
  field :identifier, type: String
  field :order, type: Integer
  field :placeholder, type: String
  field :image, type: String
  field :field_type, type: String
  field :attr, type: String
  field :input_format, type: String
  field :maxlength, type: Integer
  field :demand_attribute, type: String

  # Arthur fields
  field :input_image, type: String
  field :positive_operator, type: String
  field :negative_operator, type: String

  auto_increment :auto_identifier, collection: :v3_answer_sequences

end
