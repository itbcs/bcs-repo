function dpePerformance(){
  var content = $('.v2-about-diagnostic .content');
  var dpePerformance = content.first();

  if($(dpePerformance).data('dpe') == "A"){
    $('.dpe-container .one').addClass('scale-diagnostic-performance')
  }
  else if($(dpePerformance).data('dpe') == "B"){
    $('.dpe-container .two').addClass('scale-diagnostic-performance')
  }
  else if($(dpePerformance).data('dpe') == "C"){
    $('.dpe-container .three').addClass('scale-diagnostic-performance')
  }
  else if($(dpePerformance).data('dpe') == "D"){
    $('.dpe-container .four').addClass('scale-diagnostic-performance')
  }
  else if($(dpePerformance).data('dpe') == "E"){
    $('.dpe-container .five').addClass('scale-diagnostic-performance')
  }
  else if($(dpePerformance).data('dpe') == "F"){
    $('.dpe-container .six').addClass('scale-diagnostic-performance')
  }
  else if($(dpePerformance).data('dpe') == "G"){
    $('.dpe-container .seven').addClass('scale-diagnostic-performance')
  }
};
dpePerformance();

function gesPerformance(){
  var content = $('.v2-about-diagnostic .content');
  var gesPerformance = content.last();

  if($(gesPerformance).data('ges') == "A"){
    $('.ges-container .one').addClass('scale-diagnostic-performance')
  }
  else if($(gesPerformance).data('ges') == "B"){
    $('.ges-container .two').addClass('scale-diagnostic-performance')
  }
  else if($(gesPerformance).data('ges') == "C"){
    $('.ges-container .three').addClass('scale-diagnostic-performance')
  }
  else if($(gesPerformance).data('ges') == "D"){
    $('.ges-container .four').addClass('scale-diagnostic-performance')
  }
  else if($(gesPerformance).data('ges') == "E"){
    $('.ges-container .five').addClass('scale-diagnostic-performance')
  }
  else if($(gesPerformance).data('ges') == "F"){
    $('.ges-container .six').addClass('scale-diagnostic-performance')
  }
  else if($(gesPerformance).data('ges') == "G"){
    $('.ges-container .seven').addClass('scale-diagnostic-performance')
  }
};
gesPerformance();
